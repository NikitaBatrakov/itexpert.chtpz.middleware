namespace ITExpert.Xml.Monica.Deserialization
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    public interface IMDeserializer<out T>
    {
        IEnumerable<T> Deserialize(IEnumerable<XElement> xElements);
    }
}