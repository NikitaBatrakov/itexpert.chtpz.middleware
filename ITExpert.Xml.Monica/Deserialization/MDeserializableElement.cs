namespace ITExpert.Xml.Monica.Deserialization
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.Xml.Monica;

    public class MDeserializableElement<T> : IMDeserializableElement, IMElement<T>
    {
        public string Name { get; }
        public IEnumerable<T> Value { get; private set; }
        private IMDeserializer<T> Deserializer { get; }

        public MDeserializableElement(string name, IMDeserializer<T> deserializer)
        {
            Name = name;
            Deserializer = deserializer;
            Value = new List<T>();
        }

        public void Deserialize(IEnumerable<XElement> xElements)
        {
             Value = Deserializer.Deserialize(xElements)?.ToArray();
        }
    }
}