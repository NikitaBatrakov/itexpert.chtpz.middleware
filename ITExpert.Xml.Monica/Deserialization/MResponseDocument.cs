﻿namespace ITExpert.Xml.Monica.Deserialization
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    public abstract class MResponseDocument : IMDeserializableDocument
    {
        protected ICollection<IMDeserializableElement> Elements { get; }

        protected MResponseDocument()
        {
            Elements = new List<IMDeserializableElement>();
        }

        protected void AddElements(params IMDeserializableElement[] elements)
        {
            foreach (var element in elements)
            {
                Elements.Add(element);
            }
        }

        protected void Deserialize(string xml)
        {
            Deserialize(xml, Elements);
        }

        private static void Deserialize(string xml, IEnumerable<IMDeserializableElement> elements)
        {
            var xDoc = XDocument.Parse(xml);
            foreach (var element in elements.ToArray())
            {
                var xElements = xDoc.Root?.Elements(element.Name).ToArray();
                if (xElements != null && xElements.Any())
                {
                    element.Deserialize(xElements);
                }
            }
        }
    }
}
