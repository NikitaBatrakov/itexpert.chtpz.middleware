namespace ITExpert.Xml.Monica.Deserialization
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    using ITExpert.Xml.Monica;

    public interface IMDeserializableElement : IMElement
    {
        void Deserialize(IEnumerable<XElement> xElement);
    }
}