namespace ITExpert.Xml.Monica
{
    using System.Collections.Generic;

    public interface IMElement
    {
        string Name { get; }
    }

    public interface IMElement<out T> : IMElement
    {
        IEnumerable<T> Value { get; }
    }
}