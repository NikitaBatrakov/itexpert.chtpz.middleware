﻿namespace ITExpert.Xml.Monica.Serialization
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;

    public class MRequestDocument : IMSerializableDocument
    {
        protected ICollection<IMSerializableElement> Elements { get; }

        public MRequestDocument()
        {
            Elements = new List<IMSerializableElement>();
        }

        public MRequestDocument(params IMSerializableElement[] elements)
        {
            Elements = elements;
        }

        public void AddElements(params IMSerializableElement[] elements)
        {
            foreach (var element in elements)
            {
                Elements.Add(element);
            }
        }

        #region Implementation of IMSerializableDocument

        public string RootName { get; set; } = "Entities";

        public string Serialize()
        {
            return Serialize(RootName, Elements);
        }

        #endregion

        private static string Serialize(string root, IEnumerable<IMSerializableElement> elements)
        {
            var settings = new XmlWriterSettings
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = true,
                OmitXmlDeclaration = true
            };
            using (var textWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    xmlWriter.WriteStartElement(root);
                    foreach (var element in elements.ToArray())
                        element?.Serialize(xmlWriter);
                    xmlWriter.WriteEndElement();
                }
                return textWriter.ToString();
            }
        }
    }
}
