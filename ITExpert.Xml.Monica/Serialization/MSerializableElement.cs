namespace ITExpert.Xml.Monica.Serialization
{
    using System.Collections.Generic;
    using System.Xml;

    using ITExpert.Xml.Monica;

    public class MSerializableElement<T> : IMElement<T>, IMSerializableElement
    {
        public string Name { get; }
        public IEnumerable<T> Value { get; }
        private IMSerializer<T> Serializer { get; }

        public MSerializableElement(string name, IMSerializer<T> serializer, params T[] value)
        {
            Name = name;
            Value = value;
            Serializer = serializer;
        }

        public string Serialize()
        {
            return Serializer.Serialize(Name, Value);
        }

        public void Serialize(XmlWriter xmlWriter)
        {
            Serializer.Serialize(xmlWriter, Name, Value);
        }
    }
}