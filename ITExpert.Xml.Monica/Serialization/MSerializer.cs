﻿namespace ITExpert.Xml.Monica.Serialization
{
    using System.Collections.Generic;
    using System.Xml;

    public abstract class MSerializer<T> : IMSerializer<T>
    {
        #region Implementation of IMSerializer<in T>

        public abstract void Serialize(XmlWriter xmlWriter, string name, IEnumerable<T> value);

        public string Serialize(string name, IEnumerable<T> value)
            => SerializationUtils.GetString(name, value, Serialize);

        #endregion
    }
}