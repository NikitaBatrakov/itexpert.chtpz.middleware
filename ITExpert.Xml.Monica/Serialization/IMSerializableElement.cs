namespace ITExpert.Xml.Monica.Serialization
{
    using System.Xml;

    using ITExpert.Xml.Monica;

    public interface IMSerializableElement : IMElement
    {
        string Serialize();
        void Serialize(XmlWriter xmlWriter);
    }
}