﻿namespace ITExpert.Xml.Monica.Serialization
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    internal static class SerializationUtils
    {
        public static string GetString<T>(string name, T obj, Action<XmlWriter, string, T> action)
        {
            var settings = new XmlWriterSettings
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = true,
                OmitXmlDeclaration = true
            };
            using (var textWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    xmlWriter.WriteStartElement(name);
                    action.Invoke(xmlWriter, name, obj);
                    xmlWriter.WriteEndElement();
                }
                return textWriter.ToString();
            }
        }
    }
}
