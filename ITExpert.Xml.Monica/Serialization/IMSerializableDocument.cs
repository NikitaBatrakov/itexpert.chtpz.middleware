namespace ITExpert.Xml.Monica.Serialization
{
    public interface IMSerializableDocument
    {
        string RootName { get; set; }
        string Serialize();
    }
}