namespace ITExpert.Xml.Monica.Serialization
{
    using System.Collections.Generic;
    using System.Xml;

    public interface IMSerializer<in T>
    {
        void Serialize(XmlWriter xmlWriter, string name, IEnumerable<T> value);
        string Serialize(string name, IEnumerable<T> value);
    }
}