namespace ITExpert.CHPTZ.DataAccess
{
    using System;

    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.Repositories;
    using ITExpert.CHPTZ.ObjectModel;

    public class DataManager : IDataManager, IDisposable
    {
        protected IServiceConnection1C Connection { get; set; }
        protected IMResolver Resolver { get; set; }

        protected DataManager()
        {
        }

        public DataManager(string login, string password)
        {
            Connection = new ServiceConnection1C(login, password);
            Resolver = new Resolver();
        }

        #region Implementation of IDataManager

        public T Service<T>() where T: BaseDataService
        {
            return (T)Activator.CreateInstance(typeof(T), Connection, Resolver);
        }

        public EntityRepository Repository(Type type)
        {
            return new EntityRepository(type, Resolver, Connection);
        }

        public EntityRepository Repository(string className)
        {
            var type = Resolver.GetClassType(className);
            return Repository(type);
        }

        public Repository<T> Repository<T>() where T : IObject1C
        {
            return new Repository<T>(Resolver, Connection);
        }

        #endregion

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Connection.Dispose();
        }

        #endregion
    }
}