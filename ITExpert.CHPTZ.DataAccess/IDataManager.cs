﻿namespace ITExpert.CHPTZ.DataAccess
{
    using System;

    using DataServices.Common;
    using Repositories;

    public interface IDataManager : IDisposable
    {
        EntityRepository Repository(Type type);
        EntityRepository Repository(string type);
        T Service<T>() where T : BaseDataService;
    }
}
