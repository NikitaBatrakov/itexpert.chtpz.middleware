namespace ITExpert.CHPTZ.DataAccess.Remotes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.ObjectModel;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Request.Documents;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public abstract class ProcedureService<T> : ProcedureService where T : IEntity, new()
    {
        protected ProcedureService(IServiceConnection1C connection, IMResolver resolver) 
            : base(connection, resolver)
        {
        }

        protected ProcedureServiceResponse Invoke(string name, string entityGuid, params object[] args)
        {
            var entity = EntityFactory.Create<T>(entityGuid);
            var request = RemoteCallRequest.Create(name, entity, args);
            return Invoke(request);
        }
    }

    public abstract class ProcedureService : BaseDataService
    {
        private RemoteCallService1C Service { get; }

        protected ProcedureService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new RemoteCallService1C(connection);
        }

        protected ProcedureServiceResponse Invoke(string name, string entity, string entityGuid, params object[] args)
        {
            var type = Resolver.GetClassType(entity);
            var obj = EntityFactory.Create(type, entityGuid);
            var request = new RemoteCallRequest(name, type, obj, args);
            return Invoke(request);
        }

        protected ProcedureServiceResponse Invoke(RemoteCallRequest request)
        {
            var args = ProccessArguments(request.Arguments);
            var function = new Function(request.Name, null, request.EntityType.Name, args);

            var requestMDoc = new FunctionRequestDocument(request.EntityType, new[] {request.Entity}, function, Resolver);
            var requestXml = requestMDoc.Serialize();

            var responseXml = Service.InvokeProcedure(requestXml);
            var responseMDoc = new FunctionResponseDocument(responseXml, request.ReturnType, Resolver);

            return ProcedureServiceResponse.Create(responseMDoc);
        }

        private IEnumerable<FunctionArgument> ProccessArguments(IEnumerable<object> objects)
        {
            foreach (var arg in objects)
            {
                var type = arg.GetType();
                if (type == typeof(Link1C))
                {
                    type = Resolver.GetClassType(((Link1C)arg).Type);
                }
                if (type != typeof(string) && typeof(IEnumerable).IsAssignableFrom(type))
                {
                    yield return new FunctionArgument(type, GetArrayValue(arg));
                }
                else
                {
                    yield return new FunctionArgument(type, arg.ToString());
                }
            }
        }

        private static string GetArrayValue(object arg)
        {
            var array = (IEnumerable)arg;
            var arrayString = "";
            foreach (var value in array)
            {
                arrayString = $"{arrayString},{value}";
            }
            arrayString = arrayString.Remove(0, 1);
            return arrayString;
        }
    }
}