﻿namespace ITExpert.CHPTZ.DataAccess.Remotes
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.ObjectModel;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Request.Documents;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public abstract class FunctionService<T> : FunctionService where T : IEntity, new()
    {
        protected FunctionService(IServiceConnection1C connection, IMResolver resolver) : base(connection, resolver)
        {
        }

        protected FunctionServiceResponse Invoke(string name, params object[] args)
        {
            var entity = new T();
            var request = RemoteCallRequest.Create(name, entity, args);
            return Invoke(request);
        }

        protected FunctionServiceResponse Invoke<TOut>(string name, params object[] args)
        {
            var entity = new T();
            var request = RemoteCallRequest.Create<T, TOut>(name, entity, args);
            return Invoke(request);
        }
    }

    public abstract class FunctionService : BaseDataService
    {
        private RemoteCallService1C Service { get; }

        protected FunctionService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new RemoteCallService1C(connection);
        }

        protected FunctionServiceResponse Invoke(string name, string entity, params object[] args)
        {
            var type = Resolver.GetInternalType(entity);
            var obj = EntityFactory.Create(type);
            var request = new RemoteCallRequest(name, type, obj, args);
            return Invoke(request);
        }

        protected FunctionServiceResponse Invoke(RemoteCallRequest request)
        {
            var args = request.Arguments.Select(ConvertArgument).ToArray();
            var function = new Function(request.Name, request.ReturnType.Name, request.EntityType.Name, args);

            var requestMDoc = new FunctionRequestDocument(request.EntityType, new[] {request.Entity}, function, Resolver);
            var requestXml = requestMDoc.Serialize();

            var responseXml = Service.InvokeFunction(requestXml);
            var responseMDoc = new FunctionResponseDocument(responseXml, request.ReturnType, Resolver);

            return FunctionServiceResponse.Create(responseMDoc);
        }

        private FunctionArgument ConvertArgument(object obj)
        {
            if (obj is Link1C)
            {
                var link = (Link1C)obj;
                var type = Resolver.GetClassType(link.Type);
                return new FunctionArgument(type, link.Value);
            }

            if (obj is IEntity)
            {
                var entity = (IEntity)obj;
                return new FunctionArgument(obj.GetType(), entity.Link.Value);
            }

            return new FunctionArgument(obj.GetType(), obj.ToString());
        }
    }
}
