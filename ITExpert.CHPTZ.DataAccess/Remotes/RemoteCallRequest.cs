﻿namespace ITExpert.CHPTZ.DataAccess.Remotes
{
    using System;
    using System.Collections.Generic;

    public class RemoteCallRequest
    {
        public string Name { get; }
        public Type EntityType { get; }
        public Type ReturnType { get; }
        public object Entity { get; }
        public IEnumerable<object> Arguments { get; }

        public RemoteCallRequest(string name, Type entityType, object entity, params object[] arguments) 
            : this(name, entityType, entityType, entity, arguments)
        {
        }

        public RemoteCallRequest(string name, Type entityType, Type returnType, object entity, params object[] arguments)
        {
            Name = name;
            EntityType = entityType;
            ReturnType = returnType;
            Entity = entity;
            Arguments = arguments;
        }

        public static RemoteCallRequest Create<T>(string name, T entity, params object[] args)
        {
            return Create<T, T>(name, entity, args);
        }

        public static RemoteCallRequest Create<TIn, TOut>(string name, TIn entity, params object[] args)
        {
            return new RemoteCallRequest(name, typeof(TIn), typeof(TOut), entity, args);
        }
    }
}
