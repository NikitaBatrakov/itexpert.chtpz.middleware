namespace ITExpert.CHPTZ.DataAccess.Remotes
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.DataAccess.Types;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class ProcedureServiceResponse : ServiceResponse
    {
        public IDictionary<string, PropertyMetadata> Metadata { get; }
        public IEnumerable<object> Entities { get; }

        public ProcedureServiceResponse(ServiceResult serviceResult, IDictionary<string, PropertyMetadata> metadata, IEnumerable<object> entities) : base(serviceResult)
        {
            Metadata = metadata;
            Entities = entities;
        }

        public static ProcedureServiceResponse Create(FunctionResponseDocument mDoc)
        {
            var serviceResult = mDoc.ServiceResult.Value.First();
            var metadata = mDoc.Metadata.Value.FirstOrDefault();
            var entities = mDoc.Entities.Value;
            return new ProcedureServiceResponse(serviceResult, metadata, entities);
        }
    }
}