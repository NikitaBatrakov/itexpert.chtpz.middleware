﻿namespace ITExpert.CHPTZ.DataAccess.Remotes
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.DataAccess.Types;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class FunctionServiceResponse : ServiceResponse
    {
        public IEnumerable<object> Entities { get; }
        public IEnumerable<FormAction> Actions { get; }

        public FunctionServiceResponse(ServiceResult serviceResult, IEnumerable<object> entities, IEnumerable<FormAction> actions)
            : base(serviceResult)
        {
            Entities = entities;
            Actions = actions;
        }

        public static FunctionServiceResponse Create(FunctionResponseDocument mDoc)
        {
            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault();
            var entities = mDoc.Entities.Value;
            var actions = mDoc.Actions.Value;
            return new FunctionServiceResponse(serviceResult, entities, actions);
        }
    }
}
