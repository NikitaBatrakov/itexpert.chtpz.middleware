﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendCommunications.Types;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Builders;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class PresetFilterDataService : BaseDataService<PresetFilterService1C>
    {
        public PresetFilterDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public MQueryResult GetEmployeesFunctionalGroups(string employeeGuid)
        {
            var xml = Service.GetEmployeesFunctionalGroups(employeeGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(EmployeeFunctionalGroup), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult GetEmployeesCurrentChangeRequests(string employeeGuid, MQuery query)
        {
            return ExecuteChangeRequestFilter(employeeGuid, query, Service.GetEmployeesCurrentChangeRequests);
        }

        public MQueryResult GetEmployeeGroupsCurrentChangeRequests(string employeeGuid, MQuery query)
        {
            return ExecuteChangeRequestFilter(employeeGuid, query, Service.GetEmployeeGroupsCurrentChangeRequests);
        }

        private MQueryResult ExecuteChangeRequestFilter(string employeeGuid, MQuery query, Func<string, GetEntityArguments, string> func)
        {
            var type = typeof(ChangeRequest);
            var args = ParseToGetEntity(type, "ChangeRequest", query);
            var xml = func(employeeGuid, args);
            var mDoc = new QueryEntitiesDocument(xml, typeof(ChangeRequest), Resolver);
            return new MQueryResult(mDoc.ServiceResult.Value.FirstOrDefault(), mDoc.Entities.Value,
                                    mDoc.TotalResult.Value.FirstOrDefault());
        }

        //TODO: Rewrite
        //Special aliasing is required because of all the joins
        private GetEntityArguments ParseToGetEntity(Type type, string alias, MQuery query)
        {
            var queryBuilder = new MQueryParser(type, Resolver, alias);
            var args = new GetEntityArguments
                       {
                           Fields = queryBuilder.ParseFields(query.Fields),
                           Limit = query.Limit,
                           Offset = query.Offset
                       };
            if (string.IsNullOrEmpty(query.Sort))
            {
                return args;
            }

            args.Sort = queryBuilder.ParseSort(query.Sort);
            args.Sort = AdjustSort(args.Sort, alias);
            return args;
        }

        private static string AdjustSort(string sort, string alias)
        {
            var fields = sort.Split(',');
            var result = fields.Aggregate("", (current, field) => current + $"{alias}.{field},");
            return result.Length > 1 ? result.RemoveLast() : result;
        }
    }
}
