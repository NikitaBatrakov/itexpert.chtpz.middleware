﻿namespace ITExpert.CHPTZ.DataAccess.DataServices.AttachmentDataServcies
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.FileManagement;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Request.Documents;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class AttachmentDataService : BaseDataService
    {
        private AttachmentService1C Service { get; }

        public AttachmentDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new AttachmentService1C(connection);
        }        

        public FileInfo GetFileInfo(string guid)
        {
            var attachment = GetAttachmentInfo(guid);
            return GetFileInfo(attachment);
        }

        public FileInfo GetFileInfo(Attachment1C attachmentProperty)
        {
            var filename = new FileName(attachmentProperty.Guid, attachmentProperty.Name.GetLastPart('.'));
            var attachment = new Attachment
                             {
                                 Link = new Link1C(attachmentProperty.Guid),
                                 Filename = filename.Name,
                                 Extension = filename.Extension,
                                 Date = attachmentProperty.Date,
                                 Employee = Link1C.Create<Employee>(attachmentProperty.EmployeeGuid),
                                 
                             };
            return GetFileInfo(attachment);
        }

        public FileInfo GetFileInfo(Attachment attachment)
        {
            var fileInfo = attachment.GetFileInfo();
            if (fileInfo.Exists && attachment.Date != null && fileInfo.CreationTime < attachment.Date.Value)
            {
                return fileInfo;
            }
            attachment = GetAttachment(attachment.Link.Value);
            WriteOnDisk(fileInfo, attachment.Value);
            return attachment.GetFileInfo();
        }

        public MQueryResult AddAttachments(IEnumerable<Attachment> attachments)
        {
            var type = typeof(Attachment);

            var requestMDoc = new EntitiesRequestDocument(attachments, type, Resolver);
            var requestXml = requestMDoc.Serialize();

            var responseXml = Service.AddAttachment(requestXml);
            var responseMDoc = new EntitiesResponseDocument(responseXml, type, Resolver);

            return MQueryResult.Create(responseMDoc);
        }

        private Attachment GetAttachmentInfo(string guid)
        {
            return GetAttachmentEntity(guid, returnFile: false);
        }

        private Attachment GetAttachment(string guid)
        {
            return GetAttachmentEntity(guid, returnFile: true);
        }

        private Attachment GetAttachmentEntity(string guid, bool returnFile)
        {
            var xml = Service.GetAttachment(guid, returnFile);
            var mDoc = new EntitiesResponseDocument(xml, typeof(Attachment), Resolver);
            return (Attachment)mDoc.Entities.Value.FirstOrDefault();
        }

        private static void WriteOnDisk(FileInfo fileInfo, string base64)
        {
            using (var fs = fileInfo.Create())
            using (var writer = new BinaryWriter(fs))
                writer.Write(Convert.FromBase64String(base64));
        }
    }
}
