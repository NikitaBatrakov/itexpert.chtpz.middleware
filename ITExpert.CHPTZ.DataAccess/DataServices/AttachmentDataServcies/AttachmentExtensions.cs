namespace ITExpert.CHPTZ.DataAccess.DataServices.AttachmentDataServcies
{
    using System.IO;

    using ITExpert.CHPTZ.DataAccess.FileManagement;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    internal static class AttachmentExtensions
    {
        public static FileInfo GetFileInfo(this Attachment attachment)
        {
            var path = FileManager.GetFilePath(attachment.Link?.Value, attachment.Extension);
            return new FileInfo(path);
        }
    }
}