﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class OperatorDataService : BaseDataService
    {
        private OperatorService1C Service { get; }

        public OperatorDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new OperatorService1C(connection);
        }

        #region Implementation of IOperatorService

        public MQueryResult RegisterPhoneCall(string phoneNumber, string recordLink, string userGuid)
        {
            var xml = Service.RegisterPhoneCall(phoneNumber, recordLink, userGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(ActivityJournal), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult CreateRequest(string entity, string initiatorGuid, string userGuid, string info, string phoneCallGuid)
        {
            var type = Resolver.GetClassType(entity);
            var type1C = type.GetName1C();
            var responseXml = Service.CreateRequest(type1C, initiatorGuid, userGuid, info, phoneCallGuid);
            var mDoc = new EntitiesResponseDocument(responseXml, type, Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult GetStatus(string entity, string entityGuid, string phoneCallGuid)
        {
            var type = Resolver.GetClassType(entity);
            var type1C = type.GetName1C();
            var responseXml = Service.GetStatus(type1C, entityGuid, phoneCallGuid);
            var mDoc = new EntitiesResponseDocument(responseXml, type, Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult GetEmployeesRequests(string employeeGuid, MQuery query)
        {
            var type = typeof(EmployeesRequest);
            var args = MQueryConverter.Convert(query, type, Resolver);
            var xml = Service.GetEmployeesRequests(employeeGuid, args);
            var mDoc = new EntitiesResponseDocument(xml, type, Resolver);
            return MQueryResult.Create(mDoc);
        }

        #endregion
    }
}
