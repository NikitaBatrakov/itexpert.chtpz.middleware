﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class TimezoneDataService : BaseDataService
    {
        private TimezoneService1C Service { get; }

        public TimezoneDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new TimezoneService1C(connection);
        }

        public MQueryResult GetTimezones(string employeeGuid)
        {
            var xml = Service.GetTimezones(employeeGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(Timezone), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult SetTimezones(string employeeGuid, string timezoneGuid)
        {
            var xml = Service.SetTimezone(employeeGuid, timezoneGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(Timezone), Resolver);
            return MQueryResult.Create(mDoc);
        }
    }
}
