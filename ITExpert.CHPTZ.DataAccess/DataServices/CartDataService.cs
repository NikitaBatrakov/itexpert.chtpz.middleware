﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Builders;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.DataAccess.Types;
    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Request.Documents;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class CartDataService : BaseDataService
    {
        private CartService1C Service { get; }

        public CartDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new CartService1C(connection);
        }

        public ServiceValueResponse<IEnumerable<TreeNode<Service>>> GetServices(string employeeGuid)
        {
            var xml = Service.GetServiceCatalogue(employeeGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(Service), Resolver);
            var tree = new TreeBuilder<Service, string>(
                x => x.Link.Value, 
                x => x.Parent.Value,
                x => x.Parent.Value == "Undefined").
                Build(mDoc.Entities.Value);
            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault();
            return new ServiceValueResponse<IEnumerable<TreeNode<Service>>>(serviceResult, tree);
        }

        public ServiceValueResponse<IEnumerable<TreeNode<ZnoTemplate>>> GetServiceTemplates(string serviceGuid)
        {
            var xml = Service.GetServiceTemplates(serviceGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(ZnoTemplate), Resolver);
            var tree = new TreeBuilder<ZnoTemplate, string>(
                x => x.Link.Value,
                x => x.Parent.Value,
                x => x.Parent.Value == "Undefined").
                Build(mDoc.Entities.Value);
            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault();
            return new ServiceValueResponse<IEnumerable<TreeNode<ZnoTemplate>>>(serviceResult, tree);
        }

        public MQueryResult GetServiceRequestOrder(string serviceRequestGuid)
        {
            var xml = Service.GetCartElement(typeof(ServiceRequest).GetName1C(), serviceRequestGuid);
            return GetOrder(xml);
        }

        public MQueryResult GetAgreementsOrder(string taskGuid)
        {
            var xml = Service.GetCartElement(typeof(Task).GetName1C(), taskGuid);
            return GetOrder(xml);
        }

        public MQueryResult CreateCart(IEnumerable<CartElement> items, IEnumerable<TemplateAttributes> attributes)
        {
            var cartElementsMDoc = new CartElementsDocument(items);
            var cartElementsXml = cartElementsMDoc.Serialize();

            var attributesMDoc = new TemplateAttributesDocument(attributes);
            var attributesXml = attributesMDoc.Serialize();

            var responseXml = Service.CreateCart(cartElementsXml, attributesXml);
            var mDoc = new EntitiesResponseDocument(responseXml, typeof(ServiceRequest), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult UpdateCart(ServiceRequest cart, IEnumerable<CartElement> items)
        {
            var itemsArr = items.ToArray();
            var cartXml = GetCartXml(cart, itemsArr);
            var itemsXml = GetItemsXml(itemsArr);
            return GetUpdateResponse(cartXml, itemsXml);
        }

        public MQueryResult UpdateCart(IEnumerable<CartElement> items)
        {
            var itemsXml = GetItemsXml(items.ToArray());
            return GetUpdateResponse("<Entities/>", itemsXml);
        }

        private MQueryResult GetOrder(string xml)
        {
            var mDoc = new CartOrderListResponseDocument(xml, Resolver);
            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault();
            var orders = mDoc.Orders.Value?.ToArray();
            if (orders == null || !orders.Any())
            {
                return new MQueryResult(serviceResult, new object[] {}, 0);
            }
            AssignComments(orders, mDoc.Comments.Value.ToArray());
            AssignTemplateAttributes(orders, mDoc.TemplatesAttributes.Value.ToArray());
            return new MQueryResult(serviceResult, orders, orders.Length);
        }

        private static void AssignComments(Order[] orders, OrderComment[] comments)
        {
            var orderLookup = orders.ToDictionary(x => x.CommentGuid.Value);
            var validComments =
                comments.Where(
                    x => x.CommentGuid != null && x.CommentGuid.Value != "Null" && x.CommentGuid.Value != "Undefined");
            foreach (var comment in validComments)
            {
                Order order;
                var isCommentFound = orderLookup.TryGetValue(comment.CommentGuid.Value, out order);
                if (isCommentFound)
                {
                    order.Comments.Add(comment);
                }
            }
        }

        private static void AssignTemplateAttributes(Order[] orders, ServiceTemplateAdditionalAttribute[] templateAttributes)
        {
            var orderLookup = orders.ToDictionary(x => x.ServiceRequestTemplate.Value);
            var validTemplateAttributes =
                templateAttributes.Where(
                    x => x.Service != null && x.Service.Value != "Null" && x.Service.Value != "Undefined");
            foreach (var templateAttribute in validTemplateAttributes)
            {
                Order order;
                var isTemplateFound = orderLookup.TryGetValue(templateAttribute.ServiceRequestTemplate.Value, out order);
                if (isTemplateFound)
                {
                    order.TemplateAttributes.Add(templateAttribute);
                }
            }
        }

        private string GetCartXml(ServiceRequest cart, CartElement[] items)
        {
            FIX_PrepareCart(cart, items);

            var entitiesMDoc = new EntitiesRequestDocument(new[] { cart }, typeof(ServiceRequest), Resolver);
            var entitiesXml = entitiesMDoc.Serialize();
            return entitiesXml;
        }

        private static string GetItemsXml(CartElement[] items)
        {
            var cartElementsMDoc = new CartElementsDocument(items);
            return cartElementsMDoc.Serialize();
        }

        private MQueryResult GetUpdateResponse(string cartXml, string itemsXml)
        {
            var responseXml = Service.UpdateCart(cartXml, itemsXml);
            var responseMDoc = new EntitiesResponseDocument(responseXml, typeof(ServiceRequest), Resolver);
            return MQueryResult.Create(responseMDoc);
        }

        //This is a temporary fix. Check in 1C if may be removed.
        private static void FIX_PrepareCart(ServiceRequest cart, CartElement[] items)
        {
            if (cart.Date == null)
            {
                cart.Date = new DateTime();
            }
            if (items.Length > 1)
            {
                cart.AgregatingZno = true;
            }
        }
    }
}
