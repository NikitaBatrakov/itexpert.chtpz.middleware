﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.DataAccess.Types;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.ObjectModel.Dtos.SearchDtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class SearchDataService : BaseDataService
    {
        private SearchService1C Service { get; }

        public SearchDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new SearchService1C(connection);
        }

        public ServiceValueResponse<UserFilter> GetParameters(string entity, string properties)
        {
            var type = Resolver.GetClassType(entity);
            var xml = Service.GetSearchParameters(type.GetName1C(), properties ?? "");
            var mDoc = new SearchEntitiesDocument(xml, type, Resolver);

            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault();
            var entities = mDoc.Entities.Value.First();
            return new ServiceValueResponse<UserFilter>(serviceResult, entities);
        }

        public ServiceValueResponse<IEnumerable<string>> SearchTypes(string query)
        {
            var xml = Service.SearchTypes(query);
            var mDoc = new PrimitiveValueDocuement<string>(xml);
            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault();
            var typesString = mDoc.Values.Value.FirstOrDefault();
            if (string.IsNullOrEmpty(typesString))
            {
                return new ServiceValueResponse<IEnumerable<string>>(serviceResult, Enumerable.Empty<string>());
            }
            var types = typesString.Split(',').Select(x => Resolver.GetInternalType(x.Trim()).Name);
            return new ServiceValueResponse<IEnumerable<string>>(serviceResult, types);
        }

        public MQueryResult Search(string query, string entity, MQuery queryArguments)
        {
            var type = Resolver.GetClassType(entity);
            var args = MQueryConverter.Convert(queryArguments, type, Resolver);
            var xml = Service.Search(query, type.GetName1C(), args);
            var mDoc = new EntitiesResponseDocument(xml, type, Resolver);
            return MQueryResult.Create(mDoc);
        }
    }
}
