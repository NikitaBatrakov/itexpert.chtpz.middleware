namespace ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Functions
{
    using System;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class IncidentFunctionSerivce : FunctionService<Incident>
    {
        public IncidentFunctionSerivce(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public FunctionServiceResponse GetAvailableFuncitonalGroups(string serviceGuid)
            => Invoke<FunctionalGroups>("�������������������", Link1C.Create<Service>(serviceGuid));

        public FunctionServiceResponse GetAvailableEmployeesRequest(string serviceGuid, string functionalGroupGuid)
            =>
                Invoke<Employee>("��������������������������", Link1C.Create<FunctionalGroups>(functionalGroupGuid), 
                    Link1C.Create<Service>(serviceGuid));

        public FunctionServiceResponse GetAvailableSla(string employeeGuid, DateTime date)
            => Invoke<Sla>("�����������������SLA", Link1C.Create<Employee>(employeeGuid), date);

        public FunctionServiceResponse GetAvailableServices(string employeeGuid, DateTime date)
            => Invoke<Service>("�����������������������", Link1C.Create<Employee>(employeeGuid), date);
    }
}