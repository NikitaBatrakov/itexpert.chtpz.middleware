namespace ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Functions
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class ServiceFunctionService : FunctionService<Service>
    {
        public ServiceFunctionService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public FunctionServiceResponse GetAvailableEmployeeServices(string employeeGuid)
            => Invoke("�����������������������", Link1C.Create<Employee>(employeeGuid));
    }
}