﻿namespace ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Functions
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class ObjectsLinkFunctionService : FunctionService<ObjectsLink>
    {
        public ObjectsLinkFunctionService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public FunctionServiceResponse GetAvailableRelationTypes(Link1C masterLink, string slaveType)
            => Invoke<LinkType>("ПолучитьДоступныеТипыСвязи", masterLink, Resolver.GetExternalTypeName(slaveType));
    }

    public class ObjectsLinkProcedureService : ProcedureService<ObjectsLink>
    {
        public ObjectsLinkProcedureService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public ProcedureServiceResponse SetType(IEntity entity)
        {
            var request = new RemoteCallRequest("УстановитьТипПоТипуСвязи", typeof(ObjectsLink), entity);
            return Invoke(request);
        }
    }
}
