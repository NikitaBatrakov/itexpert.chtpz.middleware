namespace ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Functions
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class TaskFunctionService : FunctionService<Task>
    {
        public TaskFunctionService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public FunctionServiceResponse GetAvailableEmployeesRequest(string functionalGroup)
            => Invoke<Employee>("��������������������������", Link1C.Create<FunctionalGroups>(functionalGroup));
    }
}