﻿namespace ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Procedures
{
    using System;
    using System.Reflection;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.ObjectModel;

    public class CommonProcedureService : ProcedureService
    {
        public CommonProcedureService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public ProcedureServiceResponse GetContextualMetadata(string entity, string objectGuid = "Undefined")
            => Invoke("ПриСозданииНаСервере", entity, objectGuid);

        public ProcedureServiceResponse Postprocess(string entity, string objectGuid)
            => Invoke("ПриЗакрытии", entity, objectGuid);

        public ProcedureServiceResponse LockObject(string entity, string objectGuid)
            => Invoke("ЗаблокироватьОбъект", entity, objectGuid);

        public ProcedureServiceResponse OnPropertyChanged(Type type, IEntity obj, PropertyInfo property)
        {
            var propName = property.GetCustomAttribute<Property1CAttribute>().Name;
            return Invoke(new RemoteCallRequest("ПриИзменении", type, obj, propName));
        }
    }
}
