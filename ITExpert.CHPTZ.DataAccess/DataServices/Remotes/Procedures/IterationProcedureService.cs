﻿namespace ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Procedures
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class IterationProcedureService : ProcedureService<Iteration>
    {
        public IterationProcedureService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public ProcedureServiceResponse CreateHelpRequest(string iterationGuid, string changeRequestGuid, string teamGuid, int resource)
            => Invoke("ЗапроситьПомощь", iterationGuid, Link1C.Create<ChangeRequest>(changeRequestGuid), Link1C.Create<FunctionalGroups>(teamGuid), resource);

        public ProcedureServiceResponse SetDevelopingStatus(string iterationGuid, int resource)
            => Invoke("ВключитьЗадачуНаРазработку", iterationGuid, resource);
 
        public ProcedureServiceResponse UpdateResource(string iterationGuid, int resource)
            => Invoke("ИзменитьТрудоемкость", iterationGuid, resource);

        public ProcedureServiceResponse Adjust(string iterationGuid) => Invoke("ПодгонкаИтерацииПодТрудозатраты", iterationGuid);

        public ProcedureServiceResponse ChangeTaskType(string iterationGuid, string taskTypeGuid) => Invoke("ИзменитьТипЗадачи", iterationGuid, Link1C.Create<AdditionalRequisiteType>(taskTypeGuid));

        public ProcedureServiceResponse Exclude(string iterationGuid) => Invoke("ИсключитьИзПлана", iterationGuid);
    }
}
