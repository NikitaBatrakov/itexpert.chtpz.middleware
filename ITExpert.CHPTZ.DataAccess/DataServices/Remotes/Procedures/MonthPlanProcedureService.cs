﻿namespace ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Procedures
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class MonthPlanProcedureService : ProcedureService<MonthPlan>
    {
        public MonthPlanProcedureService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public ProcedureServiceResponse Approve(string entityGuid) => Invoke("УтвердитьМесячныйПлан", entityGuid);

        public ProcedureServiceResponse Update(string entityGuid) => Invoke("ИзменитьМесячныйПлан", entityGuid);

        public ProcedureServiceResponse FillWithProjects(string entityGuid) => Invoke("АвтозаполнитьПроектными", entityGuid);

        public ProcedureServiceResponse FillWithNonProjects(string entityGuid, string teamGuid)
            => Invoke("АвтозаполнитьНепроектными", entityGuid, Link1C.Create<FunctionalGroups>(teamGuid));

        public ProcedureServiceResponse AddComment(string entityGuid, string teamGuid, string comment)
            => Invoke("ДобавитьКомментарий", entityGuid, Link1C.Create<FunctionalGroups>(teamGuid), comment);

        public ProcedureServiceResponse ApproveReport(string entityGuid) => Invoke("УтвердитьОтчетность", entityGuid);
    }
}
