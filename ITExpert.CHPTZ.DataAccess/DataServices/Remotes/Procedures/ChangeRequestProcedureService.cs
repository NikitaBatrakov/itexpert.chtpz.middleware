﻿namespace ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Procedures
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class ChangeRequestProcedureService : ProcedureService<ChangeRequest>
    {
        public ChangeRequestProcedureService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {

        }

        public ProcedureServiceResponse IncludeInMonthPlan(string entityGuid, string monthPlanGuid, string teamGuid,
                                                           string taskTypeGuid, int resource)
            =>
                Invoke("ВключитьВПлан", entityGuid, Link1C.Create<MonthPlan>(monthPlanGuid),
                       Link1C.Create<FunctionalGroups>(teamGuid), Link1C.Create<AdditionalRequisiteType>(taskTypeGuid), resource);
    }
}
