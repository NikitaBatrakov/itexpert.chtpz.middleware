﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.DataAccess.Repositories;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class RelationDataService : BaseDataService
    {
        private RelationService1C Service { get; }

        public RelationDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new RelationService1C(connection);
        }

        public MQueryResult GetRelations(string entity, string objectGuid, MQuery arguments)
        {
            var entity1C = Resolver.GetExternalTypeName(entity);
            var type = typeof(RelationEntity);
            var args = MQueryConverter.Convert(arguments, type, Resolver);
            var xml = Service.GetRelations(entity1C, objectGuid, args);
            var mDoc = new EntitiesResponseDocument(xml, type, Resolver);
            return new MQueryResult(mDoc.ServiceResult.Value.First(), mDoc.Entities.Value, mDoc.TotalResult.Value.FirstOrDefault());
        }
    }
}
