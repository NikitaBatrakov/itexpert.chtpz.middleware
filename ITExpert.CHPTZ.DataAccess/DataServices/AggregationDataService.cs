﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendCommunications.Types;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class AggregationDataService : BaseDataService<AggregationService1C>
    {
        public AggregationDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public MQueryResult GetAggregationList(MQuery query)
        {
            return GetResponse(query, Service.GetAggregationList);
        }

        public MQueryResult GetExtendedAggregaionList(MQuery query)
        {
            return GetResponse(query, Service.GetExtendedAggregationList);
        }

        private MQueryResult GetResponse(MQuery query, Func<GetEntityArguments, string> func)
        {
            var type = typeof(AggregationList);
            var args = MQueryConverter.Convert(query, type, Resolver, "");
            var xml = func(args);
            var mDoc = new EntitiesResponseDocument(xml, type, Resolver);
            return MQueryResult.Create(mDoc);
        }
    }
}
