﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Procedures;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.ObjectModel.Entities.MonthPlanEntities;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class MonthPlanDataService : BaseDataService
    {
        private MonthPlanService1C Service { get; }
        
        public IterationProcedureService IterationProcedures { get; }
        public MonthPlanProcedureService MonthPlanProcedures { get; }
        public ChangeRequestProcedureService ChangeRequestProcedures { get; }

        public MonthPlanDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new MonthPlanService1C(connection);            
            IterationProcedures = new IterationProcedureService(connection, resolver);
            MonthPlanProcedures = new MonthPlanProcedureService(connection, resolver);
            ChangeRequestProcedures = new ChangeRequestProcedureService(connection, resolver);
        }

        public MQueryResult GetBusinessProcessLevel()
        {
            var xml = Service.GetMonthPlanLevel();
            var mDoc = new EntitiesResponseDocument(xml, typeof(BusinessProcess), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult GetDirectionLevel(string businessProcessGuid)
        {
            var xml = Service.GetMonthPlanLevel(businessProcessGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(Direction), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult GetFunctionalGroupLevel(string businessProcessGuid, string directionGuid)
        {
            var xml = Service.GetMonthPlanLevel(businessProcessGuid, directionGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(FunctionalGroups), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult GetChangeRequests(string teamGuid, string directionGuid, string monthPlanGuid)
        {
            var xml = Service.GetUnallocatedChangeRequests(teamGuid, directionGuid, monthPlanGuid);
            var mDoc = new MonthPlanDocument(xml, typeof(MonthPlanChangeRequest), Resolver);
            return new MQueryResult(mDoc.ServiceResult.Value.FirstOrDefault(), mDoc.Entities.Value,
                                    mDoc.TotalResult.Value.FirstOrDefault());
        }

        public MQueryResult GetIterations(string teamGuid, string monthPlanGuid)
        {
            var xml = Service.GetIterations(teamGuid, monthPlanGuid);
            var mDoc = new MonthPlanDocument(xml, typeof(MonthPlanIteration), Resolver);
            return new MQueryResult(mDoc.ServiceResult.Value.FirstOrDefault(), mDoc.Entities.Value,
                                    mDoc.TotalResult.Value.FirstOrDefault());
        }

        public MQueryResult GetAvailableTaskTypes()
        {
            var xml = Service.GetAvailableTaskTypes();
            var mDoc = new EntitiesResponseDocument(xml, typeof(AdditionalRequisiteType), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public int CheckIfMayBeExcluded(string iterationGuid)
        {
            return Service.CheckIterationEliminate(iterationGuid);
        }

        public int GetDefaultLaborCosts(string monthPlanGuid, string changeRequestGuid)
        {
            return Service.GetDefaultLaborCosts(monthPlanGuid, changeRequestGuid);
        }

        public int GetTeamResource(string teamGuid, string monthPlanGuid)
        {
            return Service.GetTeamResource(teamGuid, monthPlanGuid);
        }

        public MQueryResult CheckMonthPlan(int year, int month)
        {
            var xml = Service.CheckMonthPlan(year, month);
            var mDoc = new EntitiesResponseDocument(xml, typeof(AdditionalRequisiteType), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult GetMonthPlanActions(string teamGuid, string monthPlanGuid)
        {
            var xml = Service.GetMonthPlanActions(teamGuid, monthPlanGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(AdditionalRequisiteType), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult GetMonthPlanVersions(int year, int month)
        {
            var xml = Service.GetMonthPlanVersions(year, month);
            var mDoc = new EntitiesResponseDocument(xml, typeof(MonthPlan), Resolver);
            return MQueryResult.Create(mDoc);
        }
    }
}
