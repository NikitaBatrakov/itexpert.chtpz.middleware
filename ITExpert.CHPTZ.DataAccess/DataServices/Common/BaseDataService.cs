﻿namespace ITExpert.CHPTZ.DataAccess.DataServices.Common
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Common;

    public abstract class BaseDataService<TService1C> : BaseDataService where TService1C : BaseService1C
    {
        protected TService1C Service { get; }

        protected BaseDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = (TService1C)System.Activator.CreateInstance(typeof(TService1C), connection);
        }
    }

    public abstract class BaseDataService
    {
        internal IServiceConnection1C Connection { get; }
        internal IMResolver Resolver { get; }

        protected BaseDataService(IServiceConnection1C connection, IMResolver resolver)
        {
            Connection = connection;
            Resolver = resolver;
        }
    }
}
