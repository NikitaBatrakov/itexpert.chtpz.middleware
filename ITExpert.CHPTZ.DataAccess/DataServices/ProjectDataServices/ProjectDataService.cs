﻿namespace ITExpert.CHPTZ.DataAccess.DataServices.ProjectDataServices
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;

    public class ProjectDataService : BaseDataService
    {
        public ProjectProcedureService ProjectProcedures { get; }

        public ProjectDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            ProjectProcedures = new ProjectProcedureService(connection, resolver);
        }
    }
}
