﻿namespace ITExpert.CHPTZ.DataAccess.DataServices.ProjectDataServices
{
    using Common;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class ProjectProcedureService : ProcedureService<Project>
    {
        public ProjectProcedureService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public ProcedureServiceResponse ReleaseProject(string projectGuid)
        => Invoke("РеализацияПроекта", projectGuid);

        public ProcedureServiceResponse CloseProject(string projectGuid)
        => Invoke("Закрыть", projectGuid);

        public ProcedureServiceResponse ShiftProject(string projectGuid, int months, string teamGuid = "")
        => Invoke("СдвинутьПроект", projectGuid, months, Link1C.Create<FunctionalGroups>(teamGuid));

        public ProcedureServiceResponse CreateEvent(string projectGuid)
        => Invoke("СоздатьМероприятие", projectGuid);

        public ProcedureServiceResponse RemoveEvent(string projectGuid, string eventGuid)
        => Invoke("УдалитьМероприятие", projectGuid, Link1C.Create<Event>(eventGuid));

        public ProcedureServiceResponse AddTeamToEvent(string projectGuid, string eventGuid, string teamGuid)
        => Invoke("ДобавитьКомандуВМероприятие", projectGuid, Link1C.Create<Event>(eventGuid), Link1C.Create<FunctionalGroups>(teamGuid));

        public ProcedureServiceResponse ChangeTeam(string projectGuid, string projectTeamGuid, string teamGuid)
        => Invoke("ИзменитьПроектнуюКоманду", projectGuid, Link1C.Create<ProjectTeam>(projectTeamGuid), Link1C.Create<FunctionalGroups>(teamGuid));

        public ProcedureServiceResponse DistributeTeamResources(string projectGuid, string projectTeamGuid, int[] resources)
        => Invoke("РаспределитьРесурсКоманды", projectGuid, Link1C.Create<ProjectTeam>(projectTeamGuid), resources);

        public ProcedureServiceResponse RemoveTeamFromEvent(string projectGuid, string eventGuid, string teamGuid)
        => Invoke("УдалитьКомандуИзМероприятия", projectGuid, Link1C.Create<Event>(eventGuid), Link1C.Create<FunctionalGroups>(teamGuid));

        public ProcedureServiceResponse InitializeExecutionStage(string projectGuid, string eventGuid, string teamGuid)
        => Invoke("ПеревестиНаИсполнение", projectGuid, Link1C.Create<Event>(eventGuid), Link1C.Create<FunctionalGroups>(teamGuid));
    }
}
