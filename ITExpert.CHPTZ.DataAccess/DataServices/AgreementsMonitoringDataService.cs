﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class AgreementsMonitoringDataService : BaseDataService<AgreementsMonitoringService1C>
    {
        public AgreementsMonitoringDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public MQueryResult MonitorAgreements(string requestEntity, string requestGuid, MQuery query)
        {
            var type = typeof(ArgreementsMonitoringObject);
            var requestEntity1C = Resolver.GetExternalTypeName(requestEntity);
            var args = MQueryConverter.Convert(query, type, Resolver, "");
            var xml = Service.MonitorAgreements(requestEntity1C, requestGuid, args);
            var mDoc = new EntitiesResponseDocument(xml, type, Resolver);
            return MQueryResult.Create(mDoc);
        }
    }
}
