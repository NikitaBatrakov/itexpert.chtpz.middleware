﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class DocumentsTreeDataService : BaseDataService<DocumentsTreeService1C>
    {
        public DocumentsTreeDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
        }

        public MQueryResult GetDocumentsTree(IEnumerable<string> guids)
        {
            var xml = Service.GetDocumentsTree(guids);
            var mDoc = new EntitiesResponseDocument(xml, typeof(Document), Resolver);
            return MQueryResult.Create(mDoc);
        }
    }
}
