﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class AdditionalAttributesDataService : BaseDataService
    {
        private AdditionalAttributesSerivce1C Serivce { get; }

        public AdditionalAttributesDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Serivce = new AdditionalAttributesSerivce1C(connection);
        }

        public IEnumerable<AdditionalAttributeValue> GetValues(string entity, string entityGuid)
        {
            var name1C = Resolver.GetExternalTypeName(entity);
            var xml = Serivce.GetAdditionalAttributesValues(name1C, entityGuid);
            var mDoc = new AttributesValuesDocument(xml, Resolver);
            return mDoc.AttributesValues.Value;
        }

        public IEnumerable<AdditionalAttributeType> GetTypes(string typeGuid)
        {
            var xml = Serivce.GetAdditionalAttributes(typeGuid);
            var mDoc = new AttributesTypesDocument(xml, Resolver);
            return mDoc.AttributesDetail.Value;
        }
    }
}
