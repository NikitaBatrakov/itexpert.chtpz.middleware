﻿namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.DataAccess.Types;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Request.Documents;
    using ITExpert.CHPTZ.Serializer.Response.Documents;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;

    public class NotificationDataService : BaseDataService
    {
        private NotificationService1C Service { get; }

        public NotificationDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new NotificationService1C(connection);
        }

        public MQueryResult GetNotifications(string employeeGuid)
        {
            var xml = Service.GetNotifications(employeeGuid);
            var mDoc = new EntitiesResponseDocument(xml, typeof(NotificationWithAttachment), Resolver);
            return MQueryResult.Create(mDoc);
        }

        public ServiceResponse ReadNotifications(IEnumerable<NotificationWithAttachment> notifications)
        {
            var type = typeof(NotificationWithAttachment);
            var requestMDoc = new EntitiesRequestDocument(notifications, type, Resolver);
            var requestXml = requestMDoc.Serialize();
            var responseXml = Service.ReadNotifications(requestXml);
            var responseMDoc = new EntitiesResponseDocument(responseXml, type, Resolver);
            return new ServiceResponse(responseMDoc.ServiceResult.Value.FirstOrDefault());
        }

        public int GetNotificationsNumber(string employeeGuid)
        {
            return Service.GetNotificationsCount(employeeGuid);
        }

        public ServiceValueResponse<IEnumerable<NotificationsNumber>> GetNotificationsNumber(IEnumerable<string> employeesGuids)
        {
            var xml = Service.GetNotifications(employeesGuids);
            var mDoc = new NotificationsNumberDocument(xml);
            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault();
            var value = mDoc.Entities.Value;
            return new ServiceValueResponse<IEnumerable<NotificationsNumber>>(serviceResult, value);
        }

        public ServiceResponse ReadObjectNotifications(string employeeGuid, string entity, string objectGuid)
        {
            entity = Resolver.GetExternalTypeName(entity) ?? entity;
            var xml = Service.ReadObjectNotifications(employeeGuid, entity, objectGuid);
            var mDoc = new ResultableResponseDocument(xml);
            return new ServiceResponse(mDoc.ServiceResult.Value.FirstOrDefault());
        }
    }
}
