namespace ITExpert.CHPTZ.DataAccess.DataServices
{
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.Types;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;

    public class DefaultFilterDataService : BaseDataService
    {
        private DefaultFilterService1C Service { get; }

        public DefaultFilterDataService(IServiceConnection1C connection, IMResolver resolver)
            : base(connection, resolver)
        {
            Service = new DefaultFilterService1C(connection);
        }

        public string GetDefaultFilter(string entity, string employeeGuid, string placeGuid)
        {
            return Service.GetDefaultFilter(entity, employeeGuid, placeGuid);
        }

        public ServiceResponse SetDefaultFilter(string entity, string employeeGuid, string viewGuid, string placeGuid)
        {
            var xml = Service.SetDefaultFilter(viewGuid, entity, employeeGuid, placeGuid);
            var mDoc = new ResultableResponseDocument(xml);
            return new ServiceResponse(mDoc.ServiceResult.Value.FirstOrDefault());
        }

        public string GetGroupFilter(string entity, string employeeGuid)
        {
            return Service.GetGroupFilter(entity, employeeGuid);
        }
    }
}