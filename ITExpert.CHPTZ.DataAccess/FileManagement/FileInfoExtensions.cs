namespace ITExpert.CHPTZ.DataAccess.FileManagement
{
    using System.IO;

    public static class FileInfoExtensions
    {
        public static FileName GetFileName(this FileInfo fileInfo)
        {
            return new FileName(fileInfo.Name);
        }
    }
}