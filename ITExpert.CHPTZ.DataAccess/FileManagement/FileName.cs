namespace ITExpert.CHPTZ.DataAccess.FileManagement
{
    public struct FileName
    {
        public string Name { get; }

        public string Extension { get; }

        public string NameWithExtension => $"{Name}.{Extension}";

        public FileName(string name, string extension)
        {
            Name = name;
            Extension = extension;
        }

        public FileName(string fileNameWithExtension)
        {
            var dotIndex = fileNameWithExtension.LastIndexOf('.');
            Name = fileNameWithExtension.Substring(0, dotIndex);
            Extension = fileNameWithExtension.Substring(dotIndex + 1);
        }
    }
}