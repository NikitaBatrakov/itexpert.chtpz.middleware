namespace ITExpert.CHPTZ.DataAccess.FileManagement
{
    using System.Configuration;

    public class FileManager
    {
        private static readonly string StoragePath = ConfigurationManager.AppSettings["WebServiceStorage"];

        private const string FilesDirectory = "Files";

        public static string GetFilePath(string filename, string extension)
        {
            var path = $"{StoragePath}\\{FilesDirectory}\\{filename}.{extension}";            
            return path;
        }

        public static string GetFileUrl(string baseUrl, FileName fileName)
        {
            return $"{baseUrl}/{FilesDirectory}/{fileName.Name}.{fileName.Extension}";
        }
    }
}