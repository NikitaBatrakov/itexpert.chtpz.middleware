﻿namespace ITExpert.CHPTZ.DataAccess.Types
{
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public class ServiceResponse
    {
        public ServiceResult ServiceResult { get; }

        public ServiceResponse(ServiceResult serviceResult)
        {
            ServiceResult = serviceResult;
        }
    }
}
