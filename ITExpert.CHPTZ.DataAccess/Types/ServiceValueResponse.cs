namespace ITExpert.CHPTZ.DataAccess.Types
{
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public class ServiceValueResponse<T> : ServiceResponse
    {
        public T Value { get; }

        public ServiceValueResponse(ServiceResult serviceResult, T value)
            : base(serviceResult)
        {
            Value = value;
        }
    }
}