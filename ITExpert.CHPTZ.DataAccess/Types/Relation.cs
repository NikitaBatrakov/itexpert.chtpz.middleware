﻿namespace ITExpert.CHPTZ.DataAccess.Types
{
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class Relation
    {
        public ObjectsLink ObjectsLink { get; set; }
        public IEntity RelatedObject { get; set; }

        public Relation(ObjectsLink objectsLink, IEntity relatedObject)
        {
            ObjectsLink = objectsLink;
            RelatedObject = relatedObject;
        }
    }
}
