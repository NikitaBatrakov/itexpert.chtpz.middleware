﻿namespace ITExpert.CHPTZ.DataAccess.Types
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class CartOrderResponse
    {
        public ServiceResult ServiceResult { get; }
        public Order Value { get; }
        public IEnumerable<OrderComment> Comments { get; }
        public IEnumerable<ServiceTemplateAdditionalAttribute> TemplateAttributes { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CartOrderResponse(ServiceResult serviceResult, Order value, IEnumerable<OrderComment> comments, IEnumerable<ServiceTemplateAdditionalAttribute> templateAttributes)
        {
            ServiceResult = serviceResult;
            Value = value;
            Comments = comments;
            TemplateAttributes = templateAttributes;
        }
    }
}
