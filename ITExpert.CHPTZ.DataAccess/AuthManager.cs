﻿namespace ITExpert.CHPTZ.DataAccess
{
    using System.Configuration;
    using System.Linq;
    using System.Security.Authentication;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.ObjectModel;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class AuthManager : DataManager
    {
        private AuthenticationService1C Service { get; }

        public AuthManager()
        {
            var adminLogin = ConfigurationManager.AppSettings["WebServiceLogin"];
            var adminPassword = ConfigurationManager.AppSettings["WebServicePassword"];
            if (adminLogin == null || adminPassword == null)
            {
                throw new ConfigurationErrorsException("Service Login or Password for authentication is not found in Web.confg.");
            }
            Connection = new ServiceConnection1C(adminLogin, adminPassword);
            Resolver = new Resolver();
            Service = new AuthenticationService1C(Connection);
        }

        public User Authenticate(string login, string password)
        {
            var xml = Service.Authenticate(login, password);
            var mDoc = new EntitiesResponseDocument(xml, typeof(User), Resolver);
            var serviceResult = mDoc.ServiceResult.Value.First();
            if (!serviceResult.IsOk)
            {
                throw new AuthenticationException();
            }
            return (User)mDoc.Entities.Value.First();
        }
    }
}
