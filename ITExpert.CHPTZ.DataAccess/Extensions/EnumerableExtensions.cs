﻿namespace ITExpert.CHPTZ.DataAccess.Extensions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess.MQueries.Builders;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;

    public static class EnumerableExtensions
    {
        public static IEnumerable<TreeNode<T>> ToTree<T, TKey>(this IEnumerable<T> enumerable, Func<T, TKey> getIdFunc,
                                               Func<T, TKey> getParentIdFunc, Func<T, bool> isRootFunc) where T : class
        {
            var treeBuilder = new TreeBuilder<T, TKey>(getIdFunc, getParentIdFunc, isRootFunc);
            return treeBuilder.Build(enumerable);
        }
    }
}
