namespace ITExpert.CHPTZ.DataAccess.Repositories
{
    using System;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;

    using ObjectModel;

    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.BackendCommunications.Services.Interfaces;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class Repository<T> : Repository where T : IObject1C
    {
        internal Repository(IMResolver resolver, IServiceConnection1C connection)
            : base(typeof(T), resolver, connection) { }
    }

    public class Repository : AbstractRepository
    {
        protected IReadService1C ReadService { get; }

        internal Repository(Type type, IMResolver resolver, IServiceConnection1C connection) 
            : base(type, resolver, connection)
        {
            ReadService = new ReadService1C(connection);
            CurrentQuery = new MQueryWithOptions();
        }

        public override MQueryResult Query(MQueryWithOptions query)
        {
            var args = MQueryConverter.Convert(query, Type, Resolver);
            var xml = ReadService.GetEntities(Type.GetName1C(), args);
            var mDoc = new EntitiesResponseDocument(xml, Type, Resolver, query.WithMetadata);
            return MQueryResult.Create(mDoc);
        }

        public override MQueryResult Query(MQuery query)
        {
            var args = MQueryConverter.Convert(query, Type, Resolver);
            var xml = ReadService.GetEntities(Type.GetName1C(), args);
            var mDoc = new EntitiesResponseDocument(xml, Type, Resolver);
            return MQueryResult.Create(mDoc);
        }

        public override MQueryResult Query()
        {
            if (!string.IsNullOrEmpty(CurrentQuery.Fields) && CurrentQuery.Fields.EndsWith(","))
            {
                CurrentQuery.Fields = CurrentQuery.Fields.RemoveLast();
            }
            AddFilterTypes();
            return Query(CurrentQuery);
        }

        private void AddFilterTypes()
        {
            if (!IsSingleType && TypeCounter > Types.Count)
            {
                throw new FormatException("Guid Types for Composite properties were not specified." +
                                          $"Expected number of types: {TypeCounter}. Number of types specified: {Types.Count}." +
                                          "Use 'params Type[] types' argument in Where, AndWhere, OrWhere functions");
            }
            if (TypeCounter > 0)
            {
                var type = Types.First();
                for (var i = 0; i < TypeCounter; i++)
                {
                    Types.Add(type);
                }
            }
            CurrentQuery.Filter = string.Format(CurrentQuery.Filter, Types.ToArray());
        }
    }
}