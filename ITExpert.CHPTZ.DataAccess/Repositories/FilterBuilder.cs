﻿namespace ITExpert.CHPTZ.DataAccess.Repositories
{
    using System;
    using System.Linq.Expressions;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    internal class FilterBuilder<T> where T : IObject1C
    {
        public int TypeCounter { get; set; }

        public string Build(Expression<Func<T, bool>> expression)
        {
            var logicalExpr = expression.Body as BinaryExpression;
            if (logicalExpr == null)
                throw new Exception("Expression in Where clause is not valid - " +
                                    "use only simple expressions like [x => x.Type == value]. " +
                                    "Boolean properties should NOT be used like [x => x.IsOk], " +
                                    "use [x => x.IsOk == true] instead");
            return GetFilter(logicalExpr);
        }

        public string GetFilterString(ExpressionType comparisonType, MemberExpression conditionMemberExpr,
            object value, string prefix = "")
        {
            var name = GetMemeberName(conditionMemberExpr);
            var valStr = value.ToString();
            var marker = $"{{{TypeCounter}}}";
            if (name.StartsWith(marker))
            {
                name = name.Replace(marker, "");
                valStr = $"{value},{marker}";
                TypeCounter++;
            }
            var comparison = GetComparisonOperator(comparisonType);
            return $"{prefix}{name}{comparison.InternalSign}\"{valStr}\"";
        }

        private string GetFilter(BinaryExpression logicalExpr)
        {
            var firstConditionExpr = logicalExpr.Right as BinaryExpression;
            return firstConditionExpr == null
                ? ParseSimpleExpression(logicalExpr)
                : ParseComplexExpression(logicalExpr, firstConditionExpr);
        }

        private string ParseSimpleExpression(BinaryExpression logicalExpr)
        {
            var value = GetValue(logicalExpr.Right);
            var comparisonType = logicalExpr.NodeType;
            var conditionMemberExpr = (MemberExpression)logicalExpr.Left;
            return GetFilterString(comparisonType, conditionMemberExpr, value);
        }

        private string ParseComplexExpression(BinaryExpression logicalExpr, BinaryExpression firstConditionExpr)
        {
            var value = GetValue(firstConditionExpr.Right);
            var comparisonType = firstConditionExpr.NodeType;
            var conditionMemberExpr = (MemberExpression)firstConditionExpr.Left;

            var logicalOperator = GetLogicalOperator(logicalExpr.NodeType);
            var leftCondition = GetFilter((BinaryExpression)logicalExpr.Left);
            var prefix = $"{leftCondition} {logicalOperator} ";

            return GetFilterString(comparisonType, conditionMemberExpr, value, prefix);
        }

        private object GetValue(Expression valueExpression)
        {
            var accessor = Expression.Lambda<Func<object>>(Expression.Convert(valueExpression, typeof(object))).Compile();
            var value = accessor();
            if (value == null)
            {
                throw new Exception("Expression in Where clause is not valid - " +
                                    "right part of the expression is null");
            }
            return value;
        }

        private string GetMemeberName(MemberExpression expression, string type = "")
        {
            var memberExpr = expression.Expression as MemberExpression;
            if (memberExpr == null)
            {
                return string.IsNullOrEmpty(type) ? expression.Member.Name : $"{type}{expression.Member.Name}";
            }
            if (memberExpr.Type == typeof(Composite1C))
            {
                type = $"{{{TypeCounter}}}";
            }
            return GetMemeberName(memberExpr, type);
        }

        private string GetLogicalOperator(ExpressionType nodeType)
        {
            switch (nodeType)
            {
                case ExpressionType.OrElse:
                    return "OR";
                case ExpressionType.AndAlso:
                    return "AND";
                default:
                    throw new Exception("Unexpected logical operator - only && and || are supported");
            }
        }

        private ComparisonOperator1C GetComparisonOperator(ExpressionType nodeType)
        {
            switch (nodeType)
            {
                case ExpressionType.Equal:
                    return new ComparisonOperator1C(ComparisonSign.Equal);
                case ExpressionType.NotEqual:
                    return new ComparisonOperator1C(ComparisonSign.NotEqual);
                case ExpressionType.LessThan:
                    return new ComparisonOperator1C(ComparisonSign.LessThan);
                case ExpressionType.GreaterThan:
                    return new ComparisonOperator1C(ComparisonSign.GreaterThan);
                case ExpressionType.LessThanOrEqual:
                    return new ComparisonOperator1C(ComparisonSign.LessThanOrEqual);
                case ExpressionType.GreaterThanOrEqual:
                    return new ComparisonOperator1C(ComparisonSign.GreaterThanOrEqual);
                default:
                    throw new Exception("Unexpected comparison operator - only ==, !=, >, < are supported");
            }
        }
    }
}
