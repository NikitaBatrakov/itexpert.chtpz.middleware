namespace ITExpert.CHPTZ.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;
    using ITExpert.CHPTZ.BackendCommunications.Services.Interfaces;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Serializer.Request.Documents;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class EntityRepository : Repository
    {
        private IWriteService1C WriteService { get; }

        public EntityRepository(Type type, IMResolver resolver, IServiceConnection1C connection)
            : base(type, resolver, connection)
        {
            WriteService = new WriteService1C(connection);
        }

        public object GetById(string id)
        {
            //TODO: make filter builder
            CurrentQuery = new MQueryWithOptions { Filter = $"Link::\"{id}\"" };
            return Query().Entities.First();
        }

        public MQueryResult Add(params IObject1C[] entities)
        {
            var mDoc = CreateOrUpdateEntity(entities, isCreate: true);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult Update(params IObject1C[] entities)
        {
            var mDoc = CreateOrUpdateEntity(entities, isCreate: false);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult DeleteByIds(params string[] ids)
        {
            var entities = new List<IEntity>();
            foreach (var id in ids)
            {
                var entity = (IEntity)Activator.CreateInstance(Type);
                entity.Link = new Link1C(id);
                entity.IsMarkedToDelete = true;
                entities.Add(entity);
            }
            var mDoc = CreateOrUpdateEntity(entities, isCreate: false);
            return MQueryResult.Create(mDoc);
        }

        public MQueryResult Delete(params IEntity[] entities)
        {
            foreach (var entity in entities)
            {
                entity.IsMarkedToDelete = true;
            }
            var mDoc = CreateOrUpdateEntity(entities, isCreate: false);
            return MQueryResult.Create(mDoc);
        }

        private EntitiesResponseDocument CreateOrUpdateEntity(IEnumerable<IObject1C> entities, bool isCreate)
        {
            var requestMDoc = new EntitiesRequestDocument(entities, Type, Resolver);
            var requestXml = requestMDoc.Serialize();

            var responseXml = isCreate
                                  ? WriteService.CreateEntity(requestXml)
                                  : WriteService.UpdateEntity(requestXml);
            var responseMDoc = new EntitiesResponseDocument(responseXml, Type, Resolver);

            return responseMDoc;
        }
    }
}