﻿namespace ITExpert.CHPTZ.DataAccess.Repositories.AbstractRepositoryExtensions
{
    public static class RepositorySortingExtensions
    {
        /// <summary>
        /// Sorts results by specified field
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="fields">Fields</param>
        /// <returns>Repository</returns>
        public static AbstractRepository OrderBy(this AbstractRepository repository, params string[] fields)
        {
            repository.CurrentQuery.Sort += string.Join(",", fields);
            return repository;
        }

        /// <summary>
        /// Sort results by specified field descending
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="fields">Fields</param>
        /// <returns>Repository</returns>
        public static AbstractRepository OrderByDescending(this AbstractRepository repository, params string[] fields)
        {
            for (var i = 0; i < fields.Length; i++)
            {
                fields[i] += " DESC";
            }
            repository.CurrentQuery.Sort += string.Join(",", fields);
            return repository;
        }
    }
}
