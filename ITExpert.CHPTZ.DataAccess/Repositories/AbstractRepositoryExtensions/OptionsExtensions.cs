﻿namespace ITExpert.CHPTZ.DataAccess.Repositories.AbstractRepositoryExtensions
{
    using System;

    using ITExpert.CHPTZ.DataAccess.MQueries.Types;

    public static class RepositoryOptionsExtensions
    {
        /// <summary>
        /// Specifies MQuery options
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="options">Options (can be piped with '|')</param>
        /// <returns>Repository</returns>
        public static AbstractRepository WithOptions(this AbstractRepository repository, MQueryOptions options = MQueryOptions.None)
        {
            switch (options)
            {
                case MQueryOptions.WithMetadata:
                    repository.CurrentQuery.WithMetadata = true;
                    break;
                case MQueryOptions.WithTables:
                    repository.CurrentQuery.WithTables = true;
                    break;
                case MQueryOptions.WithAttachments:
                    repository.CurrentQuery.WithAttachments = true;
                    break;
                case MQueryOptions.WithAdditionalAttributes:
                    repository.CurrentQuery.WithAttributes = true;
                    break;
                case MQueryOptions.All:
                    repository.CurrentQuery.WithMetadata = true;
                    repository.CurrentQuery.WithTables = true;
                    repository.CurrentQuery.WithAttachments = true;
                    repository.CurrentQuery.WithAttributes = true;
                    break;
                case MQueryOptions.None:
                    repository.CurrentQuery.WithMetadata = false;
                    repository.CurrentQuery.WithTables = false;
                    repository.CurrentQuery.WithAttachments = false;
                    repository.CurrentQuery.WithAttributes = false;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(options), options, null);
            }
            return repository;
        }
    }
}
