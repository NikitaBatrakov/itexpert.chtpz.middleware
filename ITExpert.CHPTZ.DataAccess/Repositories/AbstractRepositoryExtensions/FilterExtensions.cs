﻿namespace ITExpert.CHPTZ.DataAccess.Repositories.AbstractRepositoryExtensions
{
    using System;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    public static class RepositoryFilterExtensions
    {
        private static AbstractRepository SetFilter(AbstractRepository repository, string field,
                                                    ComparisonSign comparison, string value,
                                                    FilterLogicalOperator logicalOperator)
        {
            var comparisonOperator = new ComparisonOperator1C(comparison);
            var sign = comparisonOperator.InternalSign;
            var filter = $"{field}{sign}\"{value}\"";

            if (string.IsNullOrEmpty(repository.CurrentQuery.Filter))
            {
                logicalOperator = FilterLogicalOperator.None;
            }
            switch (logicalOperator)
            {
                case FilterLogicalOperator.None:
                    repository.CurrentQuery.Filter = filter;
                    break;
                case FilterLogicalOperator.And:
                    repository.CurrentQuery.Filter = $"{repository.CurrentQuery.Filter} AND {filter}";
                    break;
                case FilterLogicalOperator.Or:
                    repository.CurrentQuery.Filter = $"{repository.CurrentQuery.Filter} OR {filter}";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(logicalOperator), logicalOperator, null);
            }

            
            return repository;
        }

        /// <summary>
        /// Specifies condition for selecting entities
        /// Clears previously specified Where, AndWhere, OrWhere filters
        /// Only simple boolean binary expressions are supported
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="field"> </param>
        /// <param name="comparison"> </param>
        /// <param name="value"> </param>
        /// <returns>Builder</returns>
        public static AbstractRepository Where(this AbstractRepository repository, string field, ComparisonSign comparison, string value)
        {
            return SetFilter(repository, field, comparison, value, FilterLogicalOperator.None);
        }

        /// <summary>
        /// Adds condition for selecting entities after AND operator
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="field">Field</param>
        /// <param name="comparison">Comparison operator</param>
        /// <param name="value">Value</param>
        /// <returns>Builder</returns>
        public static AbstractRepository AndWhere(this AbstractRepository repository, string field, ComparisonSign comparison, string value)
        {
            return SetFilter(repository, field, comparison, value, FilterLogicalOperator.And);
        }

        /// <summary>
        /// Adds condition for selecting entities after OR operator
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="field">Field</param>
        /// <param name="comparison">Comparison operator</param>
        /// <param name="value">Value</param>
        /// <returns>Builder</returns>
        public static AbstractRepository OrWhere(this AbstractRepository repository, string field, ComparisonSign comparison, string value)
        {
            return SetFilter(repository, field, comparison, value, FilterLogicalOperator.Or);
        }

        /// <summary>
        /// Specifies that property value should match one of the values in list
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="field">Field</param>
        /// <param name="comparison">Comparison operator</param>
        /// <param name="values">Values</param>
        /// <returns>Builder</returns>
        public static AbstractRepository WhereIn(this AbstractRepository repository, string field, ComparisonSign comparison, params string[] values)
        {
            foreach (var value in values)
            {
                SetFilter(repository, field, comparison, value, FilterLogicalOperator.Or);
            }
            return repository;
        }

        /// <summary>
        /// Specifies that Link property shoud match one of the links in argument list.
        /// Types specification is not needed and will cause an exception when made.
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="ids">IDs</param>
        /// <returns>Builder</returns>
        public static AbstractRepository WithId(this AbstractRepository repository, params string[] ids)
        {
            return repository.WhereIn("Link", ComparisonSign.Equal, ids);
        }
    }
}
