﻿namespace ITExpert.CHPTZ.DataAccess.Repositories.AbstractRepositoryExtensions
{
    public static class RepositoryRowNumberExtensions
    {
        /// <summary>
        /// Specifies number of returned entities
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="limit">Number of entities to be returned</param>
        /// <returns>Builder</returns>
        public static AbstractRepository Take(this AbstractRepository repository, int limit)
        {
            repository.CurrentQuery.Limit = limit;
            return repository;
        }

        /// <summary>
        /// Specifies query offset
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="offset">Number of entities to be skipped</param>
        /// <returns>Builder</returns>
        public static AbstractRepository Skip(this AbstractRepository repository, int offset)
        {
            repository.CurrentQuery.Offset = offset;
            return repository;
        }
    }
}
