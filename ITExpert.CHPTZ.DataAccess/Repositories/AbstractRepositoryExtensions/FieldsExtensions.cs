﻿namespace ITExpert.CHPTZ.DataAccess.Repositories.AbstractRepositoryExtensions
{
    public static class AbstractRepositoryFieldsExtensions
    {
        /// <summary>
        /// Specifies returned entity properties.
        /// If not present, all properties returned.
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="names">Proprety names</param>
        /// <returns>Builder</returns>
        public static AbstractRepository Field(this AbstractRepository repository, params string[] names)
        {
            repository.CurrentQuery.Fields += string.Join(",", names);
            return repository;
        }
    }
}
