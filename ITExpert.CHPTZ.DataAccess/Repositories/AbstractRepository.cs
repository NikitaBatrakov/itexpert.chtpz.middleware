namespace ITExpert.CHPTZ.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;

    public abstract class AbstractRepository
    {
        internal Type Type { get; }
        protected IMResolver Resolver { get; }

        internal MQueryWithOptions CurrentQuery { get; set; }
        internal int TypeCounter { get; set; }
        internal bool IsSingleType { get; set; }
        internal ICollection<string> Types { get; }

        public AbstractRepository(Type type, IMResolver resolver, IServiceConnection1C connection)
        {
            Type = type;
            Resolver = resolver;
            CurrentQuery = new MQueryWithOptions();
            Types = new List<string>();
        }

        public abstract MQueryResult Query(MQueryWithOptions query);
        public abstract MQueryResult Query(MQuery query);
        public abstract MQueryResult Query();
    }

    public abstract class AbstractEntityRepository : AbstractRepository
    {
        public AbstractEntityRepository(Type type, IMResolver resolver, IServiceConnection1C connection)
            : base(type, resolver, connection)
        {
        }

        public abstract object GetById(string id);
        public abstract MQueryResult Add(params IEntity[] entities);
        public abstract MQueryResult Update(params IEntity[] entities);
        public abstract MQueryResult DeleteByIds(params string[] ids);
        public abstract MQueryResult Delete(params IEntity[] entities);
    }
}