﻿namespace ITExpert.CHPTZ.DataAccess.Repositories.GenericRepositoryExtensions
{
    using System;
    using System.Linq.Expressions;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    public static class FieldsExtensions
    {
        /// <summary>
        /// Specifies returned entity properties.
        /// If not present, all properties returned.
        /// </summary>
        /// <typeparam name="TProp">Property type</typeparam>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="repository">Repository</param>
        /// <param name="expression">Propreties selector</param>
        /// <returns>Builder</returns>
        public static Repository<T> Field<T, TProp>(this Repository<T> repository, Expression<Func<T, TProp>> expression) where T : IObject1C
        {
            var memberExpr = (MemberExpression)expression.Body;
            var name = memberExpr.Member.Name;
            repository.CurrentQuery.Fields = $"{repository.CurrentQuery.Fields}{name},";
            return repository;
        }
    }
}
