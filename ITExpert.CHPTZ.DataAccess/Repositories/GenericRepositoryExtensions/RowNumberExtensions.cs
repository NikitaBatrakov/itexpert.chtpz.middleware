﻿namespace ITExpert.CHPTZ.DataAccess.Repositories.GenericRepositoryExtensions
{
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    public static class RowNumberExtensions
    {
        /// <summary>
        /// Specifies number of returned entities
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="limit">Number of entities to be returned</param>
        /// <returns>Builder</returns>
        public static Repository<T> Take<T>(this Repository<T> repository, int limit) where T : IObject1C
        {
            repository.CurrentQuery.Limit = limit;
            return repository;
        }

        /// <summary>
        /// Specifies query offset
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="offset">Number of entities to be skipped</param>
        /// <returns>Builder</returns>
        public static Repository<T> Skip<T>(this Repository<T> repository, int offset) where T : IObject1C
        {
            repository.CurrentQuery.Offset = offset;
            return repository;
        }
    }
}
