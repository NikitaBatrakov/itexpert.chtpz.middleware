﻿namespace ITExpert.CHPTZ.DataAccess.Repositories.GenericRepositoryExtensions
{
    using System;
    using System.Linq.Expressions;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    public static class SortingExtensions
    {
        /// <summary>
        /// Sorts result by specified property
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="TProp">Property type</typeparam>
        /// <param name="repository">Repository</param>
        /// <param name="expression">Property selector</param>
        /// <returns>Repository</returns>
        public static Repository<T> OrderBy<T,TProp>(this Repository<T> repository, Expression<Func<T, TProp>> expression) where T : IObject1C
        {
            return GetSorting(repository, expression, isDesc: false);
        }

        /// <summary>
        /// Sorts result by specified property descending
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="TProp">Property type</typeparam>
        /// <param name="repository">Repository</param>
        /// <param name="expression">Property selector</param>
        /// <returns>Repository</returns>
        public static Repository<T> OrderByDescending<T,TProp>(this Repository<T> repository, Expression<Func<T, TProp>> expression) where T : IObject1C
        {
            return GetSorting(repository, expression, isDesc: true);
        }

        private static Repository<T> GetSorting<T, TProp>(Repository<T> repository, Expression<Func<T, TProp>> expression, bool isDesc) where T : IObject1C
        {
            var memberExpr = (MemberExpression)expression.Body;
            var sorting = memberExpr.Member.Name;
            if (isDesc)
            {
                sorting += " DESC";
            }
            repository.CurrentQuery.Sort = string.IsNullOrEmpty(repository.CurrentQuery.Sort) ? sorting : $"{repository.CurrentQuery.Sort},{sorting}";
            return repository;
        }
    }
}
