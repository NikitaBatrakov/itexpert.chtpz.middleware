﻿namespace ITExpert.CHPTZ.DataAccess.Repositories.GenericRepositoryExtensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    public static class FilterExtensions
    {
        private static Repository<T> SetFilter<T>(Repository<T> repository, Expression<Func<T, bool>> expression, FilterLogicalOperator logicalOperator,
                                               params Type[] types) where T : IObject1C
        {
            var filterBuilder = new FilterBuilder<T> { TypeCounter = repository.TypeCounter };
            var filter = filterBuilder.Build(expression);
            var currentFilter = repository.CurrentQuery.Filter;
            switch (logicalOperator)
            {
                case FilterLogicalOperator.None:
                    break;
                case FilterLogicalOperator.And:
                    filter = string.IsNullOrEmpty(currentFilter) ? filter : $"{currentFilter} AND {filter}";
                    break;
                case FilterLogicalOperator.Or:
                    filter = string.IsNullOrEmpty(currentFilter) ? filter : $"{currentFilter} OR {filter}";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(logicalOperator), logicalOperator, null);
            }

            repository.CurrentQuery.Filter = filter;
            repository.TypeCounter = filterBuilder.TypeCounter;
            foreach (var type in types)
            {
                repository.Types.Add(type.Name);
            }

            return repository;
        }

        /// <summary>
        /// Specifies condition for selecting entities
        /// Clears previously specified Where, AndWhere, OrWhere filters
        /// Only simple boolean binary expressions are supported
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="expression">Condtion</param>
        /// <param name="types">Types of composite fields in expression if present. 
        /// Should be specified in exact same order as composite fields in expression.</param>
        /// <returns>Builder</returns>
        public static Repository<T> Where<T>(this Repository<T> repository, Expression<Func<T, bool>> expression, params Type[] types) where T : IObject1C
        {
            return SetFilter(repository, expression, FilterLogicalOperator.None, types);
        }

        /// <summary>
        /// Adds condition for selecting entities after AND operator
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="expression">Condition</param>
        /// <param name="types">Types of composite fields in expression if present. 
        /// Should be specified in exact same order as composite fields in expression.</param>
        /// <returns>Builder</returns>
        public static Repository<T> AndWhere<T>(this Repository<T> repository, Expression<Func<T, bool>> expression, params Type[] types) where T : IObject1C
        {
            return SetFilter(repository, expression, FilterLogicalOperator.And, types);
        }

        /// <summary>
        /// Adds condition for selecting entities after OR operator
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="expression">Condition</param>
        /// <param name="types">Types of composite fields in expression if present. 
        /// Should be specified in exact same order as composite fields in expression.</param>
        /// <returns>Builder</returns>
        public static Repository<T> OrWhere<T>(this Repository<T> repository, Expression<Func<T, bool>> expression, params Type[] types) where T : IObject1C
        {
            return SetFilter(repository, expression, FilterLogicalOperator.Or, types);
        }

        /// <summary>
        /// Specifies that property value should match one of the values in list
        /// </summary>
        /// <typeparam name="TProp">Property Type</typeparam>
        /// <typeparam name="T">Entity Type</typeparam>
        /// <param name="repository">Repository</param>
        /// <param name="expression">Property selector</param>
        /// <param name="list">List of posible values</param>
        /// <param name="type">Type of GUIDs in array if present</param>
        /// <returns>Builder</returns>
        public static Repository<T> WhereIn<T, TProp>(this Repository<T> repository,
                                                      Expression<Func<T, TProp>> expression, IEnumerable<TProp> list,
                                                      Type type = null) where T : IObject1C
        {
            var filterBuilder = new FilterBuilder<T> {TypeCounter = repository.TypeCounter};
            var memberExpr = (MemberExpression)expression.Body;
            var filter = "";
            foreach (var value in list)
            {
                filter = filterBuilder.GetFilterString(ExpressionType.Equal, memberExpr, value, $"{filter} OR ");
            }

            if (filter.Length > 4)
            {
                filter = filter.Remove(0, 4);
            }

            repository.CurrentQuery.Filter = filter;
            if (type == null)
            {
                return repository;
            }

            repository.IsSingleType = true;
            repository.Types.Add(type.Name);
            return repository;
        }

        /// <summary>
        /// Specifies that Link property shoud match one of the links in argument list.
        /// Types specification is not needed and will cause an exception when made.
        /// </summary>
        /// <param name="repository">Repository</param>
        /// <param name="ids">Object IDs</param>
        /// <returns>Builder</returns>
        public static Repository<T> WithId<T>(this Repository<T> repository, params string[] ids) where T : IEntity
        {
            foreach (var id in ids.Where(x => !string.IsNullOrEmpty(x)))
            {
                repository.OrWhere(x => x.Link.Value == id);
            }
            return repository;
        }
    }
}
