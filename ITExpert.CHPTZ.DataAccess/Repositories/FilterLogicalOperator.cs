﻿namespace ITExpert.CHPTZ.DataAccess.Repositories
{
    internal enum FilterLogicalOperator
    {
        None, And, Or
    }
}
