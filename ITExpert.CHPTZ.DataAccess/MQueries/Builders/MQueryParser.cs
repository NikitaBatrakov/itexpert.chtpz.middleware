﻿namespace ITExpert.CHPTZ.DataAccess.MQueries.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Common.Extensions;

    //TODO: Rewrite
    public class MQueryParser
    {
        public Type Type { get; }
        
        public string Alias { get; set; }

        public string Parameters => ValueParser.Parameters;

        private string AliasPrefix => string.IsNullOrEmpty(Alias) ? "" : $"{Alias}.";

        private PropertyChainParser PropertyChainParser { get; }
        private FilterValueParser ValueParser { get; }

        public MQueryParser(Type type, IMResolver resolver, string alias = "obj")
        {
            Type = type;
            Alias = alias;
            PropertyChainParser = new PropertyChainParser(type, resolver);
            ValueParser = new FilterValueParser(type, resolver);
        }

        public string Build(string entity, string filter, string[] fields)
        {
            var columns = ParseResolvedFields(fields);
            if (string.IsNullOrEmpty(filter))
            {
                return $"ВЫБРАТЬ {columns} ИЗ {entity} КАК {Alias}";
            }
            filter = filter.Replace("[", "").Replace("]", "");
            var conditions = ParseFilter(filter);
            return string.IsNullOrEmpty(conditions) 
                ? $"ВЫБРАТЬ {columns} ИЗ {entity} КАК {Alias}"
                : $"ВЫБРАТЬ {columns} ИЗ {entity} КАК {Alias} ГДЕ {conditions}";
        }

        public IEnumerable<string> ResolveFields(string fields)
        {
            var split = fields.Split(',');
            var props = split.Select(propertyName => Type.GetPropertyIgnoreCase(propertyName)).ToArray();
            //Alias is not used on purpose. 1C will thron an exception otherwise.
            return props.Select(propertyInfo => propertyInfo.GetCustomAttribute<Property1CAttribute>().Name.GetLastPart('.'));
        }

        public string ParseResolvedFields(ICollection<string> fields)
        {
            if (fields.Count == 0) return "*";
            //Alias is not used on purpose. 1C will thron an exception otherwise.
            var columns = fields.Select(field => $"{field.GetLastPart('.')}").ToArray();
            return string.Join(",", columns, 0, fields.Count);
        }

        public string ParseFields(string fields, bool usePropertiesNamesAsAliases = true)
        {
            var result = "";
            if (string.IsNullOrEmpty(fields))
            {
                return ParseAllTypePropertiesAsFields(usePropertiesNamesAsAliases);
            }

            var split = fields.Split(',').ToList();
            if (!split.Contains("link", StringComparer.InvariantCultureIgnoreCase))
            {
                split.Add("link");
            }

            foreach (var internalField in split)
            {
                var externalField =
                    Type.GetPropertyIgnoreCase(internalField).
                         GetCustomAttribute<Property1CAttribute>().
                         Name.GetLastPart('.');
                if (usePropertiesNamesAsAliases)
                {
                    externalField = $"{externalField} КАК {internalField}";
                }
                result += $"{AliasPrefix}{externalField},";
            }

            return result.RemoveLast();
        }

        public string ParseAllTypePropertiesAsFields(bool usePropertiesNamesAsAliases = true)
        {
            var result = "";
            var props = Type.GetProperties();
            foreach (var prop in props)
            {
                var field = prop.GetCustomAttribute<Property1CAttribute>()?.Name.GetLastPart('.');
                if (string.IsNullOrEmpty(field)) continue;
                result += $"{AliasPrefix}{field}";
                if (usePropertiesNamesAsAliases)
                {
                    result += $" КАК {prop.Name},";
                }
                else
                {
                    result += ",";
                }
            }
            return result.RemoveLast();
        }

        public static string ParseParameters(string internalParams, IMResolver resolver)
        {
            if (string.IsNullOrEmpty(internalParams)) return "";
            var parameters = "";
            foreach (var param in internalParams.Split(';'))
            {
                var paramArr = param.Split(',');
                if (paramArr.Length < 2)
                    continue;
                string typeName;
                try
                {
                    typeName = resolver.GetExternalTypeName(paramArr[1]);
                }
                catch
                {
                    continue;
                }
                parameters += $"{paramArr[0]},{typeName};";
            }

            return parameters.Remove(parameters.Length - 1);
        }

        public string ParseSort(string internalSort)
        {
            if (string.IsNullOrEmpty(internalSort)) return "";
            var sortFields = internalSort.Split(',');
            var sort = "";
            foreach (var sortField in sortFields)
            {
                var sortFieldArr = sortField.Split(' ');
                var chain = PropertyChainParser.Parse(sortFieldArr[0]);
                var fieldName1C = chain.Item1;
                if (string.IsNullOrEmpty(fieldName1C)) continue;
                if (sortFieldArr.Length > 1 && (sortFieldArr[1] == "-" || sortFieldArr[1].ToUpper() == "DESC"))
                    fieldName1C += " УБЫВ";
                sort += fieldName1C + ", ";
            }
            return sort.Length < 2 ? "" : sort.Remove(sort.Length - 2);
        }



        public string ParseFilter(string filter)
        {
            if (string.IsNullOrEmpty(filter)) return "";
            var match = GetMatch(filter);
            var result = "";
            do
            {
                var internalField = match.Groups["field"].Value;
                var tuple = PropertyChainParser.Parse(internalField);
                var field = $"{AliasPrefix}{tuple.Item1}";
                var prop = tuple.Item2;

                var internalValue = match.Groups["value"].Value;

                if (internalValue.StartsWith("\"["))
                {
                    result += ParseArrayValue(field, internalValue, prop, match);
                    continue;
                }

                var value = ValueParser.Parse(prop, internalValue);
                result += ParseMatch(match, field, value);
            }
            while ((match = match.NextMatch()).Success);

            return result;
        }

        private string ParseArrayValue(string field, string internalValue, PropertyInfo prop, Match match)
        {
            string expressions = "(", openingBrace = "", closingBrace = "", appendix = "";
            internalValue = internalValue.Replace("\"[", "").Replace("]\"", "");
            var valueArray = internalValue.Split(',');
            var ind = 0;
            do
            {
                var value = valueArray[ind];

                if (prop.PropertyType == typeof(string))
                {
                    value = $"\"{value}\"";
                }
                value = ValueParser.Parse(prop, value);

                var components = ParseMatch(match, field, value);

                if (ind == 0)
                {
                    openingBrace = components.OpeningBrace;
                    closingBrace = components.ClosingBrace;
                    appendix = components.Appendix;
                }

                expressions += $"{components.Field} {components.Comparison} {components.Value} OR ";
                ind++;
            }
            while (ind < valueArray.Length);

            expressions = expressions.RemoveLast(4) + ")";
            return $"{openingBrace}{expressions}{appendix}{closingBrace}";
        }

        private static Match GetMatch(string filter) 
        {
            const string comparison = @"(?<comparison>>>|<<|>:|<:|::|!:|\*|\*%|%\*|%%|!\*|!\*%|!%\*|!%%)";
            const string field = @"(?<field>(?:[A-Za-z0-9\.]*))";
            const string value = @"(?<value>"".*?"")";
            const string appendix = @"(?<appendix>\sOR\s|\sAND\s)?";
            const string openingBrace = @"(?<opening_brace>\(*)";
            const string closingBrace = @"(?<closing_brace>\)*)";
            var regex = new Regex($"{openingBrace}{field}{comparison}{value}{closingBrace}{appendix}");
            var match = regex.Match(filter);
            if (!match.Success)
            {
                throw new FormatException("Filter format exception: Filter format is incorrect.");
            }
            return match;
        }

        private static FilterComponents ParseMatch(Match match, string field, string value)
        {
            var openingBrace = match.Groups["opening_brace"].Value;
            var closingBrace = match.Groups["closing_brace"].Value;
            var comparison = match.Groups["comparison"].Value;
            var appendix = match.Groups["appendix"].Value;

            var likeOperators = new[] {"*", "*%", "%*", "%%", "!*", "!*%", "!%*", "!%%"};
            if (likeOperators.Contains(comparison))
            {
                return ParseLikeOperator(field, value, comparison, openingBrace, closingBrace, appendix);
            }

            comparison = ComparisonOperator1C.ConvertInternalToCommonSign(comparison);
            return new FilterComponents(field, value, comparison, openingBrace, closingBrace, appendix);
        }

        private static FilterComponents ParseLikeOperator(string field, string value, string comparison,
                                                          string openingBrace, string closingBrace, string appendix)
        {
            var isNegative = false;
            if (comparison.StartsWith("!"))
            {
                comparison = comparison.Remove(0, 1);
                isNegative = true;
            }

            switch (comparison)
            {
                case "*%":
                    value = $"{value.RemoveLast()}%\"";
                    break;
                case "%*":
                    value = $"\"%{value.Remove(0, 1)}";
                    break;
                case "%%":
                    value = $"\"%{value.Remove(0, 1).RemoveLast()}%\"";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            comparison = isNegative ? "НЕ ПОДОБНО" : "ПОДОБНО";
            return new FilterComponents(field, value, comparison, openingBrace, closingBrace, appendix);
        }
    }
}

