﻿namespace ITExpert.CHPTZ.DataAccess.MQueries.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.Serializer.Common.Extensions;

    public class FilterValueParser
    {
        public string Parameters => string.Join(";", _parameters);
        private readonly ICollection<string> _parameters;

        private Type Type { get; }
        private IMResolver Resolver { get; }

        public FilterValueParser(Type type, IMResolver resolver)
        {
            Type = type;
            Resolver = resolver;
            _parameters = new List<string>();
        }

        public string Parse(PropertyInfo prop, string value)
        {
            value = value.Replace("\"", "");

            if (value.StartsWith("["))
            {
                value = value.Remove(0, 1).RemoveLast();
                var split = value.Split(',');
                return ParseArray(prop, split);
            }

            switch (prop.GetType1CInfo())
            {
                case Type1CInfo.String:
                    return $"\"{value}\"";
                case Type1CInfo.DataTime:
                    return ParseDateValue(value);
                case Type1CInfo.Link:
                    return ParseLinkValue(prop, value);
                case Type1CInfo.Composite:
                    return ParseCompositeValue(value);
                case Type1CInfo.Number:
                case Type1CInfo.Boolean:
                case Type1CInfo.Table:
                    return value;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private string ParseArray(PropertyInfo prop, string[] values)
        {
            var parsedValues = new string[values.Length];
            for (var i = 0; i < values.Length; i++)
            {
                parsedValues[i] = Parse(prop, values[i]);
            }
            return $"({string.Join(",", parsedValues)})";
        }

        private static string ParseDateValue(string value)
        {
            var date = string.IsNullOrEmpty(value) ? new DateTime() : DateTime.Parse(value, CultureInfo.InvariantCulture);
            return $"ДАТАВРЕМЯ({date.Year},{date.Month},{date.Day},{date.Hour},{date.Minute},{date.Second})";
        }

        private string ParseCompositeValue(string value)
        {
            if (string.Equals(value, "НЕОПРЕДЕЛЕНО", StringComparison.InvariantCultureIgnoreCase))
            {
                return "НЕОПРЕДЕЛЕНО";
            }
            var valueSplit = value.Split(';');
            if (valueSplit.Length != 2)
            {
                throw new FormatException("When field is composite, type of object identified with GUID should be specified after ';'");
            }
            value = valueSplit[0];
            var type = Resolver.GetExternalTypeName(valueSplit[1]);
            _parameters.Add($"{value},{type}");
            return $"&Value{_parameters.Count}";
        }

        private string ParseLinkValue(PropertyInfo prop, string value)
        {
            var valueSplit = value.Split(';');
            value = valueSplit[0];
            var type = prop.GetCustomAttribute<Type1CAttribute>().Type;
            if (type.ToLowerInvariant() == "id")
            {
                type = Type.GetName1C();
            }
            _parameters.Add($"{value},{type}");
            return $"&Value{_parameters.Count}";
        }
    }
}
