﻿namespace ITExpert.CHPTZ.DataAccess.MQueries.Builders
{
    using System;
    using System.Reflection;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.Common.Extensions;

    public class PropertyChainParser
    {
        private Type Type { get; }
        private IMResolver Resolver { get; }

        public PropertyChainParser(Type type, IMResolver resolver)
        {
            Type = type;
            Resolver = resolver;
        }

        public Tuple<string, PropertyInfo> Parse(string internalProprtyChain)
        {
            var split = internalProprtyChain.Split('.');
            var externalChain = "";
            var type = Type;
            PropertyInfo prop = null;
            foreach (var property in split)
            {
                if (type == null)
                    throw new FormatException("One of the properties in chain is not of reference type. " +
                                              "Only final property in chain can be of value type.");

                prop = type.GetPropertyIgnoreCase(property);
                if (prop == null)
                    throw new FormatException($"One of the filter fields is invalid. Wrong part: '{property}'");

                var name1C = prop.GetCustomAttribute<Property1CAttribute>().Name.GetLastPart('.');
                externalChain = $"{externalChain}{name1C}.";

                var typeAttr = prop.GetCustomAttribute<Type1CAttribute>();
                //TODO: Composite field
                var type1C = typeAttr.Type;
                type = Resolver.GetInternalType(type1C, throwOnError:false);
            }
            externalChain = externalChain.RemoveLast();
            return new Tuple<string, PropertyInfo>(externalChain, prop);
        }
    }
}
