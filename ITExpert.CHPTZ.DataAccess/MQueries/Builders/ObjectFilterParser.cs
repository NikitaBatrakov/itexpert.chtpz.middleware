namespace ITExpert.CHPTZ.DataAccess.MQueries.Builders
{
    using System;
    using System.Reflection;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Serializer.Common.Extensions;

    public class ObjectFilterParser
    {
        public string Parameters => ValueParser.Parameters;

        public string Alias { get; }
        private string AliasPrefix => string.IsNullOrEmpty(Alias) ? "" : $"{Alias}.";

        private PropertyChainParser PropertyChainParser { get; }
        private FilterValueParser ValueParser { get; }

        public ObjectFilterParser(Type type, IMResolver resolver, string alias = "obj")
        {
            PropertyChainParser = new PropertyChainParser(type, resolver);
            ValueParser = new FilterValueParser(type, resolver);
            Alias = alias;
        }

        public string Parse(FilterElement root)
        {
            return ProcessNode(root);
        }

        private string ProcessNode(FilterElement node)
        {
            var expression = node.IsLogical ? GetLogicalExpression(node) : GetComparisonExpression(node);
            return expression;
        }

        private string GetLogicalExpression(FilterElement node)
        {
            var expression = "";
            var logicalOperator = ParseLogicalOperator(node.Operator);
            foreach (var child in node.Nodes)
            {
                var negation = child.IsNegative ? "�� " : "";
                var subexpression = ProcessNode(child);
                expression += $"{negation}({subexpression}) {logicalOperator} ";
            }
            return expression.RemoveLast(logicalOperator.Length + 2);
        }

        private string GetComparisonExpression(FilterElement node)
        {
            var chain = PropertyChainParser.Parse(node.Field);
            var field = $"{AliasPrefix}{chain.Item1}";

            var comparison = ParseComparisonOperator(node.Operator);

            var value = ProcessValueForOperator(comparison.Sign, node.Value, chain.Item2);
            value = ValueParser.Parse(chain.Item2, value);

            return $"{field} {comparison.CommonSign} {value}";
        }

        private static string ParseLogicalOperator(string logicalOperator)
        {
            switch (logicalOperator.ToLowerInvariant())
            {
                case "and":
                    return "�";
                case "or":
                    return "���";
                default:
                    throw new FormatException($"Logical operator '{logicalOperator}' not recognized. Only 'And' and 'Or' are supported.");
            }
        }

        private static ComparisonOperator1C ParseComparisonOperator(string comparisonName)
        {
            ComparisonSign comparisonOperator;
            var isParsingFailed = !Enum.TryParse(comparisonName, true, out comparisonOperator);
            if (isParsingFailed)
            {
                throw new FormatException($"Comparison operator '{comparisonName}' is not valid");
            }
            return new ComparisonOperator1C(comparisonOperator);
        }

        private static string ProcessValueForOperator(ComparisonSign comparisonSign, string value, PropertyInfo prop)
        {
            switch (comparisonSign)
            {
                case ComparisonSign.Equal:
                case ComparisonSign.NotEqual:
                case ComparisonSign.GreaterThan:
                case ComparisonSign.LessThan:
                case ComparisonSign.GreaterThanOrEqual:
                case ComparisonSign.LessThanOrEqual:
                case ComparisonSign.Like:
                case ComparisonSign.In:
                    return value;
                case ComparisonSign.StartsWith:
                    return $"{value}%";
                case ComparisonSign.EndsWith:
                    return $"%{value}";
                case ComparisonSign.Contains:
                    return $"%{value}%";
                case ComparisonSign.IsEmpty:
                    return ProcessIsEmptyOperator(prop);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static string ProcessIsEmptyOperator(PropertyInfo prop)
        {
            switch (prop.GetType1CInfo())
            {
                case Type1CInfo.String:
                    return "";
                case Type1CInfo.Number:
                    return "0";
                case Type1CInfo.Boolean:
                    return "false";
                case Type1CInfo.DataTime:
                    //TODO: Maybe ���������() should be used
                    return new DateTime().ToUtcIsoString();
                case Type1CInfo.Link:
                    return new Guid().ToString();
                case Type1CInfo.Composite:
                    throw new NotImplementedException();
                case Type1CInfo.Table:
                    throw new NotImplementedException();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}