﻿namespace ITExpert.CHPTZ.DataAccess.MQueries.Builders
{
    using System;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess.MQueries.Types;

    public class TreeBuilder<T, TKey>
    {
        private Dictionary<TKey, TreeNode<T>> NodePool { get; }
        private Func<T, TKey> GetIdFunc { get; }
        private Func<T, TKey> GetParentIdFunc { get; }
        private Func<T, bool> IsRootFunc { get; }

        public TreeBuilder(Func<T, TKey> getIdFunc, Func<T, TKey> getParentIdFunc, Func<T, bool> isRootFunc)
        {
            GetIdFunc = getIdFunc;
            GetParentIdFunc = getParentIdFunc;
            IsRootFunc = isRootFunc;
            NodePool = new Dictionary<TKey, TreeNode<T>>();
        }

        public IEnumerable<TreeNode<T>> Build(IEnumerable<object> flatList)
        {
            var rootNodes = new List<TreeNode<T>>();
            foreach (var item in flatList)
            {
                var castedItem = (T)item;
                var node = GetNode(castedItem);
                if (IsRootFunc.Invoke(node.Value))
                {
                    rootNodes.Add(node);
                }
                else
                {
                    var parentNode = GetParentNode(castedItem);
                    parentNode.Children.Add(node);
                }
            }
            return rootNodes;
        }

        private TreeNode<T> GetNode(T item)
        {
            TreeNode<T> node;
            var id = GetIdFunc.Invoke(item);
            if (NodePool.TryGetValue(id, out node))
            {
                node.Value = item;
            }
            else
            {
                node = new TreeNode<T>(item);
                NodePool.Add(id, node);
            }
            return node;
        }

        private TreeNode<T> GetParentNode(T item)
        {
            TreeNode<T> parentNode;
            var parentId = GetParentIdFunc.Invoke(item);
            if (NodePool.TryGetValue(parentId, out parentNode))
            {
                return parentNode;
            }

            parentNode = new TreeNode<T>();
            NodePool.Add(parentId, parentNode);
            return parentNode;
        }
    }
}
