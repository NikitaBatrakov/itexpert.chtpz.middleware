﻿    namespace ITExpert.CHPTZ.DataAccess.MQueries.Types
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    public class FilterElement
    {
        public string Field { get; set; }
        public string Value { get; set; }
        public string Operator { get; set; }
        public bool IsNegative { get; set; }
        public IEnumerable<FilterElement> Nodes { get; set; }

        public bool IsLogical => Nodes != null;

        public FilterElement()
        {
            
        }

        public FilterElement(string field, ComparisonSign comparison, string value)
        {
            Field = field;
            Value = value;
            Operator = comparison.ToString();
        }

        public FilterElement(LogicalSign logicalOperator, params FilterElement[] nodes)
        {
            Operator = logicalOperator.ToString();
            Nodes = nodes;
        }
    }
}
