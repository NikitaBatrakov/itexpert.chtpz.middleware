namespace ITExpert.CHPTZ.DataAccess.MQueries.Types
{
    public class MQuery
    {
        public string Fields { get; set; } = "";
        public string Filter { get; set; } = "";
        public FilterElement ComplexFilter { get; set; }
        public string Sort { get; set; } = "";
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}