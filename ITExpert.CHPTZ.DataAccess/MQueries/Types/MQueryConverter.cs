namespace ITExpert.CHPTZ.DataAccess.MQueries.Types
{
    using System;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Types;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.DataAccess.MQueries.Builders;

    internal class MQueryConverter
    {
        public static GetEntityArguments Convert(MQueryWithOptions query, Type type, IMResolver resolver,
                                                 string alias = null)
        {
            var args = Convert((MQuery)query, type, resolver, alias);
            args.WithAttachments = query.WithAttachments;
            args.WithAttributes = query.WithAttributes;
            args.WithTables = query.WithTables;
            args.TablesFieldsNames = query.TablesFieldsNames;
            args.TablesNames = query.TablesNames;
            return args;
        }

        public static GetEntityArguments Convert(MQuery query, Type type, IMResolver resolver, string alias = null)
        {
            alias = alias ?? type.GetName1C().GetLastPart('.');
            var builder = new MQueryParser(type, resolver, alias);

            var args = new GetEntityArguments {Limit = query.Limit, Offset = query.Offset};
            if (!string.IsNullOrEmpty(query.Fields))
            {
                var rawFields = builder.ResolveFields(query.Fields).ToArray();
                args.Fields = builder.ParseResolvedFields(rawFields);
            }
            args.Sort = builder.ParseSort(query.Sort);

            if (query.ComplexFilter != null)
            {
                var filterParser = new ObjectFilterParser(type, resolver, alias);
                args.Filter = filterParser.Parse(query.ComplexFilter);
                args.Parameters = filterParser.Parameters;
            }
            else
            {
                args.Filter = builder.ParseFilter(query.Filter);
                args.Parameters = builder.Parameters;
            }

            return args;
        }
    }
}