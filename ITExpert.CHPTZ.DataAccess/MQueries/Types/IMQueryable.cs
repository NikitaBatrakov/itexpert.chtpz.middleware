namespace ITExpert.CHPTZ.DataAccess.MQueries.Types
{
    public interface IMQueryable
    {
        string Fields { get; }
        string Filter { get; }
        string Sort { get; }
        int Limit { get; }
        int Offset { get; }
    }
}