namespace ITExpert.CHPTZ.DataAccess.MQueries.Types
{
    public struct FilterComponents
    {
        public string Field { get; set; }
        public string Value { get; set; }
        public string Comparison { get; set; }

        public string OpeningBrace { get; set; }
        public string ClosingBrace { get; set; }

        public string Appendix { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public FilterComponents(string field, string value, string comparison)
            : this()
        {
            Field = field;
            Value = value;
            Comparison = comparison;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public FilterComponents(string field, string value, string comparison, string openingBrace, string closingBrace, string appendix)
        {
            Field = field;
            Value = value;
            Comparison = comparison;
            OpeningBrace = openingBrace;
            ClosingBrace = closingBrace;
            Appendix = appendix;
        }

        #region Overrides of ValueType

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> containing a fully qualified type name.
        /// </returns>
        public override string ToString()
        {
            return $"{OpeningBrace}{Field} {Comparison} {Value}{ClosingBrace}{Appendix}";
        }

        #endregion
    }
}