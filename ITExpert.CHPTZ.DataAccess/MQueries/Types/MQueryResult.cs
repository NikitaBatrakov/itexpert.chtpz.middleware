﻿namespace ITExpert.CHPTZ.DataAccess.MQueries.Types
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class MQueryResult
    {
        public ServiceResult ServiceResult { get; private set; }
        public IEnumerable<object> Entities { get; private set; }
        public IDictionary<string, PropertyMetadata> Metadata { get; private set; }
        public int TotalResult { get; private set; }

        public MQueryResult(ServiceResult serviceResult, IEnumerable<object> entities)
        {
            ServiceResult = serviceResult;
            Entities = entities;
        }

        public MQueryResult(ServiceResult serviceResult, IEnumerable<object> entities, int totalResult)
        {
            ServiceResult = serviceResult;
            Entities = entities;
            TotalResult = totalResult;
        }

        public MQueryResult(ServiceResult serviceResult, IEnumerable<object> entities, IDictionary<string, PropertyMetadata> metadata, int totalResult)
        {
            ServiceResult = serviceResult;
            Entities = entities;
            Metadata = metadata;
            TotalResult = totalResult;
        }

        public static MQueryResult Create(EntitiesResponseDocument mDoc)
        {
            var serviceResult = mDoc.ServiceResult.Value.First();
            var entities = mDoc.Entities.Value;
            var metadata = mDoc.Metadata?.Value.FirstOrDefault();
            var totalResult = mDoc.TotalResult.Value.FirstOrDefault();

            return new MQueryResult(serviceResult, entities, metadata, totalResult);
        }
    }
}
