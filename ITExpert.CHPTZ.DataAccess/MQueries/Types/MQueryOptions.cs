﻿namespace ITExpert.CHPTZ.DataAccess.MQueries.Types
{
    using System;

    [Flags]
    public enum MQueryOptions
    {
        /// <summary>
        /// Specifies that nothing but entities with their primary properties shoud be returned
        /// </summary>
        None = 0,

        /// <summary>
        /// Specifies that entities should be returned with their metadata
        /// </summary>
        WithMetadata = 1,

        /// <summary>
        /// Specifies that entities properties with Table type should be returned
        /// </summary>
        WithTables = 2,

        /// <summary>
        /// Specifies that entities should be returned with link to their attachments
        /// </summary>
        WithAttachments = 4,

        /// <summary>
        /// Specifies that entities should be returned with their additional attributes
        /// </summary>
        WithAdditionalAttributes = 8,

        /// <summary>
        /// Specifies that everything shoud be returned
        /// </summary>
        All = 1 | 2 | 4 | 8
    }
}