namespace ITExpert.CHPTZ.DataAccess.MQueries.Types
{
    public class MQueryWithOptions : MQuery
    {
        public bool WithAttributes { get; set; }
        public bool WithAttachments { get; set; }
        public bool WithTables { get; set; }
        public bool WithMetadata { get; set; }
        public string TablesNames { get; set; } = "";
        public string TablesFieldsNames { get; set; } = "";
    }
}