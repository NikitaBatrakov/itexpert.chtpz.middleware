namespace ITExpert.CHPTZ.DataAccess.MQueries.Types
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class TreeNode<T>
    {
        [DataMember(Name = "value")]
        public T Value { get; set; }

        [DataMember(Name = "children")]
        public ICollection<TreeNode<T>> Children { get; }

        public TreeNode()
        {
            Children = new List<TreeNode<T>>();
        }

        public TreeNode(T value)
        {
            Value = value;
            Children = new List<TreeNode<T>>();
        }

        public TreeNode(T value, ICollection<TreeNode<T>> children)
        {
            Value = value;
            Children = children;
        }
    }
}