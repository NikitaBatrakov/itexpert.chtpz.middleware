namespace ITExpert.CHPTZ.BackendTypes.Fundamentals
{
    using System.ComponentModel;

    public enum ComparisonSign
    {
        [Description("�����")]
        Equal,

        [Description("�� �����")]
        NotEqual,

        [Description("������")]
        GreaterThan,

        [Description("������")]
        LessThan,

        [Description("������ ��� �����")]
        GreaterThanOrEqual,

        [Description("������ ��� �����")]
        LessThanOrEqual,

        [Description("�������")]
        Like,

        [Description("���������� �")]
        StartsWith,

        [Description("������������� ��")]
        EndsWith,

        [Description("��������")]
        Contains,

        [Description("�� ���������")]
        IsEmpty,

        [Description("������ �")]
        In
    }
}