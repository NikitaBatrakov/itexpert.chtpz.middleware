﻿namespace ITExpert.CHPTZ.BackendTypes.Fundamentals
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    public interface IEntity : IObject1C
    {
        Link1C Link { get; set; }
        bool? IsMarkedToDelete { get; set; }
        IEnumerable<AdditionalAttribute1C> Attributes { get; set; }
        ICollection<Attachment1C> Attachments { get; set; }
    }
}
