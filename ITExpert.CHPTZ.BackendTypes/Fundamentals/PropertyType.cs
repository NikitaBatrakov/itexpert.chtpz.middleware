namespace ITExpert.CHPTZ.BackendTypes.Fundamentals
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.Common.Extensions;

    [DataContract]
    public class PropertyType
    {
        #region Constants

        public const string Number = "Number";
        public const string String = "String";
        public const string Boolean = "Boolean";
        public const string DateTime = "Date";
        public const string Table = "DataTable";
        public const string Id = "Id";
        public const string Composite = "Composite";
        public const string Base64 = "ValueStorage";

        #endregion

        #region Properties

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "typePerformance")]
        public string TypePerformance { get; set; }

        [DataMember(Name = "types")]
        public IEnumerable<PropertyType> Types { get; set; }

        public bool IsComposite => Type == Composite;
        public bool IsTable => Type == Table;
        public bool IsPrimitive => IsPrimitiveType(Type);
        public bool IsLink => !IsComposite && !IsTable && !IsPrimitive;

        #endregion

        #region Ctors

        public PropertyType()
        {
            
        }

        public PropertyType(string type, IEnumerable<PropertyType> types = null)
        {
            Type = type;
            Types = types;
        }

        public PropertyType(string type, string typePerformance, IEnumerable<PropertyType> types = null)
        {
            Type = type;
            TypePerformance = typePerformance;
            Types = types;
        }

        public PropertyType(string name, string type, string typePerformance, IEnumerable<PropertyType> types = null)
        {
            Name = name;
            Type = type;
            TypePerformance = typePerformance;
            Types = types;
        }

        #endregion

        #region Public static Methods

        public static object ParsePrimitiveValue(string type, string value)
        {
            switch (type)
            {
                case Number:
                    return double.Parse(value);
                case DateTime:
                    return value.ParseToDate();
                case Boolean:
                    return bool.Parse(value);
                case Base64:
                case String:
                    return value;
                default:
                    throw new Exception($"Type '{type}' is not found");
            }
        }

        public static Type ParsePrimitiveType(string type)
        {
            switch (type)
            {
                case Number:
                    return typeof(double);
                case DateTime:
                    return typeof(DateTime);
                case Boolean:
                    return typeof(bool);
                case Base64:
                case String:
                    return typeof(string);
                default:
                    throw new Exception($"Type '{type}' is not found");
            }
        }

        public static bool IsPrimitiveType(string type1C)
        {
            return type1C == String
                || type1C == Number
                || type1C == DateTime
                || type1C == Boolean
                || type1C == Base64;
        }

        #endregion
    }
}