namespace ITExpert.CHPTZ.BackendTypes.Fundamentals
{
    internal enum Code1C
    {
        Ok = 0,
        IncorrectUser = 100,
        IncorrectPassword = 101,
        NotAuthorized = 102,
        ObjectLocked = 306
    }
}