﻿namespace ITExpert.CHPTZ.BackendTypes.Fundamentals
{
    using System;
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.Common.Extensions;

    [DataContract]
    public class ComparisonOperator1C
    {
        [DataMember(Name = "sign")]
        public ComparisonSign Sign { get; }

        [DataMember(Name = "internalSign")]
        public string InternalSign { get; }

        [DataMember(Name = "commonSign")]
        public string CommonSign { get; }

        [DataMember(Name = "verbose")]
        public string Verbose { get; }

        public ComparisonOperator1C(ComparisonSign comparisonSign1C)
        {
            Sign = comparisonSign1C;
            InternalSign = GetInternalSing(comparisonSign1C);
            CommonSign = GetCommonSign(comparisonSign1C);
            Verbose = comparisonSign1C.GetDescription();
        }

        public static ComparisonOperator1C ParseVerbose(string verboseSign)
        {
            switch (verboseSign.ToLowerInvariant())
            {
                case "равно":
                    return new ComparisonOperator1C(ComparisonSign.Equal);
                case "не равно":
                    return new ComparisonOperator1C(ComparisonSign.NotEqual);
                case "больше":
                    return new ComparisonOperator1C(ComparisonSign.GreaterThan);
                case "меньше":
                    return new ComparisonOperator1C(ComparisonSign.LessThan);
                case "больше или равно":
                    return new ComparisonOperator1C(ComparisonSign.GreaterThanOrEqual);
                case "меньше или равно":
                    return new ComparisonOperator1C(ComparisonSign.LessThanOrEqual);
                case "подобно":
                    return new ComparisonOperator1C(ComparisonSign.Like);
                default:
                    throw new NotSupportedException($"'{verboseSign}' comparison operator is not supported");
            }
        }

        public static ComparisonOperator1C ParseInternal(string internalSign)
        {
            switch (internalSign)
            {
                case "::":
                    return new ComparisonOperator1C(ComparisonSign.Equal);
                case "!:":
                    return new ComparisonOperator1C(ComparisonSign.NotEqual);
                case ">>":
                    return new ComparisonOperator1C(ComparisonSign.GreaterThan);
                case "<<":
                    return new ComparisonOperator1C(ComparisonSign.LessThan);
                case ">:":
                    return new ComparisonOperator1C(ComparisonSign.GreaterThanOrEqual);
                case "<:":
                    return new ComparisonOperator1C(ComparisonSign.LessThanOrEqual);
                case "*":
                    return new ComparisonOperator1C(ComparisonSign.Like);
                default:
                    throw new NotSupportedException($"'{internalSign}' comparison operator is not supported");
            }
        }

        public static string ConvertInternalToCommonSign(string sign)
        {
            var internalSign = ParseInternal(sign);
            return internalSign.CommonSign;
        }

        private static string GetCommonSign(ComparisonSign comparisonSign1C)
        {
            switch (comparisonSign1C)
            {
                case ComparisonSign.IsEmpty:
                case ComparisonSign.Equal:
                    return "=";
                case ComparisonSign.NotEqual:
                    return "<>";
                case ComparisonSign.GreaterThan:
                    return ">";
                case ComparisonSign.LessThan:
                    return "<";
                case ComparisonSign.GreaterThanOrEqual:
                    return ">=";
                case ComparisonSign.LessThanOrEqual:
                    return "<=";
                case ComparisonSign.StartsWith:
                case ComparisonSign.EndsWith:
                case ComparisonSign.Contains:
                case ComparisonSign.Like:
                    return "ПОДОБНО";
                case ComparisonSign.In:
                    return "В";
                default:
                    throw new ArgumentOutOfRangeException(nameof(comparisonSign1C), comparisonSign1C, null);
            }
        }

        private static string GetInternalSing(ComparisonSign comparisonSign1C)
        {
            switch (comparisonSign1C)
            {
                case ComparisonSign.IsEmpty:
                case ComparisonSign.Equal:
                    return "::";
                case ComparisonSign.NotEqual:
                    return "!:";
                case ComparisonSign.GreaterThan:
                    return ">>";
                case ComparisonSign.LessThan:
                    return "<<";
                case ComparisonSign.GreaterThanOrEqual:
                    return ">:";
                case ComparisonSign.LessThanOrEqual:
                    return "<:";
                case ComparisonSign.StartsWith:
                case ComparisonSign.EndsWith:
                case ComparisonSign.Contains:
                case ComparisonSign.Like:
                    return "*";
                case ComparisonSign.In:
                    return "In";
                default:
                    throw new ArgumentOutOfRangeException(nameof(comparisonSign1C), comparisonSign1C, null);
            }
        }
    }
}