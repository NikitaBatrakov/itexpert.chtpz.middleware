﻿namespace ITExpert.CHPTZ.BackendTypes.Fundamentals
{
    public enum Type1CInfo
    {
        String, Number, Boolean, DataTime, Link, Composite, Table
    }
}
