﻿namespace ITExpert.CHPTZ.BackendTypes.Fundamentals
{
    public enum LogicalSign
    {
        And, Or
    }
}
