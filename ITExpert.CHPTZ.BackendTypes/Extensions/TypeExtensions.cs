﻿namespace ITExpert.CHPTZ.BackendTypes.Extensions
{
    using System;
    using System.Collections;
    using System.Reflection;

    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    public static class TypeExtensions
    {
        public static string GetName1C(this Type type)
        {
            var name1C = GetPrimitiveTypeName1C(type);
            if (name1C != null)
            {
                return name1C;
            }

            if (typeof(IEntity).IsAssignableFrom(type))
            {
                return type.GetCustomAttribute<Entity1CAttribute>()?.Name;
            }

            if (typeof(IRegister).IsAssignableFrom(type))
            {
                return type.GetCustomAttribute<Register1CAttribute>()?.Name;
            }

            if (typeof(IEnumerable).IsAssignableFrom(type))
            {
                return GetArrayName1C(type);
            }

            throw new NotImplementedException($"Type '{type.Name}' has no name in 1C");
        }

        private static string GetArrayName1C(Type type)
        {
            if (!typeof(IEnumerable).IsAssignableFrom(type))
            {
                return null;
            }
            var types = type.GetGenericArguments();
            var elementType = types.Length != 0 ? types[0] : type.GetElementType();
            return $"{elementType.GetName1C()}[]";
        }

        private static string GetPrimitiveTypeName1C(Type type)
        {
            switch (type.Name)
            {
                case "String":
                    return "String";
                case "Int32":
                case "Double":
                case "Float":
                    return "Number";
                case "Boolean":
                    return "Boolean";
                case "DateTime":
                    return "Date";
                default:
                    return null;
            }
        }

        public static PropertyInfo GetPropertyIgnoreCase(this Type type, string propertyName)
        {
            return type.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.IgnoreCase);
        }
    }
}
