﻿namespace ITExpert.CHPTZ.BackendTypes.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ResolvementException : Exception
    {
        public ResolvementException()
        {
        }

        public ResolvementException(string message)
            : base(message)
        {
        }

        public ResolvementException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected ResolvementException(SerializationInfo info,
                              StreamingContext context)
            : base(info, context)
        {
        }
    }
}
