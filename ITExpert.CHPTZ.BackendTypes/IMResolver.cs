﻿namespace ITExpert.CHPTZ.BackendTypes
{
    using System;

    public interface IMResolver
    {
        /// <summary>
        /// Returns System.Type by 1C type name
        /// </summary>
        /// <param name="name1C">1C Type name</param>
        /// <param name="throwOnError">False if all exceptions should be suppressed</param>
        /// <returns>Matching Type</returns>
        Type GetInternalType(string name1C, bool throwOnError = true);

        /// <summary>
        /// Returns 1C type name matching specified Type
        /// </summary>
        /// <param name="type">Type</param>
        /// <param name="throwOnError">False if all exceptions should be suppressed</param>
        /// <returns>1C type name</returns>
        string GetExternalTypeName(Type type, bool throwOnError = true);

        /// <summary>
        /// Returns 1C type name matching specified Type name 
        /// </summary>
        /// <param name="className">Type name</param>
        /// <param name="throwOnError">False if all exceptions should be suppressed</param>
        /// <returns>1C type name</returns>
        string GetExternalTypeName(string className, bool throwOnError = true);

        /// <summary>
        /// Returns Type with specified name
        /// </summary>
        /// <param name="name">Type name</param>
        /// <param name="throwOnError">False if all exceptions should be suppressed</param>
        /// <returns>Type</returns>
        Type GetClassType(string name, bool throwOnError = true);
    }
}
