﻿namespace ITExpert.CHPTZ.BackendTypes.PropertyTypes
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class Link1C : IEquatable<Link1C>
	{
        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "linkPerformance")]
        public string LinkPerformance { get; set; }

        public Link1C()
        {
            
        }

        public Link1C(string value, string type = "Id")
        {
            Value = value;
            LinkPerformance = "";
            Type = type;
        }

        public Link1C(string value, string linkPerformance, string type)
        {
            Value = value;
            LinkPerformance = linkPerformance;
            Type = type;
        }

        public static Link1C Empty => new Link1C(new Guid().ToString(), "Undefined");

        public static Link1C Undefined = new Link1C("Undefined", "Undefined");

        public static Link1C Create<T>(string guid)
        {
            return new Link1C(guid, null, typeof(T).Name);
        }

        public bool Equals(Link1C link)
	    {
            return link.Value == Value && link.Type == Type;
        }

        #region Overrides of Object

        public override string ToString()
        {
            return Value;
        }

        #endregion
	}
}