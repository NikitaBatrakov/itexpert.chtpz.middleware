﻿namespace ITExpert.CHPTZ.BackendTypes.PropertyTypes
{
    using System.Runtime.Serialization;

    [DataContract]
    public class Composite1C
    {
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "value")]
        public object Value { get; set; }

        [DataMember(Name = "linkPerformance")]
        public string LinkPerformance { get; set; }

        public Composite1C()
        {
            
        }

        public Composite1C(object value, string type, string linkPerformance = null)
        {
            Type = type;
            Value = value;
            LinkPerformance = linkPerformance;
        }

        public static Composite1C CreateEmpty()
        {
            return new Composite1C("Undefined", "Undefined");
        }
    }
}
