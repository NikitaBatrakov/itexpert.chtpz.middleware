﻿namespace ITExpert.CHPTZ.BackendTypes.PropertyTypes
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class Attachment1C
    {
        [DataMember(Name = "size")]
        public int Size { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "date")]
        public DateTime Date { get; set; }

        [DataMember(Name = "service")]
        public int Service { get; set; }

        [DataMember(Name = "guid")]
        public string Guid { get; set; }

        [DataMember(Name = "src")]
        public string Src { get; set; }

        [DataMember(Name = "employeeName")]
        public string EmployeeName { get; set; }

        [DataMember(Name = "employeeGuid")]
        public string EmployeeGuid { get; set; }

        [DataMember(Name = "isMarkedToDelete")]
        public bool IsMarkedToDelete { get; set; }

        [DataMember(Name = "isNotRelevant")]
        public bool IsNotRelevant { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }
    }
}
