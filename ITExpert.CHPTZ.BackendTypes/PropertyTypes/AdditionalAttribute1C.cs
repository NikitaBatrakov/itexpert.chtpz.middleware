﻿namespace ITExpert.CHPTZ.BackendTypes.PropertyTypes
{
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    [DataContract]
    public class AdditionalAttribute1C
    {
        [DataMember(Name = "guid")]
        public string Guid { get; set; }

        [DataMember(Name = "type")]
        public PropertyType Type { get; set; }

        [DataMember(Name = "verboseName")]
        public string VerboseName { get; set; }

        [DataMember(Name = "value")]
        public object Value { get; set; }

        public AdditionalAttribute1C()
        {
            
        }

        public AdditionalAttribute1C(PropertyType type, string guid, string verboseName, object value)
        {
            Guid = guid;
            Type = type;
            VerboseName = verboseName;
            Value = value;
        }
    }
}
