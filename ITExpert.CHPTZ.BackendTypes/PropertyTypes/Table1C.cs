﻿namespace ITExpert.CHPTZ.BackendTypes.PropertyTypes
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class Table1C
    {
        [DataMember(Name = "rows")]
        public IEnumerable<IEnumerable<object>> Rows { get; set; }

        public Table1C()
        {
            Rows = new List<IEnumerable<object>>();
        }

        public Table1C(IEnumerable<IEnumerable<object>> rows)
        {
            Rows = rows;
        }
    }
}
