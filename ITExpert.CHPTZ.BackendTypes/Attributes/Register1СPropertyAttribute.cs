﻿namespace ITExpert.CHPTZ.BackendTypes.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Property)]
    public class Register1СPropertyAttribute : Attribute
    {
        public Register1CPropertyKind Kind { get; }

        public Register1СPropertyAttribute(Register1CPropertyKind kind)
        {
            Kind = kind;
        }
    }
}
