namespace ITExpert.CHPTZ.BackendTypes.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Enum)]
    public class Enum1CAttribute : Attribute
    {
        public string Name { get; }

        public string Verbose { get; }

        public Enum1CAttribute(string name)
        {
            Name = name;
        }

        public Enum1CAttribute(string name, string verbose)
        {
            Name = name;
            Verbose = verbose;
        }
    }
}