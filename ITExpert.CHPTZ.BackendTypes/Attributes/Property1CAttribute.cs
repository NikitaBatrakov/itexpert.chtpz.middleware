namespace ITExpert.CHPTZ.BackendTypes.Attributes
{
    using System;

    using ITExpert.CHPTZ.Common.Extensions;

    [AttributeUsage(AttributeTargets.Property)]
    public class Property1CAttribute : Attribute
    {
        public string Name { get; }
        public string Verbose { get; }

        public Property1CAttribute(string name)
        {
            Name = name;
            Verbose = name.GetLastPart('.');
        }

        public Property1CAttribute(string name, string verbose)
        {
            Name = name;
            if (string.IsNullOrEmpty(verbose)) verbose = name.GetLastPart('.');
            Verbose = verbose;
        }
    }
}