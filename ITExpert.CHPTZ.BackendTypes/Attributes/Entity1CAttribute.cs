namespace ITExpert.CHPTZ.BackendTypes.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public class Entity1CAttribute : Attribute
    {
        public string Name { get; }
        public string Verbose { get; }

        public Entity1CAttribute(string name)
        {
            Name = name;
            Verbose = name;
        }

        public Entity1CAttribute(string name, string verbose)
        {
            Name = name;
            Verbose = verbose;
        }
    }
}