namespace ITExpert.CHPTZ.BackendTypes.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public class Register1CAttribute : Attribute
    {
        public string Name { get; }
        public string Verbose { get; }

        public Register1CAttribute(string name)
        {
            Name = name;
        }

        public Register1CAttribute(string name, string verbose)
        {
            Name = name;
            Verbose = verbose;
        }
    }
}