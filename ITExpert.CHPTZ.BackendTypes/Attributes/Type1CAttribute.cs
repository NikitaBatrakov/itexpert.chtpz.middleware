namespace ITExpert.CHPTZ.BackendTypes.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class Type1CAttribute : Attribute
    {
        public string Type { get; }

        public string TypePerformance { get; }

        public string[] Types { get; }

        public string[] TypesPerfomances { get; set; }

        public string Name { get; set; }

        //public bool IsComposite => Types != null && Types.Length > 0;
        public bool IsComposite { get; }

        public Type1CAttribute(string type, string typePerformance)
        {
            Type = type;
            TypePerformance = typePerformance;
        }

        public Type1CAttribute(string name, string type, string typePerformance) : this(type, typePerformance)
        {
            Name = name;
        }

        public Type1CAttribute(string[] types, string[] typesPerformances)
        {
            if (types.Length != typesPerformances.Length)
                throw new Exception("Types and TypesPerformances should be of the same length");
            Types = types;
            TypesPerfomances = typesPerformances;
            IsComposite = true;
        }

        public Type1CAttribute(string name, string[] types, string[] typesPerformances) : this(types, typesPerformances)
        {
            Name = name;
        }
    }
}