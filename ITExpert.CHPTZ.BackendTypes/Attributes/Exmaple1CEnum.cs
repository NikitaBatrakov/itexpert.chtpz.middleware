namespace ITExpert.CHPTZ.BackendTypes.Attributes
{
    using System.ComponentModel;

    [Enum1C("EnumName")]
    public enum Exmaple1CEnum
    {
        [Description("��")]
        Before,

        [Description("��")]
        InTheMiddle,

        [Description("��")]
        After,

        [Description("��")]
        LongAfter
    }
}