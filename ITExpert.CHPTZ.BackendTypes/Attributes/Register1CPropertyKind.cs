namespace ITExpert.CHPTZ.BackendTypes.Attributes
{
    public enum Register1CPropertyKind
    {
        Key,
        Value,
        Info
    }
}