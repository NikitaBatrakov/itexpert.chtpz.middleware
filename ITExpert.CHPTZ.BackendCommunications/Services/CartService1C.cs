namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    public class CartService1C : BaseService1C
    {
        public CartService1C(IServiceConnection1C connection)
            : base(connection) { }

        public string GetServiceCatalogue(string guid) => Invoke("ServicesCatalog", guid).ToString();

        public string GetServiceTemplates(string guid) => Invoke("GetTemplates", guid).ToString();

        public string GetCartElement(string entity, string guid) => Invoke("GetOrderList", entity, guid).ToString();

        public string CreateCart(string cartElementsXml, string templateAttributesXml)
            => Invoke("CreateCart", cartElementsXml, templateAttributesXml).ToString();

        public string UpdateCart(string serviceRequestXml, string cartElementsXml)
            => Invoke("UpdateEntity", serviceRequestXml, cartElementsXml).ToString();
    }
}