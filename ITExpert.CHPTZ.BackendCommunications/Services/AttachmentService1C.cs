namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    public class AttachmentService1C : BaseService1C
    {
        public AttachmentService1C(IServiceConnection1C connection)
            : base(connection) { }

        public string GetAttachment(string fileLink, bool returnFile) => Invoke("GetAttachedFiles", fileLink, returnFile).ToString();

        public string AddAttachment(string file) => Invoke("GiveAttachedFiles", file).ToString();
    }
}