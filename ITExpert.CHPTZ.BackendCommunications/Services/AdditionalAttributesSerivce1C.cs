namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services.Interfaces;

    public class AdditionalAttributesSerivce1C : BaseService1C
    {
        public AdditionalAttributesSerivce1C(IServiceConnection1C connection)
            : base(connection) { }

        public string GetAdditionalAttributes(string guid) => Invoke("ReturnAdditionalDetails", guid).ToString();

        public string GetAdditionalAttributesValues(string entity, string guid)
            => Invoke("ReturnAdditionalDetailsValues", entity, guid).ToString();
    }
}