﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    public class MonthPlanService1C : BaseService1C
    {
        public MonthPlanService1C(IServiceConnection1C connection)
            : base(connection) { }

        #region Implementation of IMonthPlanService

        public string GetMonthPlanLevel(string businessProcessGuid = "", string directionGuid = "")
        {
            return Invoke("GetDevGroups", businessProcessGuid, directionGuid).ToString();
        }

        public string GetUnallocatedChangeRequests(string teamGuid, string directionGuid, string monthPlanGuid)
        {
            return Invoke("GetUnallocatedChngRequest", teamGuid, directionGuid, monthPlanGuid).ToString();
        }

        public string GetIterations(string teamGuid, string monthPlanGuid)
        {
            return Invoke("GetIterations", teamGuid, monthPlanGuid).ToString();
        }

        public string GetAvailableTaskTypes()
        {
            return Invoke("GetAvailableTaskTypes").ToString();
        }

        public int CheckIterationEliminate(string iterationGuid)
        {
            return (int)Invoke("CheckIterationEliminate", iterationGuid);
        }

        public string CheckMonthPlan(int year, int month)
        {
            return (string)Invoke("CheckMonthPlan", year, month);
        }

        public string GetMonthPlanActions(string teamGuid, string monthPlanGuid)
        {
            return (string)Invoke("GetMonthPlanActions", monthPlanGuid, teamGuid);
        }

        public int GetDefaultLaborCosts(string monthPlanGuid, string changeRequestGuid)
        {
            return (int)Invoke("GetDefaultLabourCosts", monthPlanGuid, changeRequestGuid);
        }

        public int GetTeamResource(string teamGuid, string monthPlanGuid)
        {
            return (int)Invoke("GetTeamResource", monthPlanGuid, teamGuid);
        }

        public string GetMonthPlanVersions(int year, int month)
        {
            return (string)Invoke("GetMonthPlanVersions", year, month);
        }

        #endregion
    }
}
