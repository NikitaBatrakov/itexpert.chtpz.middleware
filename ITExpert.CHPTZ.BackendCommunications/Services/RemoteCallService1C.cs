namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    public class RemoteCallService1C : BaseService1C
    {
        public RemoteCallService1C(IServiceConnection1C connection)
            : base(connection) { }

        public string InvokeProcedure(string xml) => Invoke("PerformProcedure", xml).ToString();

        public string InvokeFunction(string xml) => Invoke("PerformFunction", xml).ToString();
    }
}