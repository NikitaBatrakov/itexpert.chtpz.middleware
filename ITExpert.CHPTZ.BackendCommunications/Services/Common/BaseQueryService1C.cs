namespace ITExpert.CHPTZ.BackendCommunications.Services.Common
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public abstract class BaseQueryService1C : BaseService1C
    {
        protected const string Null = "NULL";

        protected BaseQueryService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        protected static string FormatQuery(string query, GetEntityArguments arguments)
        {
            var fields = string.IsNullOrEmpty(arguments.Fields) ? "" : $",{arguments.Fields}";
            var filter = string.IsNullOrEmpty(arguments.Filter) ? "" : $"��� {arguments.Filter}";
            return string.Format(query, fields, filter);
        }

        protected string ExecuteQuery(string query, params QueryParameter[] parameters)
        {
            return ExecuteQuery(query, new GetEntityArguments(), parameters);
        }

        protected string ExecuteQuery(string query, GetEntityArguments arguments, params QueryParameter[] parameters)
        {
            int limit = arguments.Limit, offset = arguments.Offset;
            AdjustLimitOffset(ref limit, ref offset);
            var parametersString = GetParameters(arguments.Parameters, parameters);
            return
                Invoke("ExecuteArbitraryQuery", query, parametersString, arguments.Offset, arguments.Limit, arguments.Sort).
                    ToString();
        }

        private static string GetParameters(string filterParameters, IEnumerable<QueryParameter> queryParameters)
        {
            var parametersString = string.Join(";", queryParameters);
            if (string.IsNullOrEmpty(parametersString))
            {
                return filterParameters;
            }
            return string.IsNullOrEmpty(filterParameters) ? parametersString : $"{filterParameters};{parametersString}";
        }
    }
}