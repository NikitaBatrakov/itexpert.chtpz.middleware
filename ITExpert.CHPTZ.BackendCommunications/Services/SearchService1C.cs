namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public class SearchService1C : BaseService1C
    {
        public SearchService1C(IServiceConnection1C connection)
            : base(connection) { }

        public string GetSearchParameters(string entity, string type = "")
        {
            return Invoke("GetSerch", entity, type).ToString();
        } 

        public string SearchTypes(string query)
        {
            return Invoke("GetObjectsFullTextSearching", query).ToString();
        }

        public string Search(string query, string entity, GetEntityArguments args)
        {
            int limit = args.Limit, offset = args.Offset;
            AdjustLimitOffset(ref limit, ref offset);
            return
                Invoke("GetResultFullTextSearching", args.Fields, entity, limit, offset, args.Parameters, query,
                       args.Filter).ToString();
        }
    }
}