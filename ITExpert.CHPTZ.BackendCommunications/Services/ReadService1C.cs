﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using System;

    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services.Interfaces;
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public class ReadService1C : BaseService1C, IReadService1C
    {
        public ReadService1C(IServiceConnection1C connection)
            : base(connection) { }

        public string GetEntities(string entityName1C, GetEntityArguments args)
        {
            int limit = args.Limit, offset = args.Offset;
            AdjustLimitOffset(ref limit, ref offset);
            return
                Invoke("GetEntity", entityName1C, args.Fields, args.Filter, args.Parameters, args.WithAttributes,
                       args.WithTables, args.TablesNames, args.TablesFieldsNames, offset, limit, args.WithAttachments,
                       args.Sort).ToString();
        }

        public string GetEntitiesTree(string entityName1C, GetEntityArguments args)
        {
            int limit = args.Limit, offset = args.Offset;
            AdjustLimitOffset(ref limit, ref offset);
            return
                Invoke("GetTree", entityName1C, args.Fields, args.Filter, args.Parameters, args.WithAttributes, args.WithTables,
                       args.TablesNames, args.TablesFieldsNames, offset, limit, args.WithAttachments, args.Sort).
                    ToString();
        }
    }
}