﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services.Common;
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public class RelationService1C : BaseQueryService1C
    {
        public RelationService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        public string GetRelations(string entity, string guid, GetEntityArguments arguments)
        {
            var query = FormatQuery(Query, arguments);
            return ExecuteQuery(query, arguments, new QueryParameter(guid, entity));
        }

        #region Query

        private static readonly string Query =
$@"ВЫБРАТЬ 
	ВЫБОР КОГДА relations.ГлавныйОбъект = &Value1 
 		ТОГДА relations.ПодчиненныйОбъект
 		ИНАЧЕ relations.ГлавныйОбъект
 	КОНЕЦ КАК RelatedObject,
 	ВЫБОР КОГДА relations.ГлавныйОбъект = &Value1 
 		ТОГДА type.НазваниеПрямойСвязи
 		ИНАЧЕ type.НазваниеОбратнойСвязи
 	КОНЕЦ КАК LinkType,
 	ВЫБОР КОГДА relations.ГлавныйОбъект = &Value1 
 		ТОГДА True
 		ИНАЧЕ False
 	КОНЕЦ КАК IsMaster,
    relations.Ссылка КАК RelationLink
ПОМЕСТИТЬ query
ИЗ Справочник.ITEXP_СвязиОбъектов КАК relations
	ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ITEXP_ТипСвязи КАК type
		ПО (type.Ссылка = relations.ТипСвязи)
ГДЕ (relations.ГлавныйОбъект = &Value1 
	ИЛИ relations.ПодчиненныйОбъект = &Value1)
    И НЕ relations.ПометкаУдаления;

ВЫБРАТЬ
    entity.Номер КАК Number,
    entity.ShortDescription КАК Subject,
    entity.Status КАК Status,
    entity.Status КАК IncidentStatus, 
    {Null} КАК ServiceRequestStatus, 
    {Null} КАК ChangeRequestStatus, 
    {Null} КАК TaskStatus,
    entity.Initiator КАК Creator,
    entity.Employee КАК Responsible,
    entity.FunctionalGroup КАК FunctionalGroup,
    entity.Дата КАК RegistrationDate,
    entity.SolutionDate КАК Deadline,
    entity.Ссылка КАК Link,
    entity.Тип КАК AdditionalAttributeType,
    ""Инцидент"" КАК Entity,
 query.LinkType,
 query.IsMaster,
 query.RelationLink
ПОМЕСТИТЬ query2
ИЗ query КАК query
  ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.Incident КАК entity
  	ПО (query.RelatedObject = entity.Ссылка)
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Номер,
    entity.ShortDescription,
    entity.Status,
    {Null},
    entity.Status,
    {Null},
    {Null},
    entity.Initiator,
    entity.Employee,
    entity.FunctionalGroup,
    entity.Дата,
    entity.SolutionDate,
    entity.Ссылка,
    entity.Тип,
    ""Запрос на обслуживание"",
 query.LinkType,
 query.IsMaster,
 query.RelationLink
ИЗ query КАК query
  ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ServicRequest КАК entity
  	ПО (query.RelatedObject = entity.Ссылка)
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Номер,
    entity.Тема,
    entity.Статус,
    {Null},
    {Null},
    entity.Статус,
    {Null},
    entity.Инициатор,
    entity.Ответственный,
    NULL,
    entity.Дата,
    NULL,
    entity.Ссылка,
    entity.Тип,
    ""Запрос на изменение"",
 query.LinkType,
 query.IsMaster,
 query.RelationLink
ИЗ query КАК query
  ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ChangeRequest КАК entity
  	ПО (query.RelatedObject = entity.Ссылка)
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Номер,
    entity.Theme,
    entity.Status,
    {Null},
    {Null},
    {Null},
    entity.Status,
    entity.Initiator,
    entity.Employee,
    entity.FunctionalGroup,
    entity.Дата,
    entity.Deadline,
    entity.Ссылка,
    entity.Тип,
    ""Задача"",
 query.LinkType,
 query.IsMaster,
 query.RelationLink
ИЗ query КАК query
  ВНУТРЕННЕЕ СОЕДИНЕНИЕ Задача.Task КАК entity
  	ПО (query.RelatedObject = entity.Ссылка)
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Код,
    entity.Описание,
    entity.Статус,
    {Null},
    {Null},
    {Null},
    {Null},
    entity.Инициатор,
    entity.Ответственный,
    {Null},
    entity.ПлановаяДатаНачала,
    entity.ПлановаяДатаОкончания,
    entity.Ссылка,
    {Null},
    ""Проект"",
 query.LinkType,
 query.IsMaster,
 query.RelationLink
ИЗ query КАК query
  ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ITEXP_Проекты КАК entity
  	ПО (query.RelatedObject = entity.Ссылка)
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Код,
    entity.Наименование,
    entity.Статус,
    {Null},
    {Null},
    {Null},
    {Null},
    {Null},
    {Null},
    entity.ГруппаАдминистрирования,
    {Null},
    {Null},
    entity.Ссылка,
    entity.Тип,
    ""Конфигурационная единица"",
 query.LinkType,
 query.IsMaster,
 query.RelationLink
ИЗ query КАК query
  ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ITEXP_КЕ КАК entity
  	ПО (query.RelatedObject = entity.Ссылка)
ГДЕ НЕ entity.ПометкаУдаления;

ВЫБРАТЬ RelationLink, Link{{0}}
ИЗ query2 КАК query2
{{1}}";
        #endregion        
    }
}
