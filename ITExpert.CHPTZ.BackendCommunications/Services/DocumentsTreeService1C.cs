﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public class DocumentsTreeService1C : BaseService1C
    {
        public DocumentsTreeService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        public string GetDocumentsTree(IEnumerable<string> guids)
        {
            const string entityName = "Справочник.ITEXP_Документ";
            const string fields = "Родитель";

            var guidsArray = guids.ToArray();
            var filter = GetFilter("ТипДокумента", guidsArray);
            var parameters = GetParameters(guidsArray);
            return Invoke("GetTree", entityName, fields, filter, parameters, false, false, "", "", 0, 0, false, "").ToString();
        }

        private static string GetFilter(string field, string[] guids)
        {
            var arrayValue = GetFilterArrayValue(guids.Length);
            return $"{field} В ({arrayValue})";
        }

        private static string GetFilterArrayValue(int guidsCount)
        {
            var filter = "";
            for (var i = 1; i < guidsCount + 1; i++)
            {
                filter = $"{filter},&Value{i}";
            }
            return filter.Remove(0, 1);
        }

        private static string GetParameters(string[] guids)
        {
            var parameters = "";
            foreach (var guid in guids)
            {
                parameters = $"{parameters};{guid},Справочник.ITEXP_ТипДокумента";
            }
            return parameters.Remove(0, 1);
        }
    }
}
