namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services.Interfaces;

    public class WriteService1C : BaseService1C, IWriteService1C
    {
        public WriteService1C(IServiceConnection1C connection)
            : base(connection) { }

        public string CreateEntity(string entities) => Invoke("CreateEntity", entities, "", "").ToString();

        public string UpdateEntity(string entities) => Invoke("UpdateEntity", entities, "").ToString();

        public string CreateOnBase(string entity, string baseLink, string procedure, string procedureParams)
            => Invoke("CreateOnBase", entity, baseLink, procedure, procedureParams).ToString();
    }
}