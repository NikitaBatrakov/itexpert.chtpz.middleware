namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    internal class CommonService1C : BaseService1C
    {
        public CommonService1C(IServiceConnection1C connection)
            : base(connection) { }

        public string Authenticate(string login, string password) => Invoke("Authorization", login, password).ToString();

        public string GetMetadata() => Invoke("GetMetadata").ToString();
    }
}