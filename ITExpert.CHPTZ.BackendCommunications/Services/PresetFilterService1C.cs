﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services.Common;
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public class PresetFilterService1C : BaseQueryService1C
    {
        public PresetFilterService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        private static readonly string EmployeeEntity = "Справочник.Employee";

        public string GetEmployeesFunctionalGroups(string employeeGuid)
        {
            return ExecuteQuery(EmployeesFunctionGroupsQuery, new QueryParameter(employeeGuid, EmployeeEntity));
        }

        public string GetEmployeesCurrentChangeRequests(string employeeGuid, GetEntityArguments arguments)
        {
            var query = FormatQuery(EmployeesCurrentChangeReqeustsQuery, arguments);
            return ExecuteQuery(query, arguments, new QueryParameter(employeeGuid, EmployeeEntity));
        }

        public string GetEmployeeGroupsCurrentChangeRequests(string employeeGuid, GetEntityArguments arguments)
        {
            var query = FormatQuery(EmployeeGroupsCurrentChangeRequestsQuery, arguments);
            return ExecuteQuery(query, arguments, new QueryParameter(employeeGuid, EmployeeEntity));
        }

        #region EmployeesFunctionGroupsQuery

        private static readonly string EmployeesFunctionGroupsQuery =
@"ВЫБРАТЬ Ссылка
ИЗ Справочник.FunctionalGroups.Employees
ГДЕ Employee = &Value1";

        #endregion

        #region EmployeeGroupsCurrentChangeRequestQuery

        private static readonly string EmployeeGroupsCurrentChangeRequestsQuery =
@"ВЫБРАТЬ FunctionalGroupsEmployees.Ссылка
ПОМЕСТИТЬ ВТ_ФГ
ИЗ Справочник.FunctionalGroups.Employees КАК FunctionalGroupsEmployees
ГДЕ FunctionalGroupsEmployees.Employee = &Value1
СГРУППИРОВАТЬ ПО FunctionalGroupsEmployees.Ссылка;

ВЫБРАТЬ ChangeRequest.Ссылка{0}
ИЗ ВТ_ФГ КАК ВТ_ФГ
    ЛЕВОЕ СОЕДИНЕНИЕ Задача.Task КАК Task
        ЛЕВОЕ СОЕДИНЕНИЕ Документ.ChangeRequest КАК ChangeRequest
			ПО Task.ОснованиеБП = ChangeRequest.Ссылка
		ПО ВТ_ФГ.Ссылка = Task.FunctionalGroup
ГДЕ
	НЕ Task.ПометкаУдаления
    И НЕ Task.ОснованиеБП.ПометкаУдаления
    И ТИПЗНАЧЕНИЯ(Task.ОснованиеБП) = ТИП(Документ.ChangeRequest)
	И Task.БизнесПроцесс <> ЗНАЧЕНИЕ(БизнесПроцесс.ВыполнениеАлгоритма.ПустаяСсылка)	
	И (Task.Status = ЗНАЧЕНИЕ(Справочник.TaskStatus.НазначеноНаГруппу)
        ИЛИ Task.Status = ЗНАЧЕНИЕ(Справочник.TaskStatus.ВРаботе))";

        #endregion

        #region EmployeesCurrentChangeReqeustsQuery

        private static readonly string EmployeesCurrentChangeReqeustsQuery =
            @"ВЫБРАТЬ ChangeRequest.Ссылка{0}
ИЗ Задача.Task КАК Task
    ЛЕВОЕ СОЕДИНЕНИЕ Документ.ChangeRequest КАК ChangeRequest
        ПО Task.ОснованиеБП = ChangeRequest.Ссылка
ГДЕ
	НЕ Task.ПометкаУдаления
    И НЕ Task.ОснованиеБП.ПометкаУдаления
    И ТИПЗНАЧЕНИЯ(Task.ОснованиеБП) = ТИП(Документ.ChangeRequest)
	И Task.БизнесПроцесс <> ЗНАЧЕНИЕ(БизнесПроцесс.ВыполнениеАлгоритма.ПустаяСсылка)
	И Task.Status = ЗНАЧЕНИЕ(Справочник.TaskStatus.ВРаботе)
	И Task.Employee = &Value1";

        #endregion
    }
}
