﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services.Common;
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public class OperatorService1C : BaseQueryService1C
    {
        public OperatorService1C(IServiceConnection1C connection)
            : base(connection) { } 

        #region Implementation of IOperatorService

        public string RegisterPhoneCall(string phoneNumber, string recordLink, string userGuid)
        {
            return Invoke("Operator_RegisterPhoneCall", phoneNumber, recordLink, userGuid).ToString();
        }

        public string CreateRequest(string entity, string initiator, string userGuid, string info, string phoneCallGuid)
        {
            return Invoke("Operator_CreateRequest", entity, initiator, userGuid, info, phoneCallGuid).ToString();
        }

        public string GetStatus(string entity, string entityGuid, string phoneCallGuid)
        {
            return Invoke("Operator_GetStatus", entity, entityGuid, phoneCallGuid).ToString();
        }

        #endregion

        public string GetEmployeesRequests(string employeeGuid, GetEntityArguments args)
        {
            var query = FormatQuery(EmployeesRequestsQuery, args);
            return ExecuteQuery(query, args, new QueryParameter(employeeGuid, "Справочник.Employee"));
        }

        #region EmployeesRequestsQuery

        private static readonly string EmployeesRequestsQuery =
            @"ВЫБРАТЬ
    Incident.Ссылка КАК Link,
    Incident.Info КАК Description,
    Incident.Status.Наименование КАК Status,
    Incident.Status.ЦветовойКод КАК StatusColorCode,
    Incident.Дата КАК Date,
    Incident.Тип.Наименование КАК Type,
    ""Инцидент"" КАК Entity
ПОМЕСТИТЬ query
ИЗ Документ.Incident КАК Incident
ГДЕ НЕ Incident.ПометкаУдаления
    И Incident.User = &Value1
    И Incident.Status<> ЗНАЧЕНИЕ(Справочник.IncidentStatus.Закрыто)

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    ServicRequest.Ссылка,
    ServicRequest.Info,
    ServicRequest.Status.Наименование,
    ServicRequest.Status.ЦветовойКод,
    ServicRequest.Дата,
    ServicRequest.Тип.Наименование,
    ""Запрос на обслуживание""
ИЗ Документ.ServicRequest КАК ServicRequest
ГДЕ НЕ ServicRequest.ПометкаУдаления
    И ServicRequest.User = &Value1
    И ServicRequest.Status<> ЗНАЧЕНИЕ(Справочник.ServicRequestStatus.Закрыто)

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    ChangeRequest.Ссылка,
    ChangeRequest.Информация,
    ChangeRequest.Статус.Наименование,
    ChangeRequest.Статус.ЦветовойКод,
    ChangeRequest.Дата,
    ChangeRequest.Тип.Наименование,
    ""Запрос на изменение""
ИЗ Документ.ChangeRequest КАК ChangeRequest
ГДЕ НЕ ChangeRequest.ПометкаУдаления
    И ChangeRequest.Инициатор = &Value1
    И ChangeRequest.Статус<> ЗНАЧЕНИЕ(Справочник.ChangeRequestStatus.Закрыт);

ВЫБРАТЬ *
ИЗ query КАК query
{1}
УПОРЯДОЧИТЬ ПО query.Date УБЫВ";

        #endregion
    }
}
