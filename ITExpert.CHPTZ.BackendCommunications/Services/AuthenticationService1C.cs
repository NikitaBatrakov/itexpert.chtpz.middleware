﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    public class AuthenticationService1C : BaseService1C
    {
        public AuthenticationService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        public string Authenticate(string login, string password)
        {
            password = EncryptPassword(password);
            return Invoke("Authorization", login, password).ToString();
        }

        private static string EncryptPassword(string password)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            var shaPassword = sha.ComputeHash(Encoding.UTF8.GetBytes(password));
            var passwordHashBase64 = Convert.ToBase64String(shaPassword);
            return passwordHashBase64;
        }
    }
}
