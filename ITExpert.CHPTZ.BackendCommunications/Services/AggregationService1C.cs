﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services.Common;
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public class AggregationService1C : BaseQueryService1C
    {
        public AggregationService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        public string GetAggregationList(GetEntityArguments arguments)
        {
            var query = FormatQuery(AggregationQuery, arguments);
            return ExecuteQuery(query, arguments);
        }

        public string GetExtendedAggregationList(GetEntityArguments arguments)
        {
            var query = FormatQuery(ExtendedAggregationQuery, arguments);
            return ExecuteQuery(query, arguments);
        }

        #region AggregationQuery

        private static readonly string AggregationQuery = 
$@"ВЫБРАТЬ
    entity.Номер КАК Number,
    entity.ShortDescription КАК Subject,
    entity.Status КАК Status,
    entity.Status КАК IncidentStatus, 
    {Null} КАК ServiceRequestStatus, 
    {Null} КАК ChangeRequestStatus, 
    {Null} КАК TaskStatus,
    entity.Initiator КАК Creator,
    entity.Employee КАК Responsible,
    entity.FunctionalGroup КАК FunctionalGroup,
    entity.Дата КАК RegistrationDate,
    entity.SolutionDate КАК Deadline,
    entity.Ссылка КАК Link,
    entity.Тип КАК AdditionalAttributeType,
    entity.ShortDescription КАК ShortDescription,
    ""Инцидент"" КАК Entity
ПОМЕСТИТЬ query
ИЗ Документ.Incident КАК entity
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Номер,
    entity.ShortDescription,
    entity.Status,
    {Null},
    entity.Status,
    {Null},
    {Null},
    entity.Initiator,
    entity.Employee,
    entity.FunctionalGroup,
    entity.Дата,
    entity.SolutionDate,
    entity.Ссылка,
    entity.Тип,
    entity.ShortDescription,
    ""Запрос на обслуживание""
ИЗ Документ.ServicRequest КАК entity
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Номер,
    entity.Тема,
    entity.Статус,
    {Null},
    {Null},
    entity.Статус,
    {Null},
    entity.Инициатор,
    entity.Ответственный,
    NULL,
    entity.Дата,
    NULL,
    entity.Ссылка,
    entity.Тип,
    entity.Информация,
    ""Запрос на изменение""
ИЗ Документ.ChangeRequest КАК entity
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Номер,
    entity.Theme,
    entity.Status,
    {Null},
    {Null},
    {Null},
    entity.Status,
    entity.Initiator,
    entity.Employee,
    entity.FunctionalGroup,
    entity.Дата,
    entity.Deadline,
    entity.Ссылка,
    entity.Тип,
    entity.Information,
    ""Задача""
ИЗ Задача.Task КАК entity
ГДЕ НЕ entity.ПометкаУдаления;

ВЫБРАТЬ Link{{0}}
ИЗ query КАК query
{{1}}";

        #endregion

        #region ExtendedAggregationQuery

        private static readonly string ExtendedAggregationQuery = $@"ВЫБРАТЬ
    entity.Номер КАК Number,
    entity.ShortDescription КАК Subject,
    entity.Status КАК Status,
    entity.Status КАК IncidentStatus, 
    {Null} КАК ServiceRequestStatus, 
    {Null} КАК ChangeRequestStatus, 
    {Null} КАК TaskStatus,
    entity.Initiator КАК Creator,
    entity.Employee КАК Responsible,
    entity.FunctionalGroup КАК FunctionalGroup,
    entity.Дата КАК RegistrationDate,
    entity.SolutionDate КАК Deadline,
    entity.Ссылка КАК Link,
    entity.Тип КАК AdditionalAttributeType,
    entity.ShortDescription КАК ShortDescription,
    ""Инцидент"" КАК Entity
ПОМЕСТИТЬ query
ИЗ Документ.Incident КАК entity
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Номер,
    entity.ShortDescription,
    entity.Status,
    {Null},
    entity.Status,
    {Null},
    {Null},
    entity.Initiator,
    entity.Employee,
    entity.FunctionalGroup,
    entity.Дата,
    entity.SolutionDate,
    entity.Ссылка,
    entity.Тип,
    entity.ShortDescription,
    ""Запрос на обслуживание""
ИЗ Документ.ServicRequest КАК entity
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Номер,
    entity.Тема,
    entity.Статус,
    {Null},
    {Null},
    entity.Статус,
    {Null},
    entity.Инициатор,
    entity.Ответственный,
    NULL,
    entity.Дата,
    NULL,
    entity.Ссылка,
    entity.Тип,
    entity.Информация,
    ""Запрос на изменение""
ИЗ Документ.ChangeRequest КАК entity
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Номер,
    entity.Theme,
    entity.Status,
    {Null},
    {Null},
    {Null},
    entity.Status,
    entity.Initiator,
    entity.Employee,
    entity.FunctionalGroup,
    entity.Дата,
    entity.Deadline,
    entity.Ссылка,
    entity.Тип,
    entity.Information,
    ""Задача""
ИЗ Задача.Task КАК entity
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Код,
    entity.Описание,
    entity.Статус,
    {Null},
    {Null},
    {Null},
    {Null},
    entity.Инициатор,
    entity.Ответственный,
    {Null},
    entity.ПлановаяДатаНачала,
    entity.ПлановаяДатаОкончания,
    entity.Ссылка,
    {Null},
    entity.Описание,
    ""Проект""
ИЗ Справочник.ITEXP_Проекты КАК entity
ГДЕ НЕ entity.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    entity.Код,
    entity.Наименование,
    entity.Статус,
    {Null},
    {Null},
    {Null},
    {Null},
    {Null},
    {Null},
    entity.ГруппаАдминистрирования,
    {Null},
    {Null},
    entity.Ссылка,
    entity.Тип,
    entity.Описание,
    ""Конфигурационная единица""
ИЗ Справочник.ITEXP_КЕ КАК entity
ГДЕ НЕ entity.ПометкаУдаления;

ВЫБРАТЬ Link{{0}}
ИЗ query КАК query
{{1}}";

        #endregion
    }
}
