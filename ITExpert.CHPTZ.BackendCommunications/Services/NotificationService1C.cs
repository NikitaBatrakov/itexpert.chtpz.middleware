namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    public class NotificationService1C : BaseService1C
    {
        public NotificationService1C(IServiceConnection1C connection)
            : base(connection) { }

        public int GetNotificationsCount(string employeeGuid) 
            => (int)Invoke("GetNumberNotifications", employeeGuid);

        public string GetNotifications(string employeeGuid) 
            => Invoke("GetNotifications", employeeGuid).ToString();

        public string GetNotifications(IEnumerable<string> employeeGuids)
        {
            //Called directly to avoid exessive logging
            return Connection.Client.GetNumberEmployeeNotifications(string.Join(";", employeeGuids));
        } 

        public string ReadNotifications(string notificationsXml)
            => Invoke("CleanNotifications", notificationsXml).ToString();

        public string ReadObjectNotifications(string employeeGuid, string entity, string guid)
        {
            //Called directly to avoid exessive logging
            return Connection.Client.CleanNotificationsByGUID(employeeGuid, entity, guid);
        } 
    }
}