namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    public class DefaultFilterService1C : BaseService1C
    {
        public DefaultFilterService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        public string GetDefaultFilter(string entity, string employeeGuid, string placeGuid)
            => Invoke("GetDefaultFilter", entity, employeeGuid, placeGuid).ToString();

        public string SetDefaultFilter(string viewGuid, string entity, string employeeGuid, string placeGuid)
            => Invoke("SetDefaultFilter", viewGuid, entity, employeeGuid, placeGuid).ToString();

        public string GetGroupFilter(string entity, string employeeGuid)
            => Invoke("GetGroupFilter", entity, employeeGuid).ToString();
    }
}