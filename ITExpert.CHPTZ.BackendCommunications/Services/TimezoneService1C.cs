﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    public class TimezoneService1C : BaseService1C
    {
        public TimezoneService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        public string GetTimezones(string employeeGuid)
            => Invoke("GetTimeZones", employeeGuid).ToString();

        public string SetTimezone(string employeeGuid, string timezoneGuid)
            => Invoke("ChangeUsersTimeZone", employeeGuid, timezoneGuid).ToString();
    }
}
