﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services.Common;
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public class AgreementsMonitoringService1C : BaseQueryService1C
    {
        public AgreementsMonitoringService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        public string MonitorAgreements(string requestEntity, string requestGuid, GetEntityArguments arguments)
        {
            var query = FormatQuery(Query, arguments);
            return ExecuteQuery(query, arguments, new QueryParameter(requestGuid, requestEntity));
        }

        #region Query

        private static readonly string Query = 
@"ВЫБРАТЬ
    Task.Ссылка КАК Link,
    Task.Номер КАК Number,
    Task.Theme КАК Name,
    Task.Employee КАК Responsible,
    Task.Status КАК Status,
    ВЫБОР
        КОГДА Task.Action.ТипДействия = ЗНАЧЕНИЕ(Перечисление.ТипыЗадач.Согласование)
            ИЛИ Task.Predecessor.Action.ТипДействия = ЗНАЧЕНИЕ(Перечисление.ТипыЗадач.Согласование)
        ТОГДА ""Agreement""
        ИНАЧЕ ""Task""
    КОНЕЦ КАК Type
ПОМЕСТИТЬ query
ИЗ Задача.Task КАК Task
ГДЕ (Task.ОснованиеБП = &Value1 ИЛИ
    Task.Основание = &Value1) И
    (Task.Action.ТипДействия <> ЗНАЧЕНИЕ(Перечисление.ТипыЗадач.ВыполняетсяАвтоматически) ИЛИ
        Task.Action = ЗНАЧЕНИЕ(Справочник.ДействияСценария.ПустаяСсылка))
    И НЕ Task.АгрегирующаяЗадача
    И НЕ Task.ВторичноеСогласованиеЗаказов
    И НЕ Task.ПометкаУдаления

ОБЪЕДИНИТЬ ВСЕ

ВЫБРАТЬ
    NULL,
    NULL,
    Step.НазваниеЭтапа,
    Step.Сотрудник,
    Step.Статус,
    ""AgreementStep""
ИЗ РегистрСведений.ITEXP_ШагиСогласований КАК Step
ГДЕ Step.Обращение = &Value1 И Step.ЗадачаСогласование = ЗНАЧЕНИЕ(Задача.Task.ПустаяСсылка);

ВЫБРАТЬ *
ИЗ query
{1}";

        #endregion
    }
}
