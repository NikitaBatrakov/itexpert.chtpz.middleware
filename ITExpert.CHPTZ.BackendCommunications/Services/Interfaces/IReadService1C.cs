﻿namespace ITExpert.CHPTZ.BackendCommunications.Services.Interfaces
{
    using ITExpert.CHPTZ.BackendCommunications.Types;

    public interface IReadService1C
    {
        string GetEntities(string entityName1C, GetEntityArguments args);
        string GetEntitiesTree(string entityName1C, GetEntityArguments args);
    }
}
