namespace ITExpert.CHPTZ.BackendCommunications.Services.Interfaces
{
    public interface IWriteService1C
    {
        string CreateEntity(string entities);
        string UpdateEntity(string entities);
        string CreateOnBase(string entities, string baseLink, string procedure, string procedureParams);
    }
}