﻿namespace ITExpert.CHPTZ.BackendCommunications.Services
{
    using ITExpert.CHPTZ.BackendCommunications.Common;
    using ITExpert.CHPTZ.BackendCommunications.Connection;

    public class MetadataService1C : BaseService1C
    {
        public MetadataService1C(IServiceConnection1C connection)
            : base(connection)
        {
        }

        public string GetMetadata()
        {
            return Connection.Client.GetMetadata();
        }
    }
}
