﻿namespace ITExpert.CHPTZ.BackendCommunications.Connection
{
    using System;

    using ITExpert.CHPTZ.BackendCommunications.WebService1C;

    public interface IServiceConnection1C : IDisposable
    {
        TEXP_ITSMServicePortTypeClient Client { get; }
    }
}
