﻿namespace ITExpert.CHPTZ.BackendCommunications.Connection
{
    using System;
    using System.Configuration;
    using System.ServiceModel;
    using System.Text;

    using ITExpert.CHPTZ.BackendCommunications.WebService1C;

    public class ServiceConnection1C : IServiceConnection1C
    {
        public TEXP_ITSMServicePortTypeClient Client { get; }

        public ServiceConnection1C(string login, string password, string wsdl = null)
        {
            if (wsdl == null)
            {
                wsdl = ConfigurationManager.AppSettings["WebServiceUrl"];
            }
            Client = GetClient(login, password, wsdl);
        }

        private static TEXP_ITSMServicePortTypeClient GetClient(string login, string password, string wsdl)
        {
            var endpointAddress = new EndpointAddress(new Uri(wsdl));
            var binding = GetBinding();
            var client = new TEXP_ITSMServicePortTypeClient(binding, endpointAddress);
            if (client.ClientCredentials != null)
            {
                AssingCredentials(login, password, client);
            }
            return client;
        }

        private static BasicHttpBinding GetBinding()
        {
            var binding = new BasicHttpBinding
            {
                Security = new BasicHttpSecurity
                {
                    Mode = BasicHttpSecurityMode.TransportCredentialOnly,
                    Transport = { ClientCredentialType = HttpClientCredentialType.Basic }
                },
                MaxReceivedMessageSize = int.MaxValue,
                ReceiveTimeout = TimeSpan.MaxValue,
                TextEncoding = Encoding.UTF8
            };
            binding.CreateBindingElements();
            return binding;
        }

        private static void AssingCredentials(string login, string password, TEXP_ITSMServicePortTypeClient client)
        {
            var name = Encoding.UTF8.GetBytes(login);
            var pass = Encoding.UTF8.GetBytes(password);
            if (client.ClientCredentials == null)
            {
                throw new Exception("Failed to connect to 1C - ClientCredentials is Null");
            }
            client.ClientCredentials.UserName.UserName = Encoding.Default.GetString(name);
            client.ClientCredentials.UserName.Password = Encoding.Default.GetString(pass);
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            try
            {
                Client.Close();
            }
            catch
            {
                Client.Abort();
            }
        }

        #endregion
    }
}
