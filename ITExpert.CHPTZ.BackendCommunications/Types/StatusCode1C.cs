namespace ITExpert.CHPTZ.BackendCommunications.Types
{
    using System.Net;

    public static class StatusCode1C
    {
        public static int GetHttpCode(int code1C)
        {
            switch (code1C)
            {
                case (int)Code1C.OK:
                    return (int)HttpStatusCode.OK;
                case (int)Code1C.IncorrectUser:
                case (int)Code1C.IncorrectPassword:
                case (int)Code1C.NotAuthorized:
                    return (int)HttpStatusCode.Unauthorized;
                default:
                    return (int)HttpStatusCode.InternalServerError;
            }
        }
    }
}