namespace ITExpert.CHPTZ.BackendCommunications.Types
{
    public enum Code1C
    {
        OK = 0,
        IncorrectUser = 100,
        IncorrectPassword = 101,
        NotAuthorized = 102,
        ObjectLocked = 306
    }
}