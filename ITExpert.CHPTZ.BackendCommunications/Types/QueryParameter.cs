namespace ITExpert.CHPTZ.BackendCommunications.Types
{
    public struct QueryParameter
    {
        public string Value { get; }
        public string Type { get; }

        public QueryParameter(string value, string type)
        {
            Value = value;
            Type = type;
        }

        #region Overrides of ValueType

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> containing a fully qualified type name.
        /// </returns>
        public override string ToString()
        {
            return $"{Value},{Type}";
        }

        #endregion
    }
}