﻿namespace ITExpert.CHPTZ.BackendCommunications.Types
{
    public class GetEntityArguments
    {
        public string Fields { get; set; } = "";
        public int Offset { get; set; } = 0;
        public int Limit { get; set; } = 0;
        public string Sort { get; set; } = "";
        public string Filter { get; set; } = "";
        public string Parameters { get; set; } = "";
        public bool WithAttributes { get; set; } = false;
        public bool WithAttachments { get; set; } = false;
        public bool WithTables { get; set; } = false;
        public string TablesNames { get; set; } = "";
        public string TablesFieldsNames { get; set; } = "";

        public override string ToString()
        {
            return 
$@"- Fields: {Fields}
- Offset: {Offset}
- Limit: {Limit}
- Sort: {Sort}
- Filter: {Filter}
- Parameters: {Parameters}
- WithAttributes: {WithAttributes}
- WithAttachments: {WithAttachments}
- WithTables: {WithTables}
- TableNames: {TablesNames}
- TableFieldNames: {TablesFieldsNames}";
        }
    }
}
