namespace ITExpert.CHPTZ.BackendCommunications.Common
{
    using System;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Exceptions;
    using ITExpert.CHPTZ.Common;
    using ITExpert.CHPTZ.Common.Extensions;

    using NLog;

    public abstract class BaseService1C
    {
        protected IServiceConnection1C Connection { get; }
        protected Logger Logger { get; }


        protected BaseService1C(IServiceConnection1C connection)
        {
            Connection = connection;
            //Logger = LogManager.GetLogger(GetType().FullName);
            Logger = LogManager.GetLogger("1C");
        }

        protected object Invoke(string name, params object[] arguments)
        {
            try
            {
                var result = Invoker.Invoke(Connection.Client, name, arguments);
                LogInvokation(name, arguments, result);
                return result;
            }
            catch (Exception e)
            {
                throw new BackendConnectionException(e.Message);
            }
        }

        /// <summary>
        /// Fixes unusual 1C behavior with Limit and Offset.
        /// 1C follows the rule: LastRowNumber = Offset + Limit.
        /// Hence Limit is the first row number; Offset is the number of rows.
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        protected static void AdjustLimitOffset(ref int limit, ref int offset)
        {
            if (limit > 0)
            {
                offset++;
            }
            if (offset > 0)
            {
                limit--;
            }
        }

        private void LogInvokation(string name, object[] arguments, object result)
        {
            var info = new LogEventInfo
                       {
                           Level = LogLevel.Info,
                           Message = name,
                           LoggerName = "1C",
                           Properties =
                           {
                               {"Service", GetType().Name },
                               {"Method", name},
                               {"Arguments", string.Join("\n\n", arguments)},
                               {"Result", result}
                           }
                       };
            Logger.Log(info);
            //Logger.Info($"{name} with arguments:\n\t- {string.Join("\n\t- ", arguments)}\nreturned:\n{result.ToString().MakeBlock()}");
        }
    }
}