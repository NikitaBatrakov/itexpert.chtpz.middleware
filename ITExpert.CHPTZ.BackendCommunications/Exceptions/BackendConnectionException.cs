﻿using System;

namespace ITExpert.CHPTZ.BackendCommunications.Exceptions
{
    using System.Runtime.Serialization;

    [Serializable]
    public class BackendConnectionException : Exception
    {
        public BackendConnectionException() 
            : base("An exception was thown while connecting to backend") { }

        public BackendConnectionException(string message) 
            : base($"An exception was thown while connecting to backend: {message}") { }

        public BackendConnectionException(string message, Exception inner)
            : base(message, inner) { }

        protected BackendConnectionException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
