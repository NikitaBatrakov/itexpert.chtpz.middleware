﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class GetEntityResponse : IResponsible
    {
        public ServiceResult ServiceResult { get; set; }

        public int? TotalResults { get; }

        public IDictionary<string, PropertyMetadata> Metadata { get; }

        public IEnumerable<object> Entities { get; }

        public GetEntityResponse(ServiceResult serviceResult, int? totalResults, IEnumerable<object> entities)
        {
            ServiceResult = serviceResult;
            TotalResults = totalResults;
            Entities = entities;
        }

        public static GetEntityResponse Create(MQueryResult data)
        {
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public GetEntityResponse(ServiceResult serviceResult, int? totalResults, IDictionary<string, PropertyMetadata> metadata, IEnumerable<object> entities)
        {
            ServiceResult = serviceResult;
            TotalResults = totalResults;
            Metadata = metadata;
            Entities = entities;
        }

        public static GetEntityResponse Create(EntitiesResponseDocument mDoc)
        {
            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault() ?? ServiceResult.CreateOk();
            var totalResults = mDoc.TotalResult.Value.FirstOrDefault();
            var metadata = mDoc.Metadata.Value.FirstOrDefault();
            var entities = mDoc.Entities.Value;
            return new GetEntityResponse(serviceResult, totalResults, metadata, entities);
            
        }
    }
}
