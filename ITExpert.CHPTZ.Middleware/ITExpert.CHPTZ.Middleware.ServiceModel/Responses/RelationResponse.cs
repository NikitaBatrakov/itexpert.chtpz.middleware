namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess;
    using ITExpert.CHPTZ.DataAccess.Types;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses.DTO;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public class RelationResponse : IResponsible
    {
        public ServiceResult ServiceResult { get; set; }
        public int? TotalResults { get; }
        public IEnumerable<Relation> Relations { get; set; }

        public RelationResponse(ServiceResult serviceResult, int? totalResults)
        {
            ServiceResult = serviceResult;
            TotalResults = totalResults;
            Relations = new List<Relation>();
        }

        public RelationResponse(ServiceResult serviceResult, int? totalResults, IEnumerable<Relation> relations)
        {
            ServiceResult = serviceResult;
            TotalResults = totalResults;
            Relations = relations;
        }
    }
}