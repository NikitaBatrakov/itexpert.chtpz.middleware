﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class CreateOrUpdateEntityResponse : IResponsible
    {
        public IEnumerable<object> Entities { get; }
        public ServiceResult ServiceResult { get; set; }

        public CreateOrUpdateEntityResponse(IEnumerable<object> entities, ServiceResult serviceResult)
        {
            Entities = entities;
            ServiceResult = serviceResult;
        }

        public static CreateOrUpdateEntityResponse Create(EntitiesResponseDocument mDoc)
        {
            return new CreateOrUpdateEntityResponse(mDoc.Entities.Value, mDoc.ServiceResult.Value.FirstOrDefault());
        }
    }
}
