﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class GetDocumentTreeResponse : IResponsible
    {
        public IEnumerable<TreeNode<Document>> Entities { get; }
        public ServiceResult ServiceResult { get; set; }

        public GetDocumentTreeResponse(IEnumerable<TreeNode<Document>> entities, ServiceResult serviceResult)
        {
            Entities = entities;
            ServiceResult = serviceResult;
        }
    }
}
