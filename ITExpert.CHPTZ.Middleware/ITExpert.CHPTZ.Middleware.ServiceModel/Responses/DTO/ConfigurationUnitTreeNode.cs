﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses.DTO
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class ConfigurationUnitTreeNode
    {
        [DataMember(Name = "name")]
        public string Name { get; }

        [DataMember(Name = "isSelectable")]
        public bool IsSelectable { get; set; }

        [DataMember(Name = "childrenIds")]
        public ICollection<string> ChildrenIds { get; }

        public ConfigurationUnitTreeNode(string name, bool isSelectable = false)
        {
            Name = name;
            IsSelectable = isSelectable;
            ChildrenIds = new List<string>();
        }
    }
}
