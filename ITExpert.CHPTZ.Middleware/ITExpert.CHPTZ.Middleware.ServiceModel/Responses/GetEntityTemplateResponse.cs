﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public class GetEntityTemplateResponse : IResponsible
    {
        public IDictionary<string, PropertyMetadata> Metadata { get; }
        public IEnumerable<object> Entities { get; }
        public ServiceResult ServiceResult { get; set; }

        public GetEntityTemplateResponse(IDictionary<string, PropertyMetadata> metadata, object entitiy, ServiceResult serviceResult)
        {
            Metadata = metadata;
            Entities = new[] {entitiy};
            ServiceResult = serviceResult;
        }
    }
}
