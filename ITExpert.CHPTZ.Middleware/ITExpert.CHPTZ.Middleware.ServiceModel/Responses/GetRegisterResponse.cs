﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class GetRegisterResponse : IResponsible
    {
        public ServiceResult ServiceResult { get; set; }
        public int? TotalResults { get; }
        public IDictionary<string, PropertyMetadata> Metadata { get; }
        public IEnumerable<object> Entities { get; }

        public GetRegisterResponse(ServiceResult serviceResult, IEnumerable<object> entities)
        {
            ServiceResult = serviceResult;
            Entities = entities;
        }

        public GetRegisterResponse(ServiceResult serviceResult, int? totalResults, IEnumerable<object> entities)
        {
            ServiceResult = serviceResult;
            TotalResults = totalResults;
            Entities = entities;
        }

        public GetRegisterResponse(ServiceResult serviceResult, int? totalResults, IDictionary<string, PropertyMetadata> metadata, IEnumerable<IRegister> entities)
        {
            ServiceResult = serviceResult;
            TotalResults = totalResults;
            Metadata = metadata;
            Entities = entities;
        }

        public static GetRegisterResponse Create(RegisterResponseDocument mDoc)
        {
            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault() ?? ServiceResult.CreateOk();
            var totalResults = mDoc.TotalResult.Value.FirstOrDefault();
            var metadata = mDoc.Metadata.Value.FirstOrDefault();
            var entities = mDoc.Entities.Value;
            return new GetRegisterResponse(serviceResult, totalResults, metadata, entities);

        }
    }
}
