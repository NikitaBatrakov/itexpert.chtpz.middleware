﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class FunctionResponse : IResponsible
    {
        public ServiceResult ServiceResult { get; set; }
        public IEnumerable<FormAction> Actions { get; }
        public IDictionary<string, PropertyMetadata> Metadata { get; }
        public IEnumerable<object> Entities { get; }

        public FunctionResponse(ServiceResult serviceResult, IEnumerable<object> entities)
        {
            ServiceResult = serviceResult;
            Entities = entities;
        }

        public FunctionResponse(ServiceResult serviceResult, IDictionary<string, PropertyMetadata> metadata, IEnumerable<object> entities)
        {
            ServiceResult = serviceResult;
            Metadata = metadata;
            Entities = entities;
        }

        public FunctionResponse(ServiceResult serviceResult, IEnumerable<FormAction> actions, 
            IDictionary<string, PropertyMetadata> metadata, IEnumerable<object> entities)
        {
            ServiceResult = serviceResult;
            Actions = actions;
            Metadata = metadata;
            Entities = entities;
        }

        public static FunctionResponse Create(FunctionResponseDocument mDoc)
        {
            var serviceResult = mDoc.ServiceResult.Value.FirstOrDefault() ?? ServiceResult.CreateOk();
            var metadata = mDoc.Metadata.Value.FirstOrDefault();
            var entities = mDoc.Entities.Value;
            var actions = mDoc.Actions.Value != null && mDoc.Actions.Value.Any() ? mDoc.Actions.Value : null;
            return new FunctionResponse(serviceResult, actions, metadata, entities);

        }
    }
}
