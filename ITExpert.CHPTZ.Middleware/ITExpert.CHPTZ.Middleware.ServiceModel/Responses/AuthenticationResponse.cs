﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class AuthenticationResponse
    {
        public string Token { get; }
        public User User { get; }

        public AuthenticationResponse(string token, User user)
        {
            Token = token;
            User = user;
        }
    }
}
