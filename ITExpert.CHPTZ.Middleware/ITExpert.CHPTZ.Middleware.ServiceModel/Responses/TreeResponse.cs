﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public class TreeResponse<T> : IResponsible
    {
        public IEnumerable<TreeNode<T>> Entities { get; }
        public ServiceResult ServiceResult { get; set; }

        public TreeResponse(IEnumerable<TreeNode<T>> entities, ServiceResult serviceResult)
        {
            Entities = entities;
            ServiceResult = serviceResult;
        }
    }
}
