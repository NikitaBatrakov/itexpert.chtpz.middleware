﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents;

    public class UniversalEntityResponse
    {
        public ServiceResult ServiceResult { get; set; }

        public int? TotalResults { get; }

        public IEnumerable<IDictionary<string, ObjectProperty1C>> Entities { get; }

        public UniversalEntityResponse(ServiceResult serviceResult, int? totalResults, IEnumerable<IDictionary<string, ObjectProperty1C>> entities)
        {
            ServiceResult = serviceResult;
            TotalResults = totalResults;
            Entities = entities;
        }

        public static UniversalEntityResponse Create(UniversalEntityDocument mDoc)
        {
            return new UniversalEntityResponse(mDoc.ServiceResult.Value.FirstOrDefault(),
                                               mDoc.TotalResult.Value.FirstOrDefault(), mDoc.Entities.Value.Select(x => x.ToDictionary()));
        }
    }
}
