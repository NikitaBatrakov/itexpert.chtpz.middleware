﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.ObjectModel.Dtos.SearchDtos;

    public class GetSearchResponse : IResponsible
    {
        public ServiceResult ServiceResult { get; set; }

        public UserFilterParameters Parameters { get; }
        public IEnumerable<AvailableEntityField> Fields { get; }
        public IEnumerable<AvailableEntityFilter> Filters { get; }

        public GetSearchResponse(ServiceResult serviceResult, UserFilter entities)
        {
            ServiceResult = serviceResult;
            Fields = entities.Fields;
            Filters = entities.Filters;
            Parameters = entities.Parameters;
        }
    }
}
