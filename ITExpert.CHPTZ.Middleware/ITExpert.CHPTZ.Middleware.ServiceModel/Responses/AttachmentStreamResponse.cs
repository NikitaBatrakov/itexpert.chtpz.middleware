﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Collections.Generic;
    using System.IO;

    using ServiceStack;
    using ServiceStack.Web;

    public class AttachmentStreamResponse : IHasOptions, IStreamWriter
    {
        #region Implementation of IHasOptions

        public IDictionary<string, string> Options { get; }

        #endregion

        private Stream ResponseStream { get; }

        public AttachmentStreamResponse(Stream responseStream, string filename)
        {
            ResponseStream = responseStream;
            Options = new Dictionary<string, string>
                      {
                          {"Content-Type", "application/octet-stream"},
                          {"Content-Disposition", $"attachment; filename=\"{filename}\";"}
                      };
        }

        #region Implementation of IStreamWriter

        public void WriteTo(Stream responseStream)
        {
            if (ResponseStream == null)
                return;

            ResponseStream.WriteTo(responseStream);
            responseStream.Flush();
        }

        #endregion


    }
}
