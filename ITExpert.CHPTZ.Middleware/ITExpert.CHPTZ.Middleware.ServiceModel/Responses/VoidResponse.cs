﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using System.Linq;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;

    public class VoidResponse : IResponsible
    {
        #region Implementation of IResponsible

        public ServiceResult ServiceResult { get; set; }

        #endregion

        public VoidResponse(ServiceResult serviceResult)
        {
            ServiceResult = serviceResult;
        }

        public static VoidResponse Create(ResultableResponseDocument mDoc)
        {
            return new VoidResponse(mDoc.ServiceResult.Value.FirstOrDefault());
        }
    }
}
