﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Responses
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public class ValueResponse<T> : IResponsible
    {
        public T Value { get; }
        public ServiceResult ServiceResult { get; set; }

        public ValueResponse(T value)
        {
            Value = value;
            ServiceResult = ServiceResult.CreateOk();
        }

        public ValueResponse(ServiceResult serviceResult, T value)
        {
            Value = value;
            ServiceResult = serviceResult;
        }
    }
}
