﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.TimezoneRequests
{
    using ServiceStack;

    [Route("/timezone/{TimezoneGuid}", "POST")]
    public class SetTimezoneRequest
    {
        public string TimezoneGuid { get; set; }
    }
}