﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.SearchRequests
{
    using ServiceStack;

    [Route("/search/entities/{Query}", "GET, POST")]
    public class SearchTypesRequest
    {
        public string Query { get; set; }
    }
}