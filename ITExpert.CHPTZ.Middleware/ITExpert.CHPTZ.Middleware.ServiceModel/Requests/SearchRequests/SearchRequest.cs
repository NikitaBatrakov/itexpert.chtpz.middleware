﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.SearchRequests
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;

    using ServiceStack;
    
    [Route("/search/{Entity}/{Query}", "GET, POST")]
    public class SearchRequest : BaseGetRequest
    {
        public string Query { get; set; }
    }
}