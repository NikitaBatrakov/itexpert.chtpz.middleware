namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.SearchRequests
{
    using ServiceStack;

    [Route("/search/data/{Entity}", "GET")]
    public class AdvancedSearchRequest
    {
        public string Entity { get; set; }
        public string Properties { get; set; } = "";
    }
}