﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AdditionalAttributeRequests
{
    using ServiceStack;

    [Route("/attribute/{Entity}", "GET")]
    public class AdditionalAttributeValueRequest
    {
        public string Entity { get; set; }
        public string Guid { get; set; }
    }
}
