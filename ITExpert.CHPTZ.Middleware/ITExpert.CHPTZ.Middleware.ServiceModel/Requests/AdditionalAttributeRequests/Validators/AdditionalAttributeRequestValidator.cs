﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AdditionalAttributeRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class AdditionalAttributeRequestValidator : AbstractValidator<AdditionalAttributesRequest>
    {
        public AdditionalAttributeRequestValidator()
        {
            RuleFor(x => x.Guid).NotEmpty();
        }
    }
}
