﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AdditionalAttributeRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class AdditionalAttributeValueRequestValidator : AbstractValidator<AdditionalAttributeValueRequest>
    {
        public AdditionalAttributeValueRequestValidator()
        {
            RuleFor(x => x.Guid).NotEmpty();
        }
    }
}
