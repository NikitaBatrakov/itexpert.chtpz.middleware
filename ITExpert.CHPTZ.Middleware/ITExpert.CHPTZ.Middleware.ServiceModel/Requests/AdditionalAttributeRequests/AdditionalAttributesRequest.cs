namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AdditionalAttributeRequests
{
    using ServiceStack;

    [Route("/attribute", "GET")]
    public class AdditionalAttributesRequest
    {
        public string Guid { get; set; }
    }
}