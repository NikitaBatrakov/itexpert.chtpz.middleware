﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonitoringRequests
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;

    using ServiceStack;

    [Route("/monitoring/{TargetEntity}/{Guid}", "GET, POST")]
    public class GetMonitoringRequest : BaseGetRequest
    {
        public string TargetEntity { get; set; }
        public string Guid { get; set; }
    }
}
