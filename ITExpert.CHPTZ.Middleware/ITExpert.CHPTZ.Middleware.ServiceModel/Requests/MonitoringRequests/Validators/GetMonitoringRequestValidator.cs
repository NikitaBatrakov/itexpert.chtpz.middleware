﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonitoringRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class GetMonitoringRequestValidator : AbstractValidator<GetMonitoringRequest>
    {
        public GetMonitoringRequestValidator()
        {
            RuleFor(x => x.TargetEntity).NotEmpty();
            RuleFor(x => x.Guid).NotEmpty();
        }
    }
}
