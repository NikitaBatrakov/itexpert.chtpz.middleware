﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonitoringRequests
{
    using ServiceStack;

    [Route("/monitoring/metadata", "GET")]
    public class GetMonitoringMetadataRequest
    {
        public string Fields { get; set; }
    }
}