﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.NotificationRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class ReadObjectsNotificationsValidator : AbstractValidator<ReadObjectNotifications>
    {
        public ReadObjectsNotificationsValidator()
        {
            RuleFor(x => x.Entity).NotEmpty();
            RuleFor(x => x.Guid).NotEmpty();
        }
    }
}
