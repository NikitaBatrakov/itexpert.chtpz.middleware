﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.NotificationRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class ReadNotificationRequestValidator : AbstractValidator<ReadNotificationRequest>
    {
        public ReadNotificationRequestValidator()
        {
            RuleFor(x => x.Notifications).NotEmpty();
        }
    }
}
