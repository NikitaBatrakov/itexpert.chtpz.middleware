namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.NotificationRequests
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.ObjectModel.Entities;

    using ServiceStack;

    [Route("/notifications/", "POST")]
    public class ReadNotificationRequest
    {
        public IEnumerable<NotificationWithAttachment> Notifications { get; set; }
    }
}