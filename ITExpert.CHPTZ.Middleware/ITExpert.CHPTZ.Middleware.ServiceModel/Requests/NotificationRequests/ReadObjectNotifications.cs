﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.NotificationRequests
{
    using ServiceStack;

    [Route("/notification/{Entity}/{Guid}", "POST")]
    public class ReadObjectNotifications
    {
        public string Entity { get; set; }
        public string Guid { get; set; }
    }
}
