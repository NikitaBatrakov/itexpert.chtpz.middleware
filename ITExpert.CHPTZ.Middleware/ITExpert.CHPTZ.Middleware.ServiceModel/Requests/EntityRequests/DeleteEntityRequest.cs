﻿using System.Collections.Generic;
using ServiceStack;

namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    [Route("/entity/{Entity}/{Guid}", "DELETE")]
    public class DeleteEntityRequest
    {
        public string Entity { get; set; }
        public string Guid { get; set; }
    }

    [Route("/entity/{Entity}", "DELETE")]
    public class DeleteEntitiesRequest
    {
        public string Entity { get; set; }
        public IEnumerable<string> Guids { get; set; }
    }

    [Route("/entity/delete", "POST")]
    public class DeleteEntitiesWithMultipleTypesRequest
    {
        public Dictionary<string, string[]> Entities { get; set; }
    }
}
