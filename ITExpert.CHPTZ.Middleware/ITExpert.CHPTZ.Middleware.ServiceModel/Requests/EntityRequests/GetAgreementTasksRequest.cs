﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using ServiceStack;

    [Route("/entity/agreementTask", "GET, POST")]
    public class GetAgreementTasksRequest : BaseGetEntitiesRequest
    {
        
    }
}
