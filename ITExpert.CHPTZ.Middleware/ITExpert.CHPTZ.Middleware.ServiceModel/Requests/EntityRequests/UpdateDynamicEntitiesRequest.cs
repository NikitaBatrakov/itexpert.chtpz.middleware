﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using ITExpert.CHPTZ.ObjectModel.Dtos.DynamicEntityDtos;

    using ServiceStack;

    [Route("/entity/dynamic/update", "POST")]
    public class UpdateDynamicEntitiesRequest : ICreateOrUpdateDynamicEntities
    {
        public TypesDictionary Entities { get; set; }
    }
}
