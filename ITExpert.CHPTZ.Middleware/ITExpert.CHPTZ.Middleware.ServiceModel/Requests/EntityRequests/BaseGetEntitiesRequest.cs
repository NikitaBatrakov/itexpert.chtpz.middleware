namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using System;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;

    public class BaseGetEntitiesRequest : BaseGetRequest
    {
        public bool WithAttributes { get; set; } = false;
        public bool WithAttachments { get; set; } = false;
        public bool WithTables { get; set; } = false;
        public bool WithMetadata { get; set; } = false;
        public string TablesNames { get; set; } = "";
        public string TablesFieldsNames { get; set; } = "";

        protected BaseGetEntitiesRequest()
        {
            
        }

        public BaseGetEntitiesRequest(string entity)
        {
            Entity = entity;
        }

        public BaseGetEntitiesRequest(Type type)
        {
            Entity = type.Name;
        }
    }
}