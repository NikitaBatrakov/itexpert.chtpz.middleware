﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using ITExpert.CHPTZ.ObjectModel.Dtos.DynamicEntityDtos;

    using ServiceStack;

    [Route("/entity/dynamic", "POST")]
    public class CreateDynamicEntitiesRequest : ICreateOrUpdateDynamicEntities
    {
        public TypesDictionary Entities { get; set; }
    }
}
