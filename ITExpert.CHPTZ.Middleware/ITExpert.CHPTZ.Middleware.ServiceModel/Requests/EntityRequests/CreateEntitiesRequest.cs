namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using System.Collections.Generic;

    using ServiceStack;

    [Route("/entity/{Entity}", "POST")]
    public class CreateEntitiesRequest : ICreateUpdateEntitiesRequest
    {
        public string Entity { get; set; }
        public IEnumerable<object> Entities { get; set; }
    }
}