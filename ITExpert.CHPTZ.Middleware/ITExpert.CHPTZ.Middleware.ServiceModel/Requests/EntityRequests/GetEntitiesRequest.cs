namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using System;

    using ServiceStack;

    [Route("/entity/{Entity}/read", "GET, POST")]
    [Route("/entity/{Entity}", "GET")]
    public class GetEntitiesRequest : BaseGetEntitiesRequest
    {
        public GetEntitiesRequest() { }

        public GetEntitiesRequest(string entity) : base(entity) { }

        public GetEntitiesRequest(Type type) : base(type) { }
    }
}