﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using ITExpert.CHPTZ.ObjectModel.Dtos.DynamicEntityDtos;

    public interface ICreateOrUpdateDynamicEntities
    {
        TypesDictionary Entities { get; set; }
    }
}
