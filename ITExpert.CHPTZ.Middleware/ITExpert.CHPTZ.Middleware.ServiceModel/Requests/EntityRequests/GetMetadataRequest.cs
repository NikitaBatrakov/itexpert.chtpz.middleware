namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using ServiceStack;

    [Route("/entity/{Entity}/metadata", "GET")]
    [Route("/register/{Entity}/metadata", "GET")]
    public class GetMetadataRequest
    {
        public string Entity { get; set; }
        public string Fields { get; set; }
    }
}