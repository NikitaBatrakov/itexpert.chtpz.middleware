﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using ServiceStack;

    [Route("/entity/{Entity}/{Guid}", "GET")]
    public class GetEntityRequest
    {
        public string Entity { get; set; }
        public string Guid { get; set; }
        public string Fields { get; set; } = "";
        public bool WithAttributes { get; set; } = false;
        public bool WithAttachments { get; set; } = false;
        public bool WithTables { get; set; } = false;
        public bool WithMetadata { get; set; } = false;
    }
}
