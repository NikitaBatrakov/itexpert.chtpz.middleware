﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class UpdateEntityRequestValidator : AbstractValidator<UpdateEntitiesRequest>
    {
        public UpdateEntityRequestValidator()
        {
            RuleFor(x => x.Entities).NotEmpty();
        }
    }
}
