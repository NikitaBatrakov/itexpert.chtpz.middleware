﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class CreateOnBaseRequestValidator : AbstractValidator<CreateOnBaseRequest>
    {
        public CreateOnBaseRequestValidator()
        {
            RuleFor(x => x.BaseLink).NotEmpty();
            RuleFor(x => x.Procedure).NotEmpty();
            RuleFor(x => x.ProcedureParams).NotEmpty();
        }
    }
}
