﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class CreateEntityRequestValidator : AbstractValidator<CreateEntitiesRequest>
    {
        public CreateEntityRequestValidator()
        {
            RuleFor(x => x.Entities).NotEmpty();
        }
    }
}
