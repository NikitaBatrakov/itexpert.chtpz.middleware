﻿using System.Threading;
using ServiceStack.FluentValidation;

namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests.Validators
{
    public class DeleteEntityRequestValidator : AbstractValidator<DeleteEntityRequest>
    {
        public DeleteEntityRequestValidator()
        {
            RuleFor(x => x.Entity).NotEmpty();
            RuleFor(x => x.Guid).NotEmpty();
        }
    }

    public class DeleteEntitiesRequestValidator : AbstractValidator<DeleteEntitiesRequest>
    {
        public DeleteEntitiesRequestValidator()
        {
            RuleFor(x => x.Entity).NotEmpty();
            RuleFor(x => x.Guids).NotEmpty();
        }
    }

    public class DeleteEntitiesWithMultipleTypesRequestValidator :
        AbstractValidator<DeleteEntitiesWithMultipleTypesRequest>
    {
        public DeleteEntitiesWithMultipleTypesRequestValidator()
        {
            RuleFor(x => x.Entities).NotEmpty();
        }
    }
}
