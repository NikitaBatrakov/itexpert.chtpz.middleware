namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using ServiceStack;

    [Route("/entity/{Entity}/createonbase", "POST")]
    public class CreateOnBaseRequest
    {
        public string Entity { get; set; }
        public string BaseLink { get; set; }
        public string Procedure { get; set; }
        public string ProcedureParams { get; set; }
    }
}