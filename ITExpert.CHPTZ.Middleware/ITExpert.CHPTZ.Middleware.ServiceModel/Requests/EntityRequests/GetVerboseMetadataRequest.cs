﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using ServiceStack;

    [Route("/entity/{Entity}/metadata/verbose")]
    [Route("/register/{Entity}/metadata/verbose")]
    public class GetVerboseMetadataRequest
    {
        public string Entity { get; set; }
        public string Fields { get; set; }
    }
}
