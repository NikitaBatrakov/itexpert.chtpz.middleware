namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using System.Collections.Generic;

    public interface ICreateUpdateEntitiesRequest
    {
        string Entity { get; set; }
        IEnumerable<object> Entities { get; set; }
    }
}