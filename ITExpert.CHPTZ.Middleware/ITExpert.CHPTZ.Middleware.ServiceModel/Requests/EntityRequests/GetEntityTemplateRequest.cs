﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using ServiceStack;

    [Route("/entity/{Entity}/template")]
    public class GetEntityTemplateRequest
    {
        public string Entity { get; set; }
    }
}
