namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests
{
    using System.Collections.Generic;

    using ServiceStack;

    [Route("/entity/{Entity}/update", "POST")]
    public class UpdateEntitiesRequest : ICreateUpdateEntitiesRequest
    {
        public string Entity { get; set; }
        public IEnumerable<object> Entities { get; set; }
    }
}