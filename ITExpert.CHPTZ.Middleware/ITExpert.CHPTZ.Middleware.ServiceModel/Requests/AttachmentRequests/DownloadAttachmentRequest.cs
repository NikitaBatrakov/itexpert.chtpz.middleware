namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AttachmentRequests
{
    using ServiceStack;

    [Route("/attachment/download/{Guid}", "GET")]
    public class DownloadAttachmentRequest
    {
        public string Guid { get; set; }
    }
}