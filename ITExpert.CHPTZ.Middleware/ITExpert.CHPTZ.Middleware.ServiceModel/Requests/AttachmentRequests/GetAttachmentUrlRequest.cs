﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AttachmentRequests
{
    using ServiceStack;

    [Route("/attachment/{Guid}", "GET")]
    public class GetAttachmentUrlRequest
    {
        public string Guid { get; set; }
        public bool AsAttachment { get; set; } = false;
    }
}
