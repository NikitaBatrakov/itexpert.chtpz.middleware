﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AttachmentRequests
{
    using System;

    using ServiceStack;

    [Route("/attachment/{Entity}/{EntityGuid}", "PUT")]
    public class AddAttachmentRequest
    {
        public string Entity { get; set; }
        public string EntityGuid { get; set; }
    }
}