﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AttachmentRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class AddAttachmentValidator : AbstractValidator<AddAttachmentRequest>
    {
        public AddAttachmentValidator()
        {
            RuleFor(x => x.Entity).NotEmpty();
            RuleFor(x => x.EntityGuid).NotEmpty();
        }
    }
}
