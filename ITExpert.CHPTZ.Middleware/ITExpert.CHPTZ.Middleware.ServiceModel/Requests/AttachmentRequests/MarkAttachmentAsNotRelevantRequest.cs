﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AttachmentRequests
{
    using ServiceStack;

    [Route("/attachment/{Guid}/notRelevant", "POST")]
    public class MarkAttachmentAsNotRelevantRequest
    {
        public string Guid { get; set; }
    }
}
