﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.OperatorRequests.Validators
{
    using ServiceStack.FluentValidation;
    public class GetStatusRequestValidator : AbstractValidator<GetStatusRequest>
    {
        public GetStatusRequestValidator()
        {
            RuleFor(x => x.CallGuid).NotEmpty();
            RuleFor(x => x.Entity).NotEmpty();
            RuleFor(x => x.EntityGuid).NotEmpty();
        }
    }
}
