﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.OperatorRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class CreateRequestRequestValidator : AbstractValidator<CreateRequestRequest>
    {
        public CreateRequestRequestValidator()
        {
            RuleFor(x => x.CallGuid).NotEmpty();
            RuleFor(x => x.Entity).NotNull();
        }
    }
}
