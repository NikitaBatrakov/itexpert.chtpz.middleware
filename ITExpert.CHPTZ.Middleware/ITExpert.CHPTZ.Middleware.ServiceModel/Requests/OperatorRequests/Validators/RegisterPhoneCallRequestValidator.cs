﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.OperatorRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class RegisterPhoneCallRequestValidator : AbstractValidator<RegisterPhoneCallRequest>
    {
        public RegisterPhoneCallRequestValidator()
        {
            RuleFor(x => x.User).NotEmpty();
            RuleFor(x => x.Phone).NotEmpty();
            RuleFor(x => x.Record).NotEmpty();
        }
    }
}
