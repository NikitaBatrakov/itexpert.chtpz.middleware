﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.OperatorRequests
{
    using ServiceStack;

    [Route("/operator/call/{CallGuid}/{Entity}", "POST")]
    public class CreateRequestRequest
    {
        public string CallGuid { get; set; }
        public string Entity { get; set; }

        public string Initiator { get; set; }
        public string User { get; set; }

        public string Info { get; set; } = "";
    }
}
