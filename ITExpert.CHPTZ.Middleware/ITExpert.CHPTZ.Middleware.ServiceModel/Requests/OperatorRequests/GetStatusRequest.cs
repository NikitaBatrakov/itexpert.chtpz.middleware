﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.OperatorRequests
{
    using ServiceStack;

    [Route("/operator/call/{CallGuid}/{Entity}/{EntityGuid}", "GET")]
    public class GetStatusRequest
    {
        public string Entity { get; set; }

        public string CallGuid { get; set; }

        public string EntityGuid { get; set; }
    }
}
