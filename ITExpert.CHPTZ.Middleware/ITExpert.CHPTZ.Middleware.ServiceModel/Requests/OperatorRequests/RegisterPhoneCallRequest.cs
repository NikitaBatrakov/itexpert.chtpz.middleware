﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.OperatorRequests
{
    using ServiceStack;

    [Route("/operator/call/register", "POST")]
    public class RegisterPhoneCallRequest
    {
        public string Phone { get; set; }
        public string Record { get; set; }
        public string User { get; set; }
    }
}
