﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.OperatorRequests
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;

    using ServiceStack;

    [Route("/operator/requests/{EmployeeGuid}")]
    public class GetEmployeesRequestsRequest : BaseGetRequest
    {
        public string EmployeeGuid { get; set; }
    }
}
