namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AggregationRequests
{
    using ServiceStack;

    [Route("/aggregation/metadata", "GET")]
    public class GetAggregationMetadataRequest
    {
        public string Fields { get; set; }
    }
}