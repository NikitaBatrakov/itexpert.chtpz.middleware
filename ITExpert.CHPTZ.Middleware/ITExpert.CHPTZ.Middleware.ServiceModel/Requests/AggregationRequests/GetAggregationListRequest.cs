﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AggregationRequests
{
    using System;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;

    using ServiceStack;

    [Route("/aggregation/", "GET, POST")]
    public class GetAggregationListRequest : BaseGetRequest
    {
    }
}
