namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AggregationRequests
{
    using ServiceStack;

    [Route("/aggregation/metadata/verbose", "GET")]
    public class GetAggregationVerboseMetadataRequest
    {
        public string Fields { get; set; }
    }
}