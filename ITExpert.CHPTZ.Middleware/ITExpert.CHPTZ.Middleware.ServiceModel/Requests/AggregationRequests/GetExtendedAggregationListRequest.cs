namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AggregationRequests
{
    using System;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;

    using ServiceStack;

    [Route("/aggregation/extended", "GET, POST")]
    public class GetExtendedAggregationListRequest : BaseGetRequest
    {
    }
}