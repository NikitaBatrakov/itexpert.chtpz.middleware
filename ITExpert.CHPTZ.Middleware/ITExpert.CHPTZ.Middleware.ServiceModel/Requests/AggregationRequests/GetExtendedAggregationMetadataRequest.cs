namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AggregationRequests
{
    using ServiceStack;

    [Route("/aggregation/extended/metadata", "GET")]
    public class GetExtendedAggregationMetadataRequest
    {
        public string Fields { get; set; }
    }
}