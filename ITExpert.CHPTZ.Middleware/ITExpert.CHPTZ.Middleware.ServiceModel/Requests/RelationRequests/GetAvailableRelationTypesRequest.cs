﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RelationRequests
{
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    using ServiceStack;

    [Route("/relation/types/", "POST")]
    public class GetAvailableRelationTypesRequest
    {
        public Link1C MasterObject { get; set; }
        public string SlaveType { get; set; }
    }
}
