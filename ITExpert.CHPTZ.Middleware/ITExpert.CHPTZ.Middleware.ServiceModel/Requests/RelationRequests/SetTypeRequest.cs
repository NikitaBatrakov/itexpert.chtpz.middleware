namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RelationRequests
{
    using ServiceStack;

    [Route("/relation/settype/", "POST")]
    public class SetTypeRequest
    {
        public object Entity { get; set; }
    }
}