﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RelationRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class GetAvailableRelationTypesRequestValidator : AbstractValidator<GetAvailableRelationTypesRequest>
    {
        public GetAvailableRelationTypesRequestValidator()
        {
            RuleFor(x => x.MasterObject).NotEmpty();
            RuleFor(x => x.SlaveType).NotEmpty();
        }
    }
}
