﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RelationRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class SetTypeRequestValidator : AbstractValidator<SetTypeRequest>
    {
        public SetTypeRequestValidator()
        {
            RuleFor(x => x.Entity).NotEmpty();
        }
    }
}
