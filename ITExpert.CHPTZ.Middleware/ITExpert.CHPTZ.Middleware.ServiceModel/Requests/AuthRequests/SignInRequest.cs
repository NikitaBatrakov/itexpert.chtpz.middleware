﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AuthRequests
{
    using ServiceStack;

    [Route("/signin", "GET, POST")] //TODO: Remove GET
    public class SignInRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
