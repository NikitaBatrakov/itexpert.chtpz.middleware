﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AuthRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class SignInValidator : AbstractValidator<SignInRequest>
    {
        public SignInValidator()
        {
            RuleFor(x => x.Login).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}
