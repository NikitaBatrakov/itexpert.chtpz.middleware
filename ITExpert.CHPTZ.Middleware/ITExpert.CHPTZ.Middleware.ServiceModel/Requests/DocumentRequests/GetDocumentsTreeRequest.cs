﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.DocumentRequests
{
    using System.Collections.Generic;

    using ServiceStack;

    [Route("/entity/document/tree")]
    public class GetDocumentsTreeRequest
    {
        public IEnumerable<string> Guids { get; set; }
    }
}
