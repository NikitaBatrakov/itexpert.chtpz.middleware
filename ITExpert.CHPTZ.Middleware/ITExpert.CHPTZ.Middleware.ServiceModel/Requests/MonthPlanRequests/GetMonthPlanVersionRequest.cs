﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests
{
    using ServiceStack;

    [Route("/plan/month/version/{Year}/{Month}", "GET")]
    public class GetMonthPlanVersionRequest
    {
        public int Year { get; set; }
        public int Month { get; set; }
    }
}
