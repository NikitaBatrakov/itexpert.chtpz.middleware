namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests
{
    using ServiceStack;

    [Route("/plan/month/{MonthPlan}/team/{Team}/resource", "GET")]
    public class GetTeamResourceRequest
    {
        public string MonthPlan { get; set; }
        public string Team { get; set; } 
    }
}