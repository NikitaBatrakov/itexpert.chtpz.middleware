namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests
{
    using ServiceStack;

    [Route("/plan/month/{year}/{month}/check", "GET")]
    public class CheckMonthPlanRequest
    {
        public int Year { get; set; }

        public int Month { get; set; }
    }
}