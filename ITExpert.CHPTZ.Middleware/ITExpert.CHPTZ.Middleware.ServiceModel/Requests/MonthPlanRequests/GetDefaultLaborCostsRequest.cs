﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests
{
    using ServiceStack;

    [Route("/plan/month/{MonthPlan}/changeRequest/{ChangeRequest}/costs", "GET")]
    public class GetDefaultLaborCostsRequest
    {
        public string MonthPlan { get; set; }
        public string ChangeRequest { get; set; } 
    }
}