﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests
{
    using ServiceStack;

    [Route("/plan/month/changeRequest/", "GET")]
    public class GetMonthPlanChangeRequestsRequest
    {
        public string TeamGuid { get; set; }

        public string DirectionGuid { get; set; }

        public string MonthPlanGuid { get; set; }
    }
}
