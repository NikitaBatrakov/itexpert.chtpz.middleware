﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests
{
    using ServiceStack;

    [Route("/plan/month/tree/{BusinessProcessGuid}", "GET")]
    public class GetMonthPlanDirectionRequest
    {
        public string BusinessProcessGuid { get; set; }
    }
}