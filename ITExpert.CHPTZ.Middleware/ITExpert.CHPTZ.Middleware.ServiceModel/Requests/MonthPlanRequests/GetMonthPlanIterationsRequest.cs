﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests
{
    using ServiceStack;

    [Route("/plan/month/iteration/", "GET")]
    public class GetMonthPlanIterationsRequest
    {
        public string TeamGuid { get; set; }

        public string MonthPlanGuid { get; set; }
    }
}
