﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class MonthPlanDirectionRequestValidator : AbstractValidator<GetMonthPlanDirectionRequest>
    {
        public MonthPlanDirectionRequestValidator()
        {
            RuleFor(x => x.BusinessProcessGuid).NotEmpty();
        }
    }
}
