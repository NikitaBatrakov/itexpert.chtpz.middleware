namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class CheckMonthPlanRequestValidator : AbstractValidator<CheckMonthPlanRequest>
    {
        public CheckMonthPlanRequestValidator()
        {
            RuleFor(x => x.Year).GreaterThan(0);
            RuleFor(x => x.Month).GreaterThan(0).LessThan(13);
        }
    }
}