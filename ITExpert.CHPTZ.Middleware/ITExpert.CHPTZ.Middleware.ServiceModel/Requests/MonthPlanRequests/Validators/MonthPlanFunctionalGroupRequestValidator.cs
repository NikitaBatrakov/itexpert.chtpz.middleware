﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class MonthPlanFunctionalGroupRequestValidator : AbstractValidator<GetMonthPlanFunctionalGroupRequest>
    {
        public MonthPlanFunctionalGroupRequestValidator()
        {
            RuleFor(x => x.BusinessProcessGuid).NotEmpty();
            RuleFor(x => x.DirectionGuid).NotEmpty();
        }
    }
}
