﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class MonthPlanVersionRequestValidator : AbstractValidator<GetMonthPlanVersionRequest>
    {
        public MonthPlanVersionRequestValidator()
        {
            RuleFor(x => x.Month).GreaterThan(0);
            RuleFor(x => x.Year).GreaterThan(0);
        }
    }
}
