namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests
{
    using ServiceStack;

    [Route("/plan/month/{MonthPlan}/team/{Team}/actions", "GET")]
    public class GetMonthPlanActionsRequest
    {
        public string MonthPlan { get; set; }
        public string Team { get; set; }
    }
}