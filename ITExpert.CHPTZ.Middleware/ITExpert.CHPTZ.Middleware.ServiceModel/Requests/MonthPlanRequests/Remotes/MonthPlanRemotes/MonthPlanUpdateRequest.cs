namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.MonthPlanRemotes
{
    using ServiceStack;

    [Route("/plan/month/{EntityGuid}/update", "POST")]
    public class MonthPlanUpdateRequest
    {
        public string EntityGuid { get; set; }
    }
}