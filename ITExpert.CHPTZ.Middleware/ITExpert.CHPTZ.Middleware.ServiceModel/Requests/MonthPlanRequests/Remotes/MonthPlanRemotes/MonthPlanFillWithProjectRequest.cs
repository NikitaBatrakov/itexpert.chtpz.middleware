﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.MonthPlanRemotes
{
    using ServiceStack;

    [Route("/plan/month/{EntityGuid}/fill/project", "POST")]
    public class MonthPlanFillWithProjectRequest
    {
        public string EntityGuid { get; set; }
    }
}