namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.MonthPlanRemotes
{
    using ServiceStack;

    [Route("/plan/month/{EntityGuid}/comment", "POST")]
    public class MonthPlanAddCommentRequest
    {
        public string EntityGuid { get; set; }
        public string Team { get; set; }
        public string Comment { get; set; }
    }
}