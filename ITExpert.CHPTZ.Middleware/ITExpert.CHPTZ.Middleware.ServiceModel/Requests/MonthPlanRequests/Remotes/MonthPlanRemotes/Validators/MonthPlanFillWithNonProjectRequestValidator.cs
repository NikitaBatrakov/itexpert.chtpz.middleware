namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.MonthPlanRemotes.Validators
{
    using ServiceStack.FluentValidation;

    public class MonthPlanFillWithNonProjectRequestValidator : AbstractValidator<MonthPlanFillWithNonProjectRequest>
    {
        public MonthPlanFillWithNonProjectRequestValidator()
        {
            RuleFor(x => x.Team).NotEmpty();
        }
    }
}