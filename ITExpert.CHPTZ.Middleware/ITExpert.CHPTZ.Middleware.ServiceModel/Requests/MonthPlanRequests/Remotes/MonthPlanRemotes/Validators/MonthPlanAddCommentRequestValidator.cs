namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.MonthPlanRemotes.Validators
{
    using ServiceStack.FluentValidation;

    public class MonthPlanAddCommentRequestValidator : AbstractValidator<MonthPlanAddCommentRequest>
    {
        public MonthPlanAddCommentRequestValidator()
        {
            RuleFor(x => x.Team).NotEmpty();
            RuleFor(x => x.Comment).NotEmpty();
        }
    }
}