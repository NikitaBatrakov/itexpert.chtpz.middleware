﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.MonthPlanRemotes
{
    using ServiceStack;

    [Route("/plan/month/{EntityGuid}/approve/reports", "POST")]
    public class MonthPlanApproveReportRequest
    {
        public string EntityGuid { get; set; }
    }
}
