namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.MonthPlanRemotes
{
    using ServiceStack;

    [Route("/plan/month/{EntityGuid}/fill/nonproject", "POST")]
    public class MonthPlanFillWithNonProjectRequest
    {
        public string EntityGuid { get; set; }
        public string Team { get; set; }
    }
}