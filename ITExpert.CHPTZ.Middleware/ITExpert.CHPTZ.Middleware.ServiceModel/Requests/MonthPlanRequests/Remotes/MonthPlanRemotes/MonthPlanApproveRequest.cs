﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.MonthPlanRemotes
{
    using ServiceStack;

    [Route("/plan/month/{EntityGuid}/approve", "POST")]
    public class MonthPlanApproveRequest
    {
        public string EntityGuid { get; set; }
    }
}
