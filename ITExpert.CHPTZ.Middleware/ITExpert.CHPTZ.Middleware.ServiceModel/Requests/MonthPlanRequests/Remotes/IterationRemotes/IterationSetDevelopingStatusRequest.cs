namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes
{
    using ServiceStack;

    [Route("/plan/month/iteration/{IterationGuid}/developing", "POST")]
    public class IterationSetDevelopingStatusRequest
    {
        public string IterationGuid { get; set; }

        public int Resource { get; set; }
    }
}