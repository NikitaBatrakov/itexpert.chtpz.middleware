﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes
{
    using ServiceStack;

    [Route("/plan/month/iteration/{IterationGuid}/exclude/check")]
    public class IterationCheckIfMayBeExcludedRequest
    {
        public string IterationGuid { get; set; }
    }
}
