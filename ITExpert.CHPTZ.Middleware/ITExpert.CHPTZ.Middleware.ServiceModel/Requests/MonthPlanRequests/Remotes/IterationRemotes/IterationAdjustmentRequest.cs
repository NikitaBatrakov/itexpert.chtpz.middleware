namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes
{
    using ServiceStack;

    [Route("/plan/month/iteration/{IterationGuid}/adjust", "POST")]
    public class IterationAdjustmentRequest
    {
        public string IterationGuid { get; set; }
    }
}