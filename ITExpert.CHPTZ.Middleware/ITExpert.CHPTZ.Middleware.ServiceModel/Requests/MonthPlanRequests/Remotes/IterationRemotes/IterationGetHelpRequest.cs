﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes
{
    using ServiceStack;

    [Route("/plan/month/iteration/{IterationGuid}/help", "POST")]
    public class IterationGetHelpRequest
    {
        public string ChangeRequest { get; set; }

        public string Team { get; set; }

        public int Resource { get; set; }

        public string IterationGuid { get; set; }
    }
}
