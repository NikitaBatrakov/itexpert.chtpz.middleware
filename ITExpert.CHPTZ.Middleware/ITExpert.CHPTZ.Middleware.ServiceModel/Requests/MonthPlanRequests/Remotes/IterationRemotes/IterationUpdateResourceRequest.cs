namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes
{
    using ServiceStack;

    [Route("/plan/month/iteration/{IterationGuid}/resource", "POST")]
    public class IterationUpdateResourceRequest
    {
        public string IterationGuid { get; set; }

        public int Resource { get; set; }
    }
}