namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes
{
    using ServiceStack;

    [Route("/plan/month/iteration/{IterationGuid}/changeTaskType", "POST")]
    public class IterationChangeTaskTypeRequest
    {
        public string IterationGuid { get; set; }
        public string TaskType { get; set; }
    }
}