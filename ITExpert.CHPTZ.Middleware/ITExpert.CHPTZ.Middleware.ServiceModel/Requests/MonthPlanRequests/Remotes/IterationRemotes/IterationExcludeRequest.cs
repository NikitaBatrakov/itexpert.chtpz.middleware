namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes
{
    using ServiceStack;

    [Route("/plan/month/iteration/{IterationGuid}/exclude", "POST")]
    public class IterationExcludeRequest
    {
        public string IterationGuid { get; set; }
    }
}