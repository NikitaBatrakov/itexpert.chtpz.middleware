namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes.Validators
{
    using ServiceStack.FluentValidation;

    public class IterationUpdateResourceRequestValidator : AbstractValidator<IterationUpdateResourceRequest>
    {
        public IterationUpdateResourceRequestValidator()
        {
            RuleFor(x => x.Resource).GreaterThan(0);
        }
    }
}