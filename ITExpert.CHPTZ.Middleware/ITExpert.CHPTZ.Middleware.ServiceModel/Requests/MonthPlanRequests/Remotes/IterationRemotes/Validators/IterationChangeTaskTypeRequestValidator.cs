namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes.Validators
{
    using ServiceStack.FluentValidation;

    public class IterationChangeTaskTypeRequestValidator : AbstractValidator<IterationChangeTaskTypeRequest>
    {
        public IterationChangeTaskTypeRequestValidator()
        {
            RuleFor(x => x.TaskType).NotEmpty();
        }
    }
}