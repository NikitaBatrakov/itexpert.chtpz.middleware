namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes.Validators
{
    using ServiceStack.FluentValidation;

    public class IterationGetHelpRequestValidator : AbstractValidator<IterationGetHelpRequest>
    {
        public IterationGetHelpRequestValidator()
        {
            RuleFor(x => x.ChangeRequest).NotEmpty();
            RuleFor(x => x.Team).NotEmpty();
            RuleFor(x => x.Resource).NotEmpty();
        }
    }
}