namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes.Validators
{
    using ServiceStack.FluentValidation;

    public class IterationSetDevelopingStatusRequestValidator : AbstractValidator<IterationSetDevelopingStatusRequest>
    {
        public IterationSetDevelopingStatusRequestValidator()
        {
            RuleFor(x => x.Resource).GreaterThan(0);
        }
    }
}