﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.ChangeRequestRemotes
{
    using ServiceStack;

    [Route("/plan/month/changeRequest/{EntityGuid}/include")]
    public class ChangeRequestIncludeInPlanRequest
    {
        public string EntityGuid { get; set; }
        public string MonthPlan { get; set; }
        public string Team { get; set; }
        public string TaskType { get; set; }
        public int Resource { get; set; }
    }
}
