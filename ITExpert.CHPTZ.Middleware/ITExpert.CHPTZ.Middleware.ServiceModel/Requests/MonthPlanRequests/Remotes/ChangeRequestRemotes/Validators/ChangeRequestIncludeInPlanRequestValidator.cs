﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.ChangeRequestRemotes.Validators
{
    using ServiceStack.FluentValidation;

    public class ChangeRequestIncludeInPlanRequestValidator : AbstractValidator<ChangeRequestIncludeInPlanRequest>
    {
        public ChangeRequestIncludeInPlanRequestValidator()
        {
            RuleFor(x => x.MonthPlan).NotEmpty();
            RuleFor(x => x.Team).NotEmpty();
            RuleFor(x => x.TaskType).NotEmpty();
            RuleFor(x => x.Resource).GreaterThan(0);
        }
    }
}