namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests
{
    using ServiceStack;

    [Route("/plan/month/tree/{BusinessProcessGuid}/{DirectionGuid}", "GET")]
    public class GetMonthPlanFunctionalGroupRequest
    {
        public string BusinessProcessGuid { get; set; }
        public string DirectionGuid { get; set; }
    }
}