﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ActivityJournalRequests
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RegisterRequests;

    using ServiceStack;
    using ServiceStack.FluentValidation;

    [Route("/register/ActivityJournal/orphans", "GET, POST")]
    public class GetActivityJournalRequest : BaseGetRequest
    {
         
    }
}