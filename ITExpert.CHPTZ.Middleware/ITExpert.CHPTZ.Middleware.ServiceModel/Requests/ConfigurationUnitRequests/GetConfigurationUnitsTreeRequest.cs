﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ConfigurationUnitRequests
{
    using System.Collections.Generic;

    using ServiceStack;

    [Route("/ConfigurationUnits/tree", "POST")]
    public class GetConfigurationUnitsTreeRequest
    {
        public string RelationTypeGuid { get; set; }

        public IEnumerable<string> ConfigurationUnitsGuids { get; set; }
    }
}
