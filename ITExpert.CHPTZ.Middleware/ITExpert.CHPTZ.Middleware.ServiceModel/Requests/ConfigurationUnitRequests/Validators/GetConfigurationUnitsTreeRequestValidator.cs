﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ConfigurationUnitRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class GetConfigurationUnitsTreeRequestValidator : AbstractValidator<GetConfigurationUnitsTreeRequest>
    {
        public GetConfigurationUnitsTreeRequestValidator()
        {
            RuleFor(x => x.ConfigurationUnitsGuids).NotEmpty();
            RuleFor(x => x.RelationTypeGuid).NotEmpty();
        }
    }
}
