﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    using ServiceStack;

    [Route("/cart", "POST")]
    public class CreateCartRequest
    {
        public IEnumerable<CartElement> CartElements { get; set; }
        public IEnumerable<TemplateAttributes> Attributes { get; set; }
    }
}
