namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests
{
    using ServiceStack;

    [Route("/cart/order/ServiceRequest/{Guid}", "GET")]
    public class GetServiceRequestItemsRequest
    {
        public string Guid { get; set; }
    }
}