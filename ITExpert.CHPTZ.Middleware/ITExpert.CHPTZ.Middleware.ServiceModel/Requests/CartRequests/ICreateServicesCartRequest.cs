namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public interface ICreateServicesCartRequest : IUpdateServiceCartRequest
    {
        IEnumerable<TemplateAttributes> Attributes { get; }
    }

    public interface IUpdateServiceCartRequest
    {
        ServiceRequest ServiceRequest { get; }
        IEnumerable<CartElement> CartElements { get; }
    }
}