namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    using ServiceStack;

    [Route("/cart/approve", "POST")]
    public class ApproveCartRequest : IUpdateServiceCartRequest
    {
        public ServiceRequest ServiceRequest { get; set; }
        public IEnumerable<CartElement> CartElements { get; set; }
    }
}