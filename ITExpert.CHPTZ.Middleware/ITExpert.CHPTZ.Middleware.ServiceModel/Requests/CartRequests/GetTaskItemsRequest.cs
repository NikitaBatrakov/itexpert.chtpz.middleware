namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests
{
    using ServiceStack;

    [Route("/cart/tasks/{Guid}", "GET")]
    public class GetTaskItemsRequest
    {
        public string Guid { get; set; }
    }
}