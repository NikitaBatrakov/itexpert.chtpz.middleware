﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests
{
    using ServiceStack;

    [Route("/services", "GET")]
    public class GetServiceCatalogueRequest
    {
        public string EmployeeGuid { get; set; }
    }
}
