﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class ApproveCartRequestValidator : AbstractValidator<ApproveCartRequest>
    {
        public ApproveCartRequestValidator()
        {
            RuleFor(x => x.CartElements).NotEmpty();
        }
    }
}
