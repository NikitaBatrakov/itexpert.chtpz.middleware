﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class SaveCartRequestValidator : AbstractValidator<CreateCartRequest>
    {
        public SaveCartRequestValidator()
        {
            RuleFor(x => x.CartElements).NotEmpty();
        }
    }
}
