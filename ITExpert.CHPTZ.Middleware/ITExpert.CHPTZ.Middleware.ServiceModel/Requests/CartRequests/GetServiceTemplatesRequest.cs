namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests
{
    using ServiceStack;

    [Route("/services/{ServiceGuid}/templates", "GET")]
    public class GetServiceTemplatesRequest
    {
        public string ServiceGuid { get; set; }
    }
}