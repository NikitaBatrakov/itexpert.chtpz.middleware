namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests
{
    using ServiceStack;

    [Route("/cart/order/agreements/{Guid}", "GET")]
    public class GetAgreementsItemsRequest
    {
        public string Guid { get; set; }
    }
}