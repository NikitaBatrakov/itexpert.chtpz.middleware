﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.FileRequests
{
    using ServiceStack;
    using ServiceStack.FluentValidation;

    [Route("/files/{Name}/{Extension}", "GET")]
    public class DownloadFileRequest
    {
        public string Name { get; set; }

        public string Extension { get; set; }
    }
}