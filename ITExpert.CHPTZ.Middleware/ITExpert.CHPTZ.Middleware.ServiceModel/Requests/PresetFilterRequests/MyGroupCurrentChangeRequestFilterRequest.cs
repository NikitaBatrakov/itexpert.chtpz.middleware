﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.PresetFilterRequests
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;

    using ServiceStack;

    [Route("/entity/changeRequest/filter/groupsCurrent", "GET, POST")]
    public class MyGroupCurrentChangeRequestFilterRequest : BaseGetRequest
    {
        
    }
}
