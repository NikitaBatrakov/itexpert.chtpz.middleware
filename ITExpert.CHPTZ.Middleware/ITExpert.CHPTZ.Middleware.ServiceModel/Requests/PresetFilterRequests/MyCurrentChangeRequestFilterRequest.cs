﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.PresetFilterRequests
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;

    using ServiceStack;

    [Route("/entity/changeRequest/filter/myCurrent", "GET, POST")]
    public class MyCurrentChangeRequestFilterRequest : BaseGetRequest
    {
    }
}
