namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ExportRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class XlsExportRequestValidator : AbstractValidator<ExportTableRequest>
    {
        public XlsExportRequestValidator()
        {
            RuleFor(x => x.Table).NotNull();
            RuleFor(x => x.Table.Rows).NotNull().NotEmpty();
        }
    }
}