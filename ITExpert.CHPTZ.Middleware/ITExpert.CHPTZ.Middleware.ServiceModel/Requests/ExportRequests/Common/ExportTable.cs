namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ExportRequests.Common
{
    public class ExportTable
    {
        public ExportTableCell[] Head { get; set; }
        public ExportTableCell[][] Rows { get; set; }
    }

    public class ExportTableCell
    {
        public string Value { get; set; }

        public string Url { get; set; }
    }
}