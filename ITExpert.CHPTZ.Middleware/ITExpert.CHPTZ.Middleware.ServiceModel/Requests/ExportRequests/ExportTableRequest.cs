﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ExportRequests
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ExportRequests.Common;

    using ServiceStack;

    [Route("/export/{Extension}")]
    public class ExportTableRequest
    {
        public string Extension { get; set; }
        public ExportTable Table { get; set; }
    }
}