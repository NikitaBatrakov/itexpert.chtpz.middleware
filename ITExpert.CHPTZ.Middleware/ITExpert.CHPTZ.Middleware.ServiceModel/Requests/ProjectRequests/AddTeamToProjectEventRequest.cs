namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/event/{Event}/team", "POST")]
    public class AddTeamToProjectEventRequest
    {
        public string Project { get; set; }
        public string Event { get; set; }
        public string Team { get; set; }
    }
}