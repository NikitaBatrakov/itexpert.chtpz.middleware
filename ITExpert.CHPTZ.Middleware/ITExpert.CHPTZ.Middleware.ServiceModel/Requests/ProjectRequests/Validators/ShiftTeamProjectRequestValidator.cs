namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class ShiftTeamProjectRequestValidator : AbstractValidator<ShiftTeamProjectRequest>
    {
        public ShiftTeamProjectRequestValidator()
        {
            RuleFor(x => x.Months).GreaterThan(0).LessThan(13);
        }
    }
}