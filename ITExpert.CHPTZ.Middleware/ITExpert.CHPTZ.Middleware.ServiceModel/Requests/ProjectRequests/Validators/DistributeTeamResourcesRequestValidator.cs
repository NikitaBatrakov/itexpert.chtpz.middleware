namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class DistributeTeamResourcesRequestValidator : AbstractValidator<DistributeTeamResourcesRequest>
    {
        public DistributeTeamResourcesRequestValidator()
        {
            RuleFor(x => x.Resources).NotNull().NotEmpty();
            RuleFor(x => x.Resources.Length).LessThan(13);
        }
    }
}