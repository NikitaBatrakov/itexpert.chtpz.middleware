namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests.Validators
{
    using ServiceStack.FluentValidation;

    public class ShiftProjectRequestValidator : AbstractValidator<ShiftProjectRequest>
    {
        public ShiftProjectRequestValidator()
        {
            RuleFor(x => x.Months).GreaterThan(0).LessThan(13);
        }
    }
}