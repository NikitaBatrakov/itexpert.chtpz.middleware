namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/projectTeam/{ProjectTeam}/distribute", "POST")]
    public class DistributeTeamResourcesRequest
    {
        public string Project { get; set; }
        public string ProjectTeam { get; set; }
        public int[] Resources { get; set; }
    }
}