namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/event/{Event}/team/{Team}/remove", "POST")]
    public class RemoveTeamFromProjectEventRequest
    {
        public string Project { get; set; }
        public string Event { get; set; }
        public string Team { get; set; }
    }
}