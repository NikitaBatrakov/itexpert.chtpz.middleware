namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/event", "POST")]
    public class CreateProjectEventRequest
    {
        public string Project { get; set; }
    }
}