namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/team/{Team}/projectTeam/change", "POST")]
    public class ChangeProjectTeamRequest
    {
        public string Project { get; set; }
        public string Team { get; set; }
        public string ProjectTeam { get; set; }
    }
}