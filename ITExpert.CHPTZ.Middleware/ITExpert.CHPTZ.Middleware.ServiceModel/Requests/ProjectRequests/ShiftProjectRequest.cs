namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/shift", "POST")]
    public class ShiftProjectRequest
    {
        public string Project { get; set; }
        public int Months { get; set; }
    }
}