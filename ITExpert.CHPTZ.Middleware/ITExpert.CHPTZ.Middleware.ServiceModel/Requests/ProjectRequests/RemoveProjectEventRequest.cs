namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/event/{Event}/remove", "POST")]
    public class RemoveProjectEventRequest
    {
        public string Project { get; set; }
        public string Event { get; set; }
    }
}