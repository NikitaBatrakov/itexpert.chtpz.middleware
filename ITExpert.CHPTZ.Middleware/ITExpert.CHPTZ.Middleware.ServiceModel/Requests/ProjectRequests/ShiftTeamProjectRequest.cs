namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/team/{Team}/shift", "POST")]
    public class ShiftTeamProjectRequest
    {
        public string Project { get; set; }
        public string Team { get; set; }
        public int Months { get; set; }
    }
}