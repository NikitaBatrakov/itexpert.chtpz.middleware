﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/release", "POST")]
    public class ReleaseProjectRequest
    {
        public string Project { get; set; }
    }
}