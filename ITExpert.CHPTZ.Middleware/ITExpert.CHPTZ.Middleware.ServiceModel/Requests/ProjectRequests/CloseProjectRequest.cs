namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests
{
    using ServiceStack;

    [Route("/project/{Project}/close", "POST")]
    public class CloseProjectRequest
    {
        public string Project { get; set; } 
    }
}