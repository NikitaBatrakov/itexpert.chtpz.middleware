﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RegisterRequests
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;

    using ServiceStack;

    [Route("/register/{Entity}", "GET, POST")]
    public class GetRegisterRequest : BaseGetRequest
    {
    }
}
