namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.DefaultFilterRequests
{
    using ServiceStack;

    [Route("/entity/{Entity}/view/{ViewGuid}/place/{PlaceGuid}/filter/default/", "POST")]
    public class SetDefaultFilterRequest
    {
        public string Entity { get; set; }
        public string ViewGuid { get; set; }
        public string PlaceGuid { get; set; }
    }
}