﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.DefaultFilterRequests
{
    using ServiceStack;
    using ServiceStack.FluentValidation;

    [Route("/entity/{Entity}/view/place/{PlaceGuid}/filter/default", "GET")]
    public class GetDefaultFilterRequest
    {
        public string Entity { get; set; }
        public string PlaceGuid { get; set; }
    }
}