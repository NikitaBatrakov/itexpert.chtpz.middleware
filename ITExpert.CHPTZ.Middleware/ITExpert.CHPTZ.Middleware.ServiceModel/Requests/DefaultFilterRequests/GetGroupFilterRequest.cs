namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.DefaultFilterRequests
{
    using ServiceStack;

    [Route("/entity/{Entity}/view/filter/group/default", "GET")]
    public class GetGroupFilterRequest
    {
        public string Entity { get; set; }
    }
}