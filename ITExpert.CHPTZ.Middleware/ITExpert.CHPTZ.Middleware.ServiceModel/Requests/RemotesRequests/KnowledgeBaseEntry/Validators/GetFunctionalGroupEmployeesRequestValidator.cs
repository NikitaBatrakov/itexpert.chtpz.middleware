namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.KnowledgeBaseEntry.Validators
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.KnowledgeBaseEntry;

    using ServiceStack.FluentValidation;

    public class GetFunctionalGroupEmployeesRequestValidator : AbstractValidator<GetFunctionalGroupEmployeesRequest>
    {
        public GetFunctionalGroupEmployeesRequestValidator()
        {
            RuleFor(x => x.FunctionalGroup).NotEmpty();
        }
    }
}