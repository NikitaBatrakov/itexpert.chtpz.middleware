namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.KnowledgeBaseEntry
{
    using ServiceStack;

    [Route("/entity/KnowledgeBaseEntry/employees", "GET")]
    public class GetFunctionalGroupEmployeesRequest
    {
        public string FunctionalGroup { get; set; }
    }
}