namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Service.Validators
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Service;

    using ServiceStack.FluentValidation;

    public class GetAvailableEmployeeServicesRequestValidator : AbstractValidator<GetAvailableEmployeeServicesRequest>
    {
        public GetAvailableEmployeeServicesRequestValidator()
        {
            RuleFor(x => x.Employee).NotEmpty();
        }
    }
}