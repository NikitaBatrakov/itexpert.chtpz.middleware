namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Service
{
    using ServiceStack;

    [Route("/entity/Service/services", "GET")]
    public class GetAvailableEmployeeServicesRequest
    {
        public string Employee { get; set; }
    }
}