namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.ServiceRequest
{
    using ServiceStack;

    [Route("/entity/ServiceRequest/functionalGroups/{ServiceGuid}")]
    public class GetAvailableFunctionalGroupsRequest
    {
        public string ServiceGuid { get; set; }
    }
}