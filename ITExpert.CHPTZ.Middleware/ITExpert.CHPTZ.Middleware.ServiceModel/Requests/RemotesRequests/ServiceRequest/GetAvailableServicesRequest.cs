namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.ServiceRequest
{
    using System;

    using ServiceStack;

    [Route("/entity/ServiceRequest/services", "GET")]
    public class GetAvailableServicesRequest
    {
        public string Employee { get; set; }
        public DateTime Date { get; set; }
    }
}