namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.ServiceRequest
{
    using System;

    using ServiceStack;

    [Route("/entity/ServiceRequest/sla", "GET")]
    public class GetAvailableSlaRequest
    {
        public string Employee { get; set; }
        public DateTime Date { get; set; }
    }
}