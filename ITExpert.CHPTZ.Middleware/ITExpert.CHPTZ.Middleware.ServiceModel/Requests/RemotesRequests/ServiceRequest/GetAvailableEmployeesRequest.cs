namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.ServiceRequest
{
    using ServiceStack;

    [Route("/entity/ServiceRequest/employees", "GET")]
    public class GetAvailableEmployeesRequest
    {
        public string Service { get; set; }
        public string FunctionalGroup { get; set; }
    }
}