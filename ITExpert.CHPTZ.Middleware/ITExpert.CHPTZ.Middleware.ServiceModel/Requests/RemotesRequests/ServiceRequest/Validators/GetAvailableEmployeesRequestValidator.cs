namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.ServiceRequest.Validators
{
    using ServiceStack.FluentValidation;

    public class GetAvailableEmployeesRequestValidator : AbstractValidator<ServiceRequest.GetAvailableEmployeesRequest>
    {
        public GetAvailableEmployeesRequestValidator()
        {
            RuleFor(x => x.Service);
            RuleFor(x => x.FunctionalGroup);
        }
    }
}