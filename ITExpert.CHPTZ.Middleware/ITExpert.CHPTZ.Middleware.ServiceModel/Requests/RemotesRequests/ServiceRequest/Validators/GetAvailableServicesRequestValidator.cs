namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.ServiceRequest.Validators
{
    using System;

    using ServiceStack.FluentValidation;

    public class GetAvailableServicesRequestValidator : AbstractValidator<ServiceRequest.GetAvailableServicesRequest>
    {
        public GetAvailableServicesRequestValidator()
        {
            RuleFor(x => x.Employee).NotEmpty();
            RuleFor(x => x.Date).NotEqual(new DateTime());
        }
    }
}