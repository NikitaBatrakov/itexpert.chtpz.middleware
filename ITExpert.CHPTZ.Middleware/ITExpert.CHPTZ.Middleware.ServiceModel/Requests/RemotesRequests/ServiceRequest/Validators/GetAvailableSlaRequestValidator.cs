namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.ServiceRequest.Validators
{
    using System;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.ServiceRequest;

    using ServiceStack.FluentValidation;

    public class GetAvailableSlaRequestValidator : AbstractValidator<GetAvailableSlaRequest>
    {
        public GetAvailableSlaRequestValidator()
        {
            RuleFor(x => x.Employee).NotEmpty();
            RuleFor(x => x.Date).NotEqual(new DateTime());
        }
    }
}