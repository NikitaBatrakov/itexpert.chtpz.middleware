namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.ServiceRequest.Validators
{
    using ServiceStack.FluentValidation;

    public class GetAvailableFunctionalGroupsRequestValidator : AbstractValidator<ServiceRequest.GetAvailableFunctionalGroupsRequest>
    {
        public GetAvailableFunctionalGroupsRequestValidator()
        {
            RuleFor(x => x.ServiceGuid).NotEmpty();
        }
    }
}