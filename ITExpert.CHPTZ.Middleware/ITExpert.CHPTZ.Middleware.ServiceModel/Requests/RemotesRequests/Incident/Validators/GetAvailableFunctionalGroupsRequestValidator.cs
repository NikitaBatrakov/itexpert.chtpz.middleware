namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident.Validators
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident;

    using ServiceStack.FluentValidation;

    public class GetAvailableFunctionalGroupsRequestValidator : AbstractValidator<GetAvailableFunctionalGroupsRequest>
    {
        public GetAvailableFunctionalGroupsRequestValidator()
        {
            RuleFor(x => x.ServiceGuid).NotEmpty();
        }
    }
}