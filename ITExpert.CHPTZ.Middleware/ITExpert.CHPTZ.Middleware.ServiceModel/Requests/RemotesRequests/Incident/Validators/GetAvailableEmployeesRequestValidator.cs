namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident.Validators
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident;

    using ServiceStack.FluentValidation;

    public class GetAvailableEmployeesRequestValidator : AbstractValidator<GetAvailableEmployeesRequest>
    {
        public GetAvailableEmployeesRequestValidator()
        {
            RuleFor(x => x.Service);
            RuleFor(x => x.FunctionalGroup);
        }
    }
}