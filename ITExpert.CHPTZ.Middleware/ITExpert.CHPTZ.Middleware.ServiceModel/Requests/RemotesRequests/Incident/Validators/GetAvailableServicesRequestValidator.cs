namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident.Validators
{
    using System;

    using ServiceStack.FluentValidation;

    public class GetAvailableServicesRequestValidator : AbstractValidator<Incident.GetAvailableServicesRequest>
    {
        public GetAvailableServicesRequestValidator()
        {
            RuleFor(x => x.Employee).NotEmpty();
            RuleFor(x => x.Date).NotEqual(new DateTime());
        }
    }
}