namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident.Validators
{
    using System;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident;

    using ServiceStack.FluentValidation;

    public class GetAvailableSlaRequestValidator : AbstractValidator<GetAvailableIncidentSlaRequest>
    {
        public GetAvailableSlaRequestValidator()
        {
            RuleFor(x => x.Employee).NotEmpty();
            RuleFor(x => x.Date).NotEqual(new DateTime());
        }
    }
}