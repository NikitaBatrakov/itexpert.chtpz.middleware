namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident
{
    using System;

    using ServiceStack;

    [Route("/entity/Incident/sla", "GET")]
    public class GetAvailableIncidentSlaRequest
    {
        public string Employee { get; set; }
        public DateTime Date { get; set; }
    }
}