namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident
{
    using System;

    using ServiceStack;

    [Route("/entity/Incident/services", "GET")]
    public class GetAvailableServicesRequest
    {
        public string Employee { get; set; }
        public DateTime Date { get; set; }
    }
}