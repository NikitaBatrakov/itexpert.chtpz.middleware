﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident
{
    using ServiceStack;

    [Route("/entity/Incident/functionalGroups/{ServiceGuid}")]
    public class GetAvailableFunctionalGroupsRequest
    {
        public string ServiceGuid { get; set; }
    }
}
