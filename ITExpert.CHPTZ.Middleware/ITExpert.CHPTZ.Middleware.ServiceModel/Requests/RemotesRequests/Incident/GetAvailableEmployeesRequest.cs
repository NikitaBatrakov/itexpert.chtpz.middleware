﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident
{
    using ServiceStack;

    [Route("/entity/Incident/employees", "GET")]
    public class GetAvailableEmployeesRequest
    {
        public string Service { get; set; }
        public string FunctionalGroup { get; set; }
    }
}
