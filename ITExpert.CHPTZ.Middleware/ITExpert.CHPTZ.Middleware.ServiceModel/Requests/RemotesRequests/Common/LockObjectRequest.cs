﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Common
{
    using ServiceStack;

    [Route("/entity/{Entity}/lock", "POST")]
    public class LockObjectRequest
    {
        public string Entity { get; set; }
        public string Guid { get; set; }
    }
}
