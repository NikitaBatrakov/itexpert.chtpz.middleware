namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Common
{
    using ServiceStack;

    [Route("/entity/{EntityTypeName}/closing/{EntityGuid}", "GET, POST")]
    public class OnClosureRequest
    {
        public string EntityTypeName { get; set; }
        public string EntityGuid { get; set; }
    }
}