namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Common
{
    using ServiceStack;

    [Route("/entity/{Entity}/metadata/contextual", "GET")]
    public class GetContextualMetadataRequest
    {
        public string Entity { get; set; }
    }

    [Route("/entity/{Entity}/{Guid}/metadata/contextual", "GET")]
    public class GetObjectContextualMetadataRequest
    {
        public string Entity { get; set; }
        public string Guid { get; set; }
    }
}