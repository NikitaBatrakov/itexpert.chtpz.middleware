﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Common
{
    using ServiceStack;

    [Route("/entity/{Entity}/{EntityGuid}/OnChange/", "POST")]
    public class OnPropertyChangedFunctionRequest
    {
        public string Entity { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
        public string EntityGuid { get; set; }
    }
}
