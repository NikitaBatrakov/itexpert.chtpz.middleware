﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Common.Validators
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Common;

    using ServiceStack.FluentValidation;

    public class OnClosureRequestValidator : AbstractValidator<OnClosureRequest>
    {
        public OnClosureRequestValidator()
        {
        }
    }
}
