﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Common.Validators
{
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Common;

    using ServiceStack.FluentValidation;

    public class LockObjectRequestValidator : AbstractValidator<LockObjectRequest>
    {
        public LockObjectRequestValidator()
        {
            RuleFor(x => x.Guid).NotEmpty();
        }
    }
}
