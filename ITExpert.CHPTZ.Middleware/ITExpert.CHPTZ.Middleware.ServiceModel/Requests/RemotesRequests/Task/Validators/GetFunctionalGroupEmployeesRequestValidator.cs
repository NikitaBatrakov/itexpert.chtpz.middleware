﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Task.Validators
{
    using ServiceStack.FluentValidation;
    public class GetFunctionalGroupEmployeesRequestValidator : AbstractValidator<GetFunctionalGroupEmployeesRequest>
    {
        public GetFunctionalGroupEmployeesRequestValidator()
        {
        }
    }
}
