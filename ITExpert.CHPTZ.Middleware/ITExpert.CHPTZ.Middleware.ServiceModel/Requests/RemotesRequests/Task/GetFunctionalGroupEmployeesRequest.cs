namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Task
{
    using ServiceStack;

    [Route("/entity/Task/employees", "GET")]
    public class GetFunctionalGroupEmployeesRequest
    {
        public string FunctionalGroup { get; set; }
    }
}