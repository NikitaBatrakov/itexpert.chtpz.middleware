﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Requests.LoggingRequests
{
    using ServiceStack;

    [Route("/log", "POST")]
    public class WriteLogRequest
    {
        public string Text { get; set; }
    }
}