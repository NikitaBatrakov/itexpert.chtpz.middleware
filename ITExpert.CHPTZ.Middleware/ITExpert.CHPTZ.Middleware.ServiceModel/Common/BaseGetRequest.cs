﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Common
{
    public class BaseGetRequest
    {
        public string Entity { get; set; }
        public string Fields { get; set; } = "";
        public int Offset { get; set; } = 0;
        public int Limit { get; set; } = 0;
        public string Filter { get; set; } = "";
        public DataAccess.MQueries.Types.FilterElement ComplexFilter { get; set; }
        public string Sort { get; set; } = "";
    }
}
