﻿namespace ITExpert.CHPTZ.Middleware.ServiceModel.Common
{
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public interface IResponsible
    {
        ServiceResult ServiceResult { get; set; }
    }
}
