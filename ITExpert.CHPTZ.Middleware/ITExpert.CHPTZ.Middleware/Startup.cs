﻿using ITExpert.CHPTZ.Middleware;

using Microsoft.Owin;

[assembly: OwinStartup(typeof(Startup), "Configuration")]
namespace ITExpert.CHPTZ.Middleware
{
    using ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Notifications;

    using Microsoft.AspNet.SignalR;

    using Owin;

    /// <summary>
    /// SignalR entry point
    /// </summary>
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            NotificationsPoller.Start();
            GlobalHost.HubPipeline.AddModule(new SignalRExceptionHandler());
            app.MapSignalR(new HubConfiguration { EnableJSONP = true, EnableDetailedErrors = true });
        }
    }
}