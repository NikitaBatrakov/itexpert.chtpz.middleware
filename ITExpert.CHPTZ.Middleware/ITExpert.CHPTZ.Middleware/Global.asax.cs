﻿using System;
using ServiceStack;

namespace ITExpert.CHPTZ.Middleware
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            var Licens = "2779-e1JlZjoyNzc5LE5hbWU6WkFPIEl0IEV4cGVydCxUeXBlOkluZGllLEhhc2g6RWd1eXd4Q3UrWkNlUmdCZU12UldWcEJoeGJ3cWpNeVVDSXJnOUU2ZVBjcG5PcHRzUU5BWG1teTZHb2UzUzczT20vSzU4YTNlaCs4UHYrc2RTV0c4Yy9WRnN4K3k0MWxDN05DVmVyTEV3VkZyaVhlcUdqQmQwR3hrMFYxMkNTajlTdEptYVdwYkJRZEJmRExuZ3ZSclBJbEpQaVczVGZLdGNFZ056QmRiOEVFPSxFeHBpcnk6MjAxNi0wNy0xOX0=";
            Licensing.RegisterLicense(Licens);
            new AppHost().Init();
        }
    }
}