﻿namespace ITExpert.CHPTZ.Middleware
{
    using System;
    using System.Net;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;

    using Funq;
    using NLog;

    using ServiceStack;
    using ServiceStack.Text;
    using ServiceStack.Web;

    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests.Validators;

    using ServiceStack.Validation;
    using Common.Extensions;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class AppHost : AppHostBase
    {
        private readonly IEnumerable<string> tokenFreeUrls = new[]
                                                             {
                                                                 "/signin",
                                                                 "/metadata",
                                                                 "/files",
                                                                 "/signalr",
                                                                 "/test",
                                                                 "/log",
                                                                 "/postman"
                                                             };

        /// <summary>
        /// Default constructor.
        /// Base constructor requires a name and assembly to locate web service classes. 
        /// </summary>
        public AppHost()
            : base("ITExpert.CHPTZ.Middleware", typeof(RestfulService).Assembly) { }

        /// <summary>
        /// Application specific configuration
        /// This method should initialize any IoC resources utilized by your web service classes.
        /// </summary>
        /// <param name="container"></param>
        public override void Configure(Container container)
        {
            ConfigureJson();
            WireDependencies(container);
            AddPlugins();
            SetConfig(GetHostConfig());
            PreRequestFilters.Add(OriginFilter);
            PreRequestFilters.Add(TestTokenFilter);
            PreRequestFilters.Add(CheckTokenFilter);
            GlobalResponseFilters.Add(BackendExceptionsFilter);
            OnEndRequestCallbacks.Add(LoggerCallback);
        }        

        private static void ConfigureJson()
        {
            JsConfig<DateTime>.SerializeFn = x => x.ToUtcIsoString();
            JsConfig<DateTime?>.SerializeFn = x => x?.ToUtcIsoString();
            JsConfig.DateHandler = DateHandler.ISO8601;
            JsConfig.ExcludeTypeInfo = true;
            JsConfig.EmitCamelCaseNames = true;
        }

        private static void WireDependencies(Container container)
        {
            container.RegisterValidators(typeof(CreateEntityRequestValidator).Assembly);
        }

        private void AddPlugins()
        {
            Plugins.Add(GetCorsPlugin());
            Plugins.Add(new ValidationFeature());
            Plugins.Add(new PostmanFeature());
        }

        private static CorsFeature GetCorsPlugin()
        {
            return new CorsFeature(allowedMethods: "GET, POST, PUT, DELETE, PATCH, OPTIONS",
                            allowedHeaders: "Content-Type, Authorization, Token", allowCredentials: true,
                            allowedOrigins: "");
        }

        private static HostConfig GetHostConfig()
        {
            return new HostConfig
                   {
                       DebugMode = true,
                       MapExceptionToStatusCode =
                       {
                           {
                               typeof(AuthenticationException),
                               (int)HttpStatusCode.Unauthorized
                           },
                           {
                               typeof(TypeLoadException),
                               (int)HttpStatusCode.NotFound
                           },
                           {
                               typeof(TimeoutException),
                               (int)HttpStatusCode.RequestTimeout
                           },
                       }
                   };
        }

        private static void OriginFilter(IRequest request, IResponse response)
        {
            var origin = request.Headers.Get("Origin");
            if (origin != null)
            {
                response.AddHeader(HttpHeaders.AllowOrigin, origin);
            }
        }

        //TODO: For testing only - Remove this method in production
        private static void TestTokenFilter(IRequest request, IResponse response)
        {
            if (request.QueryString.AllKeys.Contains("test")) request.Headers.Add("Token", "test");
            else if (request.QueryString.AllKeys.Contains("admin")) request.Headers.Add("Token", "admin");
        }

        private void CheckTokenFilter(IRequest request, IResponse response)
        {
            var logger = LogManager.GetLogger("auth");
            if (IsTokenValid(request))
            {
                return;
            }

            logger.Error($"No token specified for {request.AbsoluteUri}");
            response.StatusCode = (int)HttpStatusCode.Unauthorized;
            response.StatusDescription = "Unauthorized";
            response.EndRequest();
        }

        private bool IsTokenValid(IRequest request)
        {
            foreach (var tokenFreeUrl in tokenFreeUrls)
            {
                if (request.PathInfo.ToLowerInvariant().StartsWith(tokenFreeUrl)) return true;
            }

            return  
                request.Verb == "OPTIONS" || 
                !string.IsNullOrEmpty(request.GetHeader("Token"));
        }

        private static void BackendExceptionsFilter(IRequest request, IResponse response, object obj)
        {
            var responsible = response.Dto as IResponsible;
            if (responsible?.ServiceResult == null) return;
            if (responsible.ServiceResult.IsOk)
            {
                responsible.ServiceResult = null;
                return;
            }
            response.StatusCode = 500;
            response.Dto = responsible.ServiceResult;

            //throw new BackendException(responsible.ServiceResult);
        }

        private static void LoggerCallback(IRequest request)
        {
            if (request.Verb == "OPTIONS") return;
            var input = request.Dto?.Dump() ?? "NO INPUT";
            string output;
            try
            {
                output = request.Response.Dto.Dump();
            }
            catch (NullReferenceException ex)
            {
                //Some ServiceStack issue
                output = "Not a text response";
            }

            var status = $"{request.Response.StatusCode} ({request.Response.StatusDescription})";
            var url = $"{request.AbsoluteUri} ({request.Verb})";
            var logger = LogManager.GetLogger("frontend");
            var message = new LogEventInfo
                          {
                              LoggerName = "frontend",
                              Level = LogLevel.Info,
                              Message = url,
                              Properties =
                              {
                                  {"Input", input},
                                  {"Output", output},
                                  {"Status", status},
                                  {"Url", url}
                              }
                          };

            if (request.Response.StatusCode >= 400)
            {
                message.Level = LogLevel.Error;
            }
            logger.Log(message);
        }

        public override void OnExceptionTypeFilter(Exception ex, ResponseStatus responseStatus)
        {
            if (ex is AuthenticationException) return;

            LogManager.GetLogger("exceptions").Error(ex);

            if (OnException<TimeoutException>(ex, responseStatus, "[1C] 1C is not responding.")) return;
            if (OnException<WebException>(ex, responseStatus, "[1C, M] 1C endpoint not found - " +
                "Middleware service reference to 1C is probably out of date, " +
                "or 1C web service changed its address. ")) return;
            if (OnException<FaultException>(ex, responseStatus, "[M, 1C] Service connection to 1C " +
                "is abrupted with exception.")) return;
            //if (OnException<BackendException>(ex, responseStatus, "[F, M, 1C] 1C web service function returned an error")) return;
        }

        private static bool OnException<T>(Exception ex, ResponseStatus status, string message = null) where T : Exception
        {
            var targetException = ex as T;
            if (targetException == null)
            {
                return false;
            }
            status.ErrorCode = targetException.ToErrorCode();
            status.Message = message;
            status.StackTrace = null;
            status.Errors = new List<ResponseError>
                            {
                                new ResponseError
                                {
                                    ErrorCode = targetException.ToErrorCode(),
                                    Message = targetException.Message
                                }
                            };
            return true;
        }
    }
}