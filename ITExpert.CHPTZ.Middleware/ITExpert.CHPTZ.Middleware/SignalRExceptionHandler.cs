﻿namespace ITExpert.CHPTZ.Middleware
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR.Hubs;

    using NLog;

    public class SignalRExceptionHandler : HubPipelineModule
    {
        #region Overrides of HubPipelineModule

        /// <summary>
        /// This is called when an uncaught exception is thrown by a server-side hub method or the incoming component of a
        ///             module added later to the <see cref="T:Microsoft.AspNet.SignalR.Hubs.IHubPipeline"/>. Observing the exception using this method will not prevent
        ///             it from bubbling up to other modules.
        /// </summary>
        /// <param name="exceptionContext">Represents the exception that was thrown during the server-side invocation.
        ///             It is possible to change the error or set a result using this context.
        ///             </param><param name="invokerContext">A description of the server-side hub method invocation.</param>
        protected override void OnIncomingError(ExceptionContext exceptionContext, IHubIncomingInvokerContext invokerContext)
        {
            //LogManager.GetCurrentClassLogger().Error(exceptionContext.Error);
            LogManager.GetLogger("signalr").Error(exceptionContext.Error);
            base.OnIncomingError(exceptionContext, invokerContext);
        }

        #endregion
    }
}