namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Auth
{
    using System;

    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    using NLog;

    using ServiceStack;
    using ServiceStack.Redis;

    public class SessionProvider : ISessionProvider
    {
        public string Token { get; }
        public UserSession Session { get; }

        public SessionProvider(string token)
        {
            Token = token;
            Session = GetSession(token);
        }

        private static UserSession GetSession(string token)
        {
            using (var redisManager = new PooledRedisClientManager())
            using (var redis = redisManager.GetClient())
            {
                try
                {
                    var session = ChooseSession(redis, token);
                    if (session == null)
                    {
                        throw new AuthenticationException($"Session for token '{token}' not found.");
                    }
                    if (string.IsNullOrEmpty(session.EmployeeGuid))
                    {
                        throw new Exception("User authenticated but EmployeeGuid not set. Try to sign out and sign in again");
                    }
                    redis.ExpireEntryIn(token, new TimeSpan(48, 0, 0));
                    session.User = new User { Employee = new Link1C(session.EmployeeGuid, "���������") };
                    return session;
                }
                catch(Exception e)
                {
                    throw new AuthenticationException(e.Message);
                }
            }
        }

        private static UserSession ChooseSession(IRedisClient redis, string token)
        {
            //TODO: For testing only - Remove this in production (method should return redis.As<UserSession>().GetValue(token);)

            switch (token.ToLowerInvariant())
            {
                case "test":
                    return new UserSession
                           {
                               UserName = "������",
                               Password = "password3",
                               EmployeeGuid = "e05a5cbc-41aa-11e5-8252-001c42c9212f"
                    };
                case "admin":
                    return new UserSession
                    {
                        UserName = "Admin",
                        Password = "",
                        EmployeeGuid = "EmpoyeeGuid"
                    };
                default:
                    return redis.As<UserSession>().GetValue(token);
            }
        }
    }
}