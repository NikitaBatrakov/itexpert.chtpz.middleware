﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Auth
{
    public interface ISessionProvider
    {
        string Token { get; }
        UserSession Session { get; }
    }
}
