﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Auth
{
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class UserSession
    {
        public User User { get; set; }
        public string TokenId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string EmployeeGuid { get; set; }
        public string FullUsername { get; set; }
    }
}
