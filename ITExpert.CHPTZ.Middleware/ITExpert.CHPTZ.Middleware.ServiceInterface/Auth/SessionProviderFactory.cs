﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Auth
{
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public interface ISessionProviderFactory
    {
        ISessionProvider Create(string token);
    }

    public class SessionProviderFactory : ISessionProviderFactory
    {
        public ISessionProvider Create(string token)
        {
            return new SessionProvider(token);
        }
    }

    public class MockSessionProvider : ISessionProvider
    {
        public string Token { get; }
        public UserSession Session { get; }

        public MockSessionProvider(string token)
        {
            Token = token;
            var employeeGuid = $"EmployeeGuid{token}";
            Session = new UserSession
                      {
                          EmployeeGuid = employeeGuid,
                          UserName = "Admin",
                          Password = "",
                          TokenId = token,
                          User = new User {Employee = new Link1C(employeeGuid, "")},
                          FullUsername = $"User{token}"
                      };
        }
    }

    public class MockSessionProviderFactory : ISessionProviderFactory
    {
        public ISessionProvider Create(string token)
        {
            return new MockSessionProvider(token);
        }
    }
}
