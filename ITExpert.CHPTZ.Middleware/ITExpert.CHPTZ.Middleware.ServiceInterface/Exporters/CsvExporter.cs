namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Exporters
{
    using System.IO;
    using System.Linq;
    using System.Text;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ExportRequests.Common;

    public class CsvExporter : ITableExporter
    {
        #region Implementation of ITableExporter

        public void Export(ExportTable table, Stream stream)
        {
            var hearder = string.Join(",", table.Head.Select(x => x.Value));
            var text = string.Join("\n", table.Rows.Select(x => string.Join(",", x.Select(y => y.Value))));
            var writer = new StreamWriter(stream, Encoding.UTF8);
            writer.Write($"{hearder}\n{text}");
            writer.Flush();
        }

        #endregion
    }
}