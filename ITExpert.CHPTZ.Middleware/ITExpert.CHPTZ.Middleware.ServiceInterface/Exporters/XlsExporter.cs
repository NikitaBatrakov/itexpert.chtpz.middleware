﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Exporters
{
    using System.IO;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ExportRequests.Common;

    using NPOI.HSSF.UserModel;
    using NPOI.HSSF.Util;
    using NPOI.SS.UserModel;

    public class XlsExporter : ITableExporter
    {
        private IWorkbook Workbook { get; }
        private ISheet Sheet { get; }

        private ICellStyle HyperlinkCellStyle
        {
            get
            {
                var font = Workbook.CreateFont();
                font.Underline = FontUnderlineType.Single;
                font.Color = HSSFColor.Blue.Index;
                var hyperlinkStyle = Workbook.CreateCellStyle();
                hyperlinkStyle.SetFont(font);
                return hyperlinkStyle;
            }
        }

        public XlsExporter()
        {
            Workbook = new HSSFWorkbook();
            Sheet = Workbook.CreateSheet();
        }

        public void Export(ExportTable table, Stream stream)
        {
            WriteHead(Sheet, table.Head);

            for (var i = 0; i < table.Rows.Length; i++)
            {
                var row = Sheet.CreateRow(i + 1);
                if (table.Rows[i] == null) continue;
                WriteRow(row, table.Rows[i]);
            }

            Workbook.Write(stream);
        }

        private void WriteHead(ISheet sheet, ExportTableCell[] head)
        {
            var row = sheet.CreateRow(0);
            if (head == null) return;
            WriteRow(row, head);
        }

        private void WriteRow(IRow row, ExportTableCell[] cellValues)
        {
            for (var i = 0; i < cellValues.Length; i++)
            {
                var cell = row.CreateCell(i);
                cell.SetCellValue(cellValues[i]?.Value ?? "");

                if (string.IsNullOrEmpty(cellValues[i]?.Url))
                {
                    continue;
                }

                var hyperlink = new HSSFHyperlink(HyperlinkType.Url)
                                {
                                    Address = cellValues[i].Url,
                                    Label = cellValues[i].Value
                                };

                cell.Hyperlink = hyperlink;
                cell.CellStyle = HyperlinkCellStyle;
            }
        }
    }
}
