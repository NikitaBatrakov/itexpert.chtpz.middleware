namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Exporters
{
    using System.IO;

    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ExportRequests.Common;

    public interface ITableExporter
    {
        void Export(ExportTable table, Stream stream);
    }
}