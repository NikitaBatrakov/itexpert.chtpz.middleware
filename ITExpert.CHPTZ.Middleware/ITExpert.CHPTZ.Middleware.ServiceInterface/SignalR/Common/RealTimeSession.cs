﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Common
{
    using System.Collections.Concurrent;

    using ITExpert.CHPTZ.Middleware.ServiceInterface.Auth;

    public class RealTimeSession
    {
        /// <summary>
        /// Guids of the connection assosiated with one user.
        /// </summary>
        /// <remarks>
        /// Dictionary is used for its ability to remove any specific element from collection,
        /// which is not present in any other concurrent collection.
        /// The Value of this dictionary is always an empty string.
        /// </remarks>
        public ConcurrentDictionary<string, string> ConnectionIds { get; }

        /// <summary>
        /// Application session.
        /// </summary>
        public UserSession UserSession { get; }

        public RealTimeSession(UserSession userSession)
        {
            UserSession = userSession;
            ConnectionIds = new ConcurrentDictionary<string, string>();
        }

        public RealTimeSession(UserSession userSession, params string[] connectionIds)
        {
            UserSession = userSession;
            ConnectionIds = new ConcurrentDictionary<string, string>();
            foreach (var connectionId in connectionIds)
            {
                ConnectionIds.TryAdd(connectionId, "");
            }
        }
    }
}
