﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Common
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using ITExpert.CHPTZ.Middleware.ServiceInterface.Auth;

    using Microsoft.AspNet.SignalR;

    using NLog;

    public abstract class BaseHub : Hub
    {
        protected ILogger Logger { get; set; }

        protected string Token => Context.QueryString["Token"];

        protected ISessionProviderFactory SessionProviderFactory { get; }

        protected BaseHub()
        {
            SessionProviderFactory = new SessionProviderFactory();
            //SessionProviderFactory = new MockSessionProviderFactory();

            Logger = LogManager.GetLogger(GetType().FullName);            
        }

        protected BaseHub(ISessionProviderFactory sessionProviderFactory)
        {
            SessionProviderFactory = sessionProviderFactory;
        }

        private ISessionProvider Authorize()
        {
            return SessionProviderFactory.Create(Token);
        }

        protected abstract void AddOrUpdateConnection(string connectionId, ISessionProvider sessionProvider);
        protected abstract void RemoveConnection(string connectionId, ISessionProvider sessionProvider);
        protected abstract IEnumerable<string> GetConnectionIds(ISessionProvider sessionProvider);

        #region Overrides of Hub

        /// <summary>
        /// Called when the connection connects to this hub instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Threading.Tasks.Task"/>
        /// </returns>
        public override Task OnConnected()
        {
            var sessionProvider = Authorize();
            AddOrUpdateConnection(Context.ConnectionId, sessionProvider);
            //Logger.Info($"{sessionProvider.Session.UserName} - Connected");
            return base.OnConnected();
        }

        /// <summary>
        /// Called when a connection disconnects from this hub gracefully or due to a timeout.
        /// </summary>
        /// <param name="stopCalled">true, if stop was called on the client closing the connection gracefully;
        ///             false, if the connection has been lost for longer than the
        ///             <see cref="P:Microsoft.AspNet.SignalR.Configuration.IConfigurationManager.DisconnectTimeout"/>.
        ///             Timeouts can be caused by clients reconnecting to another SignalR server in scaleout.
        ///             </param>
        /// <returns>
        /// A <see cref="T:System.Threading.Tasks.Task"/>
        /// </returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            var sessionProvider = Authorize();
            RemoveConnection(Context.ConnectionId, sessionProvider);
            return base.OnDisconnected(stopCalled);
        }

        /// <summary>
        /// Called when the connection reconnects to this hub instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Threading.Tasks.Task"/>
        /// </returns>
        public override Task OnReconnected()
        {
            var sessionProvider = Authorize();
            if (!GetConnectionIds(sessionProvider).Contains(Context.ConnectionId))
            {
                AddOrUpdateConnection(Context.ConnectionId, sessionProvider);
            }
            return base.OnReconnected();
        }
        #endregion
    }
}
