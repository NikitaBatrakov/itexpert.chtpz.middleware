﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Common
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.Middleware.ServiceInterface.Auth;

    using NLog;

    public class ConnectedClientsCollection
    {
        private ILogger Logger { get; }

        public IEnumerable<string> Keys => Connections.Keys.ToArray();

        private ConcurrentDictionary<string, RealTimeSession> Connections { get; }

        public RealTimeSession this[string key]
        {
            get
            {
                try
                {
                    RealTimeSession connections;
                    return Connections.TryGetValue(key, out connections) ? connections : null;
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                    throw;
                }
            }
        }

        public ConnectedClientsCollection()
        {
            Connections = new ConcurrentDictionary<string, RealTimeSession>();
            Logger = LogManager.GetCurrentClassLogger();
        }

        public void AddOrUpdateConnection(string key, string connectionId, UserSession userSession)
        {
            try
            {
                Func<string, RealTimeSession> createFactory = k => new RealTimeSession(userSession, connectionId);
                Func<string, RealTimeSession, RealTimeSession> updateFactory = (k, v) => AddConnectionId(v, connectionId);
                Connections.AddOrUpdate(key, createFactory, updateFactory);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        public void RemoveConnection(string key, string connecitonId)
        {
            try
            {
                var session = this[key];
                string emptyString;
                session?.ConnectionIds?.TryRemove(connecitonId, out emptyString);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        private static RealTimeSession AddConnectionId(RealTimeSession value, string connectionId)
        {
            value.ConnectionIds.TryAdd(connectionId, "");
            return value;
        }
    }
}
