namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Locking
{
    using System;

    public class LockInfo
    {
        public string Entity { get; }
        public string Token { get; }
        public string EmployeeGuid { get; }
        public string UserName { get; }

        public DateTime LockDate { get; private set; }
        public bool IsActive { get; private set; }

        public LockInfo(string entity, string token, string employeeGuid, string userName)
        {
            Entity = entity;
            Token = token;
            EmployeeGuid = employeeGuid;
            UserName = userName;
            LockDate = DateTime.Now;
            IsActive = false;
        }

        public void Activate()
        {
            IsActive = true;
            LockDate = DateTime.Now;
        }

        public void Deactivate()
        {
            IsActive = false;
            LockDate = DateTime.Now;
        }
    }
}