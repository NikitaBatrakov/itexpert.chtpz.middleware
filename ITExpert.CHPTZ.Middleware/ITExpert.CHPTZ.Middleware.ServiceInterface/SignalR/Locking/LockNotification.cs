namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Locking
{
    using Newtonsoft.Json;

    public struct LockNotification
    {
        [JsonProperty("objectGuid")]
        public string ObjectGuid { get; }

        [JsonProperty("employeeGuid")]
        public string EmployeeGuid { get; }

        [JsonProperty("userName")]
        public string UserName { get; }

        public LockNotification(string objectGuid, string employeeGuid, string userName)
        {
            ObjectGuid = objectGuid;
            EmployeeGuid = employeeGuid;
            UserName = userName;
        }
    }
}