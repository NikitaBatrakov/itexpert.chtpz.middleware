namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Locking
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class LockStateException : Exception
    {
        public LockStateException()
        {
        }

        public LockStateException(string message)
            : base(message)
        {
        }

        public LockStateException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected LockStateException(SerializationInfo info,
                                     StreamingContext context)
            : base(info, context)
        {
        }
    }
}