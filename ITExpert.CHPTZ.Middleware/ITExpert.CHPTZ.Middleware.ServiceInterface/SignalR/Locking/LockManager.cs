﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Locking
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Auth;

    internal class LockManager
    {
        private IMResolver Resolver { get; }
        private UserSession UserSession { get; }

        /// <summary>
        /// Stores employees that are currently locking the objects.
        /// The Key is Object Guid.
        /// The Value's key is EmployeeGuid.
        /// </summary>
        public static readonly ConcurrentDictionary<string, ConcurrentDictionary<string, LockInfo>> Locks =
            new ConcurrentDictionary<string, ConcurrentDictionary<string, LockInfo>>();

        public LockManager(IMResolver resolver, UserSession userSession)
        {
            Resolver = resolver;
            UserSession = userSession;
        }

        public LockInfo Lock(string entity, string objectGuid)
        {
            var lockInfo = CreateLockInfo(entity);
            var objectLocks = Locks.GetOrAdd(objectGuid, x => new ConcurrentDictionary<string, LockInfo>());
            objectLocks.TryAdd(lockInfo.EmployeeGuid, lockInfo);
            return lockInfo;
        }

        public LockInfo ActivateLock(string entity, string objectGuid)
        {
            var lockInfo = GetLockInfo(objectGuid);
            if (lockInfo.IsActive)
            {
                throw new LockStateException();
            }
            lockInfo.Activate();
            //TODO: Lock the object in 1C
            return lockInfo;
        }

        public LockInfo DeactivateLock(string entity, string objectGuid)
        {
            var lockInfo = GetLockInfo(objectGuid);
            if (!lockInfo.IsActive)
            {
                throw new LockStateException();
            }
            lockInfo.Deactivate();
            //TODO: Unlock the object in 1C
            return lockInfo;
        }

        public LockInfo Unlock(string entity, string objectGuid)
        {
            var lockInfo = CreateLockInfo(entity);
            ConcurrentDictionary<string, LockInfo> objectLocks;
            var isObjectLocked = Locks.TryGetValue(objectGuid, out objectLocks);

            if (!isObjectLocked || !objectLocks.ContainsKey(lockInfo.EmployeeGuid))
            {
                throw new LockStateException("Object is not locked by this user");
            }

            objectLocks.TryRemove(lockInfo.EmployeeGuid, out lockInfo);
            return lockInfo;
        }

        public static ConcurrentDictionary<string, LockInfo> ClearLocks(string objectGuid)
        {
            ConcurrentDictionary<string, LockInfo> locks;
            Locks.TryRemove(objectGuid, out locks);
            return locks;
        }

        public static IEnumerable<string> ClearEmployeesLocks(string employeeGuid)
        {
            foreach (var pair in Locks)
            {
                if (!pair.Value.ContainsKey(employeeGuid)) continue;
                LockInfo lockInfo;
                pair.Value.TryRemove(employeeGuid, out lockInfo);
                yield return pair.Key;
            }
        }

        private LockInfo CreateLockInfo(string entity)
        {
            var userName = UserSession.FullUsername;
            var employeeGuid = UserSession.EmployeeGuid;
            var token = UserSession.TokenId;
            return new LockInfo(entity, token, employeeGuid, userName);
        }

        private LockInfo GetLockInfo(string objectGuid)
        {
            LockInfo lockInfo;
            try
            {
                lockInfo = Locks[objectGuid][UserSession.EmployeeGuid];
            }
            catch
            {
                throw new Exception(
                    $"Object ({objectGuid}) is not locked or subscribed on by {UserSession.UserName}. Make sure you called Subscribe first");
            }
            return lockInfo;
        }
    }
}
