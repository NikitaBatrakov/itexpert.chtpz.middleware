﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Locking
{
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Auth;

    internal class LockManagerFactory
    {
        private IMResolver Resolver { get; }

        public LockManagerFactory(IMResolver resolver)
        {
            Resolver = resolver;
        }

        public LockManager Create(UserSession session)
        {
            return new LockManager(Resolver, session);
        }
    }
}
