﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Locking
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.Middleware.ServiceInterface.Auth;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Common;
    using ITExpert.CHPTZ.ObjectModel;

    using Microsoft.AspNet.SignalR.Hubs;

    using NLog;

    internal class ConnectionLockMapCollection
    {
        internal struct ConnectionLockMap
        {
            public string ConnectionId { get; }
            public string ObjectId { get; }

            public ConnectionLockMap(string connectionId, string objectId)
            {
                ConnectionId = connectionId;
                ObjectId = objectId;
            }
        }

        /// <summary>
        /// The key is employee guid.
        /// The value's key is connection id.
        /// The value's value is object guid.
        /// </summary>
        private ConcurrentDictionary<string, ConcurrentDictionary<string, string>> Mappings { get; }

        public ConnectionLockMapCollection()
        {
             Mappings = new ConcurrentDictionary<string, ConcurrentDictionary<string, string>>();
        }

        public int CountConnections(string employeeId, string objectId)
        {
            return Mappings[employeeId].Values.Count(x => x == objectId);
        }

        public void AddOrUpdate(string employeeId, string connectionId, string objectId)
        {
            Mappings.AddOrUpdate(employeeId, x => AddValueFactory(connectionId, objectId),
                              (x, y) => UpdateValueFactory(y, connectionId, objectId));
        }

        public string TryRemove(string employeeId, string connectionId)
        {
            string objectGuid;
            Mappings[employeeId].TryRemove(connectionId, out objectGuid);
            return objectGuid;
        }

        public void TryRemoveAll(string employeeId)
        {
            ConcurrentDictionary<string, string> removedItem;
            Mappings.TryRemove(employeeId, out removedItem);
        }

        private static ConcurrentDictionary<string, string> AddValueFactory(string connectionId, string objectId)
        {
            var map = new ConcurrentDictionary<string, string>();
            map.TryAdd(connectionId, objectId);
            return map;
        }

        private static ConcurrentDictionary<string, string> UpdateValueFactory(
            ConcurrentDictionary<string, string> connectionObjectMap, string connectionId, string objectId)
        {
            connectionObjectMap.TryAdd(connectionId, objectId);
            return connectionObjectMap;
        }
    }

    [HubName("lockingHub")]
    public class LockingHub : BaseHub
    {
        private static readonly ConnectedClientsCollection Sessions = new ConnectedClientsCollection();
        
        private static readonly ConnectionLockMapCollection ConnectionLockMapCollection = new ConnectionLockMapCollection();

        private LockManagerFactory LockManagerFactory { get; }

        public LockingHub()
        {
            LockManagerFactory = new LockManagerFactory(new Resolver());
            Logger = LogManager.GetLogger("signalr");
        }

        #region Hub Methods

        public IEnumerable<LockInfo> GetLocks(string guid)
        {
            ConcurrentDictionary<string, LockInfo> locks;
            LockManager.Locks.TryGetValue(guid, out locks);
            return locks?.Values;
        }

        /// <summary>
        /// Adds inactive lock. 
        /// Client receives all notifications, 
        /// none receives notifications about client's activities.
        /// Called on Read.
        /// </summary>
        /// <param name="entity">Locked object type</param>
        /// <param name="guid">Locked object ID</param>
        /// <returns>Array of UserNames which are currently locking the object</returns>
        public IEnumerable<LockNotification> Subscribe(string entity, string guid)
        {
            var lockInfo = InvokeLockManager(x => x.Lock(entity, guid));
            var lockers =
                LockManager.Locks[guid].Where(x => x.Value.IsActive).
                                        Select(x => new LockNotification(guid, x.Value.EmployeeGuid, x.Value.UserName)).
                                        ToArray();
            Logger.Info($"SUBSCRIBE | {lockInfo.UserName} | {entity} | {guid} | {LockManager.Locks.Values.First().Count}");
            ConnectionLockMapCollection.AddOrUpdate(lockInfo.EmployeeGuid, Context.ConnectionId, guid);
            return lockers;
        }

        /// <summary>
        /// Activates lock.
        /// Every locker receives notifications about client's activities.
        /// Called on Edit.
        /// </summary>
        /// <param name="entity">Locked object type</param>
        /// <param name="guid">Locked object ID</param>
        public void LockObject(string entity, string guid)
        {
            LockInfo lockInfo;
            try
            {
                lockInfo = InvokeLockManager(x => x.ActivateLock(entity, guid));
            }
            catch (LockStateException)
            {
                return;
            }
            
            var notification = new LockNotification(guid, lockInfo.EmployeeGuid, lockInfo.UserName);
            NotifyAll(notification, CallOnLockAdded);
            Logger.Info($"LOCK | {lockInfo.UserName} | {entity} | {guid}");
        }

        /// <summary>
        /// Deactivates lock.
        /// Every locker stops receiving notifications about client's activities.
        /// Called on Chancel.
        /// </summary>
        /// <param name="entity">Locked object type</param>
        /// <param name="guid">Locked object ID</param>
        public void UnlockObject(string entity, string guid)
        {
            LockInfo lockInfo;
            try
            {
                lockInfo = InvokeLockManager(x => x.DeactivateLock(entity, guid));
            }
            catch (LockStateException)
            {
                return;
            }
            
            var notification = new LockNotification(guid, lockInfo.EmployeeGuid, lockInfo.UserName);
            NotifyAll(notification, CallOnLockRemoved);
            Logger.Info($"UNLOCK | {lockInfo.UserName} | {entity} | {guid}");
        }

        /// <summary>
        /// Removes all locks.
        /// Every locker receives notification about object is being saved.
        /// Called on Save.
        /// </summary>
        /// <param name="entity">Locked object type</param>
        /// <param name="guid">Locked object ID</param>
        public void NotifySave(string entity, string guid)
        {
            var lockInfo = InvokeLockManager(x => x.Unlock(entity, guid));
            var notification = new LockNotification(guid, lockInfo.EmployeeGuid, lockInfo.UserName);
            NotifyAll(notification, CallOnSave);
            NotifyCaller(notification, lockInfo, CallOnSave);
            Logger.Info($"SAVE | {lockInfo.UserName} | {entity} | {guid}");
            //LockManager.ClearLocks(guid);
        }

        #endregion

        #region Private methods

        private LockInfo[] GetOtherUsersLocks(string guid, string employeeGuid)
        {
            return LockManager.Locks[guid].
                Where(x => x.Key != employeeGuid).
                Select(x => x.Value).
                ToArray();
        }

        private LockInfo InvokeLockManager(Func<LockManager, LockInfo> func)
        {
            var sessionProvider = SessionProviderFactory.Create(Token);
            var lockManager = LockManagerFactory.Create(sessionProvider.Session);
            return func(lockManager);
        }

        private static void CallOnLockAdded(dynamic client, LockNotification lockNotification)
        {
            client.OnLockAdded(lockNotification);
        }

        private static void CallOnLockRemoved(dynamic client, LockNotification lockNotification)
        {
            client.OnLockRemoved(lockNotification);
        }

        private static void CallOnSave(dynamic client, LockNotification lockNotification)
        {
            client.OnObjectSaved(lockNotification);
        }

        private void NotifyCaller(LockNotification lockNotification, LockInfo lockInfo, Action<dynamic, LockNotification> action)
        {
            var lockers = new[] {lockInfo};
            NotifyAll(lockNotification, lockers, action);
        }

        private void NotifyAll(LockNotification lockNotification, Action<dynamic, LockNotification> action)
        {
            var lockers = LockManager.Locks[lockNotification.ObjectGuid];
            NotifyAll(lockNotification, lockers.Values, action);
        }

        private void NotifyAll(LockNotification lockNotification, IEnumerable<LockInfo> lockers, Action<dynamic, LockNotification> action)
        {
            foreach (var locker in lockers)
            {
                var session = Sessions[locker.EmployeeGuid];
                foreach (var connection in session.ConnectionIds.Keys)
                {
                    action.Invoke(Clients.Client(connection), lockNotification);
                    Logger.Info($"NOTIFY | {locker.UserName} | {lockNotification.ObjectGuid}");
                }
            }
        }

        #endregion

        #region Overrides of BaseHub

        protected override void AddOrUpdateConnection(string connectionId, ISessionProvider sessionProvider)
        {
            Logger.Info($"CONNECT | {sessionProvider.Session.FullUsername}");
            Sessions.AddOrUpdateConnection(sessionProvider.Session.EmployeeGuid, connectionId, sessionProvider.Session);
        }

        protected override void RemoveConnection(string connecitonId, ISessionProvider sessionProvider)
        {
            var employeeGuid = sessionProvider.Session.EmployeeGuid;
            TryUnlock(employeeGuid, connecitonId);

            Logger.Info($"DISCONNECT | {sessionProvider.Session.FullUsername}");
            Sessions.RemoveConnection(employeeGuid, connecitonId);
        }

        private void TryUnlock(string employeeGuid, string connectionId)
        {
            var objectGuid = ConnectionLockMapCollection.TryRemove(employeeGuid, connectionId);
            if (ConnectionLockMapCollection.CountConnections(employeeGuid, objectGuid) > 0)
            {
                return;
            }

            var lockInfo = LockManager.Locks[objectGuid][employeeGuid];
            UnlockObject(lockInfo.Entity, objectGuid);
        }

        protected override IEnumerable<string> GetConnectionIds(ISessionProvider sessionProvider)
        {
            return Sessions[sessionProvider.Session.EmployeeGuid]?.ConnectionIds.Keys ?? Enumerable.Empty<string>();
        }

        #endregion
    }
}
