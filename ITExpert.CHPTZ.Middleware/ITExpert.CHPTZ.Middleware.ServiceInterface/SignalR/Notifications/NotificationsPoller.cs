﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Notifications
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Threading;

    using ITExpert.CHPTZ.DataAccess;
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    using NLog;

    public class NotificationsPoller : IDisposable
    {
        private Timer TaskTimer { get; }
        private IHubConnectionContext<dynamic> Clients { get; }

        private static readonly Lazy<NotificationsPoller> Instance =
            new Lazy<NotificationsPoller>(
                () => new NotificationsPoller(GlobalHost.ConnectionManager.GetHubContext<NotificationHub>().Clients));

        private NotificationsPoller(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
            TaskTimer = new Timer(OnTimerElapsed, null, TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(3));
        }

        public static void Start()
        {
            //Making a call to Instance property
            //Poller won't start unless some call is made
            var poller = Instance.Value;
        }

        private void OnTimerElapsed(object sender)
        {
            try
            {
                SendNotifications();
            }
            catch (Exception e)
            {
                LogManager.GetLogger("exceptions").Error(e);
                throw;
            }            
        }

        private void SendNotifications()
        {
            var sessions = NotificationHub.Sessions.Keys.ToArray();
            if (!sessions.Any())
            {
                return;
            }
            var notificationsInfos = GetNotificationsInfo(sessions).ToArray();

            foreach (var number in notificationsInfos)
            {
                var connectionIds = NotificationHub.Sessions[number.EmployeeGuid].ConnectionIds;
                foreach (var connectionId in connectionIds.Keys)
                {
                    Clients.Client(connectionId).SetNotificationsNumber(number.Value);
                }
            }
        }

        private static IEnumerable<NotificationsNumber> GetNotificationsInfo(string[] employeeGuids)
        {
            using (var dataManager = GetDataManager())
            {
                return dataManager.Service<NotificationDataService>().GetNotificationsNumber(employeeGuids).Value;
            }
        }

        private static IDataManager GetDataManager()
        {
            var login = ConfigurationManager.AppSettings["WebServiceLogin"];
            var password = ConfigurationManager.AppSettings["WebServicePassword"];
            return new DataManager(login, password);
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            TaskTimer.Dispose();
        }

        #endregion
    }
}