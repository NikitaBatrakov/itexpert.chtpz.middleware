﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Notifications
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.Middleware.ServiceInterface.Auth;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.SignalR.Common;

    using Microsoft.AspNet.SignalR.Hubs;

    [HubName("notificationHub")]
    public class NotificationHub : BaseHub
    {
        //Token is the Key
        public static readonly ConnectedClientsCollection Sessions = new ConnectedClientsCollection();

        #region Overrides of BaseHub

        protected override void AddOrUpdateConnection(string connectionId, ISessionProvider sessionProvider)
        {
            Sessions.AddOrUpdateConnection(sessionProvider.Session.EmployeeGuid, connectionId, sessionProvider.Session);
        }

        protected override void RemoveConnection(string connecitonId, ISessionProvider sessionProvider)
        {
            Sessions.RemoveConnection(sessionProvider.Session.EmployeeGuid, connecitonId);
        }

        protected override IEnumerable<string> GetConnectionIds(ISessionProvider sessionProvider)
        {
            return Sessions[sessionProvider.Session.EmployeeGuid]?.ConnectionIds.Keys ?? Enumerable.Empty<string>();
        }

        #endregion
    }
}
