﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System;
    using System.Linq;

    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    using MonthPlanDataService = ITExpert.CHPTZ.DataAccess.DataServices.MonthPlanDataService;

    public class MonthPlanService : RestfulService
    {        
        public GetEntityResponse Any(GetMonthPlanBusinessProcessRequest request)
            => GetResponse(x => x.GetBusinessProcessLevel());

        public GetEntityResponse Any(GetMonthPlanDirectionRequest request)
            => GetResponse(x => x.GetDirectionLevel(request.BusinessProcessGuid));

        public GetEntityResponse Any(GetMonthPlanFunctionalGroupRequest request)
            => GetResponse(x => x.GetFunctionalGroupLevel(request.BusinessProcessGuid, request.DirectionGuid));

        public GetEntityResponse Any(GetMonthPlanChangeRequestsRequest request)
            => GetResponse(x => x.GetChangeRequests(request.TeamGuid, request.DirectionGuid, request.MonthPlanGuid));

        public GetEntityResponse Any(GetMonthPlanIterationsRequest request)
            => GetResponse(x => x.GetIterations(request.TeamGuid, request.MonthPlanGuid));

        public GetEntityResponse Any(GetAvailableTaskTypesRequest request)
            => GetResponse(x => x.GetAvailableTaskTypes());

        public int Any(IterationCheckIfMayBeExcludedRequest request)
            => GetNumberResponse(x => x.CheckIfMayBeExcluded(request.IterationGuid));

        public int Any(GetDefaultLaborCostsRequest request)
            => GetNumberResponse(x => x.GetDefaultLaborCosts(request.MonthPlan, request.ChangeRequest));

        public int Any(GetTeamResourceRequest request)
            => GetNumberResponse(x => x.GetTeamResource(request.Team, request.MonthPlan));

        public GetEntityResponse Any(CheckMonthPlanRequest request)
            => GetResponse(x => x.CheckMonthPlan(request.Year, request.Month));

        public GetEntityResponse Any(GetMonthPlanActionsRequest request)
            => GetResponse(x => x.GetMonthPlanActions(request.Team, request.MonthPlan));

        public GetEntityResponse Any(GetMonthPlanVersionRequest request)
            => GetResponse(x => x.GetMonthPlanVersions(request.Year, request.Month));

        private int GetNumberResponse(Func<MonthPlanDataService, int> func)
        {
            using (var dataManager = GetDataManager())
            {
                return func(dataManager.Service<MonthPlanDataService>());
            }
        }

        private GetEntityResponse GetResponse(Func<MonthPlanDataService, MQueryResult> func)
        {
            using (var dataManager = GetDataManager())
            {
                var service = dataManager.Service<MonthPlanDataService>();
                var data = func.Invoke(service);
                return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
            }
        }
    }
}
