﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ActivityJournalRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class ActivityJournalService : RestfulService
    {
        public GetRegisterResponse Any(GetActivityJournalRequest request)
        {
            request.ComplexFilter = GetFilter(request.ComplexFilter);
            request.Fields = GetFields(request.Fields);
            request.Entity = typeof(ActivityJournal).Name;
            var query = RequestConverter.ConvertToMQuery(request);
            var data = Query<ActivityJournal>(x => x.Query(query));
            return new GetRegisterResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        private static string GetFields(string requestedFields)
        {
            var isLinkPresent = string.IsNullOrEmpty(requestedFields) || requestedFields.ToLowerInvariant().Contains("link");
            return isLinkPresent ? requestedFields : $"{requestedFields},link";
        }

        private FilterElement GetFilter(FilterElement requestedFilter)
        {
            var filter = new FilterElement(LogicalSign.Or, GetObjectIsEmptyFilter());
            if (requestedFilter != null)
            {
                filter = new FilterElement(LogicalSign.And, requestedFilter, filter);
            }
            return filter;
        }

        private FilterElement[] GetObjectIsEmptyFilter()
        {
            const string field = "object";
            var entity = typeof(ActivityJournal);
            var prop = entity.GetPropertyIgnoreCase(field);
            var filters = 
                GetCompositePropertyTypes(prop).
                    Select(type => new FilterElement(field, ComparisonSign.Equal, $"{new Guid()};{type.Name}")).ToList();
            filters.Add(new FilterElement(field, ComparisonSign.Equal, "НЕОПРЕДЕЛЕНО"));
            return filters.ToArray();
        }

        private IEnumerable<Type> GetCompositePropertyTypes(PropertyInfo property)
        {
            return property.GetCustomAttribute<Type1CAttribute>().Types.Select(type1C => Resolver.GetInternalType(type1C));
        }
    }
}
