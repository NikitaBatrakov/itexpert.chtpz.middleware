namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System.IO;

    using ITExpert.CHPTZ.DataAccess.FileManagement;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.FileRequests;

    using ServiceStack;
    using ServiceStack.Web;

    public class FileService : RestfulService
    {
        public IHttpResult Any(DownloadFileRequest request)
        {
            var path = FileManager.GetFilePath(request.Name, request.Extension);
            if (!File.Exists(path))
            {
                throw new FileNotFoundException();
            }
            var fi = new FileInfo(path);
            return new HttpResult(fi, asAttachment: true);
        }
    }
}