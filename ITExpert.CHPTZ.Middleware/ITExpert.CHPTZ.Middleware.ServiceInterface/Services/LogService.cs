﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.LoggingRequests;

    using NLog;

    public class LogService : RestfulService
    {
        public void Any(WriteLogRequest request)
        {
            var logger = LogManager.GetLogger("frontend");
            logger.Info(request.Text);
        }
    }
}
