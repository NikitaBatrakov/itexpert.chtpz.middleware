namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;

    using ITExpert.CHPTZ.DataAccess;
    using ITExpert.CHPTZ.DataAccess.FileManagement;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Exporters;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ExportRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ExportRequests.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    using ServiceStack;

    public class ExportService : RestfulService
    {
        public ValueResponse<string> Any(ExportTableRequest request)
        {
            switch (request.Extension.ToLowerInvariant())
            {
                case "xls":
                    return new ValueResponse<string>(Export(new XlsExporter(), request.Table, "xls"));
                case "csv":
                    return new ValueResponse<string>(Export(new CsvExporter(), request.Table, "csv"));
                default:
                    throw new FormatException($"Export to '{request.Extension}' is not supported");
            }
        }

        private string Export(ITableExporter exporter, ExportTable table, string extension)
        {
            var filename = Guid.NewGuid().ToString();
            var path = FileManager.GetFilePath(filename, extension);
            using (var fileStream = File.Open(path, FileMode.Create, FileAccess.Write))
            {
                exporter.Export(table, fileStream);
            }

            Task.Run(() =>
                     {
                         Thread.Sleep(60000);
                         Logger.Info($"File '{filename}' deleted");
                         File.Delete(path);
                     });
            
            var baseUrl = Request.GetBaseUrl();
            return $"{baseUrl}/files/{filename}/{extension}";
        }
    }
}