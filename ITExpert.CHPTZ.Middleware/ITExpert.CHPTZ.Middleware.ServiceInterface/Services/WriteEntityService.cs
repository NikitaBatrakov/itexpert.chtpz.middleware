﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System;

    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.DataAccess.Repositories;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Exceptions;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    using ServiceStack.Text;

    public class WriteEntityService : RestfulService
    {
        public VoidResponse Any(DeleteEntityRequest request)
        {
            var data = Modify(request.Entity, x => x.DeleteByIds(request.Entity));
            return new VoidResponse(data.ServiceResult);
        }

        public VoidResponse Any(DeleteEntitiesRequest request)
        {
            var data = Modify(request.Entity, x => x.DeleteByIds(request.Guids.ToArray()));
            return new VoidResponse(data.ServiceResult);
        }

        public VoidResponse Any(DeleteEntitiesWithMultipleTypesRequest request)
        {
            var exceptions = (from entity in request.Entities
                             let data = Modify(entity.Key, x => x.DeleteByIds(entity.Value))
                             where !data.ServiceResult.IsOk
                             select
                                 new BackendException(
                                 $"1C has thrown the exception while deleting {entity.Key}: {data.ServiceResult.Text}")).ToArray();

            if (!exceptions.Any())
            {
                return new VoidResponse(ServiceResult.CreateOk());
            }

            throw new AggregateException(exceptions);
        }

        public CreateOrUpdateEntityResponse Any(CreateEntitiesRequest request)
        {
            return CreateOrUpdate(request, (repository, entities) => repository.Add(entities));
        }

        public CreateOrUpdateEntityResponse Any(UpdateEntitiesRequest request)
        {
            return CreateOrUpdate(request, (repository, entities) => repository.Update(entities));
        }

        public GetEntityResponse Any(CreateOnBaseRequest request)
        {
            throw new NotImplementedException();
        }

        private CreateOrUpdateEntityResponse CreateOrUpdate(ICreateUpdateEntitiesRequest request, Func<EntityRepository, IObject1C[], MQueryResult> func)
        {
            var type = Resolver.GetClassType(request.Entity);
            var entities = ParseEntities(type, request.Entities).ToArray();
            var data = Modify(request.Entity, repository => func(repository, entities));
            return new CreateOrUpdateEntityResponse(data.Entities, data.ServiceResult);
        }

        private IEnumerable<IObject1C> ParseEntities(Type type, IEnumerable<object> entities)
        {
            foreach (var jsvEntity in entities)
            {
                Logger.Debug(jsvEntity);
                IObject1C entity;
                try
                {
                    entity = (IObject1C)TypeSerializer.DeserializeFromString(jsvEntity.ToString(), type);
                }
                catch (SerializationException)
                {
                    throw new FormatException("Format of 'Entities' parameter is incorrect");
                }
                yield return entity;
            }
        }
    }
}
