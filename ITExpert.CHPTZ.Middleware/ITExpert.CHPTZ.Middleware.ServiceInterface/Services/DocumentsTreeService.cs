﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.DataAccess.Extensions;
    using ITExpert.CHPTZ.DataAccess.Repositories.GenericRepositoryExtensions;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.DocumentRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class DocumentsTreeService : RestfulService<DocumentsTreeDataService>
    {
        /// <summary>
        /// Gets Documents Tree
        /// </summary>
        /// <param name="request">Empty</param>
        /// <returns>Documents</returns>
        public GetDocumentTreeResponse Any(GetDocumentsTreeRequest request)
        {
            var data = InvokeDataService(x => x.GetDocumentsTree(request.Guids));
            var tree = GetDocumentsWithParents(data.Entities.Cast<Document>().ToArray()).ToTree(
                            x => x.Link.Value,
                            x => x.Parent.Value,
                            x => x.Parent.Value == "Undefined");
            return new GetDocumentTreeResponse(tree, data.ServiceResult);
        }

        private static IEnumerable<Document> GetDocumentsWithParents(Document[] documents)
        {
            var lookup = documents.ToDictionary(x => x.Link.Value);
            var orphanedDocs = documents.Where(x => x.Parent.Value != "Undefined" && !lookup.ContainsKey(x.Parent.Value));
            foreach (var document in orphanedDocs)
            {
                lookup.Add(document.Parent.Value, new Document {Link = document.Parent, Parent = Link1C.Undefined});
            }
            return lookup.Values;
        }
    }
}
