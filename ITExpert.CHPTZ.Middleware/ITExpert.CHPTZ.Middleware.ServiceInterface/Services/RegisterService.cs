﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RegisterRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class RegisterService : RestfulService
    {
        public GetRegisterResponse Any(GetRegisterRequest request)
        {
            using (var dataManager = GetDataManager())
            {
                var data = dataManager.Repository(request.Entity).Query(RequestConverter.ConvertToMQuery(request));
                return new GetRegisterResponse(data.ServiceResult, data.TotalResult, data.Entities);
            }
        }
    }
}
