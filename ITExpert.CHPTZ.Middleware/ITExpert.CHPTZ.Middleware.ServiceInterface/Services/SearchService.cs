namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.SearchRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public class SearchService : RestfulService<SearchDataService>
    {
        public GetSearchResponse Any(AdvancedSearchRequest request)
        {
            var data = InvokeDataService(x => x.GetParameters(request.Entity, request.Properties));
            return new GetSearchResponse(data.ServiceResult, data.Value);
        }

        public ValueResponse<IEnumerable<string>> Any(SearchTypesRequest request)
        {
            var data = InvokeDataService(x => x.SearchTypes(request.Query));
            return new ValueResponse<IEnumerable<string>>(data.ServiceResult, data.Value);
        }

        public GetEntityResponse Any(SearchRequest request)
        {
            var args = RequestConverter.ConvertToMQuery(request);
            var data = InvokeDataService(x => x.Search(request.Query, request.Entity, args));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }
    }
}