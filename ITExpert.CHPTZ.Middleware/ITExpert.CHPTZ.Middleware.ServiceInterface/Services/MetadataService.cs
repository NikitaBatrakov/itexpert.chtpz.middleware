﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class MetadataService : RestfulService
    {
        /// <summary>
        /// Gets entity metadata
        /// </summary>
        /// <param name="request">Entity</param>
        /// <returns>Metadata</returns>
        public IDictionary<string, PropertyMetadata> Any(GetMetadataRequest request)
        {
            var type = Resolver.GetClassType(request.Entity);
            if (string.IsNullOrEmpty(request.Fields))
            {
                return MetadataElement.Create(type, Resolver);
            }

            var fields = request.Fields.Split(',');
            return MetadataElement.Create(type, Resolver, fields);
        }

        public IDictionary<string, string> Any(GetVerboseMetadataRequest request)
        {
            var fullMetadata =
                Any(new GetMetadataRequest { Entity = request.Entity, Fields = request.Fields });
            return fullMetadata.ToDictionary(propertyMetadata => propertyMetadata.Key,
                                             propertyMetadata => propertyMetadata.Value.Verbose);
        }

        public GetEntityTemplateResponse Any(GetEntityTemplateRequest request)
        {
            var type = Resolver.GetClassType(request.Entity);
            var meta = MetadataElement.Create(type, Resolver);
            var entity = CreateEmptyEntity(type);
            return new GetEntityTemplateResponse(meta, entity, ServiceResult.CreateOk());
        }

        private object CreateEmptyEntity(Type type)
        {
            var entity = CHPTZ.Common.Activator.CreateInstance(type);
            var cache = TypeCache.Get(type);
            foreach (var cachedProp in cache)
            {
                SetPropertyValue(entity, type, cachedProp.Value);
            }
            return entity;
        }

        private void SetPropertyValue(object entity, Type type, PropertyCachedValue cachedProp)
        {
            var prop = cachedProp.PropertyInfo;
            if (prop.PropertyType == typeof(string))
            {
                prop.SetValue(entity, "");
            }
            else if (prop.PropertyType == typeof(bool?))
            {
                prop.SetValue(entity, false);
            }
            else if (prop.PropertyType == typeof(double?))
            {
                prop.SetValue(entity, 0D);
            }
            else if (prop.PropertyType == typeof(DateTime?))
            {
                prop.SetValue(entity, new DateTime());
            }
            else if (prop.PropertyType == typeof(Link1C))
            {
                var typeAttr = cachedProp.TypeAttributes.First();
                var typeName = typeAttr.Type.Equals("id", StringComparison.InvariantCultureIgnoreCase)
                                   ? "Id"
                                   : Resolver.GetInternalType(typeAttr.Type).Name;
                prop.SetValue(entity, new Link1C("Undefined", "Undefined", typeName));
            }
            else if (prop.PropertyType == typeof(Composite1C))
            {
                prop.SetValue(entity, Composite1C.CreateEmpty());
            }
            else if (prop.PropertyType == typeof(Table1C))
            {
                prop.SetValue(entity, new Table1C());
            }
            else
            {
                throw new Exception($"Unexpected property type encountered in DTO - {prop}");
            }
        }
    }
}
