﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AdditionalAttributeRequests;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public class AdditionalAttributesService : RestfulService<AdditionalAttributesDataService>
    {
        /// <summary>
        /// Gets additional attributes values
        /// </summary>
        /// <param name="request">Entity, Guid</param>
        /// <returns>Additional attributes</returns>
        public IEnumerable<AdditionalAttributeValue> Any(AdditionalAttributeValueRequest request)
            => InvokeDataService(x => x.GetValues(request.Entity, request.Guid));

        /// <summary>
        /// Gets additional attributes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IEnumerable<AdditionalAttributeType> Any(AdditionalAttributesRequest request)
            => InvokeDataService(x => x.GetTypes(request.Guid));

    }
}
