namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.DefaultFilterRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class DefaultFilterService : RestfulService<DefaultFilterDataService>
    {
        public ValueResponse<string> Any(GetDefaultFilterRequest request)
            => new ValueResponse<string>(InvokeDataService(x => x.GetDefaultFilter(request.Entity, EmployeeGuid, request.PlaceGuid)));

        public ValueResponse<string> Any(GetGroupFilterRequest request)
            => new ValueResponse<string>(InvokeDataService(x => x.GetGroupFilter(request.Entity, EmployeeGuid)));

        public VoidResponse Any(SetDefaultFilterRequest request)
        {
            var data = InvokeDataService(x => x.SetDefaultFilter(request.Entity, EmployeeGuid, request.ViewGuid, request.PlaceGuid));
            return new VoidResponse(data.ServiceResult);
        }
    }
}