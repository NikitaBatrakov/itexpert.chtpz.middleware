﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.TimezoneRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class TimezoneService : RestfulService<TimezoneDataService>
    {
        public GetEntityResponse Any(GetTimezonesRequest request)
        {
            var data = InvokeDataService(x => x.GetTimezones(EmployeeGuid));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        } 

        public GetEntityResponse Any(SetTimezoneRequest request)
        {
            var data = InvokeDataService(x => x.SetTimezones(EmployeeGuid, request.TimezoneGuid));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }
    }
}
