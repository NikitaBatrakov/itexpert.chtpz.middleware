﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.PresetFilterRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class PresetFilterService : RestfulService<PresetFilterDataService>
    {
        public GetEntityResponse Any(GetFunctionalGroupsRequest request)
        {
            var data = InvokeDataService(x => x.GetEmployeesFunctionalGroups(EmployeeGuid));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public GetEntityResponse Any(MyGroupCurrentChangeRequestFilterRequest request)
        {
            var query = RequestConverter.ConvertToMQuery(request);
            var data = InvokeDataService(x => x.GetEmployeeGroupsCurrentChangeRequests(EmployeeGuid, query));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public GetEntityResponse Any(MyCurrentChangeRequestFilterRequest request)
        {
            var query = RequestConverter.ConvertToMQuery(request);
            var data = InvokeDataService(x => x.GetEmployeesCurrentChangeRequests(EmployeeGuid, query));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }
    }
}
