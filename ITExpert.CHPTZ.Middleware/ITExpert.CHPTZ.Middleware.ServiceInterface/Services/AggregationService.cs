﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AggregationRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    public class AggregationService : RestfulService<AggregationDataService>
    {
        public IDictionary<string, PropertyMetadata> Any(GetAggregationMetadataRequest request)
            => GetMetadata(typeof(AggregationList), request.Fields);

        public IDictionary<string, PropertyMetadata> Any(GetExtendedAggregationMetadataRequest request)
            => GetMetadata(typeof(AggregationList), request.Fields);

        public GetEntityResponse Any(GetAggregationListRequest request)
        {
            var query = RequestConverter.ConvertToMQuery(request);
            var data = InvokeDataService(x => x.GetAggregationList(query));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public GetEntityResponse Any(GetExtendedAggregationListRequest request)
        {
            var query = RequestConverter.ConvertToMQuery(request);
            var data = InvokeDataService(x => x.GetExtendedAggregaionList(query));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }
    }
}
