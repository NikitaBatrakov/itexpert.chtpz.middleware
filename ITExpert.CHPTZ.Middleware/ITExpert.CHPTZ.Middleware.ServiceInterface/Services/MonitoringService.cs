﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonitoringRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public class MonitoringService : RestfulService<AgreementsMonitoringDataService>
    {
        public IDictionary<string, PropertyMetadata> Any(GetMonitoringMetadataRequest request)
            => GetMetadata(typeof(ArgreementsMonitoringObject), request.Fields);

        public GetEntityResponse Any(GetMonitoringRequest request)
        {
            var query = RequestConverter.ConvertToMQuery(request);
            var data = InvokeDataService(x => x.MonitorAgreements(request.TargetEntity, request.Guid, query));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }
    }
}
