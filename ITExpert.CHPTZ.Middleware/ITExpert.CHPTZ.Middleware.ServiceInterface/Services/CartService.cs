﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.DataAccess;
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.DataAccess.DataServices.AttachmentDataServcies;
    using ITExpert.CHPTZ.DataAccess.FileManagement;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.DataAccess.Types;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.CartRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using Service = ITExpert.CHPTZ.ObjectModel.Entities.Service;

    using ServiceStack;

    public class CartService : RestfulService<CartDataService>
    {
        public TreeResponse<Service> Any(GetServiceCatalogueRequest request)
        {
            using (var dataManager = GetDataManager())
            {
                var employeeGuid = string.IsNullOrEmpty(request.EmployeeGuid)
                                   ? EmployeeGuid
                                   : request.EmployeeGuid;
                var data = dataManager.Service<CartDataService>().GetServices(employeeGuid);
                SetAttachmentsSrc(dataManager, data.Value);
                return new TreeResponse<Service>(data.Value, data.ServiceResult);
            }
        }

        public TreeResponse<ZnoTemplate> Any(GetServiceTemplatesRequest request)
            => Invoke(x => x.GetServiceTemplates(request.ServiceGuid));

        public GetEntityResponse Any(CreateCartRequest request)
            => Invoke(x => x.CreateCart(request.CartElements, request.Attributes));

        public GetEntityResponse Any(UpdateCartRequest request)
            =>
                request.ServiceRequest == null
                    ? Invoke(x => x.UpdateCart(request.CartElements))
                    : Invoke(x => x.UpdateCart(request.ServiceRequest, request.CartElements));

        public GetRegisterResponse Any(GetServiceRequestItemsRequest request)
        {
            var data = InvokeDataService(x => x.GetServiceRequestOrder(request.Guid));
            return new GetRegisterResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public GetRegisterResponse Any(GetAgreementsItemsRequest request)
        {
            var data = InvokeDataService(x => x.GetAgreementsOrder(request.Guid));
            return new GetRegisterResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        #region Private Functions

        private GetEntityResponse Invoke(Func<CartDataService, MQueryResult> func)
        {
            using (var dataManager = GetDataManager())
            {
                var data = func(dataManager.Service<CartDataService>());
                return GetEntityResponse.Create(data);
            }
        }

        private TreeResponse<T> Invoke<T>(Func<CartDataService, ServiceValueResponse<IEnumerable<TreeNode<T>>>> func)
        {
            using (var dataManager = GetDataManager())
            {
                var data = func(dataManager.Service<CartDataService>());
                return new TreeResponse<T>(data.Value, data.ServiceResult);
            }
        }

        private void SetAttachmentsSrc(IDataManager dataManager, IEnumerable<TreeNode<Service>> nodes)
        {
            foreach (var treeNode in nodes)
            {
                if (treeNode.Value.Attachments != null)
                {
                    foreach (var attachment in treeNode.Value.Attachments)
                    {
                        var filename = dataManager.Service<AttachmentDataService>().GetFileInfo(attachment).GetFileName();
                        attachment.Src = FileManager.GetFileUrl(Request.GetBaseUrl(), filename);
                    }
                }
                if (treeNode.Children != null)
                {
                    SetAttachmentsSrc(dataManager, treeNode.Children);
                }
            }
        }

        #endregion
    }
}
