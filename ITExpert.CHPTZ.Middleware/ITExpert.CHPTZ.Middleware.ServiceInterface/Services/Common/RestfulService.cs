﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common
{
    using System;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.DataAccess;
    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.DataAccess.Repositories;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Auth;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    using NLog;
    using ObjectModel;
    using ServiceStack;
    using ServiceStack.Web;

    public abstract class RestfulService<TDataService> : RestfulService where TDataService : BaseDataService
    {
        protected TOut InvokeDataService<TOut>(Func<TDataService, TOut> func)
            => InvokeDataService<TDataService, TOut>(func);
    }

    public abstract class RestfulService : Service
    {
        protected IMResolver Resolver { get; }
        protected Logger Logger { get; }

        private string Token => GetToken(Request);
        private ISessionProviderFactory SessionProviderFactory { get; }
        private ISessionProvider SessionProvider => SessionProviderFactory.Create(Token);
        protected string EmployeeGuid => SessionProvider.Session.User.Employee.Value;

        protected RestfulService()
        {
            Resolver = new Resolver();
            Logger = LogManager.GetLogger(GetType().FullName);
            SessionProviderFactory = new SessionProviderFactory();
        }

        protected DataManager GetDataManager()
        {
            var sessionProvider = SessionProviderFactory.Create(Token);
            return new DataManager(sessionProvider.Session.UserName, sessionProvider.Session.Password);
        }

        protected TOut InvokeDataService<TDataService, TOut>(Func<TDataService, TOut> func) where TDataService : BaseDataService
        {
            using (var dataManager = GetDataManager())
            {
                var service = dataManager.Service<TDataService>();
                return func.Invoke(service);
            }
        }

        protected MQueryResult Query<T>(Func<Repository<T>, MQueryResult> func) where T : IObject1C
        {
            using (var dataManager = GetDataManager())
            {
                var repository = dataManager.Repository<T>();
                return func.Invoke(repository);
            }
        }

        protected MQueryResult Query(string entity, Func<AbstractRepository, MQueryResult> func)
        {
            using (var dataManager = GetDataManager())
            {
                var repository = dataManager.Repository(entity);
                return func(repository);
            }
        }

        protected MQueryResult Modify(string entity, Func<EntityRepository, MQueryResult> func)
        {
            using (var dataManager = GetDataManager())
            {
                return func(dataManager.Repository(entity));
            }
        }

        protected IDictionary<string, PropertyMetadata> GetMetadata(Type type, string fields)
        {
            if (string.IsNullOrEmpty(fields))
            {
                return MetadataElement.Create(type, Resolver);
            }

            var fieldsArr = fields.Split(',');
            return MetadataElement.Create(type, Resolver, fieldsArr);
        }

        private static string GetToken(IRequest request)
        {
            var token = request.GetHeader("Token");
            if (!string.IsNullOrEmpty(token))
            {
                return token;
            }

            var logger = LogManager.GetLogger("auth");
            logger.Error($"OAuthSession: No token found for {request.AbsoluteUri}");
            throw new AuthenticationException("Token in HTTP header is not set. See confluence for guidance");
        }
    }
}
