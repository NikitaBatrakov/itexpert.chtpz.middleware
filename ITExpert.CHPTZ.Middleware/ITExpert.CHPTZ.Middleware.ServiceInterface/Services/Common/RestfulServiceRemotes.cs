﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common
{
    using System;

    using ITExpert.CHPTZ.DataAccess.DataServices.Common;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class RestfulServiceRemotes<TService> : RestfulService where TService : BaseDataService
    {
        protected FunctionResponse InvokeProcedure(Func<TService, ProcedureServiceResponse> func)
        {
            using (var dataManager = GetDataManager())
            {
                var service = dataManager.Service<TService>();
                var data = func.Invoke(service);
                return new FunctionResponse(data.ServiceResult, data.Metadata, data.Entities);
            }
        }

        protected FunctionResponse InvokeFunction(Func<TService, FunctionServiceResponse> func)
        {
            using (var dataManager = GetDataManager())
            {
                var service = dataManager.Service<TService>();
                var data = func.Invoke(service);
                return new FunctionResponse(data.ServiceResult, data.Entities);
            }
        }
    }
}
