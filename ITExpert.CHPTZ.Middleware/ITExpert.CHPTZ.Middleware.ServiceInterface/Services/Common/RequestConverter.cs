namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common
{
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests;

    public class RequestConverter
    {
        public static MQuery ConvertToMQuery(BaseGetRequest request)
        {
            return new MQuery
                   {
                       Fields = request.Fields,
                       Filter = request.Filter,
                       ComplexFilter = request.ComplexFilter,
                       Sort = request.Sort,
                       Limit = request.Limit,
                       Offset = request.Offset
                   };
        }

        public static MQueryWithOptions ConvertToMQuery(BaseGetEntitiesRequest request)
        {
            return new MQueryWithOptions
                   {
                       Fields = request.Fields,
                       Filter = request.Filter,
                       ComplexFilter = request.ComplexFilter,
                       Sort = request.Sort,
                       Limit = request.Limit,
                       Offset = request.Offset,
                       TablesFieldsNames = request.TablesFieldsNames,
                       TablesNames = request.TablesNames,
                       WithTables = request.WithTables,
                       WithAttachments = request.WithAttachments,
                       WithAttributes = request.WithAttributes,
                       WithMetadata = request.WithMetadata,
                   };
        }
    }
}