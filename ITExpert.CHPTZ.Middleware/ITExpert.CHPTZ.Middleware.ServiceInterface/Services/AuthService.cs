﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System;

    using ITExpert.CHPTZ.DataAccess;

    using ServiceStack.Redis;

    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Auth;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AuthRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class AuthenticationService : RestfulService
    {
        public void Any(ValidateTokenRequest tokenRequest)
        {
            using (GetDataManager()) { }
        }

        public AuthenticationResponse Any(SignInRequest request)
        {
            var user = Authenticate(request.Login, request.Password);
            var token = Guid.NewGuid().ToString();
            CacheSession(user, token, request.Login, request.Password);
            return new AuthenticationResponse(token, user);
        }

        private static User Authenticate(string login, string password)
        {
            using (var authManager = new AuthManager())
            {
                return authManager.Authenticate(login, password);
            }
        }

        private static void CacheSession(User user, string token, string login, string password)
        {
            using (var redisManager = new PooledRedisClientManager())
            using (var redis = redisManager.GetClient())
            {
                var redisUser = redis.As<UserSession>();
                var session = new UserSession
                {
                    User = user,
                    TokenId = token,
                    UserName = login,
                    Password = password,
                    EmployeeGuid = user.Employee.Value
                };
                redisUser.SetValue(token, session, new TimeSpan(48, 0, 0));
            }
        }
    }
}
