﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security;

    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.DataAccess.DataServices.AttachmentDataServcies;
    using ITExpert.CHPTZ.DataAccess.FileManagement;

    using ServiceStack;
    using ServiceStack.Web;

    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.AttachmentRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.FileRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class AttachmentService : RestfulService
    {
        /// <summary>
        /// Gets url to attachment on disk
        /// </summary>
        /// <param name="request">Attachment guid</param>
        /// <returns>Attachment url</returns>
        public string Any(GetAttachmentUrlRequest request)
        {
            using (var dataManager = GetDataManager())
            {
                var fileName = dataManager.Service<AttachmentDataService>().GetFileInfo(request.Guid).GetFileName();
                return request.AsAttachment
                           ? new DownloadFileRequest {Name = fileName.Name, Extension = fileName.Extension}.ToGetUrl()
                           : FileManager.GetFileUrl(Request.GetBaseUrl(), fileName);
            }
        }

        /// <summary>
        /// Downloads attachment
        /// </summary>
        /// <param name="request">Attachment guid</param>
        /// <returns>File</returns>
        public HttpResult Any(DownloadAttachmentRequest request)
        {
            using (var dataManager = GetDataManager())
            {
                var fileInfo = dataManager.Service<AttachmentDataService>().GetFileInfo(request.Guid);
                return new HttpResult(fileInfo, asAttachment: true);
            }
        }

        /// <summary>
        /// Adds attachment
        /// </summary>
        /// <param name="request">Owner, OwnerGuid, Date</param>
        /// <returns>Backend response</returns>
        public GetEntityResponse Any(AddAttachmentRequest request)
        {
            var entity = Resolver.GetExternalTypeName(request.Entity);
            var attachments = CreateAttachmentDtos(entity, request.EntityGuid, EmployeeGuid, Request.Files);
            using (var dataManager = GetDataManager())
            {
                var response = dataManager.Service<AttachmentDataService>().AddAttachments(attachments);
                return GetEntityResponse.Create(response);
            }
        }

        /// <summary>
        /// Marks attachment as not relevant (striked out on UI)
        /// </summary>
        /// <param name="request">Attachment GUID</param>
        /// <returns></returns>
        public VoidResponse Any(MarkAttachmentAsNotRelevantRequest request)
        {
            using (var dataManager = GetDataManager())
            {
                var attachment = new Attachment
                {
                    Link = new Link1C(request.Guid),
                    IsNotRelevant = true,
                    FileDeletingEmployee = Link1C.Create<Employee>(EmployeeGuid),
                };
                var response = dataManager.Repository(typeof(Attachment)).Update(attachment);
                return new VoidResponse(response.ServiceResult);
            }
        }

        private static IEnumerable<Attachment> CreateAttachmentDtos(string entity, string entityGuid, string employeeGuid, IHttpFile[] files)
        {
            foreach (var file in files)
            {
                var fileName = new FileName(file.FileName);
                ValidateFile(fileName.Extension);
                
                yield return new Attachment
                {
                    Link = new Link1C("Id", "Undefined"),
                    Owner = new Composite1C(entity, entityGuid),
                    Date = DateTime.Now,
                    Size = file.ContentLength,
                    Name = file.FileName,
                    Filename = fileName.Name,
                    Extension = fileName.Extension,
                    Value = Convert.ToBase64String(file.InputStream.ReadFully()),
                    Employee = new Link1C(employeeGuid, typeof(Employee).Name)
                };
            }
        }

        private static void ValidateFile(string extension)
        {
            var badExts = new[] { "exe", "conf", "cmd", "bat" };
            if (badExts.Contains(extension))
            {
                throw new SecurityException($".{extension} files are forbidden for uploading");
            }
        }
    }
}
