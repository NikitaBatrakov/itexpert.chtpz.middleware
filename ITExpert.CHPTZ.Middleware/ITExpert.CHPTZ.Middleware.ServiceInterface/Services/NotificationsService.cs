﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.NotificationRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class NotificationsService : RestfulService<NotificationDataService>
    {
        public GetEntityResponse Any(GetNotificationsRequest request)
        {
            var data = InvokeDataService(x => x.GetNotifications(EmployeeGuid));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public VoidResponse Any(ReadNotificationRequest request)
        {
            var data = InvokeDataService(x => x.ReadNotifications(request.Notifications));
            return new VoidResponse(data.ServiceResult);
        }

        public int Any(NotificationsCountRequest request)
        {
            return InvokeDataService(x => x.GetNotificationsNumber(EmployeeGuid));
        }

        public VoidResponse Any(ReadObjectNotifications request)
        {
            var data = InvokeDataService(x => x.ReadObjectNotifications(EmployeeGuid, request.Entity, request.Guid));
            return new VoidResponse(data.ServiceResult);
        }
    }
}
