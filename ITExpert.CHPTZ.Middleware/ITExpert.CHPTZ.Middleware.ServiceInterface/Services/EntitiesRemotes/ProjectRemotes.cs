﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.EntitiesRemotes
{
    using ITExpert.CHPTZ.DataAccess.DataServices.ProjectDataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ProjectRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class ProjectServiceRemotes : RestfulServiceRemotes<ProjectDataService>
    {
        public FunctionResponse Any(AddTeamToProjectEventRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.AddTeamToEvent(request.Project, request.Event, request.Team));

        public FunctionResponse Any(ChangeProjectTeamRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.ChangeTeam(request.Project, request.ProjectTeam, request.Team));

        public FunctionResponse Any(CloseProjectRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.CloseProject(request.Project));

        public FunctionResponse Any(CreateProjectEventRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.CreateEvent(request.Project));

        public FunctionResponse Any(DistributeTeamResourcesRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.DistributeTeamResources(request.Project, request.ProjectTeam, request.Resources));

        public FunctionResponse Any(InitializeProjectEventExecutionStageRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.InitializeExecutionStage(request.Project, request.Event, request.Team));

        public FunctionResponse Any(ReleaseProjectRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.ReleaseProject(request.Project));

        public FunctionResponse Any(RemoveProjectEventRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.RemoveEvent(request.Project, request.Event));

        public FunctionResponse Any(RemoveTeamFromProjectEventRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.RemoveTeamFromEvent(request.Project, request.Event, request.Team));

        public FunctionResponse Any(ShiftProjectRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.ShiftProject(request.Project, request.Months));

        public FunctionResponse Any(ShiftTeamProjectRequest request)
            => InvokeProcedure(x => x.ProjectProcedures.ShiftProject(request.Project, request.Months, request.Team));
    }
}
