namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.EntitiesRemotes
{
    using ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Functions;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Service;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class ServiceService : RestfulServiceRemotes<ServiceFunctionService>
    {
        public FunctionResponse Any(GetAvailableEmployeeServicesRequest request)
            => InvokeFunction(x => x.GetAvailableEmployeeServices(request.Employee));
    }
}