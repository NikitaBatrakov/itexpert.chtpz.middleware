﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.EntitiesRemotes
{
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.ChangeRequestRemotes;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class ChangeRequestService : RestfulServiceRemotes<MonthPlanDataService>
    {
        public FunctionResponse Any(ChangeRequestIncludeInPlanRequest request)
            =>
                InvokeProcedure(
                    x =>
                    x.ChangeRequestProcedures.IncludeInMonthPlan(request.EntityGuid, request.MonthPlan, request.Team,
                                                                 request.TaskType, request.Resource));
    }
}
