﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.EntitiesRemotes
{
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.MonthPlanRemotes;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class MonthPlanService : RestfulServiceRemotes<MonthPlanDataService>
    {
        public FunctionResponse Any(MonthPlanApproveRequest request)
            => InvokeProcedure(x => x.MonthPlanProcedures.Approve(request.EntityGuid));

        public FunctionResponse Any(MonthPlanUpdateRequest request)
            => InvokeProcedure(x => x.MonthPlanProcedures.Update(request.EntityGuid));

        public FunctionResponse Any(MonthPlanFillWithProjectRequest request)
            => InvokeProcedure(x => x.MonthPlanProcedures.FillWithProjects(request.EntityGuid));

        public FunctionResponse Any(MonthPlanFillWithNonProjectRequest request)
            => InvokeProcedure(x => x.MonthPlanProcedures.FillWithNonProjects(request.EntityGuid, request.Team));

        public FunctionResponse Any(MonthPlanAddCommentRequest request)
            => InvokeProcedure(x => x.MonthPlanProcedures.AddComment(request.EntityGuid, request.Team, request.Comment));

        public FunctionResponse Any(MonthPlanApproveReportRequest request)
            => InvokeProcedure(x => x.MonthPlanProcedures.ApproveReport(request.EntityGuid));
    }
}
