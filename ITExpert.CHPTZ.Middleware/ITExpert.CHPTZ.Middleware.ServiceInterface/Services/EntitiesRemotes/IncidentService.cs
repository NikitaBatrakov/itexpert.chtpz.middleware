namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.EntitiesRemotes
{
    using ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Functions;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Incident;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class IncidentService : RestfulServiceRemotes<IncidentFunctionSerivce>
    {
        public FunctionResponse Any(GetAvailableFunctionalGroupsRequest request)
            => InvokeFunction(x => x.GetAvailableFuncitonalGroups(request.ServiceGuid));

        public FunctionResponse Any(GetAvailableEmployeesRequest request)
            => InvokeFunction(x => x.GetAvailableEmployeesRequest(request.Service, request.FunctionalGroup));

        public FunctionResponse Any(GetAvailableServicesRequest request)
            => InvokeFunction(x => x.GetAvailableServices(request.Employee, request.Date));

        public FunctionResponse Any(GetAvailableIncidentSlaRequest request)
            => InvokeFunction(x => x.GetAvailableSla(request.Employee, request.Date));
    }
}