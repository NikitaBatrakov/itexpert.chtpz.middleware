namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.EntitiesRemotes
{
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.MonthPlanRequests.Remotes.IterationRemotes;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class IterationService : RestfulServiceRemotes<MonthPlanDataService>
    {
        public FunctionResponse Any(IterationGetHelpRequest request)
            =>
                InvokeProcedure(
                    x =>
                    x.IterationProcedures.CreateHelpRequest(request.IterationGuid, request.ChangeRequest, request.Team,
                                                            request.Resource));

        public FunctionResponse Any(IterationSetDevelopingStatusRequest request)
            => InvokeProcedure(x => x.IterationProcedures.SetDevelopingStatus(request.IterationGuid, request.Resource));

        public FunctionResponse Any(IterationUpdateResourceRequest request)
            => InvokeProcedure(x => x.IterationProcedures.UpdateResource(request.IterationGuid, request.Resource));

        public FunctionResponse Any(IterationAdjustmentRequest request)
            => InvokeProcedure(x => x.IterationProcedures.Adjust(request.IterationGuid));

        public FunctionResponse Any(IterationChangeTaskTypeRequest request)
            => InvokeProcedure(x => x.IterationProcedures.ChangeTaskType(request.IterationGuid, request.TaskType));

        public FunctionResponse Any(IterationExcludeRequest request)
            => InvokeProcedure(x => x.IterationProcedures.Exclude(request.IterationGuid));
    }
}