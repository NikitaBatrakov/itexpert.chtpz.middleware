﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.EntitiesRemotes
{
    using System;

    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Procedures;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;
    using ITExpert.CHPTZ.ObjectModel;

    using ServiceStack.Text;

    public class CommonRemotesService : RestfulServiceRemotes<CommonProcedureService>
    {
        public FunctionResponse Any(GetContextualMetadataRequest request)
            => InvokeProcedure(x => x.GetContextualMetadata(request.Entity));

        public FunctionResponse Any(GetObjectContextualMetadataRequest request)
            => InvokeProcedure(x => x.GetContextualMetadata(request.Entity, request.Guid));

        public FunctionResponse Any(OnClosureRequest request)
            => InvokeProcedure(x => x.Postprocess(request.EntityTypeName, request.EntityGuid));

        public FunctionResponse Any(LockObjectRequest request)
            => InvokeProcedure(x => x.LockObject(request.Entity, request.Guid));

        public FunctionResponse Any(OnPropertyChangedFunctionRequest request)
        {
            var type = Resolver.GetClassType(request.Entity);
            var entity = EntityFactory.Create(type, request.EntityGuid);
            var prop = type.GetPropertyIgnoreCase(request.Field);
            if (prop == null)
            {
                throw new FormatException($"Field '{request.Field}' is not found in {request.Entity}");
            }
            var value = JsonSerializer.DeserializeFromString(request.Value, prop.PropertyType);
            prop.SetValue(entity, value);
            return InvokeProcedure(x => x.OnPropertyChanged(type, entity, prop));
        }
    }
}
