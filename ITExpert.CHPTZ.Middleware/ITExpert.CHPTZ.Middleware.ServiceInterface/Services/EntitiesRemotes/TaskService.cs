namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.EntitiesRemotes
{
    using ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Functions;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.Task;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class TaskService : RestfulServiceRemotes<TaskFunctionService>
    {
        public FunctionResponse Any(GetFunctionalGroupEmployeesRequest request)
            => InvokeFunction(x => x.GetAvailableEmployeesRequest(request.FunctionalGroup));
    }
}