namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services.EntitiesRemotes
{
    using ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Functions;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RemotesRequests.KnowledgeBaseEntry;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class KnowledgeBaseEntryService : RestfulServiceRemotes<KnowledgeBaseEntryFunctionService>
    {
        public FunctionResponse Any(GetFunctionalGroupEmployeesRequest request)
            => InvokeFunction(x => x.GetAvailableEmployeesRequest(request.FunctionalGroup));
    }
}