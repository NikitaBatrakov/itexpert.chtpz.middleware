﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.DataAccess.DataServices.Remotes.Functions;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.DataAccess.Remotes;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.RelationRequests;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    using ServiceStack.Text;

    public class RelationService : RestfulService
    {
        public FunctionResponse Any(GetAvailableRelationTypesRequest request)
        {
            var data =
                InvokeDataService<ObjectsLinkFunctionService, FunctionServiceResponse>(
                    x => x.GetAvailableRelationTypes(request.MasterObject, request.SlaveType));
            return new FunctionResponse(data.ServiceResult, data.Entities);
        }

        public FunctionResponse Any(SetTypeRequest request)
        {
            var entity = (IEntity)TypeSerializer.DeserializeFromString(request.Entity.ToString(), typeof(ObjectsLink));
            var data =
                InvokeDataService<ObjectsLinkProcedureService, ProcedureServiceResponse>(x => x.SetType(entity));
            return new FunctionResponse(data.ServiceResult, data.Metadata, data.Entities);
        }

        public GetEntityResponse Any(GetRelationsRequest request)
        {
            var args = RequestConverter.ConvertToMQuery(request);
            var data =
                InvokeDataService<RelationDataService, MQueryResult>(
                    x => x.GetRelations(request.Entity, request.Guid, args));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public IDictionary<string, PropertyMetadata> Any(GetRelationsMetadataRequest request)
            => GetMetadata(typeof(RelationEntity), request.Fields);
    }
}
