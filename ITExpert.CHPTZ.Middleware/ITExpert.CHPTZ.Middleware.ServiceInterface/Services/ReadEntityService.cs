﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.DataAccess.MQueries.Types;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.EntityRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses;

    public class ReadEntityService : RestfulService
    {
        public GetEntityResponse Any(GetEntitiesRequest request)
        {
            var query = RequestConverter.ConvertToMQuery(request);
            var data = Query(request.Entity, x => x.Query(query));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public GetEntityResponse Any(GetEntityRequest request)
        {
            var query = ConvertToMQuery(request);
            var data = Query(request.Entity, x => x.Query(query));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        private static MQueryWithOptions ConvertToMQuery(GetEntityRequest request)
        {
            return new MQueryWithOptions
                   {
                       ComplexFilter = new FilterElement("Link", ComparisonSign.Equal, request.Guid),
                       Fields = request.Fields,
                       WithTables = request.WithTables,
                       WithAttachments = request.WithAttachments,
                       WithMetadata = request.WithMetadata,
                       WithAttributes = request.WithAttributes,
                   };
        }
    }
}
