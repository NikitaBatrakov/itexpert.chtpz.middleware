﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System;

    using ITExpert.CHPTZ.DataAccess.DataServices;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.OperatorRequests;

    using GetEntityResponse = ITExpert.CHPTZ.Middleware.ServiceModel.Responses.GetEntityResponse;

    public class OperatorService : RestfulService<OperatorDataService>
    {
        public GetEntityResponse Any(GetStatusRequest request)
        {
            var data = InvokeDataService(x => x.GetStatus(request.Entity, request.EntityGuid, request.CallGuid));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public GetEntityResponse Any(CreateRequestRequest request)
        {
            var data =
                InvokeDataService(
                    x =>
                    x.CreateRequest(request.Entity, request.Initiator, request.User, request.Info, request.CallGuid));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public GetEntityResponse Any(RegisterPhoneCallRequest request)
        {
            var data = InvokeDataService(x => x.RegisterPhoneCall(request.Phone, request.Record, request.User));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }

        public GetEntityResponse Any(GetEmployeesRequestsRequest request)
        {
            var query = RequestConverter.ConvertToMQuery(request);
            var data = InvokeDataService(x => x.GetEmployeesRequests(request.EmployeeGuid, query));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        } 
    }
}
