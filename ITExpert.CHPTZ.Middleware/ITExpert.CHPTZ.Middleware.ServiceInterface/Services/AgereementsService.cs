﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using Common;

    using ITExpert.CHPTZ.ObjectModel.Entities;

    using ServiceModel.Requests.EntityRequests;
    using ServiceModel.Responses;

    using ServiceStack;

    public class AgereementsService : RestfulService
    {
        public GetEntityResponse Any(GetAgreementTasksRequest request)
        {
            request.Entity = typeof(Task).Name;
            const string argeementsFilter = "Action.ActionType::\"Согласование\"";
            request.Filter = request.Filter.IsNullOrEmpty() ? argeementsFilter : $"{request.Filter} AND {argeementsFilter}";
            var query = RequestConverter.ConvertToMQuery(request);
            var data = Query<Task>(x => x.Query(query));
            return new GetEntityResponse(data.ServiceResult, data.TotalResult, data.Entities);
        }
    }
}
