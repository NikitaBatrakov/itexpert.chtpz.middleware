﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Services
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.DataAccess;
    using ITExpert.CHPTZ.DataAccess.Repositories.GenericRepositoryExtensions;
    using ITExpert.CHPTZ.Middleware.ServiceInterface.Services.Common;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Requests.ConfigurationUnitRequests;
    using ITExpert.CHPTZ.Middleware.ServiceModel.Responses.DTO;

    public class ConfigurationUnitTreeService : RestfulService
    {
        private readonly IDictionary<string, ConfigurationUnitTreeNode> _elementPool =
            new Dictionary<string, ConfigurationUnitTreeNode>();

        public IDictionary<string, ConfigurationUnitTreeNode> Any(GetConfigurationUnitsTreeRequest request)
        {
            using (var dataManager = GetDataManager())
            {
                var relations = GetRelations(dataManager, request.RelationTypeGuid);
                FillElementPool(relations);
                var guids = GetKeysOfMissingUnits(request.ConfigurationUnitsGuids).ToArray();
                if (!guids.Any())
                {
                    return GetSortedPool();
                }

                var units = GetConfigurationUnits(dataManager, guids);
                AddMissingUnits(units);
                return GetSortedPool();
            }
        }

        private static IEnumerable<ObjectsLink> GetRelations(DataManager dataManager, string typeGuid)
        {
            return dataManager.Repository<ObjectsLink>().
                        Where(x => x.LinkType.Value == typeGuid).
                        Field(x => x.MasterObject).
                        Field(x => x.SlaveObject).
                        Query().
                        Entities.
                        Cast<ObjectsLink>().
                        ToArray();
        }

        private void FillElementPool(IEnumerable<ObjectsLink> relations)
        {
            foreach (var relation in relations)
            {
                var masterKey = relation.MasterObject.Value.ToString();
                ConfigurationUnitTreeNode master;
                var isMasterMissing = !_elementPool.TryGetValue(masterKey, out master);
                if (isMasterMissing)
                {
                    master = new ConfigurationUnitTreeNode(relation.MasterObject.LinkPerformance);
                    _elementPool.Add(masterKey, master);
                }
                var slaveKey = relation.SlaveObject.Value.ToString();
                master.ChildrenIds.Add(slaveKey);

                if (_elementPool.ContainsKey(slaveKey))
                {
                    continue;
                }

                var slave = new ConfigurationUnitTreeNode(relation.SlaveObject.LinkPerformance);
                _elementPool.Add(slaveKey, slave);
            }
        }

        private IEnumerable<string> GetKeysOfMissingUnits(IEnumerable<string> configurationUnitGuids)
        {
            foreach (var guid in configurationUnitGuids)
            {
                ConfigurationUnitTreeNode unit;
                var isUnitPresent = _elementPool.TryGetValue(guid, out unit);
                if (isUnitPresent)
                {
                    unit.IsSelectable = true;
                }
                else
                {
                    yield return guid;
                }
            }
        }

        private static IDictionary<string, ConfigurationUnitTreeNode> GetConfigurationUnits(DataManager dataManager, IEnumerable<string> guids)
        {
            return
                dataManager.Repository<ConfigurationUnit>().
                            WhereIn(x => x.Link.Value, guids.ToArray()).
                            Field(x => x.Link).
                            Query().
                            Entities.
                            Cast<ConfigurationUnit>().
                            ToDictionary(x => x.Link.Value,
                                         x => new ConfigurationUnitTreeNode(x.Link.LinkPerformance, true));
        }

        private void AddMissingUnits(IDictionary<string, ConfigurationUnitTreeNode> units)
        {
            foreach (var node in units)
            {
                if (_elementPool.ContainsKey(node.Key)) continue;
                _elementPool.Add(node.Key, node.Value);
            }
        }

        private IDictionary<string, ConfigurationUnitTreeNode> GetSortedPool()
        {
            ICollection<KeyValuePair<string, ConfigurationUnitTreeNode>> masters = 
                new List<KeyValuePair<string, ConfigurationUnitTreeNode>>();
            ICollection<KeyValuePair<string, ConfigurationUnitTreeNode>> slaves = 
                new List<KeyValuePair<string, ConfigurationUnitTreeNode>>();

            foreach (var node in _elementPool.OrderBy(x => x.Value.Name))
            {
                if (node.Value.ChildrenIds.Any())
                {
                    masters.Add(node);
                }
                else
                {
                    slaves.Add(node);
                }
            }

            var result = masters.ToDictionary(master => master.Key, master => master.Value);
            foreach (var slave in slaves)
            {
                result.Add(slave.Key, slave.Value);
            }
            return result;
        }
    }
}
