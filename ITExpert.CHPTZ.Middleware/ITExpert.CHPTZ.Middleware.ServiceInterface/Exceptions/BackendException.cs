﻿namespace ITExpert.CHPTZ.Middleware.ServiceInterface.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.BackendCommunications.Types;
    using ITExpert.CHPTZ.ObjectModel.Dtos;

    [Serializable]
    public class BackendException : Exception
    {
        public BackendException()
            : base("1C web service has thrown an Exception") { }

        public BackendException(string message)
            : base(message) { }

        public BackendException(ServiceResult serviceResult) 
            : this(serviceResult.Code, serviceResult.Text) { }

        public BackendException(int statusCode, string message) 
            : base("1C web service has thrown an Exception " +
                  $"with code {statusCode}({((Code1C)statusCode)}) " +
                  $"and message: {message}") { }

        public BackendException(string message, Exception inner)
            : base(message, inner) { }

        protected BackendException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
