﻿namespace ConsoleHelper
{
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendCommunications.Connection;
    using ITExpert.CHPTZ.BackendCommunications.Services;

    class Program
    {
        private const string _projectBasePath = @"../../../ITExpert.CHPTZ.ObjectModel\";
        private const string _entitiesPath = _projectBasePath + "Entities/Generated";
        private const string _mappingsPath = _projectBasePath + "";
        private const string _docPath = @"C:/CHTPZ/";

        private const string _translationXml = "Translations.xml";
        private static XDocument _translations;

        static void Main(string[] args)
        {
            var xml = GetMetadata();
            Generator.GenerateTranslation(xml, "C:/CHTPZ/", "tranlsations.xml");
            Generate(xml);
        }

        private static string GetMetadata()
        {
            using (var serviceConnection = new ServiceConnection1C("Admin", ""))
            {
                var metadataService = new MetadataService1C(serviceConnection);
                return metadataService.GetMetadata();
            }
        }

        private static void Generate(string xml)
        {
            var generator = new Generator(_translationXml, null);
            var result = generator.Generate(xml).ToArray();
            foreach (var classFile in result)
            {
                WriteToFile(classFile.ClassName, classFile.ClassContent);
            }
            var resolver = generator.GetResloverClass(result);
            var docs = generator.GetDocumentation(result);
            WriteToFile("ResolverMappings", resolver, "cs", _mappingsPath);
            WriteToFile("chptz.dto.docs", docs, "md", _docPath);
        }

        private static void WriteToFile(string fileName, string text, string ext = "cs", string path = _entitiesPath)
        {
            File.WriteAllText($"{path}/{fileName}.{ext}", text);
        }
    }
}