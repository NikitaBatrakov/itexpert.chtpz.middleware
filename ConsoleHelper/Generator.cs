﻿namespace ConsoleHelper
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    internal class GeneratedClassData
    {
        public string ClassName { get; }
        public string ClassContent { get; }
        public string MappingString { get; }
        public string Documentation { get; }

        public GeneratedClassData(string className, string classContent, string mapping = "", string doc = "")
        {
            ClassName = className;
            ClassContent = classContent;
            MappingString = mapping;
            Documentation = doc;
        }
    }

    internal class Generator
    {
        private XDocument Translations { get; }

        private Func<string, string[]> GetEnumValues { get; }

        public Generator(string translationPath, Func<string, string[]> getEnumValuesFunc)
        {
            Translations = XDocument.Load(translationPath);
            GetEnumValues = getEnumValuesFunc;
        }

        public string GetDocumentation(IEnumerable<GeneratedClassData> classesData)
        {
            var sortedData = classesData.OrderBy(x => x.ClassName).ToArray();
            var docs = "";
            foreach (var data in sortedData)
            {
                docs += data.Documentation;
            }
            //return Templates.Doc(docs);
            return docs;
        }

        public string GetResloverClass(IEnumerable<GeneratedClassData> classesData)
        {
            var mappings = "";
            foreach (var classData in classesData)
            {
                if (classData == null) continue;
                mappings += classData.MappingString;
            }
            return Templates.Resolver(mappings);
        }

        public IEnumerable<GeneratedClassData> Generate(string xml)
        {
            var doc = XDocument.Parse(xml);
            foreach (var meta in doc.Root.Elements())
            {
                if (meta.Name != "Entity") continue;
                var classData = ProcessNode(meta);
                if (classData == null) continue;
                yield return classData;
            }
        }

        private GeneratedClassData ProcessNode(XElement meta)
        {
            var class1C = meta.Attribute("Type").Value;
            var dataStructure = class1C.Split('.')[0].ToLowerInvariant();

            switch (dataStructure)
            {
                case "регистрсведений":
                    return GenerateEntity<Register1CAttribute>(meta, class1C, typeof(IRegister).Name);
                case "перечисление":
                    return null;
                    return GenerateEnum(meta, class1C);
                default:
                    return GenerateEntity<Entity1CAttribute>(meta, class1C, $"{typeof(BaseDto).Name}, {typeof(IEntity).Name}");
            }
        }

        private GeneratedClassData GenerateEnum(XElement meta, string class1C)
        {
            var className = GetName(class1C);
            var values = meta.Element("Value").Elements();
            foreach (var xValue in values)
            {
                var verbose = xValue.Attribute("Synonym").Value;
                var value1C = xValue.Value;
                var value = ""; //Get translation;
            }
            return null;
        }

        private GeneratedClassData GenerateEntity<TAttribute>(XElement meta, string class1C, string inheritance) where TAttribute : Attribute
        {
            var className = GetName(class1C);
            string docProps;
            var newClass =
                Templates.Class(new Templates.ClassTemplateArguments
                {
                    Attribute = GetAttr<TAttribute>(),
                    TypeName1C = class1C,
                    TypeNameVerbose = meta.Attribute("Verbose").Value,
                    Name = className,
                    Inheritance = inheritance,
                    Body = GetProps(meta, out docProps)
                });

            newClass = newClass.Replace("\t", "    ");
            var mapping = Templates.Mapping(class1C, className);
            var doc = Templates.ClassDoc(className, class1C, docProps);
            return new GeneratedClassData(className, newClass, mapping, doc);
        }

        private string GetProps(XElement meta, out string docProps)
        {
            docProps = "";
            var props = "";
            var propAttrName = GetAttr<Property1CAttribute>();
            var className = meta.Attribute("Type").Value;

            foreach (var prop in meta.Elements())
            {
                string type;
                string attributes;
                string docTypes;
                var verbose = prop.Attribute("Verbose")?.Value.Replace("\"", "\\\"");                
                GetTypeAttrs(prop, out type, out attributes, out docTypes);
                attributes += GetRegisterPropAttr(prop);
                var name = GetName(className, prop.Name.ToString());
                props +=
                    Templates.Property(new Templates.PropertyTemplateArguments
                    {
                        Attribute = propAttrName,
                        Name1C = prop.Name.ToString(),
                        Verbose = verbose,
                        Attributes = attributes,
                        Type = type,
                        Name = name
                    });
                docProps += Templates.PropertyDoc(name, type, prop.Name.ToString());
            }

            return props;
        }

        private static string GetRegisterPropAttr(XElement prop)
        {
            var kind = prop.Attribute("Kind")?.Value;
            var attrName = GetAttr<Register1СPropertyAttribute>();
            var enumName = typeof(Register1CPropertyKind).Name;
            string enumValue;
            switch (kind)
            {
                case "Key":
                    enumValue = Register1CPropertyKind.Key.ToString();
                    break;
                case "Value":
                    enumValue = Register1CPropertyKind.Value.ToString();
                    break;
                case "Info":
                    enumValue = Register1CPropertyKind.Info.ToString();
                    break;
                default:
                    return "";
            }
            return $"\n\t\t[{attrName}({enumName}.{enumValue})]";
        }

        private static void GetTypeAttrs(XElement prop, out string type, out string typeAttrs, out string docType, bool isColumn = false)
        {
            List<string> types, typePerfs;
            GetTypes(prop, out types, out typePerfs);

            var typeAttrName = GetAttr<Type1CAttribute>();
            var colName = isColumn ? $"\"{prop.Name}\", " : "";
            docType = "";
            if (types.Count > 1)
            {
                string typeArr = "", typePerfArr = "";
                for (var i = 0; i < types.Count; i++)
                {
                    var comma = i == types.Count - 1;
                    typeArr += $"\"{types[i]}\"{(comma ? "" : ", ")}";
                    typePerfArr += $"\"{typePerfs[i]}\"{(comma ? "" : ", ")}";
                    docType += $"{types[i]}, ";
                }

                docType = docType.Remove(docType.Length - 2);
                type = typeof(Composite1C).Name;
                typeAttrs = $"\n\t\t[{typeAttrName}({colName}new[] {{ {typeArr} }}, new[] {{ {typePerfArr} }})]";
            }
            else
            {
                type = GetTypeName(types.FirstOrDefault());

                if (type == typeof(Table1C).Name)
                {
                    typeAttrs = "";
                    foreach (var col in prop.Elements())
                    {
                        string colTypeAttrs, colType;
                        GetTypeAttrs(col, out colType, out colTypeAttrs, out docType, true);
                        typeAttrs += colTypeAttrs;
                    }
                    docType = "Table";
                }
                else
                {
                    docType = types.FirstOrDefault();
                    typeAttrs =
                        $"\n\t\t[{typeAttrName}({colName}\"{types.FirstOrDefault()}\", \"{typePerfs.FirstOrDefault()}\")]";
                }
            }
        }

        private static string GetAttr<T>() where T : Attribute
        {
            return typeof(T).Name.Replace("Attribute", "");
        }

        static void GetTypes(XElement prop, out List<string> types, out List<string> typePerfs)
        {
            var i = 0;
            types = new List<string>();
            typePerfs = new List<string>();
            while (true)
            {
                var nextType = prop.Attribute("Type" + i);
                if (nextType == null)
                {
                    nextType = prop.Attribute("Type");
                    if (nextType == null)
                        break;
                    types.Add(nextType.Value);
                    typePerfs.Add(prop.Attribute("TypePerformance")?.Value);
                    break;
                }

                types.Add(nextType.Value);
                typePerfs.Add(prop.Attribute("TypePerformance" + i)?.Value);
                i++;
            }
        }

        static string GetTypeName(string type)
        {
            switch (type)
            {
                case "String":
                case "ValueStorage":
                    return "string";
                case "Boolean":
                    return "bool?";
                case "Date":
                    return "DateTime?";
                case "Number":
                    return "double?";
                case "DataTable":
                    return typeof(Table1C).Name;
                case "":
                    return "object";
                case "Id":
                default:
                    return typeof(Link1C).Name;
            }
        }

        private string GetName(string className, string typeName = null)
        {
            return GetTranslation(className, BreakName(typeName));
            //return BreakName(typeName);
        }

        private static string BreakName(string type)
        {
            if (string.IsNullOrEmpty(type))
                return null;
            var typeArr = type.Split('.');
            if (typeArr.Length < 2)
                throw new Exception("Unexpected typeName");
            var name = typeArr[typeArr.Length - 1];
            name = name.Replace("ITEXP_", "");
            return name;
        }

        private string GetTranslation(string className, string propName = null)
        {
            var classXml = Translations.Root.Element(className);
            if (classXml == null)
            {
                throw new Exception($"Translation for class \"{className}\" not found.");
            }
            if (string.IsNullOrEmpty(propName))
                return classXml.Attribute("name").Value;
            var propXml = classXml.Element(propName);
            if (propXml == null)
            {
                classXml.Add(new XElement(propName));
                throw new Exception($"Translation for property \"{propName}\" not found.");
            }

            return propXml.Value;
        }



        public static void GenerateTranslation(string xml, string translationPath, string translationXml)
        {
            var doc = XDocument.Parse(xml);
            var translations = new XDocument();
            translations.Add(new XElement("Translations"));

            foreach (var entity in doc.Root.Elements())
            {
                var className = entity.Attribute("Type").Value.Replace("\"", "");
                var classTranslation = new XElement(className);
                classTranslation.Add(new XAttribute("name", BreakName(className)));

                foreach (var prop in entity.Elements())
                {
                    if (prop.Name.ToString() == "Values") continue;
                    var name = BreakName(prop.Name.ToString());
                    var translation = GetTranslationForStandartProperty(name);
                    classTranslation.Add(new XElement(name, translation, prop.Attribute("Type0") ?? prop.Attribute("Type")));
                }

                translations.Root.Add(classTranslation);
                translations.Save(translationPath + translationXml);
            }
        }

        private static string GetTranslationForStandartProperty(string name)
        {
            switch (name)
            {
                case "Проведен": return "IsCommited";
                case "Ссылка": return "Link";
                case "ПометкаУдаления": return "IsMarkedToDelete";
                case "Предопределенный": return "IsPredefined";
                case "ИмяПредопределенныхДанных": return "PredefinedDataName";
                case "Код": return "Code";
                case "Дата": return "Date";
                case "Номер": return "Number";
                case "Год": return "Year";
                case "Месяц": return "Month";
                case "Период": return "Period";
                case "Автор": return "Author";
                case "Статус": return "Status";
                case "Версия": return "Version";
                case "Наименование": return "Name";
                default: return name;
            }
        }
    }
}
