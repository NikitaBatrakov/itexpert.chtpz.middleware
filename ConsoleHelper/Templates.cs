﻿namespace ConsoleHelper
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.ObjectModel;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public static class Templates
    {
        public struct ClassTemplateArguments
        {
            public string Attribute { get; set; }
            public string TypeName1C { get; set; }
            public string TypeNameVerbose { get; set; }
            public string Name { get; set; }
            public string Inheritance { get; set; }
            public string Body { get; set; }
        }

        public struct PropertyTemplateArguments
        {
            public string Attribute { get; set; }
            public string Name1C { get; set; }
            public string Verbose { get; set; }
            public string Attributes { get; set; }
            public string Type { get; set; }
            public string Name { get; set; }
        }

        private static string GetAttributeName<T>() => typeof(T).Name.Replace("Attribute", "");

        public static string Attribute<T>(params string[] values) => $"\n\t\t[{GetAttributeName<T>()}](\"{string.Join("\",\"",values)}\")";

        public static string Class(ClassTemplateArguments args) =>
$@"//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using {typeof(Property1CAttribute).Namespace};
using {typeof(Link1C).Namespace};
using {typeof(IEntity).Namespace};

namespace {typeof(BaseDto).Namespace}
{{
    [{args.Attribute}(""{args.TypeName1C}"", ""{args.TypeNameVerbose}"")]
	public partial class {args.Name} : {args.Inheritance}
	{{
		{args.Body}
	}}
}}";

        public static string Property(PropertyTemplateArguments args) =>
$@"
        [{args.Attribute}(""{args.Name1C}"", ""{args.Verbose}"")]{args.Attributes}
        public {args.Type} {args.Name} {{ get; set; }}
";

        public static string Doc(string body) => 
$@"
{{toc:printable=true|style=disc|maxLevel=7|minLevel=1|type=list}}
{body}";

        public static string ClassDoc(string className, string descr, string body) =>
$@"h1. {className}
{{expand:{descr}}}
||Name||Type||1C Name||
{body}
{{expand}}
";

        public static string PropertyDoc(string name, string type, string name1C) => $"|{name}|{type}|{name1C}|\n";

        public static string Mapping(string name, string type) => $"\t\t\t{{ \"{name}\", typeof({type}) }},\n";

        public static string Resolver(string mappings) =>
$@"//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
namespace {typeof(Resolver).Namespace}
{{
    using Sys = System;
    using {typeof(BaseDto).Namespace};

    public partial class Resolver
    {{
        private readonly Sys.Collections.Generic.Dictionary<string, Sys.Type> _maps = 
            new Sys.Collections.Generic.Dictionary<string, Sys.Type>
        {{
{mappings}
        }};
    }}
}}";
    }
}
