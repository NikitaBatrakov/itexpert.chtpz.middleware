namespace ITExpert.CHPTZ.Common.Extensions
{
    using System;
    using System.Globalization;

    public static class DateTimeExtensions
    {
        public static string ToUtcIsoString(this DateTime date)
        {
            //return date.ToString("o");
            return new DateTime(date.Ticks, DateTimeKind.Unspecified).ToUniversalTime().ToString("o");
        }

        public static string ToCultureString(this DateTime date, string culture = "ru-RU")
        {
            return date.ToString(CultureInfo.GetCultureInfo(culture).DateTimeFormat);
        }

        public static string ToShortDateCultureString(this DateTime date, string culture = "ru-RU")
        {
            return date.ToString(CultureInfo.GetCultureInfo(culture).DateTimeFormat.ShortDatePattern);
        }
    }
}