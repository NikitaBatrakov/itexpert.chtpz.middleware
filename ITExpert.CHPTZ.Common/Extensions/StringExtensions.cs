namespace ITExpert.CHPTZ.Common.Extensions
{
    using System;
    using System.Globalization;

    public static class StringExtensions
    {
        public static string LowerFirstLetter(this string s)
        {
            if (s == "") return s;
            return char.ToLowerInvariant(s[0]) + s.Substring(1);
        }

        public static string RemoveLast(this string str, int numberOfCharachters = 1)
        {
            if (numberOfCharachters < 1)
                throw new ArgumentException("Argument numberOfCharacters should be greater than zero");
            return str.Remove(str.Length - numberOfCharachters);
        }

        public static string GetLastPart(this string str, char separator)
        {
            var split = str.Split(separator);
            return split[split.Length - 1];
        }

        public static DateTime ParseToDate(this string value, string culture = "ru-RU")
        {
            return DateTime.Parse(value, CultureInfo.GetCultureInfo(culture).DateTimeFormat);
        }

        public static string CleanNumberString1C(this string str)
        {
            //The symbol being replaced here is not a usual space (!)
            return str.Replace(" ", "");
        }

        public static string Repeat(this string str, int number)
        {
            var result = "";
            for (var i = 0; i < number; i++)
            {
                result += str;
            }
            return str;
        }

        public static string MakeBlock(this string str, string separator = "*", int length = 50)
        {
            var line = "";
            for (var i = 0; i < length; i++)
            {
                line = $"{line}{separator}";
            }
            return $"{line}\n{str}\n{line}";
        }
    }
}