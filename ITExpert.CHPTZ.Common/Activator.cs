﻿namespace ITExpert.CHPTZ.Common
{
    using System;
    using System.Reflection.Emit;

    //Source: https://ayende.com/blog/3167/creating-objects-perf-implications
    /// <summary>
    /// IL Activator which creates instances of specified Type.
    /// Only parameterless constructor of specified Type can be used to instantiate object
    /// </summary>
    public class Activator
    {
        private delegate object CreateCtor();
        private CreateCtor CtorDelegate { get; }

        public Activator(Type type)
        {
            var ctor = type.GetConstructors()[0];

            var method = new DynamicMethod("CreateIntance", type, null);
            var gen = method.GetILGenerator();
            gen.Emit(OpCodes.Newobj, ctor);
            gen.Emit(OpCodes.Ret);
            CtorDelegate = (CreateCtor) method.CreateDelegate(typeof(CreateCtor));
        }

        public object CreateInstance()
        {
            return CtorDelegate();
        }

        public static T CreateInstance<T>()
        {
            return (T) new Activator(typeof(T)).CreateInstance();
        }

        public static object CreateInstance(Type type)
        {
            return new Activator(type).CreateInstance();
        }
    }
}
