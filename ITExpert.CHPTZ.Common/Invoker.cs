namespace ITExpert.CHPTZ.Common
{
    using System;
    using System.Reflection;

    public class Invoker
    {
        public static object Invoke<T>(T obj, string methodName, params object[] args)
        {
            var method = GetMethod<T>(methodName);
            return method.Invoke(obj, args);
        }

        private static MethodInfo GetMethod<T>(string methodName)
        {
            var type = typeof(T);
            var method = type.GetMethod(methodName);
            if (method == null)
            {
                throw new ArgumentException("Method not found. Check if method name is correct");
            }
            return method;
        }
    }
}