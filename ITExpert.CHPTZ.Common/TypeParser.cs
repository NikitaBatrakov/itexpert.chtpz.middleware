﻿namespace ITExpert.CHPTZ.Common
{
    using System;

    public class TypeParser
    {
        public static Type Parse(string name, Type baseType, bool throwOnError = true)
        {
            if (string.IsNullOrEmpty(name))
            {
                if (throwOnError)
                {
                    throw new ArgumentNullException("Name argument is null");
                }
                return null;
            }

            name = name.Trim();
            var assemblyName = baseType.Assembly.FullName;
            var ns = baseType.Namespace;
            var aqn = $"{ns}.{name}, {assemblyName}";
            var type = Type.GetType(aqn, false, true);
            if (type == null && throwOnError)
            {
                throw new TypeLoadException($"Failed to retrieve '{name}' - type is not found");
            }
            return type;
        }
    }
}
