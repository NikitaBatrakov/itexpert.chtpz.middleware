﻿namespace ITExpert.CHPTZ.ObjectModel
{
    using System;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    public class EntityFactory
    {
        public static IEntity Create(Type type)
        {
            return (IEntity)Common.Activator.CreateInstance(type);
        }

        public static IEntity Create(Type type, string guid)
        {
            var obj = Create(type);
            obj.Link = new Link1C(guid);
            return obj;
        }

        public static T Create<T>(string guid) where T : IEntity, new()
        {
            return new T {Link = new Link1C(guid)};
        }
    }
}
