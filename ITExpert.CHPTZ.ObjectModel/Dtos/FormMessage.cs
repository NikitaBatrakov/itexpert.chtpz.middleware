namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System.Runtime.Serialization;

    [DataContract]
    public class FormMessage
    {
        [DataMember(Name = "field")]
        public string Field { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        public FormMessage()
        {
            
        }

        public FormMessage(string field, string value)
        {
            Field = field;
            Value = value;
        }
    }
}