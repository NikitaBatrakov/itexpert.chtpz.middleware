﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [DataContract]
    public class AdditionalAttributeValue
    {
        [DataMember(Name = "object")]
        public Link1C Object { get; set; }

        [DataMember(Name = "type")]
        public Link1C Type { get; set; }

        [DataMember(Name = "value")]
        public object Value { get; set; }

        public AdditionalAttributeValue()
        {
            
        }

        public AdditionalAttributeValue(Link1C obj, Link1C type, object value)
        {
            Object = obj;
            Type = type;
            Value = value;
        }
    }
}
