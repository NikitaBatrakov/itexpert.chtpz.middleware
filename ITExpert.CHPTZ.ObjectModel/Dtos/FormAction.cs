namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System.Runtime.Serialization;

    [DataContract]
    public class FormAction
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "available")]
        public bool Available { get; set; }

        [DataMember(Name = "visible")]
        public bool Visible { get; set; }

        public FormAction()
        {
            
        }

        public FormAction(string name, bool available = false, bool visible = false)
        {
            Name = name;
            Available = available;
            Visible = visible;
        }
    }
}