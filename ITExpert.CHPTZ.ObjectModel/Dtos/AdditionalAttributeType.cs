﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [DataContract]
    public class AdditionalAttributeType
    {
        [DataMember(Name = "link")]
        public Link1C Link { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "type")]
        public PropertyType Type { get; set; }

        [DataMember(Name = "isRequired")]
        public bool IsRequired { get; set; }

        public AdditionalAttributeType()
        {
            
        }

        public AdditionalAttributeType(string name, Link1C link, PropertyType type)
        {
            Name = name;
            Link = link;
            Type = type;
        }

        public AdditionalAttributeType(string name, string guid, string linkPerformance, PropertyType type)
        {
            Link = new Link1C(guid, linkPerformance, "Id");
            Name = name;
            Type = type;
        }
    }
}
