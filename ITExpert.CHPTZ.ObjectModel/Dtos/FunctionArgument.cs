﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System;

    public class FunctionArgument
    {
        public string Name { get; set; }

        public Type Type { get; set; }

        public string Value { get; set; }

        public FunctionArgument()
        {
            
        }

        public FunctionArgument(Type type, string value)
        {
            Name = "Arg";
            Type = type;
            Value = value;
        }

        public FunctionArgument(string name, Type type, string value)
        {
            Name = name;
            Type = type;
            Value = value;
        }

        public static FunctionArgument Create<T>(T value)
        {
            return Create("Arg", value);
        }

        public static FunctionArgument Create<T>(string name, T value)
        {
            return new FunctionArgument(name, typeof(T), value.ToString());
        }

        public static FunctionArgument Create<T>(string name, string value)
        {
            return new FunctionArgument(name, typeof(T), value);
        }

        public static FunctionArgument Create<T>(string value)
        {
            return Create<T>("Arg", value);
        }
    }
}
