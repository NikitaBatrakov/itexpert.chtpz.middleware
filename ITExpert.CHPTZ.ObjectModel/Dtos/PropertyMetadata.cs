namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    [DataContract]
    public class PropertyMetadata
    {
        [DataMember(Name = "type")]
        public PropertyType Type { get; set; }

        [DataMember(Name = "verbose")]
        public string Verbose { get; set; }

        [DataMember(Name = "available")]
        public bool? Available { get; set; }

        [DataMember(Name = "visible")]
        public bool? Visible { get; set; }

        [DataMember(Name = "required")]
        public bool? Required { get; set; }

        public PropertyMetadata()
        {
            
        }

        public PropertyMetadata(PropertyType type, string verbose)
        {
            Type = type;
            Verbose = verbose;
        }
    }
}