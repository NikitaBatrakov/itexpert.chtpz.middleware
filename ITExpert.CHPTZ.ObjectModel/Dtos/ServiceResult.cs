namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class ServiceResult
    {
        [DataMember(Name = "code")]
        public int Code { get; }

        [DataMember(Name = "text")]
        public string Text { get; }

        [DataMember(Name = "date")]
        public DateTime Date { get; } = DateTime.Now;

        public bool IsOk => Code == 0;

        public ServiceResult()
        {
            
        }

        public ServiceResult(int code, string text)
        {
            Code = code;
            Text = text;
        }

        public static ServiceResult CreateOk()
        {
            return new ServiceResult(0, "");
        }
    }
}