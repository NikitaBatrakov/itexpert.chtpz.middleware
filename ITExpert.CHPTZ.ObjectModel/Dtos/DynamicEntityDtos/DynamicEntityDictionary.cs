﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos.DynamicEntityDtos
{
    using System;
    using System.Collections.Generic;

    //Colleciton of <Prop name, Prop value> - all entity properties
    [Serializable]
    public class DynamicEntityDictionary : Dictionary<string, DynamicEntityPropertyValue> { }
}
