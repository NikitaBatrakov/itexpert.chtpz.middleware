﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos.DynamicEntityDtos
{
    public class DynamicEntityPropertyValue
    {
        public string Type { get; set; }

        public object Value { get; set; }

        public string LinkPerformance { get; set; }

        public DynamicEntityPropertyValue(string type, object value, string linkPerformance = "")
        {
            Type = type;
            Value = value;
            LinkPerformance = linkPerformance;
        }
    }
}
