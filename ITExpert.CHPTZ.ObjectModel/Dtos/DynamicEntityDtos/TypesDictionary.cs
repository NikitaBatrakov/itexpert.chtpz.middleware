﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos.DynamicEntityDtos
{
    using System;
    using System.Collections.Generic;

    //Colletion of <Type name, entity properties> - all types
    [Serializable]
    public class TypesDictionary : Dictionary<string, ICollection<DynamicEntityDictionary>> { }
}