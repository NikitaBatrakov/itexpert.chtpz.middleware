﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos
{
    using System.Collections.Generic;

    public class TemplateAttributes
    {
        public string TemplateGuid { get; set; }

        public IEnumerable<TemplateAttribute> Attributes { get; set; }

        public TemplateAttributes()
        {
            
        }

        public TemplateAttributes(string guid)
        {
            TemplateGuid = guid;
        }

        public TemplateAttributes(string guid, IEnumerable<TemplateAttribute> attributes)
        {
            TemplateGuid = guid;
            Attributes = attributes;
        }
    }
}
