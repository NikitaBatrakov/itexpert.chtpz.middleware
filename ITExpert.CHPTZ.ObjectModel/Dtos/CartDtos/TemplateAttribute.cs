﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos
{
    public class TemplateAttribute
    {
        public string AttributeTypeGuid { get; set; }

        public string ValueType { get; set; }

        public string ServiceGuid { get; set; }

        public string BusinessProcessTemplateGuid { get; set; }

        public string Value { get; set; }

        public TemplateAttribute()
        {
            
        }

        public TemplateAttribute(string attributeTypeGuid, string serviceGuid, 
                                 string bpTemplateGuid, string valueType, string value)
        {
            AttributeTypeGuid = attributeTypeGuid;
            ValueType = valueType;
            ServiceGuid = serviceGuid;
            BusinessProcessTemplateGuid = bpTemplateGuid;
            Value = value;
        }
    }
}