﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos
{
    using System.Collections.Generic;

    public class CartElement
    {
        public string ServiceGuid { get; set; }
        public string TemplateGuid { get; set; }
        public string ProcessTemplateGuid { get; set; }
        public string ConfigurationUnitGuid { get; set; }
        public string StatusGuid { get; set; }
        public IEnumerable<CartElementComment> Comments { get; set; }

        public string EmployeeGuid { get; set; }

        public string ServiceRequestGuid { get; set; }
    }
}
