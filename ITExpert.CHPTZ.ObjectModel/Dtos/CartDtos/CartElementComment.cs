namespace ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos
{
    using System;

    public class CartElementComment
    {
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public string EmployeeGuid { get; set; }
        public string ApprovingNodeName { get; set; }
    }
}