namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System.Runtime.Serialization;

    [DataContract]
    public class FoundType
    {
        [DataMember(Name = "type")]
        public string Type { get; }

        [DataMember(Name = "count")]
        public int Count { get; }

        public FoundType(string type, int count)
        {
            Type = type;
            Count = count;
        }
    }
}