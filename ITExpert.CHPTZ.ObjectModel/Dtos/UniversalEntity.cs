﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    public class ObjectProperty1C
    {
        public object Value { get; set; }
        public string Type { get; set; }
        public string LinkPerformance { get; set; }

        public ObjectProperty1C(object value)
        {
            Value = value;
        }

        public ObjectProperty1C(object value, string type, string linkPerformance)
        {
            Value = value;
            Type = type;
            LinkPerformance = linkPerformance;
        }

        public Link1C ToLink()
        {
            return new Link1C(Value.ToString(), LinkPerformance, Type);
        }
    }

    public class Entity1C : Object1C
    {
        public Link1C Link
        {
            get { return GetPropertyValueOrNull("Link").ToLink(); }
            set { SetProperty("Link", value); }
        }

        public bool? IsMarkedToDelete
        {
            get { return GetPrimitivePropertyValueOrNull<bool>("IsMarkedToDelete"); }
            set { SetPrimitivePropertyValue("IsMarkedToDelete", value); }
        }

        public IEnumerable<AdditionalAttribute1C> Attributes { get; set; }
        public ICollection<Attachment1C> Attachments { get; set; }

        public Entity1C()
        {
            Attachments = new List<Attachment1C>();
            Attachments = new List<Attachment1C>();
        }
    }

    public class Object1C
    {
        public ObjectProperty1C this[string name]
        {
            get { return Properties[name]; }
            set { Properties[name] = value; }
        }

        protected IDictionary<string, ObjectProperty1C> Properties { get; }

        public Object1C()
        {
            Properties = new Dictionary<string, ObjectProperty1C>();
        }

        public IDictionary<string, ObjectProperty1C> ToDictionary()
        {
            return Properties;
        }

        protected ObjectProperty1C GetPropertyValueOrNull(string name)
        {
            ObjectProperty1C value;
            var isValueNull = !Properties.TryGetValue(name, out value);
            return isValueNull ? null : value;
        }

        protected T? GetPrimitivePropertyValueOrNull<T>(string name) where T : struct
        {
            var prop = GetPropertyValueOrNull(name);
            return (T)Convert.ChangeType(prop.Value, typeof(T));
        }

        protected void SetProperty(string name, Link1C value)
        {
            Properties[name] = new ObjectProperty1C(value.Value, value.Type, value.LinkPerformance);
        }

        protected void SetPrimitivePropertyValue<T>(string name, T value)
        {
            var prop = GetPropertyValueOrNull(name);
            if (prop != null)
            {
                prop.Value = value;
                return;
            }

            Properties.Add(name, new ObjectProperty1C(value));
        }
    }
}
