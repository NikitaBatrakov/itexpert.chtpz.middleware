namespace ITExpert.CHPTZ.ObjectModel.Dtos.SearchDtos
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    [DataContract]
    public class AvailableEntityFilter
    {
        [DataMember(Name = "field")]
        public string Field { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "verbose")]
        public string Verbose { get; set; }

        [DataMember(Name = "isAdditionalAttribute")]
        public bool IsAdditionalAttribute { get; set; }

        [DataMember(Name = "isRequired")]
        public bool IsRequired { get; set; }

        [DataMember(Name = "isKey")]
        public bool IsKey { get; set; }

        [DataMember(Name = "isLink")]
        public bool IsLink { get; set; }

        [DataMember(Name = "operators")]
        public IEnumerable<ComparisonOperator1C> Operators { get; set; }
    }
}