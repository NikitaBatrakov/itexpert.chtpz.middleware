namespace ITExpert.CHPTZ.ObjectModel.Dtos.SearchDtos
{
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [DataContract]
    public class UserFilterParameters
    {
        [Property1C("SerchParametrs.������")]
        [DataMember(Name = "link")]
        public Link1C Link { get; set; }

        [Property1C("SerchParametrs.������������")]
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [Property1C("SerchParametrs.���������������������������")]
        [DataMember(Name = "additionalAttributeType")]
        public Link1C AdditionalAttributeType { get; set; }

        [Property1C("SerchParametrs.�������")]
        [DataMember(Name = "order")]
        public string Order { get; set; }

        [Property1C("SerchParametrs.���������������������������")]
        [DataMember(Name = "totalResults")]
        public double? TotalResults { get; set; }
    }
}