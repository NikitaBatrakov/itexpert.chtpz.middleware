﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos.SearchDtos
{
    using System.Collections.Generic;

    public class UserFilter
    {
        public UserFilterParameters Parameters { get; set; }
        public IEnumerable<AvailableEntityFilter> Filters { get; set; }
        public IEnumerable<AvailableEntityField> Fields { get; set; }
    }
}
