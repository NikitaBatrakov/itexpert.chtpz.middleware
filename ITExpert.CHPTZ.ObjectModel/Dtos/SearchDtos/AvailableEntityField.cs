namespace ITExpert.CHPTZ.ObjectModel.Dtos.SearchDtos
{
    using System.Runtime.Serialization;

    [DataContract]
    public class AvailableEntityField
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "verbose")]
        public string Verbose { get; set; }

        [DataMember(Name = "isAdditionalAttribute")]
        public bool IsAdditionalAttribute { get; set; }

        [DataMember(Name = "isKey")]
        public bool IsKey { get; set; }

    }
}