﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System.Collections.Generic;

    public class Function
    {
        public Function(string name, IEnumerable<FunctionArgument> arguments)
        {
            Name = name;
            Arguments = arguments;
        }

        public string Name { get; set; }

        public string ReturnType { get; set; }
        
        public string TargetType { get; set; }

        public IEnumerable<FunctionArgument> Arguments { get; set; }

        public Function()
        {
            
        }

        public Function(string name)
        {
            Name = name;
        }

        public Function(string name, string returnType, IEnumerable<FunctionArgument> arguments)
        {
            Name = name;
            ReturnType = returnType;
            Arguments = arguments;
        }



        public Function(string name, string returnType, string targetType, IEnumerable<FunctionArgument> arguments)
        {
            Name = name;
            ReturnType = returnType;
            TargetType = targetType;
            Arguments = arguments;
        }
    }
}