﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System.Runtime.Serialization;

    [DataContract]
    public class NotificationsNumber
    {
        [DataMember(Name = "employeeGuid")]
        public string EmployeeGuid { get; }

        [DataMember(Name = "value")]
        public int Value { get; }

        public NotificationsNumber(int value, string employeeGuid)
        {
            Value = value;
            EmployeeGuid = employeeGuid;
        }
    }
}
