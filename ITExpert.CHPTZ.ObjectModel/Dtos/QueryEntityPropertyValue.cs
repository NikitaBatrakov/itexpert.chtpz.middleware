﻿namespace ITExpert.CHPTZ.ObjectModel.Dtos
{
    using System.Runtime.Serialization;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    [DataContract]
    public class QueryEntityPropertyValue
    {
        [DataMember(Name = "type")]
        public PropertyType Type { get; set; }

        [DataMember(Name = "value")]
        public object Value { get; set; }

        public bool IsEmpty => Value == null || Value.ToString().ToLowerInvariant() == "null";

        public QueryEntityPropertyValue()
        {
            
        }

        public QueryEntityPropertyValue(PropertyType type, object value)
        {
            Type = type;
            Value = value;
        }
    }
}
