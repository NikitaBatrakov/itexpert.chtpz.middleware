﻿namespace ITExpert.CHPTZ.ObjectModel
{
    using System;
    using System.Reflection;
    using BackendTypes.Exceptions;
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.ObjectModel.Entities;

    public partial class Resolver : IMResolver
    {
        public Type GetInternalType(string name1C, bool throwOnError = true)
        {
            try
            {
                return _maps[name1C];
            }
            catch
            {
                if (throwOnError)
                {
                    throw new ResolvementException($"Failed to resolve '{name1C}' - type is not mapped");
                }
                return null;
            }
        }

        public string GetExternalTypeName(Type type, bool throwOnError = true)
        {
            if (type == null)
            {
                if (throwOnError)
                {
                    throw new ArgumentNullException("Type argument is null");
                }
                return null;
            }

            var externalType = type.GetCustomAttribute<Entity1CAttribute>()?.Name;
            if (string.IsNullOrEmpty(externalType) && throwOnError)
            {
                throw new ResolvementException(
                    $"Failed to retrieve external type name of '{type.Name}'" +
                    " - check if Entity1CAttribute is set for specified type");
            }
            return externalType;
        }

        public string GetExternalTypeName(string className, bool throwOnError = true)
        {
            var type = GetClassType(className, throwOnError);
            return GetExternalTypeName(type, throwOnError);
        }

        public Type GetClassType(string name, bool throwOnError = true)
        {
            if (string.IsNullOrEmpty(name))
            {
                if (throwOnError)
                {
                    throw new ArgumentNullException("Name argument is null");
                }
                return null;
            }

            name = name.Trim();
            var baseType = typeof(BaseDto);
            var assemblyName = baseType.Assembly.FullName;
            var ns = baseType.Namespace;
            var aqn = $"{ns}.{name}, {assemblyName}";
            var type = Type.GetType(aqn, false, true);
            if (type == null && throwOnError)
            {
                throw new ResolvementException($"Failed to retrieve '{name}' - type is not found");
            }
            return type;
        }
    }
}
