﻿using Generic = System.Collections.Generic;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    public partial class Order
    {
        public Generic.ICollection<OrderComment> Comments { get; set; } = new Generic.List<OrderComment>();

        public Generic.ICollection<ServiceTemplateAdditionalAttribute> TemplateAttributes { get; set; } =
            new Generic.List<ServiceTemplateAdditionalAttribute>();
    }
}
