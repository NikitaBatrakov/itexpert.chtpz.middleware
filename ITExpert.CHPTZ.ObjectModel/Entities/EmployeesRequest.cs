﻿using DateTime = System.DateTime;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [Entity1C("Query")]
    public class EmployeesRequest : BaseDto, IEntity
    {
        [Property1C("Link", "Ссылка")]
        [Type1C("Id", "Id")]
        public Link1C Link { get; set; }

        public bool? IsMarkedToDelete { get; set; } = null;

        [Property1C("Description", "Ссылка")]
        [Type1C("String", "Строка")]
        public string Description { get; set; }

        [Property1C("Status", "Заголовок")]
        [Type1C("String", "Строка")]
        public string Status { get; set; }

        [Property1C("StatusColorCode", "Цветовой код")]
        [Type1C("String", "Строка")]
        public string StatusColorCode { get; set; }

        [Property1C("Date", "Дата создания")]
        [Type1C("Date", "Дата")]
        public DateTime? Date { get; set; }

        [Property1C("Type", "Тип")]
        [Type1C("String", "Строка")]
        public string Type { get; set; }

        [Property1C("Entity", "Сущность")]
        [Type1C("String", "Строка")]
        public string Entity { get; set; }
    }
}
