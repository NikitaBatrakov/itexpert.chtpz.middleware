﻿namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;

    public partial class BusinessProcess
    {
        [Property1C("Справочник.ITEXP_БизнесПроцесс.active", "Активен")]
        [Type1C("Boolean", "Булево")]
        public bool? IsActive { get; set; }
    }
}
