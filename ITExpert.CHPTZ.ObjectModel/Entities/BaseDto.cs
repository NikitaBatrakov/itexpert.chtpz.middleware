﻿using System.Collections.Generic;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    public class BaseDto
    {
        public ICollection<Attachment1C> Attachments { get; set; }
        
        public IEnumerable<AdditionalAttribute1C> Attributes { get; set; }
    }
}
