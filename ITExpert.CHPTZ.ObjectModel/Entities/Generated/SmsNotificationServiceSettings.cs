//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Register1C("РегистрСведений.ITEXP_НастройкаSMSОповещенийПоУслугам", "ITEXP настройка SMSОповещений по услугам")]
    public partial class SmsNotificationServiceSettings : IRegister
    {
        
        [Property1C("РегистрСведений.ITEXP_НастройкаSMSОповещенийПоУслугам.Услуга", "Услуга")]
        [Type1C("Справочник.Service", "Услуги")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C Service { get; set; }

        [Property1C("РегистрСведений.ITEXP_НастройкаSMSОповещенийПоУслугам.Сотрудник", "Сотрудник")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C Employee { get; set; }

        [Property1C("РегистрСведений.ITEXP_НастройкаSMSОповещенийПоУслугам.ТипСогласования", "Тип согласования")]
        [Type1C("Перечисление.ТипСогласования", "Тип согласования")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C ApprovementType { get; set; }

        [Property1C("РегистрСведений.ITEXP_НастройкаSMSОповещенийПоУслугам.Расписание", "Расписание")]
        [Type1C("Справочник.Timetable", "Расписание")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C Timetable { get; set; }

    }
}