//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Register1C("РегистрСведений.ЖурналБлокировок", "Журнал блокировок")]
    public partial class LockedObject : IRegister
    {
        
        [Property1C("РегистрСведений.ЖурналБлокировок.Период", "Период")]
        [Type1C("Date", "Дата")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public DateTime? Period { get; set; }

        [Property1C("РегистрСведений.ЖурналБлокировок.Объект", "Объект")]
        [Type1C(new[] { "Задача.Task", "Документ.ServicRequest", "Документ.РаспределениеРазработчиковПоКомандам", "Документ.Problem", "Документ.ПланКоманды", "Документ.Appeal", "Документ.Incident", "Документ.УдалитьChangeRequest", "Документ.ChangeRequest", "Документ.ITEXP_ЛистУчетаВремени", "Документ.YearlyPlan", "Документ.ПроизводственныйКалендарьРазработчиков", "Документ.Итерация", "Документ.Confirm", "Документ.ШаблонРаспределенияРазработчиков", "Документ.МесячныйПлан", "Документ.Сообщение", "Документ.НормаНаСопровождение" }, new[] { "Задание", "Запрос на обслуживание", "Распределение разработчиков по командам", "Проблема", "План команды", "Обращение", "Инцидент", "Удалить Запрос на изменение", "Запрос на изменение", "Лист учета времени", "Годовой план", "Производственный календарь разработчиков", "Итерация", "Согласование", "Шаблон распределения разработчиков", "Месячный план", "Сообщение", "Норма на сопровождение" })]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Composite1C Object { get; set; }

        [Property1C("РегистрСведений.ЖурналБлокировок.Сотрудник", "Сотрудник")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C Employee { get; set; }

    }
}