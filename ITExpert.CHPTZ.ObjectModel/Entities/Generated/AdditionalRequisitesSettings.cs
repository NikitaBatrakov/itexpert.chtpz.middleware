//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Entity1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов", "Хранение настроек для доп реквизитов")]
    public partial class AdditionalRequisitesSettings : BaseDto, IEntity
    {
        
        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ИмяПредопределенныхДанных", "ИмяПредопределенныхДанных")]
        [Type1C("String", "Строка")]
        public string PredefinedDataName { get; set; }

        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.Предопределенный", "Предопределенный")]
        [Type1C("Boolean", "Булево")]
        public bool? IsPredefined { get; set; }

        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ПометкаУдаления", "ПометкаУдаления")]
        [Type1C("Boolean", "Булево")]
        public bool? IsMarkedToDelete { get; set; }

        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.Наименование", "Наименование")]
        [Type1C("String", "Строка")]
        public string Name { get; set; }

        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.Код", "Код")]
        [Type1C("Number", "Число")]
        public double? Code { get; set; }

        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.СсылкаНаНастройку", "Ссылка на настройку")]
        [Type1C("Справочник.ITEXP_НастройкиДляЗагрузкиСправочниковИлиДокументовИзБД", "Настройки для загрузки справочников/документов из БД")]
        public Link1C SettingsLink { get; set; }

        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТипДополнительногоРеквизита", "Тип дополнительного реквизита")]
        [Type1C("Справочник.ITEXP_ТипДополнительногоРеквизита", "Типы объектов")]
        public Link1C AdditionalReqisiteType { get; set; }

        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаСоответствияРеквизитаИ_Значения", "Таблица соответствия реквизита и значения")]
        [Type1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаСоответствияРеквизитаИ_Значения.НомерСтроки", "Number", "Число")]
        [Type1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаСоответствияРеквизитаИ_Значения.ИмяДополнительногоРеквизита", "String", "Строка")]
        [Type1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаСоответствияРеквизитаИ_Значения.ИмяКолонкиВ_БазеДанных", "String", "Строка")]
        [Type1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаСоответствияРеквизитаИ_Значения.ЗначениеПоУмолчанию", new[] { "Справочник.ITEXP_ИсполняемыйФайл", "Справочник.ITEXP_СтатьиБазыЗнаний", "Перечисление.ITEXP_МетодыПоискаСсылочныхДанных", "Справочник.ITEXP_ШаблоныЗаполненияЗадание", "Задача.Task", "Справочник.OperatingCategory", "Справочник.SLA", "Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов", "Справочник.Location", "Справочник.ITEXP_ШаблоныЗаполненияИнцидент", "Справочник.ITEXP_ПользовательскиеНастройкиUI", "Справочник.ITEXP_ТипБД", "Справочник.YearlyPlanStatus", "Перечисление.ТипыСообщений", "Справочник.ITEXP_СправочникОтветов", "Документ.ServicRequest", "Справочник.Priority", "Перечисление.КодыАлгоритма", "Перечисление.ТипСервера", "Перечисление.ПровайдерыSMS", "Перечисление.ВидыДнейПроизводственногоКалендаря", "Справочник.IncidentCategory", "Справочник.ITEXP_Система", "Справочник.ITEXP_Вендоры", "Справочник.ITEXP_ШаблонОтвета", "Справочник.AttachedFiles", "Перечисление.СтатусыИзвлеченияТекстаФайлов", "Документ.РаспределениеРазработчиковПоКомандам", "Документ.Problem", "Справочник.ITEXP_ТипДокумента", "Справочник.ClosingCode_ChangeRequest", "Справочник.КаталогЦОВ", "Справочник.тмп", "Перечисление.Эскалации", "Справочник.ITEXP_СостояниеВСистемеМониторинга", "Справочник.Priority_ServicRequest", "Справочник.ITEXP_Новости", "Справочник.ITEXP_ТипыФайлов", "Справочник.ITEXP_СтатусыСтатейБазыЗнаний", "Справочник.ITEXP_СтатусПроекта", "Справочник.ITEXP_ЧасовыеПояса", "Документ.ПланКоманды", "Документ.Appeal", "Справочник.ChangeRequestPriority", "Перечисление.ВыдыУведомленийПользователя", "Документ.Incident", "Справочник.InfluenceDegree", "Документ.УдалитьChangeRequest", "Документ.ChangeRequest", "Справочник.ITEXP_ШаблоныЗаполненияЗНО", "Справочник.ITEXP_НастройкаПоиска", "Справочник.ITEXP_КЕ", "Справочник.ITEXP_ТипСвязи", "Справочник.ITEXP_ШаблоныДляСозданияОбъектовПоРасписанию", "Справочник.ПочтовыеСервера", "Документ.ITEXP_ЛистУчетаВремени", "Справочник.ClosingCode_ServicRequest", "Справочник.OrganizationCategory", "Справочник.Status", "Документ.YearlyPlan", "Справочник.ITEXP_Классификатор", "Справочник.ITEXP_Мероприятие", "Справочник.ITEXP_Основания", "Boolean", "Справочник.УчетныеЗаписиЭлектроннойПочты", "Справочник.Messages", "Справочник.ITEXP_СтатусЗаказа", "Справочник.ITEXP_БизнесПроцесс", "Справочник.ITEXP_ТипЗадачи", "Справочник.UserRating", "Справочник.ITEXP_ЭтапПроекта", "Справочник.ChangeRequestStatus", "Перечисление.СтатусыМесячныхПланов", "Справочник.AnalystsRating", "Перечисление.ITEXP_ТипУчетаВремени", "Перечисление.ITEXP_ВидыЧасовыхПоясов", "Справочник.ITEXP_ШаблонОповещения", "Справочник.Lines", "Перечисление.ITEXP_ВидыЗагрузокИзБД", "Справочник.Алгоритмы", "Справочник.ITEXP_СвязиОбъектов", "Документ.ПроизводственныйКалендарьРазработчиков", "Справочник.ITEXP_ПроектнаяКоманда", "Справочник.СправочникУсловийДействий", "Справочник.SLA_ServicRequest", "Справочник.TaskClosingCode", "Справочник.Calendar", "Справочник.ДействияСценария", "Справочник.ClosingCode", "Справочник.Timetable", "Перечисление.ITEXP_ТипВопроса", "Справочник.Patch", "Справочник.IncidentStatus", "Справочник.ITEXP_СтатусыНовостей", "String", "Перечисление.ТипыЗадач", "Справочник.ITEXP_ТипыНовостей", "Справочник.ITEXP_СхемаЛицензирования", "Справочник.ITEXP_ТипДополнительногоРеквизита", "Справочник.Chat", "Документ.Итерация", "Справочник.User", "Справочник.Employee", "Справочник.TaskStatus", "Справочник.ServicRequestStatus", "Date", "Справочник.ITEXP_Проекты", "Справочник.ITEXP_УровеньИерархии", "Справочник.ITEXP_Площадки", "Документ.Confirm", "Справочник.OLA", "Number", "Справочник.ClosingCode_ConsultationRequest", "Документ.ШаблонРаспределенияРазработчиков", "Справочник.ITEXP_ТипЛицензии", "Справочник.AppealSource", "Справочник.GrField", "Справочник.Константы", "Справочник.TaskClosingCode_Document", "Справочник.Organization", "Справочник.ITEXP_Архитектура", "Справочник.ITEXP_Направление", "Справочник.ITEXP_СтатусКЕ", "Перечисление.ВариантыУсловий", "Справочник.УдалитьITEXP_СправочникВопросов", "Справочник.ГруппыСообщений", "Справочник.ITEXP_ТипСтруктуры", "Документ.МесячныйПлан", "Справочник.ITEXP_СправочникВопросов", "Справочник.InvolvementDegree", "Справочник.ITEXP_ДополнительныеСвойстваУслуги", "ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения", "Справочник.ReassignmentReason", "Справочник.ITEXP_Документ", "Перечисление.ТипыТекстовСообщения", "Документ.Сообщение", "Справочник.ЗначенияСвойствОбъектов", "Справочник.UsageTerm", "Справочник.ITEXP_КатегорияБизнесПроцесса", "Справочник.FunctionalGroups", "Справочник.Service", "БизнесПроцесс.ВыполнениеАлгоритма", "Справочник.ITEXP_НастройкиДляЗагрузкиСправочниковИлиДокументовИзБД", "Справочник.OLA_ServicRequest", "Справочник.ITEXP_ПризнакГруппировкиУслугиДляПользователя", "Перечисление.ТипыСогласований", "Справочник.SuspensionReason", "Справочник.ITEXP_График", "Справочник.ITEXP_Модель", "Справочник.ПроцедурыИсполнения", "Справочник.Module", "Перечисление.ВидыИнтерваловИсполненияЗадач", "Перечисление.ТипыТаймеров", "ТочкаМаршрутаБизнесПроцесса.ВыполнениеАлгоритма", "Справочник.IncidentCause", "Справочник.MethodOfSolution", "Документ.НормаНаСопровождение", "Справочник.ITEXP_ПризнакГруппировкиУслугиДляСпециалиста", "Справочник.ITEXP_ПроектнаяЗадача", "Справочник.ГруппыДоступа", "Справочник.CategoryAppeal", "Перечисление.ТипСогласования" }, new[] { "Исполняемый файл", "Статья базы знаний", "Методы поиска ссылочных данных", "Шаблон заполнения задания", "Задание", "Операционная категория", "Соглашения SLA (Инцидент)", "Хранение настроек для доп реквизитов", "Местоположение", "Шаблоны заполнения инцидент", "ITEXP_ПользовательскиеНастройкиUI", "Тип БД", "Статус годового плана", "Типы сообщений", "Справочник ответов", "Запрос на обслуживание", "Приоритет", "Коды алгоритма", "Тип сервера", "Провайдеры SMS", "Виды дней производственного календаря", "Категория инцидента", "Система", "Вендоры", "Шаблон ответа", "Хранимые файлы", "Статусы извлечения текста файлов", "Распределение разработчиков по командам", "Проблема", "Тип документа", "Коды закрытия ЗНИ", "Категория ЦОВ", "Тмп", "Эскалации", "Состояние в системе мониторинга", "Приоритет", "Новость", "Типы файлов", "Статус статьи базы знаний", "Статус проекта", "Часовые пояса", "План команды", "Обращение", "Приоритет", "Выды уведомлений пользователя", "Инцидент", "Степень влияния", "Удалить Запрос на изменение", "Запрос на изменение", "Шаблоны заполнения ЗНО", "Настройка поиска", "Конфигурационные единицы", "Тип связи", "Шаблон для создания объекта по расписанию", "Почтовый сервер", "Лист учета времени", "Коды закрытия ЗНО", "Категории организации", "Статусы заданий", "Годовой план", "Классификатор", "Мероприятие", "Основания", "Булево", "Учетная запись э/п", "Messages", "Статус заказа", "Бизнес процесс", "Тип задачи", "Оценка пользователя", "Этап проекта", "Статус ЗНИ", "Статусы месячных планов", "Оценка аналитика", "Тип учета времени", "Виды часовых поясов", "Шаблон оповещения", "Линии поддержки", "Виды загрузок из БД", "Шаблон процесса", "Связи объектов", "Производственный календарь разработчиков", "Проектная команда", "Справочник условий действий", "Соглашения SLA (Запрос на обслуживание)", "Код закрытия задания", "Производственный календарь", "Действие процесса", "Коды закрытия Инцидент", "Расписание", "Тип вопроса", "Патч", "Статусы инцидента", "Статус новости", "Строка", "Типы задач", "Типы новостей", "Схема лицензирования", "Типы объектов", "Chat", "Итерация", "Пользователь", "Сотрудники", "Статусы заданий", "Статусы ЗНО", "Дата", "Проект", "Уровень иерархии", "Площадки", "Согласование", "Соглашения OLA (Инцидент)", "Число", "Коды закрытия ЗНК", "Шаблон распределения разработчиков", "Тип лицензии", "Способы обращения", "Группы полей и ограничения доступа", "Константы", "Код закрытия документа задания", "Структура управления", "Архитектура", "Направление", "Статус КЕ", "Варианты условий", "Справочник вопросов", "Группа сообщений", "Тип структуры", "Месячный план", "Справочник вопросов", "Степень вовлеченности", "Дополнительные свойства услуги", "Дополнительные реквизиты и сведения", "Причина переназначения", "Документ", "Типы текстов сообщения", "Сообщение", "Значения свойств объектов", "Срок применения", "Категория бизнес-процесса", "Функциональные группы", "Услуги", "Процесс (выполнение)", "Настройки для загрузки справочников/документов из БД", "Соглашения OLA (Запрос на обслуживание)", "Признак группировки услуги для пользователя", "Типы согласований", "Причина приостановки", "График", "Модель", "Процедура исполнения", "Модуль", "Виды интервалов исполнения задач", "Типы таймеров", "Процесс (выполнение)(точка маршрута)", "Причина инцидента", "Метод решения", "Норма на сопровождение", "Признак группировки услуги для специалиста", "Проектная задача", "Группы доступа", "Категории обращения", "Тип согласования" })]
        public Table1C RequisiteValueTable { get; set; }

        [Property1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаУникальностиСсылочныхТиповДопРеквизитов", "Таблица уникальности ссылочных типов доп реквизитов")]
        [Type1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаУникальностиСсылочныхТиповДопРеквизитов.НомерСтроки", "Number", "Число")]
        [Type1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаУникальностиСсылочныхТиповДопРеквизитов.ИмяДопРеквизита", "String", "Строка")]
        [Type1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаУникальностиСсылочныхТиповДопРеквизитов.ИмяМетаданного", "String", "Строка")]
        [Type1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаУникальностиСсылочныхТиповДопРеквизитов.ИмяУникальногоРеквизитаДляСоответствующегоМетаданного", "String", "Строка")]
        [Type1C("Справочник.ITEXP_ХранениеНастроекДляДопРеквизитов.ТаблицаУникальностиСсылочныхТиповДопРеквизитов.СинонимУникальногоРеквизитаДляСоответствующегоМетаданного", "String", "Строка")]
        public Table1C LinkUniquenessTable { get; set; }

    }
}