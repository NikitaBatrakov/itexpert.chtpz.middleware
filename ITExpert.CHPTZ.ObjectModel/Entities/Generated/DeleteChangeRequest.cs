//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Entity1C("Документ.УдалитьChangeRequest", "Удалить Запрос на изменение")]
    public partial class DeleteChangeRequest : BaseDto, IEntity
    {
        
        [Property1C("Документ.УдалитьChangeRequest.Проведен", "Проведен")]
        [Type1C("Boolean", "Булево")]
        public bool? IsCommited { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ПометкаУдаления", "ПометкаУдаления")]
        [Type1C("Boolean", "Булево")]
        public bool? IsMarkedToDelete { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Дата", "Дата")]
        [Type1C("Date", "Дата")]
        public DateTime? Date { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Номер", "Номер")]
        [Type1C("String", "Строка")]
        public string Number { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Info", "Информация")]
        [Type1C("String", "Строка")]
        public string Info { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Status", "Статус")]
        [Type1C("Справочник.ChangeRequestStatus", "Статус ЗНИ")]
        public Link1C Status { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ClosingCode", "Код закрытия")]
        [Type1C("Справочник.ClosingCode_ChangeRequest", "Коды закрытия ЗНИ")]
        public Link1C ClosingCode { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Initiator", "Инициатор")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Initiator { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Operator", "Ответственный")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Operator { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Responsible", "Ответственный за документ-основание")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Responsible { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ШаблонПроцесса", "Шаблон процесса")]
        [Type1C("Справочник.Алгоритмы", "Шаблон процесса")]
        public Link1C ProcessTemplate { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.КомпонентКИС", "Компонент КИС")]
        [Type1C("String", "Строка")]
        public string KisComponent { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Направление", "Направление")]
        [Type1C("Справочник.ITEXP_Направление", "Направление")]
        public Link1C Direction { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.БизнесПроцесс", "Бизнес-процесс")]
        [Type1C("Справочник.ITEXP_БизнесПроцесс", "Бизнес процесс")]
        public Link1C BusinessProcess { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ДополнительныеНаправления", "Дополнительные направления")]
        [Type1C("Справочник.ITEXP_Направление", "Направление")]
        public Link1C AdditionalDirections { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ApplyingDate", "Дата применения")]
        [Type1C("Date", "Дата")]
        public DateTime? ApplyingDate { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ВесКритичности", "Вес критичности")]
        [Type1C("Number", "Число")]
        public double? CriticalityWeigth { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.TargetLaboriousness", "Плановая трудоемкость")]
        [Type1C("Number", "Число")]
        public double? TargetLaboriousness { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ActualLaboriousness", "Фактическая трудоемкость")]
        [Type1C("Number", "Число")]
        public double? ActualLaboriousness { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ПредварительнаяТрудоемкостьЧас", "Предварительная трудоемкость, час")]
        [Type1C("Number", "Число")]
        public double? EstimatedWorkloadPerHour { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ФактическаяТрудоемкостьЧас", "Фактическая трудоемкость, час")]
        [Type1C("Number", "Число")]
        public double? ActualWorkloadPerHour { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ПредварительнаяТрудоемкостьДень", "Предварительная трудоемкость, день")]
        [Type1C("Number", "Число")]
        public double? EstimatedWorkloadPerDay { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ФактическаяТрудоемкостьДень", "Фактическая трудоемкость, день")]
        [Type1C("Number", "Число")]
        public double? ActualWorkloadPerDay { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Weight", "Весовой коэффициент")]
        [Type1C("Number", "Число")]
        public double? Weight { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Priority", "Приоритет")]
        [Type1C("Справочник.ChangeRequestPriority", "Приоритет")]
        public Link1C Priority { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ПричинаПриостановки", "Причина приостановки")]
        [Type1C("String", "Строка")]
        public string PauseReason { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ТребуетсяФС", "Требуется ФС")]
        [Type1C("Boolean", "Булево")]
        public bool? FsRequired { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ФСПринято", "ФС принято")]
        [Type1C("Boolean", "Булево")]
        public bool? FsAccepted { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.CISModification", "Модификация КИС")]
        [Type1C("Boolean", "Булево")]
        public bool? IsKisModified { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Base", "Основание")]
        [Type1C("String", "Строка")]
        public string Base { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.UsageTerm", "Срок применения")]
        [Type1C("Справочник.UsageTerm", "Срок применения")]
        public Link1C UsageTerm { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ChangeImportance", "Важность изменения")]
        [Type1C("String", "Строка")]
        public string ChangeImportance { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.AppropriatedFunds", "Выделенные средства")]
        [Type1C("Number", "Число")]
        public double? AppropriatedFunds { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.TargetDate", "Плановый срок")]
        [Type1C("Date", "Дата")]
        public DateTime? TargetDate { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.TargetCost", "Плановая стоимость")]
        [Type1C("Number", "Число")]
        public double? TargetCost { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.DifficultyLevel", "Уровень сложности")]
        [Type1C("Number", "Число")]
        public double? DifficultyLevel { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.DevelopmentStartingDate", "Дата начала разработки")]
        [Type1C("Date", "Дата")]
        public DateTime? DevelopmentStartingDate { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.CheckReceiptDate", "Дата поступления на проверку")]
        [Type1C("Date", "Дата")]
        public DateTime? CheckReceiptDate { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ClosingDate", "Дата закрытия")]
        [Type1C("Date", "Дата")]
        public DateTime? ClosingDate { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.RelatedAppeals", "Связанные обращения")]
        [Type1C("String", "Строка")]
        public string RelatedAppeals { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Tasks", "Задачи")]
        [Type1C("String", "Строка")]
        public string Tasks { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ProjectDecision", "Проектное решение")]
        [Type1C("Boolean", "Булево")]
        public bool? ProjectDecision { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Project", "Проект")]
        [Type1C("Справочник.ITEXP_Проекты", "Проект")]
        public Link1C Project { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.KE", "KE")]
        [Type1C("Справочник.ITEXP_КЕ", "Конфигурационные единицы")]
        public Link1C ConfigurationUnit { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Comment", "Комментарий")]
        [Type1C("String", "Строка")]
        public string Comment { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Module", "Модуль")]
        [Type1C("Справочник.Module", "Модуль")]
        public Link1C Module { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.ИсполняемыйФайл", "Исполняемый файл")]
        [Type1C("Справочник.ITEXP_ИсполняемыйФайл", "Исполняемый файл")]
        public Link1C ExecutingFile { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Подтверждение", "Подтверждение")]
        [Type1C("Справочник.ITEXP_Документ", "Документ")]
        public Link1C Approvement { get; set; }

        [Property1C("Документ.УдалитьChangeRequest.Трудозатраты_ЛистУчетаВремени", "Трудозатраты лист учета времени")]
        [Type1C("String", "Строка")]
        public string TimeManagementList { get; set; }

    }
}