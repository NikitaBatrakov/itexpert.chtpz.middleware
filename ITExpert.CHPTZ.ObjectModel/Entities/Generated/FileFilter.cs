//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Register1C("РегистрСведений.ФильтрФайлов", "Фильтр файлов")]
    public partial class FileFilter : IRegister
    {
        
        [Property1C("РегистрСведений.ФильтрФайлов.Расширение", "Расширение")]
        [Type1C("String", "Строка")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public string Extension { get; set; }

        [Property1C("РегистрСведений.ФильтрФайлов.МинимальныйРазмер", ">")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public double? MinSize { get; set; }

        [Property1C("РегистрСведений.ФильтрФайлов.МаксимальныйРазмер", "<")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public double? MaxSize { get; set; }

    }
}