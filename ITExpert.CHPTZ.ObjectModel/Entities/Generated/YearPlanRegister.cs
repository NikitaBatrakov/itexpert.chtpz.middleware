//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Register1C("РегистрСведений.YearlyPlan", "Годовой план")]
    public partial class YearPlanRegister : IRegister
    {
        
        [Property1C("РегистрСведений.YearlyPlan.Период", "Период")]
        [Type1C("Date", "Дата")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public DateTime? Period { get; set; }

        [Property1C("РегистрСведений.YearlyPlan.Year", "Год")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public double? Year { get; set; }

        [Property1C("РегистрСведений.YearlyPlan.Module", "Модуль")]
        [Type1C("Справочник.Module", "Модуль")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C Module { get; set; }

        [Property1C("РегистрСведений.YearlyPlan.TotalTime", "Общее время")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public double? TotalTime { get; set; }

        [Property1C("РегистрСведений.YearlyPlan.Accompaniment", "Сопровождение")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public double? Accompaniment { get; set; }

        [Property1C("РегистрСведений.YearlyPlan.Project", "Проект")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public double? ProjectsNumber { get; set; }

        [Property1C("РегистрСведений.YearlyPlan.NoProject", "Не проект")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public double? NoProjectsNumber { get; set; }

        [Property1C("РегистрСведений.YearlyPlan.AccompanimentTime", "Сопровождение время")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public double? AccompanimentTime { get; set; }

        [Property1C("РегистрСведений.YearlyPlan.ProjectTime", "Проект время")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public double? ProjectsTime { get; set; }

        [Property1C("РегистрСведений.YearlyPlan.NoProjectTime", "Не проект время")]
        [Type1C("Number", "Число")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public double? NoProjectsTime { get; set; }

    }
}