//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Entity1C("Справочник.ChangeRequestPriority", "Приоритет")]
    public partial class ChangeRequestPriority : BaseDto, IEntity
    {
        
        [Property1C("Справочник.ChangeRequestPriority.ИмяПредопределенныхДанных", "ИмяПредопределенныхДанных")]
        [Type1C("String", "Строка")]
        public string PredefinedDataName { get; set; }

        [Property1C("Справочник.ChangeRequestPriority.Предопределенный", "Предопределенный")]
        [Type1C("Boolean", "Булево")]
        public bool? IsPredefined { get; set; }

        [Property1C("Справочник.ChangeRequestPriority.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        [Property1C("Справочник.ChangeRequestPriority.ПометкаУдаления", "ПометкаУдаления")]
        [Type1C("Boolean", "Булево")]
        public bool? IsMarkedToDelete { get; set; }

        [Property1C("Справочник.ChangeRequestPriority.Наименование", "Наименование")]
        [Type1C("String", "Строка")]
        public string Name { get; set; }

        [Property1C("Справочник.ChangeRequestPriority.Код", "Код")]
        [Type1C("String", "Строка")]
        public string Code { get; set; }

        [Property1C("Справочник.ChangeRequestPriority.WeightFrom", "Весовой коэффициент от")]
        [Type1C("Number", "Число")]
        public double? WeightFrom { get; set; }

        [Property1C("Справочник.ChangeRequestPriority.WeightTo", "Весовой коэффициент до")]
        [Type1C("Number", "Число")]
        public double? WeightTo { get; set; }

    }
}