//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Entity1C("Задача.Task", "Задание")]
    public partial class Task : BaseDto, IEntity
    {
        
        [Property1C("Задача.Task.Выполнена", "Выполнена")]
        [Type1C("Boolean", "Булево")]
        public bool? IsComplete { get; set; }

        [Property1C("Задача.Task.Наименование", "Наименование")]
        [Type1C("String", "Строка")]
        public string Name { get; set; }

        [Property1C("Задача.Task.ТочкаМаршрута", "ТочкаМаршрута")]
        [Type1C("ТочкаМаршрутаБизнесПроцесса.ВыполнениеАлгоритма", "Процесс (выполнение)(точка маршрута)")]
        public Link1C RoutePoint { get; set; }

        [Property1C("Задача.Task.БизнесПроцесс", "БизнесПроцесс")]
        [Type1C("БизнесПроцесс.ВыполнениеАлгоритма", "Процесс (выполнение)")]
        public Link1C BusinessProcess { get; set; }

        [Property1C("Задача.Task.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        [Property1C("Задача.Task.ПометкаУдаления", "ПометкаУдаления")]
        [Type1C("Boolean", "Булево")]
        public bool? IsMarkedToDelete { get; set; }

        [Property1C("Задача.Task.Дата", "Дата")]
        [Type1C("Date", "Дата")]
        public DateTime? Date { get; set; }

        [Property1C("Задача.Task.Номер", "Номер")]
        [Type1C("String", "Строка")]
        public string Number { get; set; }

        [Property1C("Задача.Task.Action", "Действие")]
        [Type1C("Справочник.ДействияСценария", "Действие процесса")]
        public Link1C Action { get; set; }

        [Property1C("Задача.Task.Base", "Основание")]
        [Type1C(new[] { "Документ.ServicRequest", "Документ.РаспределениеРазработчиковПоКомандам", "Документ.Problem", "Документ.ПланКоманды", "Документ.Appeal", "Документ.Incident", "Документ.УдалитьChangeRequest", "Документ.ChangeRequest", "Документ.ITEXP_ЛистУчетаВремени", "Документ.YearlyPlan", "Документ.ПроизводственныйКалендарьРазработчиков", "Документ.Итерация", "Документ.Confirm", "Документ.ШаблонРаспределенияРазработчиков", "Документ.МесячныйПлан", "Документ.Сообщение", "Документ.НормаНаСопровождение" }, new[] { "Запрос на обслуживание", "Распределение разработчиков по командам", "Проблема", "План команды", "Обращение", "Инцидент", "Удалить Запрос на изменение", "Запрос на изменение", "Лист учета времени", "Годовой план", "Производственный календарь разработчиков", "Итерация", "Согласование", "Шаблон распределения разработчиков", "Месячный план", "Сообщение", "Норма на сопровождение" })]
        public Composite1C Base { get; set; }

        [Property1C("Задача.Task.BeforePlannedCompletion", "До планового окончания")]
        [Type1C("Date", "Дата")]
        public DateTime? BeforePlannedCompletion { get; set; }

        [Property1C("Задача.Task.BeforeTheDeadline", "До крайнего срока")]
        [Type1C("Date", "Дата")]
        public DateTime? BeforeTheDeadline { get; set; }

        [Property1C("Задача.Task.Block", "Block")]
        [Type1C("Boolean", "Булево")]
        public bool? Block { get; set; }

        [Property1C("Задача.Task.BProcess", "Бизнес-процесс")]
        [Type1C("БизнесПроцесс.ВыполнениеАлгоритма", "Процесс (выполнение)")]
        public Link1C BProcess { get; set; }

        [Property1C("Задача.Task.ClosingCode", "Код закрытия")]
        [Type1C("Справочник.TaskClosingCode", "Код закрытия задания")]
        public Link1C ClosingCode { get; set; }

        [Property1C("Задача.Task.Comment", "Комментарий")]
        [Type1C("String", "Строка")]
        public string Comment { get; set; }

        [Property1C("Задача.Task.ConfirmingTime", "Время подтверждения")]
        [Type1C("Date", "Дата")]
        public DateTime? ConfirmingTime { get; set; }

        [Property1C("Задача.Task.Connections", "Связи")]
        [Type1C("String", "Строка")]
        public string Connections { get; set; }

        [Property1C("Задача.Task.Deadline", "Крайняя дата")]
        [Type1C("Date", "Дата")]
        public DateTime? Deadline { get; set; }

        [Property1C("Задача.Task.Done", "Выполнена")]
        [Type1C("Boolean", "Булево")]
        public bool? Done { get; set; }

        [Property1C("Задача.Task.Duration", "Суммарная длительность пропадания сервиса")]
        [Type1C("String", "Строка")]
        public string Duration { get; set; }

        [Property1C("Задача.Task.Employee", "Назначение - Сотруднику")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Employee { get; set; }

        [Property1C("Задача.Task.EndTime", "Время окончания")]
        [Type1C("Date", "Дата")]
        public DateTime? EndTime { get; set; }

        [Property1C("Задача.Task.Followers", "Последователи")]
        [Type1C("Задача.Task", "Задание")]
        public Link1C Followers { get; set; }

        [Property1C("Задача.Task.FromRegistrationToCompletion", "От регистрации до окончания")]
        [Type1C("Date", "Дата")]
        public DateTime? FromRegistrationToCompletion { get; set; }

        [Property1C("Задача.Task.FunctionalGroup", "Назначение - Группа специалистов")]
        [Type1C("Справочник.FunctionalGroups", "Функциональные группы")]
        public Link1C FunctionalGroup { get; set; }

        [Property1C("Задача.Task.ID", "Номер задания")]
        [Type1C("String", "Строка")]
        public string Id { get; set; }

        [Property1C("Задача.Task.IfChosen", "Выбрано условие")]
        [Type1C("Перечисление.ВариантыУсловий", "Варианты условий")]
        public Link1C IfChosen { get; set; }

        [Property1C("Задача.Task.Information", "Информация")]
        [Type1C("String", "Строка")]
        public string Information { get; set; }

        [Property1C("Задача.Task.Initiator", "Инициатор")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Initiator { get; set; }

        [Property1C("Задача.Task.IP", "IP")]
        [Type1C("String", "Строка")]
        public string Ip { get; set; }

        [Property1C("Задача.Task.Journal", "Журнал")]
        [Type1C("String", "Строка")]
        public string Journal { get; set; }

        [Property1C("Задача.Task.KE", "КЕ")]
        [Type1C("String", "Строка")]
        public string ConfigurationUnit { get; set; }

        [Property1C("Задача.Task.KEWork", "КЕ, на которой проводились работы")]
        [Type1C("Справочник.ITEXP_КЕ", "Конфигурационные единицы")]
        public Link1C ConfigurationUnitWork { get; set; }

        [Property1C("Задача.Task.Location", "Местоположение")]
        [Type1C("Справочник.Location", "Местоположение")]
        public Link1C Location { get; set; }

        [Property1C("Задача.Task.Organization", "Организация")]
        [Type1C("Справочник.Organization", "Структура управления")]
        public Link1C Organization { get; set; }

        [Property1C("Задача.Task.Phone", "Телефон с портала")]
        [Type1C("String", "Строка")]
        public string Phone { get; set; }

        [Property1C("Задача.Task.PhoneWorking", "Дополнительный телефон")]
        [Type1C("String", "Строка")]
        public string PhoneWorking { get; set; }

        [Property1C("Задача.Task.PlannedCompletion", "Плановое окончание")]
        [Type1C("Date", "Дата")]
        public DateTime? PlannedCompletion { get; set; }

        [Property1C("Задача.Task.PlannedDuration", "Плановая длительность")]
        [Type1C("Date", "Дата")]
        public DateTime? PlannedDuration { get; set; }

        [Property1C("Задача.Task.PlannedStart", "Плановое начало")]
        [Type1C("Date", "Дата")]
        public DateTime? PlannedStart { get; set; }

        [Property1C("Задача.Task.Predecessor", "Предшественник")]
        [Type1C(new[] { "Задача.Task", "БизнесПроцесс.ВыполнениеАлгоритма" }, new[] { "Задание", "Процесс (выполнение)" })]
        public Composite1C Predecessor { get; set; }

        [Property1C("Задача.Task.Predecessors", "Предшественники")]
        [Type1C("Задача.Task", "Задание")]
        public Link1C Predecessors { get; set; }

        [Property1C("Задача.Task.Priority", "Приоритет")]
        [Type1C("Справочник.Priority", "Приоритет")]
        public Link1C Priority { get; set; }

        [Property1C("Задача.Task.ReassignEmployee", "Переназначений между специалистами")]
        [Type1C("Number", "Число")]
        public double? ReassignEmployee { get; set; }

        [Property1C("Задача.Task.ReassignFG", "Переназначений между ФГ")]
        [Type1C("Number", "Число")]
        public double? ReassignFg { get; set; }

        [Property1C("Задача.Task.RegistrationTime", "Время регистрации")]
        [Type1C("Date", "Дата")]
        public DateTime? RegistrationTime { get; set; }

        [Property1C("Задача.Task.ResultOfExecuting", "Результат выполнения")]
        [Type1C("String", "Строка")]
        public string ResultOfExecuting { get; set; }

        [Property1C("Задача.Task.Service_AssignE", "Service assign E")]
        [Type1C("Boolean", "Булево")]
        public bool? ServiceAssignE { get; set; }

        [Property1C("Задача.Task.Service_AssignFG", "Service assign FG")]
        [Type1C("Boolean", "Булево")]
        public bool? ServiceAssignFg { get; set; }

        [Property1C("Задача.Task.StartTime", "Время начала")]
        [Type1C("Date", "Дата")]
        public DateTime? StartTime { get; set; }

        [Property1C("Задача.Task.Status", "Статус")]
        [Type1C("Справочник.TaskStatus", "Статусы заданий")]
        public Link1C Status { get; set; }

        [Property1C("Задача.Task.Surely", "Surely")]
        [Type1C("Boolean", "Булево")]
        public bool? Surely { get; set; }

        [Property1C("Задача.Task.Theme", "Тема")]
        [Type1C("String", "Строка")]
        public string Theme { get; set; }

        [Property1C("Задача.Task.TimeOfAppointment", "Время назначения")]
        [Type1C("Date", "Дата")]
        public DateTime? TimeOfAppointment { get; set; }

        [Property1C("Задача.Task.TotalLength", "Итоговая длительность")]
        [Type1C("Number", "Число")]
        public double? TotalLength { get; set; }

        [Property1C("Задача.Task.АгрегирующаяЗадача", "Агрегирующая задача")]
        [Type1C("Boolean", "Булево")]
        public bool? IsAggregatingTask { get; set; }

        [Property1C("Задача.Task.ВнешнийID", "Внешний ID")]
        [Type1C("String", "Строка")]
        public string ExternalId { get; set; }

        [Property1C("Задача.Task.ВремяВыполнения", "Время выполнения")]
        [Type1C("Date", "Дата")]
        public DateTime? CompletionTime { get; set; }

        [Property1C("Задача.Task.ВторичноеСогласованиеЗаказов", "Вторичное согласование заказов")]
        [Type1C("Boolean", "Булево")]
        public bool? SecondaryRequestApproving { get; set; }

        [Property1C("Задача.Task.Дни", "Дни")]
        [Type1C("Number", "Число")]
        public double? Days { get; set; }

        [Property1C("Задача.Task.Минуты", "Минуты")]
        [Type1C("Number", "Число")]
        public double? Minutes { get; set; }

        [Property1C("Задача.Task.НазваниеУзлаСогласования", "Название узла согласования")]
        [Type1C("String", "Строка")]
        public string ApprovalNodeName { get; set; }

        [Property1C("Задача.Task.Основание", "Основание")]
        [Type1C(new[] { "Документ.ServicRequest", "Документ.Incident", "Документ.ChangeRequest" }, new[] { "Запрос на обслуживание", "Инцидент", "Запрос на изменение" })]
        public Composite1C Basis { get; set; }

        [Property1C("Задача.Task.ОснованиеБП", "Основание БП")]
        [Type1C(new[] { "Документ.ServicRequest", "Документ.Incident", "Документ.ChangeRequest" }, new[] { "Запрос на обслуживание", "Инцидент", "Запрос на изменение" })]
        public Composite1C BpBasis { get; set; }

        [Property1C("Задача.Task.ПодтверждениеПолучено", "Подтверждение получено")]
        [Type1C("Boolean", "Булево")]
        public bool? IsConfirmed { get; set; }

        [Property1C("Задача.Task.ПричинаОтказа", "Причина отказа")]
        [Type1C("String", "Строка")]
        public string RejectionReason { get; set; }

        [Property1C("Задача.Task.РабочаяСтанция", "Рабочая станция")]
        [Type1C("String", "Строка")]
        public string WorkStation { get; set; }

        [Property1C("Задача.Task.Тип", "Тип")]
        [Type1C("Справочник.ITEXP_ТипДополнительногоРеквизита", "Типы объектов")]
        public Link1C Type { get; set; }

        [Property1C("Задача.Task.Часы", "Часы")]
        [Type1C("Number", "Число")]
        public double? Hours { get; set; }

        [Property1C("Задача.Task.Трудоемкость", "Трудоемкость")]
        [Type1C("Number", "Число")]
        public double? Workload { get; set; }

    }
}