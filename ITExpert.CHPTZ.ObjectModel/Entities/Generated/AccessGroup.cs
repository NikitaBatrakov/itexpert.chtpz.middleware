//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Entity1C("Справочник.ГруппыДоступа", "Группы доступа")]
    public partial class AccessGroup : BaseDto, IEntity
    {
        
        [Property1C("Справочник.ГруппыДоступа.ИмяПредопределенныхДанных", "ИмяПредопределенныхДанных")]
        [Type1C("String", "Строка")]
        public string PredefinedDataName { get; set; }

        [Property1C("Справочник.ГруппыДоступа.Предопределенный", "Предопределенный")]
        [Type1C("Boolean", "Булево")]
        public bool? IsPredefined { get; set; }

        [Property1C("Справочник.ГруппыДоступа.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        [Property1C("Справочник.ГруппыДоступа.ПометкаУдаления", "ПометкаУдаления")]
        [Type1C("Boolean", "Булево")]
        public bool? IsMarkedToDelete { get; set; }

        [Property1C("Справочник.ГруппыДоступа.Наименование", "Наименование")]
        [Type1C("String", "Строка")]
        public string Name { get; set; }

        [Property1C("Справочник.ГруппыДоступа.Код", "Код")]
        [Type1C("String", "Строка")]
        public string Code { get; set; }

    }
}