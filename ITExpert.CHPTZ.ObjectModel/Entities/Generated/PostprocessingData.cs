//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Register1C("РегистрСведений.СписокДанныхПостобработки", "Список данных постобработки")]
    public partial class PostprocessingData : IRegister
    {
        
        [Property1C("РегистрСведений.СписокДанныхПостобработки.ОбъектНапоминания", "Объект напоминания")]
        [Type1C(new[] { "Документ.ServicRequest", "Документ.РаспределениеРазработчиковПоКомандам", "Документ.Problem", "Документ.ПланКоманды", "Документ.Appeal", "Документ.Incident", "Документ.УдалитьChangeRequest", "Документ.ChangeRequest", "Документ.ITEXP_ЛистУчетаВремени", "Документ.YearlyPlan", "Документ.ПроизводственныйКалендарьРазработчиков", "Документ.Итерация", "Документ.Confirm", "Документ.ШаблонРаспределенияРазработчиков", "Документ.МесячныйПлан", "Документ.Сообщение", "Документ.НормаНаСопровождение" }, new[] { "Запрос на обслуживание", "Распределение разработчиков по командам", "Проблема", "План команды", "Обращение", "Инцидент", "Удалить Запрос на изменение", "Запрос на изменение", "Лист учета времени", "Годовой план", "Производственный календарь разработчиков", "Итерация", "Согласование", "Шаблон распределения разработчиков", "Месячный план", "Сообщение", "Норма на сопровождение" })]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Composite1C TargetObject { get; set; }

        [Property1C("РегистрСведений.СписокДанныхПостобработки.Тема", "Тема")]
        [Type1C("Перечисление.ВыдыУведомленийПользователя", "Выды уведомлений пользователя")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C Subject { get; set; }

        [Property1C("РегистрСведений.СписокДанныхПостобработки.Priority", "Priority")]
        [Type1C(new[] { "Справочник.Priority", "Справочник.Priority_ServicRequest" }, new[] { "Приоритет", "Приоритет" })]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public Composite1C Priority { get; set; }

        [Property1C("РегистрСведений.СписокДанныхПостобработки.RoutineTerm", "Routine term")]
        [Type1C("Date", "Дата")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public DateTime? RoutineTerm { get; set; }

        [Property1C("РегистрСведений.СписокДанныхПостобработки.TargetDate", "Target date")]
        [Type1C("Date", "Дата")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public DateTime? TargetDate { get; set; }

        [Property1C("РегистрСведений.СписокДанныхПостобработки.InfluenceDegree", "Степень влияния")]
        [Type1C("Справочник.InfluenceDegree", "Степень влияния")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public Link1C InfluenceDegree { get; set; }

        [Property1C("РегистрСведений.СписокДанныхПостобработки.InvolvementDegree", "Степень вовлеченности")]
        [Type1C("Справочник.InvolvementDegree", "Степень вовлеченности")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public Link1C InvolvementDegree { get; set; }

    }
}