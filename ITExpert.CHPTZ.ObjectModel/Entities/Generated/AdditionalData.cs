//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Register1C("РегистрСведений.ДополнительныеСведения", "Дополнительные сведения")]
    public partial class AdditionalData : IRegister
    {
        
        [Property1C("РегистрСведений.ДополнительныеСведения.Объект", "Объект")]
        [Type1C(new[] { "Задача.Task", "Документ.ServicRequest", "Документ.Incident", "Документ.ChangeRequest", "Справочник.ITEXP_КЕ", "Справочник.ITEXP_СвязиОбъектов" }, new[] { "Задание", "Запрос на обслуживание", "Инцидент", "Запрос на изменение", "Конфигурационные единицы", "Связи объектов" })]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Composite1C Object { get; set; }

        [Property1C("РегистрСведений.ДополнительныеСведения.Реквизит", "Реквизит")]
        [Type1C("ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения", "Дополнительные реквизиты и сведения")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C AdditionalAttribute { get; set; }

        [Property1C("РегистрСведений.ДополнительныеСведения.Значение", "Значение")]
        [Type1C(new[] { "Задача.Task", "Справочник.Location", "Справочник.ITEXP_ТипБД", "Перечисление.ТипСервера", "Справочник.ITEXP_Система", "Справочник.ITEXP_Вендоры", "Справочник.ITEXP_СостояниеВСистемеМониторинга", "Boolean", "Справочник.ITEXP_БизнесПроцесс", "String", "Справочник.ITEXP_СхемаЛицензирования", "Справочник.Employee", "Date", "Справочник.ITEXP_УровеньИерархии", "Number", "Справочник.ITEXP_ТипЛицензии", "Справочник.Organization", "Справочник.ITEXP_Архитектура", "Справочник.ITEXP_Направление", "Справочник.ЗначенияСвойствОбъектов", "Справочник.ITEXP_Модель", "Справочник.Module" }, new[] { "Задание", "Местоположение", "Тип БД", "Тип сервера", "Система", "Вендоры", "Состояние в системе мониторинга", "Булево", "Бизнес процесс", "Строка", "Схема лицензирования", "Сотрудники", "Дата", "Уровень иерархии", "Число", "Тип лицензии", "Структура управления", "Архитектура", "Направление", "Значения свойств объектов", "Модель", "Модуль" })]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public Composite1C Value { get; set; }

    }
}