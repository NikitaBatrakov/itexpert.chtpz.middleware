//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Entity1C("Документ.Confirm", "Согласование")]
    public partial class Confirm : BaseDto, IEntity
    {
        
        [Property1C("Документ.Confirm.Проведен", "Проведен")]
        [Type1C("Boolean", "Булево")]
        public bool? IsCommited { get; set; }

        [Property1C("Документ.Confirm.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        [Property1C("Документ.Confirm.ПометкаУдаления", "ПометкаУдаления")]
        [Type1C("Boolean", "Булево")]
        public bool? IsMarkedToDelete { get; set; }

        [Property1C("Документ.Confirm.Дата", "Дата")]
        [Type1C("Date", "Дата")]
        public DateTime? Date { get; set; }

        [Property1C("Документ.Confirm.Номер", "Номер")]
        [Type1C("Number", "Число")]
        public double? Number { get; set; }

        [Property1C("Документ.Confirm.DateN", "Срок начала")]
        [Type1C("Date", "Дата")]
        public DateTime? DateN { get; set; }

        [Property1C("Документ.Confirm.DateZ", "Срок завершения")]
        [Type1C("Date", "Дата")]
        public DateTime? DateZ { get; set; }

        [Property1C("Документ.Confirm.DateClose", "Дата закрытия")]
        [Type1C("Date", "Дата")]
        public DateTime? DateClose { get; set; }

        [Property1C("Документ.Confirm.Obj", "Основание")]
        [Type1C(new[] { "Документ.ServicRequest", "Документ.РаспределениеРазработчиковПоКомандам", "Документ.Problem", "Документ.ПланКоманды", "Документ.Appeal", "Документ.Incident", "Документ.УдалитьChangeRequest", "Документ.ChangeRequest", "Документ.ITEXP_ЛистУчетаВремени", "Документ.YearlyPlan", "Документ.ПроизводственныйКалендарьРазработчиков", "Документ.Итерация", "Документ.Confirm", "Документ.ШаблонРаспределенияРазработчиков", "Документ.МесячныйПлан", "Документ.Сообщение", "Документ.НормаНаСопровождение" }, new[] { "Запрос на обслуживание", "Распределение разработчиков по командам", "Проблема", "План команды", "Обращение", "Инцидент", "Удалить Запрос на изменение", "Запрос на изменение", "Лист учета времени", "Годовой план", "Производственный календарь разработчиков", "Итерация", "Согласование", "Шаблон распределения разработчиков", "Месячный план", "Сообщение", "Норма на сопровождение" })]
        public Composite1C ServiceRequest { get; set; }

        [Property1C("Документ.Confirm.Previous", "Предыдущее")]
        [Type1C("Документ.Confirm", "Согласование")]
        public Link1C Previous { get; set; }

        [Property1C("Документ.Confirm.Status", "Статус")]
        [Type1C("String", "Строка")]
        public string Status { get; set; }

        [Property1C("Документ.Confirm.Name", "Краткое наименование")]
        [Type1C("String", "Строка")]
        public string Name { get; set; }

        [Property1C("Документ.Confirm.Info", "Описание")]
        [Type1C("String", "Строка")]
        public string Info { get; set; }

        [Property1C("Документ.Confirm.Term", "Срок")]
        [Type1C("String", "Строка")]
        public string Term { get; set; }

        [Property1C("Документ.Confirm.Rule", "Правила")]
        [Type1C("String", "Строка")]
        public string Rule { get; set; }

        [Property1C("Документ.Confirm.Comment", "Комментарий")]
        [Type1C("String", "Строка")]
        public string Comment { get; set; }

        [Property1C("Документ.Confirm.Result", "Результат")]
        [Type1C("String", "Строка")]
        public string Result { get; set; }

        [Property1C("Документ.Confirm.Group", "Group")]
        [Type1C("Документ.Confirm.Group.НомерСтроки", "Number", "Число")]
        [Type1C("Документ.Confirm.Group.DateN", "Date", "Дата")]
        [Type1C("Документ.Confirm.Group.DateZ", "Date", "Дата")]
        [Type1C("Документ.Confirm.Group.Employee", "Справочник.Employee", "Сотрудники")]
        [Type1C("Документ.Confirm.Group.Gr", "Справочник.FunctionalGroups", "Функциональные группы")]
        [Type1C("Документ.Confirm.Group.Stop", "Boolean", "Булево")]
        [Type1C("Документ.Confirm.Group.Mast", "Boolean", "Булево")]
        [Type1C("Документ.Confirm.Group.Status", "String", "Строка")]
        [Type1C("Документ.Confirm.Group.Result", "String", "Строка")]
        [Type1C("Документ.Confirm.Group.Remark", "String", "Строка")]
        public Table1C Group { get; set; }

    }
}