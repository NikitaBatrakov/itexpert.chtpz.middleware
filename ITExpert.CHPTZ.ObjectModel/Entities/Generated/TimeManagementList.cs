//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Entity1C("Документ.ITEXP_ЛистУчетаВремени", "Лист учета времени")]
    public partial class TimeManagementList : BaseDto, IEntity
    {
        
        [Property1C("Документ.ITEXP_ЛистУчетаВремени.Проведен", "Проведен")]
        [Type1C("Boolean", "Булево")]
        public bool? IsCommited { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.ПометкаУдаления", "ПометкаУдаления")]
        [Type1C("Boolean", "Булево")]
        public bool? IsMarkedToDelete { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.Дата", "Дата")]
        [Type1C("Date", "Дата")]
        public DateTime? Date { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.Номер", "Номер")]
        [Type1C("String", "Строка")]
        public string Number { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.Автор", "Автор")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Author { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.Сотрудник", "Сотрудник")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Employee { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.ДатаСписания", "Дата списания")]
        [Type1C("Date", "Дата")]
        public DateTime? CancellationDate { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.ЗатраченноеВремяЧас", "Затраченное время, час")]
        [Type1C("Number", "Число")]
        public double? TimeSpentPerOur { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.Описание", "Описание")]
        [Type1C("String", "Строка")]
        public string Description { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.ОбъектУчетаВремени", "Объект учета времени")]
        [Type1C(new[] { "Задача.Task", "Документ.ServicRequest", "Документ.Incident", "Документ.ChangeRequest" }, new[] { "Задание", "Запрос на обслуживание", "Инцидент", "Запрос на изменение" })]
        public Composite1C TimeManagementObject { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.Задача", "Задача")]
        [Type1C("Задача.Task", "Задание")]
        public Link1C Task { get; set; }

        [Property1C("Документ.ITEXP_ЛистУчетаВремени.Статус", "Статус")]
        [Type1C("Перечисление.ITEXP_ТипУчетаВремени", "Тип учета времени")]
        public Link1C Status { get; set; }

    }
}