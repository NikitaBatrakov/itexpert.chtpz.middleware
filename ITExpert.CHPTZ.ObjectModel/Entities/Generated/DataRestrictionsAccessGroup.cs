//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Register1C("РегистрСведений.ГруппыДоступаОграниченияДанных", "Ограничение данных для группы")]
    public partial class DataRestrictionsAccessGroup : IRegister
    {
        
        [Property1C("РегистрСведений.ГруппыДоступаОграниченияДанных.GUIDСписка", "GUIDСписка")]
        [Type1C("String", "Строка")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public string TableId { get; set; }

        [Property1C("РегистрСведений.ГруппыДоступаОграниченияДанных.ГруппаДоступа", "Группа доступа")]
        [Type1C("Справочник.ГруппыДоступа", "Группы доступа")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C AccessGroup { get; set; }

        [Property1C("РегистрСведений.ГруппыДоступаОграниченияДанных.Фильтр", "Фильтр")]
        [Type1C("String", "Строка")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public string Filter { get; set; }

    }
}