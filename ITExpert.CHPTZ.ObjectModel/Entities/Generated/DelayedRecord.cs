//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Register1C("РегистрСведений.ITEXP_РегистрОтложеннойЗаписи", "Регистр отложенной записи")]
    public partial class DelayedRecord : IRegister
    {
        
        [Property1C("РегистрСведений.ITEXP_РегистрОтложеннойЗаписи.Объект", "Объект")]
        [Type1C(new[] { "Задача.Task", "Документ.ServicRequest", "Документ.Incident" }, new[] { "Задание", "Запрос на обслуживание", "Инцидент" })]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Composite1C Object { get; set; }

        [Property1C("РегистрСведений.ITEXP_РегистрОтложеннойЗаписи.ИмяПроцедуры", "Имя процедуры")]
        [Type1C("String", "Строка")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public string Procedure { get; set; }

        [Property1C("РегистрСведений.ITEXP_РегистрОтложеннойЗаписи.Сотрудник", "Сотрудник")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public Link1C Employee { get; set; }

        [Property1C("РегистрСведений.ITEXP_РегистрОтложеннойЗаписи.ТекстовоеСодержание", "Текстовое содержание")]
        [Type1C("String", "Строка")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public string Text { get; set; }

        [Property1C("РегистрСведений.ITEXP_РегистрОтложеннойЗаписи.Письмо", "Письмо")]
        [Type1C("Документ.Сообщение", "Сообщение")]
        [Register1СProperty(Register1CPropertyKind.Info)]
        public Link1C Message { get; set; }

    }
}