//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//
//     If you need to add something manually, use second part of the partial class.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using ITExpert.CHPTZ.BackendTypes.Attributes;
using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
using ITExpert.CHPTZ.BackendTypes.Fundamentals;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    [Entity1C("Справочник.SLA", "Соглашения SLA (Инцидент)")]
    public partial class Sla : BaseDto, IEntity
    {
        
        [Property1C("Справочник.SLA.ИмяПредопределенныхДанных", "ИмяПредопределенныхДанных")]
        [Type1C("String", "Строка")]
        public string PredefinedDataName { get; set; }

        [Property1C("Справочник.SLA.Предопределенный", "Предопределенный")]
        [Type1C("Boolean", "Булево")]
        public bool? IsPredefined { get; set; }

        [Property1C("Справочник.SLA.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        [Property1C("Справочник.SLA.ПометкаУдаления", "ПометкаУдаления")]
        [Type1C("Boolean", "Булево")]
        public bool? IsMarkedToDelete { get; set; }

        [Property1C("Справочник.SLA.Наименование", "Наименование")]
        [Type1C("String", "Строка")]
        public string Name { get; set; }

        [Property1C("Справочник.SLA.Код", "Код")]
        [Type1C("Number", "Число")]
        public double? Code { get; set; }

        [Property1C("Справочник.SLA.ID", "ID")]
        [Type1C("String", "Строка")]
        public string Id { get; set; }

        [Property1C("Справочник.SLA.FullName", "Наименование полное")]
        [Type1C("String", "Строка")]
        public string FullName { get; set; }

        [Property1C("Справочник.SLA.Comment", "Комментарий")]
        [Type1C("String", "Строка")]
        public string Comment { get; set; }

        [Property1C("Справочник.SLA.НеАктивен", "Не активен")]
        [Type1C("Boolean", "Булево")]
        public bool? IsNotActive { get; set; }

        [Property1C("Справочник.SLA.Timetable", "Расписание")]
        [Type1C("Справочник.Timetable", "Расписание")]
        public Link1C Timetable { get; set; }

        [Property1C("Справочник.SLA.OLA", "OLA")]
        [Type1C("Справочник.OLA", "Соглашения OLA (Инцидент)")]
        public Link1C Ola { get; set; }

        [Property1C("Справочник.SLA.Утверждено", "Утверждено")]
        [Type1C("Boolean", "Булево")]
        public bool? IsApproved { get; set; }

        [Property1C("Справочник.SLA.ОграниченияПоддержки", "Ограничения поддержки")]
        [Type1C("String", "Строка")]
        public string SupportLimitations { get; set; }

        [Property1C("Справочник.SLA.СозданНаОсновании", "Создан на основании")]
        [Type1C("Справочник.SLA", "Соглашения SLA (Инцидент)")]
        public Link1C CreatedOn { get; set; }

        [Property1C("Справочник.SLA.IndicatorsPriority", "Показатели приоритета")]
        [Type1C("Справочник.SLA.IndicatorsPriority.НомерСтроки", "Number", "Число")]
        [Type1C("Справочник.SLA.IndicatorsPriority.Priority", "Справочник.Priority", "Приоритет")]
        [Type1C("Справочник.SLA.IndicatorsPriority.ReactionTime", "Date", "Дата")]
        [Type1C("Справочник.SLA.IndicatorsPriority.SolutionsTime", "Date", "Дата")]
        [Type1C("Справочник.SLA.IndicatorsPriority.SolutionsDay", "Number", "Число")]
        public Table1C IndicatorsPriority { get; set; }

    }
}