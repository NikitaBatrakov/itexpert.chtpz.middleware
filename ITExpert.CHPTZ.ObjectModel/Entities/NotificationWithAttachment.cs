﻿using DateTime = System.DateTime;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [Register1C("Query")]
    public class NotificationWithAttachment : IRegister
    {
        [Property1C("Период", "Период")]
        [Type1C("Date", "Дата")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public DateTime? Period { get; set; }

        [Property1C("Объект", "Объект")]
        [Type1C(new[] { "Задача.Task", "Документ.ServicRequest", "Документ.РаспределениеРазработчиковПоКомандам", "Документ.Problem", "Документ.ПланКоманды", "Документ.Appeal", "Документ.Incident", "Документ.УдалитьChangeRequest", "Документ.ChangeRequest", "Документ.ITEXP_ЛистУчетаВремени", "Документ.YearlyPlan", "Документ.ПроизводственныйКалендарьРазработчиков", "Документ.Итерация", "Документ.Confirm", "Документ.ШаблонРаспределенияРазработчиков", "Документ.МесячныйПлан", "Документ.Сообщение", "Документ.НормаНаСопровождение" }, new[] { "Задание", "Запрос на обслуживание", "Распределение разработчиков по командам", "Проблема", "План команды", "Обращение", "Инцидент", "Удалить Запрос на изменение", "Запрос на изменение", "Лист учета времени", "Годовой план", "Производственный календарь разработчиков", "Итерация", "Согласование", "Шаблон распределения разработчиков", "Месячный план", "Сообщение", "Норма на сопровождение" })]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Composite1C Object { get; set; }

        [Property1C("Сотрудник", "Сотрудник")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C Employee { get; set; }

        [Property1C("ШаблонОповещения", "Шаблон оповещения")]
        [Type1C("Справочник.ITEXP_ШаблонОповещения", "Шаблон оповещения")]
        [Register1СProperty(Register1CPropertyKind.Key)]
        public Link1C Template { get; set; }

        [Property1C("ТекстОповещения", "Текст оповещения")]
        [Type1C("String", "Строка")]
        [Register1СProperty(Register1CPropertyKind.Value)]
        public string Text { get; set; }

        [Property1C("Прочитано", "Прочитано")]
        [Type1C("Boolean", "Булево")]
        [Register1СProperty(Register1CPropertyKind.Info)]
        public bool? IsRead { get; set; }

        [Property1C("СсылкаНаПрисоединенныйФайл", "Ссылка на присоединенный файл")]
        [Type1C("Справочник.AttachedFiles", "Присоединенный Файл")]
        [Register1СProperty(Register1CPropertyKind.Info)]
        public Link1C Attachment { get; set; }
    }
}
