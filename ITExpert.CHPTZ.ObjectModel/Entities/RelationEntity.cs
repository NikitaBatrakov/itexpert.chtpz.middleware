﻿using DateTime = System.DateTime;

namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [Entity1C("Query")]
    public class RelationEntity : BaseDto, IEntity
    {
        [Property1C("Link", "Ссылка")]
        [Type1C("Id", "Id")]
        public Link1C RelatedEntityLink { get; set; }

        public bool? IsMarkedToDelete { get; set; } = null;

        [Property1C("Entity", "Класс")]
        [Type1C("String", "Строка")]
        public string Entity { get; set; }

        [Property1C("Number", "Номер")]
        [Type1C("String", "Строка")]
        public string Number { get; set; }

        [Property1C("Subject", "Название")]
        [Type1C("String", "Строка")]
        public string Subject { get; set; }

        [Property1C("Status", "Статус")]
        [Type1C(new[] { "Справочник.TaskStatus", "Справочник.ServicRequestStatus", "Справочник.IncidentStatus", "Справочник.ChangeRequestStatus", "Справочник.ITEXP_СтатусКЕ" }, new[] { "Статус", "Статус", "Статус", "Статус", "Статус" })]
        public Composite1C Status { get; set; }

        [Property1C("IncidentStatus", "Статус инцидента")]
        [Type1C("Справочник.IncidentStatus", "Статус")]
        public Link1C IncidentStatus { get; set; }

        [Property1C("ServiceRequestStatus", "Статус ЗНО")]
        [Type1C("Справочник.ServicRequestStatus", "Статус")]
        public Link1C ServiceRequestStatus { get; set; }

        [Property1C("ChangeRequestStatus", "Статус ЗНИ")]
        [Type1C("Справочник.ChangeRequestStatus", "Статус")]
        public Link1C ChangeRequestStatus { get; set; }

        [Property1C("TaskStatus", "Статус задачи")]
        [Type1C("Справочник.TaskStatus", "Статус")]
        public Link1C TaskStatus { get; set; }

        [Property1C("Creator", "Пользователь")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Creator { get; set; }

        [Property1C("Responsible", "Ответственный")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Responsible { get; set; }

        [Property1C("FunctionalGroup", "Функциональная группа")]
        [Type1C("Справочник.FunctionalGroups", "Функциональные группы")]
        public Link1C FunctionalGroup { get; set; }

        [Property1C("RegistrationDate", "Время создания")]
        [Type1C("Date", "Дата")]
        public DateTime? RegistrationDate { get; set; }

        [Property1C("Deadline", "Срок решения")]
        [Type1C("Date", "Дата")]
        public DateTime? Deadline { get; set; }

        [Property1C("AdditionalAttributeType", "Тип")]
        [Type1C("Справочник.ITEXP_ТипДополнительногоРеквизита", "Типы объектов")]
        public Link1C AdditionalAttributeType { get; set; }

        [Property1C("IsMaster", "Главный объект")]
        [Type1C("Boolean", "Булево")]
        public bool IsMaster { get; set; }

        [Property1C("LinkType", "Тип связи")]
        [Type1C("String", "Строка")]
        public string RelationType { get; set; }

        [Property1C("RelationLink", "Ссылка")]
        [Type1C("Id", "Id")]
        public Link1C Link { get; set; }
    }
}
