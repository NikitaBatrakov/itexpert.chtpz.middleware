﻿namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    public partial class Direction
    {
        [Property1C("Справочник.ITEXP_Направление.active", "Активен")]
        [Type1C("Boolean", "Булево")]
        public bool? IsActive { get; set; }

        [Property1C("Справочник.FunctionalGroups.Parent", "Родитель")]
        [Type1C("Справочник.ITEXP_БизнесПроцесс", "Направление")]
        public Link1C Parent { get; set; }
    }
}
