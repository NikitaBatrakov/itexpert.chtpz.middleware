﻿namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [Entity1C("Query")]
    public class ArgreementsMonitoringObject : BaseDto, IEntity
    {
        [Property1C("Link", "Ссылка")]
        [Type1C("Id", "Id")]
        public Link1C Link { get; set; }

        [Property1C("Number", "Номер")]
        [Type1C("String", "Cтрока")]
        public string Number { get; set; }

        [Property1C("Name", "Название")]
        [Type1C("String", "Строка")]
        public string Name { get; set; }

        [Property1C("Responsible", "Назначение - Сотруднику")]
        [Type1C("Справочник.Employee", "Сотрудники")]
        public Link1C Responsible { get; set; }

        [Property1C("Status", "Статус")]
        [Type1C("Справочник.TaskStatus", "Статусы заданий")]
        public Link1C Status { get; set; }

        [Property1C("Type", "Тип объекта")]
        [Type1C("String", "Строка")]
        public string Type { get; set; }

        public bool? IsMarkedToDelete { get; set; }
    }
}
