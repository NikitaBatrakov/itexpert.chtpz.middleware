﻿namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [Entity1C("Query")]
    public class EmployeeFunctionalGroup : BaseDto, IEntity
    {
        [Property1C("Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        public bool? IsMarkedToDelete { get; set; } = null;
    }
}
