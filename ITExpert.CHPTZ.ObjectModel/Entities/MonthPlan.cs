﻿namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;

    public partial class MonthPlan
    {
        [Property1C("Документ.МесячныйПлан.Заголовок", "Заголовок")]
        [Type1C("String", "Строка")]
        public string Title { get; set; }
    }
}
