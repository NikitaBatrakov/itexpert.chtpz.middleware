﻿namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;

    public partial class Timezone
    {
        [Property1C("Справочник.ITEXP_ЧасовыеПояса.ЧасовойПоясПользователя", "Часовой пояс пользователя")]
        [Type1C("Boolean", "Булево")]
        public bool? IsSelected { get; set; }
    }
}
