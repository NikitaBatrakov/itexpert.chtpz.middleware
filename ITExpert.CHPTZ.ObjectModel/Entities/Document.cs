﻿namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;

    public partial class Document
    {
        //Not part of the dictionary in 1C. Added artificially
        [Property1C("Справочник.ITEXP_Документ.IsSelectable")]
        [Type1C("Boolean", "Булево")]
        public bool? IsSelectable { get; set; }
    }
}
