﻿namespace ITExpert.CHPTZ.ObjectModel.Entities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    public partial class ServiceTemplateAdditionalAttribute
    {
        [Property1C("РегистрСведений.ITEXP_ДополнительныеРеквизитыВРазрезеШаблонов.ТипШаблонаЗНО", "Тип Шаблона ЗНО")]
        [Type1C("Справочник.ITEXP_ТипДополнительногоРеквизита", "Типы объектов")]
        [Register1СProperty(Register1CPropertyKind.Info)]
        public Link1C TemplateType { get; set; }
    }
}
