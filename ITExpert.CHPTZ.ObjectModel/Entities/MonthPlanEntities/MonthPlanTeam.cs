namespace ITExpert.CHPTZ.ObjectModel.Entities.MonthPlanEntities
{
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    public class MonthPlanTeam
    {
        public Link1C Team { get; set; }

        public double? TotalResource { get; set; }

        public bool? IsMain { get; set; }

        public double? Resource { get; set; }

        public Link1C ChangeRequest { get; set; }
    }
}