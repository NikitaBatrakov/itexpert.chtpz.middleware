﻿using System.Collections.Generic;

using DateTime = System.DateTime;

namespace ITExpert.CHPTZ.ObjectModel.Entities.MonthPlanEntities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [Entity1C("Документ.ChangeRequest")]
    public class MonthPlanChangeRequest : IEntity, IMonthPlanEntity
    {
        [Property1C("Документ.ChangeRequest.Номер", "Номер")]
        [Type1C("String", "Строка")]
        public string Number { get; set; }

        [Property1C("Документ.ChangeRequest.Информация", "Информация")]
        [Type1C("String", "Строка")]
        public string Info { get; set; }

        [Property1C("Документ.ChangeRequest.Направление", "Направление")]
        [Type1C("Справочник.ITEXP_Направление", "Направление")]
        public Link1C Direction { get; set; }

        [Property1C("Документ.ChangeRequest.causename", "Основание")]
        [Type1C("String", "Строка")]
        public string Basis { get; set; }

        [Property1C("Документ.ChangeRequest.document", "Документ")]
        [Type1C("String", "Строка")]
        public string Document { get; set; }

        [Property1C("Документ.ChangeRequest.Трудоемкость", "Фактическая трудоемкость, час")]
        [Type1C("Number", "Число")]
        public double TotalResource { get; set; }

        [Property1C("Документ.ChangeRequest.resource", "Ресурс")]
        [Type1C("String", "Строка")]
        public double Resource { get; set; }

        [Property1C("Документ.ChangeRequest.continuation", "Является продолжением")]
        [Type1C("String", "Строка")]
        public bool IsContinuation { get; set; }

        [Property1C("Документ.ChangeRequest.ТребуетсяФС", "Требуется ФС")]
        [Type1C("Boolean", "Булево")]
        public bool? FsRequired { get; set; }

        [Property1C("Документ.ChangeRequest.ФСПринято", "ФС принято")]
        [Type1C("Boolean", "Булево")]
        public bool? FsAccepted { get; set; }

        [Property1C("Документ.ChangeRequest.Приоритет", "Приоритет")]
        [Type1C("Справочник.ChangeRequestPriority", "Приоритет")]
        public Link1C Priority { get; set; }

        [Property1C("Документ.ChangeRequest.state_title", "Заголовок состояния")]
        [Type1C("String", "Строка")]
        public string StateTitle { get; set; }

        [Property1C("Документ.ChangeRequest.statle_color", "Цвет состояния")]
        [Type1C("String", "Строка")]
        public string StateColor { get; set; }

        [Property1C("Документ.ChangeRequest.ДатаПрименения", "Дата применения")]
        [Type1C("Date", "Дата")]
        public DateTime? Deadline { get; set; }

        public IEnumerable<MonthPlanTeam> Teams { get; set; }

        public IEnumerable<MonthPlanAction> Actions { get; set; }

        #region Implementation of IEntity

        [Property1C("Документ.ChangeRequest.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        public bool? IsMarkedToDelete { get; set; }

        public IEnumerable<AdditionalAttribute1C> Attributes { get; set; }

        public ICollection<Attachment1C> Attachments { get; set; }

        #endregion
    }
}
