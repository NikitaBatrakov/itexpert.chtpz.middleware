﻿using System.Collections.Generic;

namespace ITExpert.CHPTZ.ObjectModel.Entities.MonthPlanEntities
{
    public interface IMonthPlanEntity
    {
        IEnumerable<MonthPlanTeam> Teams { get; set; }

        IEnumerable<MonthPlanAction> Actions { get; set; }
    }
}
