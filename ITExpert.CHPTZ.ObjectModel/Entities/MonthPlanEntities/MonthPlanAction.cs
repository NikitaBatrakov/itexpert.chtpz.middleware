namespace ITExpert.CHPTZ.ObjectModel.Entities.MonthPlanEntities
{
    public class MonthPlanAction
    {
        public string Name { get; set; }

        public bool? Availability { get; set; }

        public bool? Visibility { get; set; }

        public MonthPlanAction()
        {
            
        }

        public MonthPlanAction(string name, bool? availability, bool? visibility)
        {
            Name = name;
            Availability = availability;
            Visibility = visibility;
        }
    }
}