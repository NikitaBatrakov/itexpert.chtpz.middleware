﻿using System.Collections.Generic;

using DateTime = System.DateTime;

namespace ITExpert.CHPTZ.ObjectModel.Entities.MonthPlanEntities
{
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    [Entity1C("Документ.Итерация", "Итерация")]
    public class MonthPlanIteration : IEntity, IMonthPlanEntity
    {
        [Property1C("Документ.Итерация.number", "Номер")]
        [Type1C("String", "Строка")]
        public string Number { get; set; }

        [Property1C("Документ.Итерация.description", "Описание")]
        [Type1C("String", "Строка")]
        public string Info { get; set; }

        [Property1C("Документ.Итерация.direction", "Направление")]
        [Type1C("Справочник.ITEXP_Направление", "Направление")]
        public Link1C Direction { get; set; }

        [Property1C("Документ.Итерация.ЗНИСсылка", "Запрос на изменение")]
        [Type1C("Документ.ChangeRequest", "Запрос на изменение")]
        public Link1C ChangeRequest { get; set; }

        [Property1C("Документ.Итерация.cause", "Основание")]
        [Type1C("Справочник.ITEXP_Основания", "Основания")]
        public Link1C Basis { get; set; }

        [Property1C("Документ.Итерация.resourceAll", "Общий ресурс")]
        [Type1C("Number", "Число")]
        public double TotalResource { get; set; }

        [Property1C("Документ.Итерация.resource", "Ресурс")]
        [Type1C("Number", "Число")]
        public double Resource { get; set; }

        [Property1C("Документ.Итерация.Трудоемкость", "Трудоемкость")]
        [Type1C("Number", "Число")]
        public double? Workload { get; set; }

        [Property1C("Документ.Итерация.ТипЗадачи", "Тип задачи")]
        [Type1C("Справочник.ITEXP_ТипДополнительногоРеквизита", "Типы объектов")]
        public Link1C TaskType { get; set; }

        [Property1C("Документ.Итерация.deadline", "Крайний срок")]
        [Type1C("Date", "Дата")]
        public DateTime Deadline { get; set; }

        [Property1C("Документ.Итерация.haveFS", "Есть ФС")]
        [Type1C("Boolean", "Булево")]
        public bool FsAccepted { get; set; }

        [Property1C("Документ.Итерация.needFS", "Требуется ФС")]
        [Type1C("Boolean", "Булево")]
        public bool FsRequired { get; set; }

        [Property1C("Документ.Итерация.priority", "Приоритет")]
        [Type1C("Справочник.ChangeRequestPriority", "Приоритет")]
        public Link1C Priority { get; set; }

        [Property1C("Документ.Итерация.title", "Заголовок")]
        [Type1C("String", "Строка")]
        public string StateTitle { get; set; }

        [Property1C("Документ.Итерация.color", "Цвет")]
        [Type1C("String", "Строка")]
        public string StateColor { get; set; }

        [Property1C("Документ.Итерация.document", "Документ")]
        [Type1C("String", "Строка")]
        public string Document { get; set; }

        [Property1C("Документ.Итерация.МесПланМесяц", "Месяц")]
        [Type1C("Number", "Число")]
        public double Month { get; set; }

        [Property1C("Документ.Итерация.МесПланГод", "Год")]
        [Type1C("Number", "Число")]
        public double Year { get; set; }

        [Property1C("Документ.Итерация.forecast", "Прогноз")]
        [Type1C("Date", "Дата")]
        public DateTime Forecast { get; set; }

        #region Implementation of IMonthPlanEntity

        public IEnumerable<MonthPlanTeam> Teams { get; set; }

        public IEnumerable<MonthPlanAction> Actions { get; set; }

        #endregion

        #region Implementation of IEntity

        [Property1C("Документ.Итерация.Ссылка", "Ссылка")]
        [Type1C("Id", "Ссылка")]
        public Link1C Link { get; set; }

        public bool? IsMarkedToDelete { get; set; }

        public IEnumerable<AdditionalAttribute1C> Attributes { get; set; }

        public ICollection<Attachment1C> Attachments { get; set; }

        #endregion
    }
}
