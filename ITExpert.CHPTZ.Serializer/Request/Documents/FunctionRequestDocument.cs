namespace ITExpert.CHPTZ.Serializer.Request.Documents
{
    using System;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Request.Elements;
    using ITExpert.Xml.Monica.Serialization;

    public class FunctionRequestDocument : MRequestDocument
    {
        public EntitiesSerializableElement Entities { get; }
        public ParametersElement Parameters { get; }

        public FunctionRequestDocument(Type type, IEnumerable<object> entities, Function parameters, IMResolver resolver)
        {
            Parameters = new ParametersElement(parameters, resolver);
            AddElements(Parameters);
            if (entities != null)
            {
                Entities = new EntitiesSerializableElement(entities, type, resolver);
                AddElements(Entities);
            }
        }
    }
}