﻿namespace ITExpert.CHPTZ.Serializer.Request.Documents
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.CHPTZ.Serializer.Request.Elements;
    using ITExpert.Xml.Monica.Serialization;

    public class TemplateAttributesDocument : MRequestDocument
    {
        public TemplateAttributeElement Attribute { get; }

        public TemplateAttributesDocument(IEnumerable<TemplateAttributes> value)
        {
            Attribute = new TemplateAttributeElement(value);
            AddElements(Attribute);
        }
    }
}
