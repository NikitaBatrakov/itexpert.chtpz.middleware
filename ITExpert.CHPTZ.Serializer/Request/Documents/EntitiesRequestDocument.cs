﻿namespace ITExpert.CHPTZ.Serializer.Request.Documents
{
    using System;
    using System.Collections.Generic;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Request.Elements;
    using ITExpert.Xml.Monica.Serialization;

    public class EntitiesRequestDocument : MRequestDocument
    {
        public EntitiesSerializableElement Entities { get; }

        public EntitiesRequestDocument(IEnumerable<object> value, Type type, IMResolver resolver)
        {
            Entities = new EntitiesSerializableElement(value, type, resolver);
            AddElements(Entities);
        }
    }
}