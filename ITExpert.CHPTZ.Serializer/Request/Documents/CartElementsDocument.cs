﻿namespace ITExpert.CHPTZ.Serializer.Request.Documents
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.CHPTZ.Serializer.Request.Elements;
    using ITExpert.Xml.Monica.Serialization;

    public class CartElementsDocument : MRequestDocument
    {
        public CartElementElement CartElements { get; }

        public CartElementsDocument(IEnumerable<CartElement> elements)
        {
            CartElements = new CartElementElement(elements);
            AddElements(CartElements);
        }
    }
}
