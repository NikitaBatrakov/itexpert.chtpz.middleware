namespace ITExpert.CHPTZ.Serializer.Request.Elements
{
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Request.Serializers;
    using ITExpert.Xml.Monica.Serialization;

    public class ParametersElement : MSerializableElement<Function>
    {
        public ParametersElement(Function value, IMResolver resolver) 
            : this(value, new ParametersSerializer(resolver)) { }

        public ParametersElement(Function value, IMSerializer<Function> serializer) 
            : base("Parametr", serializer, value) { }
    }
}