namespace ITExpert.CHPTZ.Serializer.Request.Elements
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Request.Serializers;
    using ITExpert.Xml.Monica.Serialization;

    public class EntitiesSerializableElement : MSerializableElement<object>
    {
        public EntitiesSerializableElement(IEnumerable<object> value, Type type, IMResolver resolver) 
            : this(value, new EntitySerializer(type, resolver)) { }

        public EntitiesSerializableElement(IEnumerable<object> value, IMSerializer<object> serializer)
            : base("Entity", serializer, value.ToArray()) { }
    }
}