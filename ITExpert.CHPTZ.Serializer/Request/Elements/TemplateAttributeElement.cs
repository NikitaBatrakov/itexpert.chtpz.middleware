﻿namespace ITExpert.CHPTZ.Serializer.Request.Elements
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.CHPTZ.Serializer.Request.Serializers;
    using ITExpert.Xml.Monica.Serialization;

    public class TemplateAttributeElement : MSerializableElement<TemplateAttributes>
    {
        public TemplateAttributeElement(IEnumerable<TemplateAttributes> value, IMSerializer<TemplateAttributes> serializer)
            : base("Entity", serializer, value.ToArray()) { }

        public TemplateAttributeElement(IEnumerable<TemplateAttributes> value) 
            : this(value, new TemplateAttributeSerializer()) { }
    }
}
