﻿namespace ITExpert.CHPTZ.Serializer.Request.Elements
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.CHPTZ.Serializer.Request.Serializers;
    using ITExpert.Xml.Monica.Serialization;

    public class CartElementElement : MSerializableElement<CartElement>
    {
        public CartElementElement(IMSerializer<CartElement> serializer, params CartElement[] value)
            : base("Entity", serializer, value) { }

        public CartElementElement(IEnumerable<CartElement> value) : this(new CartElementSerializer(), value.ToArray()) { }
    }
}
