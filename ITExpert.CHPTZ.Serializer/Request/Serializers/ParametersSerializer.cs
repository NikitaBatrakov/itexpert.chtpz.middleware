﻿namespace ITExpert.CHPTZ.Serializer.Request.Serializers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.Common;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.Xml.Monica.Serialization;

    internal class ParametersSerializer : MSerializer<Function>
    {
        private const string ParametersReturnType = "ReturnType";
        private const string ParametersMetadataType = "MetadataType";

        private XmlWriter XmlWriter { get; set; }
        private IMResolver Resolver { get; }

        public ParametersSerializer(IMResolver resolver)
        {
            Resolver = resolver;
        }

        public override void Serialize(XmlWriter xmlWriter, string name, IEnumerable<Function> functions)
        {
            var function = functions.First();
            XmlWriter = xmlWriter;
            XmlWriter.WriteStartElement(name);
            WriteAttributes(function);

            if (function.Arguments == null || !function.Arguments.Any())
            {
                XmlWriter.WriteEndElement();
                return;
            }

            WriteElements(function);
            XmlWriter.WriteEndElement();
        }

        private void WriteElements(Function function)
        {
            var parameters = function.Arguments.Where(param => !string.IsNullOrEmpty(param.Value?.ToString()));
            foreach (var param in parameters)
            {
                XmlWriter.WriteStartElement(param.Name);
                XmlWriter.WriteAttributeString(TypeDeserializer.TypeAttribute, param.Type.GetName1C());
                //if (!string.IsNullOrEmpty(param.Value?.ToString()))
                //{
                    XmlWriter.WriteValue(param.Value);
                //}
                XmlWriter.WriteEndElement();
            }
        }

        private void WriteAttributes(Function function)
        {
            XmlWriter.WriteAttributeString(TypeDeserializer.TypeAttribute, function.Name);
            if (!string.IsNullOrEmpty(function.ReturnType))
            {
                var type = Resolver.GetClassType(function.ReturnType, throwOnError: false) ??
                           TypeParser.Parse(function.ReturnType, typeof(string), throwOnError: true);
                XmlWriter.WriteAttributeString(ParametersReturnType, type.GetName1C());
            }
            if (!string.IsNullOrEmpty(function.TargetType))
            {
                var metadaType = Resolver.GetExternalTypeName(function.TargetType);
                XmlWriter.WriteAttributeString(ParametersMetadataType, metadaType);
            }
        }        
    }
}
