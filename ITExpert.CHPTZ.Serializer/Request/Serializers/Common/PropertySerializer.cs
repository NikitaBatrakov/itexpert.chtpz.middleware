﻿namespace ITExpert.CHPTZ.Serializer.Request.Serializers.Common
{
    using System;
    using System.Linq;
    using System.Xml;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Common;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;

    internal class PropertySerializer
    {
        private IMResolver Resolver { get; }
        private XmlWriter XmlWriter { get; }
        private const string TableRowName = "Determenant";

        public PropertySerializer(IMResolver resolver, XmlWriter writer)
        {
            Resolver = resolver;
            XmlWriter = writer;
        }

        public void WriteProperty(string propertyName, QueryEntityPropertyValue propertyValue)
        {
            if (propertyValue.Type.IsComposite)
            {
                WriteCompositProperty(propertyName, (Composite1C)propertyValue.Value);
            }
            if (propertyValue.Type.IsTable)
            {
                WriteTableProperty(propertyName, (Table1C)propertyValue.Value, propertyValue.Type);
            }
            else
            {
                WriteSimpleProperty(propertyName, propertyValue.Value, propertyValue.Type);
            }
        }

        public void WriteProperty(object entity, PropertyCachedValue propCache)
        {
            var value = propCache.PropertyInfo.GetValue(entity);
            var propertyName = propCache.NameAttribute.Name;
            var typeAttrs = propCache.TypeAttributes.ToArray();
            if (typeAttrs.Length == 0)
            {
                return;
            }

            PropertyType type;
            if (propCache.PropertyInfo.PropertyType == typeof(Table1C))
            {
                type = PropertyTypeFactory.Create(typeAttrs, Resolver);
                WriteTableProperty(propertyName, (Table1C)value, type);
            }
            else
            {
                if (typeAttrs[0].IsComposite)
                {
                    WriteCompositProperty(propertyName, (Composite1C)value);
                }
                else
                {
                    type = PropertyTypeFactory.Create(typeAttrs[0]);
                    WriteSimpleProperty(propertyName, value, type);
                }
            }
        }

        private void WriteSimpleProperty(string name, object propValue, PropertyType propertyType)
        {
            XmlWriter.WriteStartElement(name);
            string value, type;
            if (propertyType.IsPrimitive)
            {
                type = propertyType.Type;
                try
                {
                    var date = (DateTime)propValue;
                    value = date.ToUtcIsoString();
                }
                catch (NullReferenceException)
                {
                    value = null;
                }
                catch (InvalidCastException)
                {
                    value = propValue.ToString();
                }
            }
            else
            {
                var id = propValue as Link1C;
                type = Resolver.GetExternalTypeName(propertyType.Type, throwOnError:false) ?? propertyType.Type;
                value = string.IsNullOrEmpty(id?.Value) ? TypeDeserializer.UndefinedValue : id.Value;
            }

            XmlWriter.WriteAttributeString(TypeDeserializer.TypeAttribute, type);
            if (value != null)
                XmlWriter.WriteValue(value);
            XmlWriter.WriteEndElement();
        }

        private void WriteTableProperty(string name, Table1C table, PropertyType propertyType)
        {
            XmlWriter.WriteStartElement(name);
            XmlWriter.WriteAttributeString(TypeDeserializer.TypeAttribute, PropertyType.Table);
            if (table == null)
            {
                XmlWriter.WriteEndElement();
                return;
            }
            var types = propertyType.Types.ToArray();
            foreach (var row in table.Rows)
            {
                XmlWriter.WriteStartElement(TableRowName);
                var i = 0;
                foreach (var col in row)
                {
                    var colType = types[i];
                    WriteSimpleProperty(colType.Name, col, colType);
                    i++;
                }
                XmlWriter.WriteEndElement();
            }

            XmlWriter.WriteEndElement();
        }

        private void WriteCompositProperty(string name, Composite1C propValue)
        {
            XmlWriter.WriteStartElement(name);
            var type = propValue == null ? "" : Resolver.GetExternalTypeName(propValue.Type, throwOnError:false) ?? propValue.Type;
            var value = propValue?.Value?.ToString() ?? TypeDeserializer.UndefinedValue;
            XmlWriter.WriteAttributeString(TypeDeserializer.TypeAttribute, type);
            XmlWriter.WriteValue(value);
            XmlWriter.WriteEndElement();
        }
    }
}
