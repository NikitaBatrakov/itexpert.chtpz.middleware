﻿namespace ITExpert.CHPTZ.Serializer.Request.Serializers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.Xml.Monica.Serialization;

    public class CartElementSerializer : MSerializer<CartElement>
    {
        public override void Serialize(XmlWriter xmlWriter, string name, IEnumerable<CartElement> value)
        {
            foreach (var element in value)
            {
                WriteCartElement(xmlWriter, name, element);
            }
        }

        private void WriteCartElement(XmlWriter xmlWriter, string name, CartElement element)
        {
            xmlWriter.WriteStartElement(name);

            WriteProperty(xmlWriter, "MainObject", element.ServiceRequestGuid);
            WriteProperty(xmlWriter, "Service", element.ServiceGuid);
            WriteProperty(xmlWriter, "TemplateZNO", element.TemplateGuid);
            WriteProperty(xmlWriter, "TemplateProcess", element.ProcessTemplateGuid);
            WriteProperty(xmlWriter, "KE", element.ConfigurationUnitGuid);
            WriteProperty(xmlWriter, "Employee", element.EmployeeGuid);
            WriteProperty(xmlWriter, "Status", element.StatusGuid);

            WriteComments(xmlWriter, "EntityComments", element.Comments?.ToArray());

            xmlWriter.WriteEndElement();
        }

        private void WriteProperty(XmlWriter xmlWriter, string name, string value)
        {
            xmlWriter.WriteStartElement(name);
            xmlWriter.WriteValue(value);
            xmlWriter.WriteEndElement();
        }

        private void WriteComments(XmlWriter xmlWriter, string name, CartElementComment[] comments)
        {
            xmlWriter.WriteStartElement(name);

            if (comments == null || !comments.Any())
            {
                xmlWriter.WriteEndElement();
                return;
            }

            foreach (var comment in comments.Where(comment => comment != null))
            {
                WriteComment(xmlWriter, "Comment", comment);
            }
            xmlWriter.WriteEndElement();
        }

        private void WriteComment(XmlWriter xmlWriter, string name, CartElementComment comment)
        {
            xmlWriter.WriteStartElement(name);

            WriteProperty(xmlWriter, "Text", comment.Text);
            WriteProperty(xmlWriter, "Date", comment.Date.ToUtcIsoString());
            WriteProperty(xmlWriter, "Employee", comment.EmployeeGuid);
            WriteProperty(xmlWriter, "Reconciliation", comment.ApprovingNodeName);

            xmlWriter.WriteEndElement();
        }
    }
}
