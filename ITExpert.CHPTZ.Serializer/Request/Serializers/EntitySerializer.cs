﻿namespace ITExpert.CHPTZ.Serializer.Request.Serializers
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.CHPTZ.Serializer.Request.Serializers.Common;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.Xml.Monica.Serialization;

    internal class EntitySerializer : MSerializer<object>
    {
        public Type Type { get; }
        
        private IMResolver Resolver { get; }
        private XmlWriter XmlWriter { get; set; }
        private PropertySerializer PropertySerializer { get; set; }

        public EntitySerializer(Type type, IMResolver resolver)
        {
            Type = type;
            Resolver = resolver;
        }

        public override void Serialize(XmlWriter xmlWriter, string name, IEnumerable<object> entities)
        {
            XmlWriter = xmlWriter;
            PropertySerializer = new PropertySerializer(Resolver, xmlWriter);
            var cache = TypeCache.Get(Type);
            foreach (var entity in entities)
            {
                //if (!Type.IsInstanceOfType(entity))
                //    throw new Exception($"Object ({entity}) is not an instance of the specified type ({Type.FullName})");
                WriteEntity(name, Type.GetName1C(), entity, cache);
            }
        }

        private void WriteEntity(string elementName, string entityName, object entity, IDictionary<string, PropertyCachedValue> cache)
        {
            XmlWriter.WriteStartElement(elementName);
            XmlWriter.WriteAttributeString(TypeDeserializer.TypeAttribute, entityName);
            foreach (var pair in cache)
            {
                var value = pair.Value.PropertyInfo.GetValue(entity);
                if (value == null) continue;
                PropertySerializer.WriteProperty(entity, pair.Value);
            }
            XmlWriter.WriteEndElement();
        }
    }
}
