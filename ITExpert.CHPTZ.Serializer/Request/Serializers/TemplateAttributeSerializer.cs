﻿namespace ITExpert.CHPTZ.Serializer.Request.Serializers
{
    using System.Collections.Generic;
    using System.Xml;

    using ITExpert.CHPTZ.ObjectModel.Dtos.CartDtos;
    using ITExpert.Xml.Monica.Serialization;

    public class TemplateAttributeSerializer : MSerializer<TemplateAttributes>
    {
        private const string AttributeElementName = "Реквизит";

        public override void Serialize(XmlWriter xmlWriter, string name, IEnumerable<TemplateAttributes> value)
        {
            foreach (var templateAttributes in value)
            {
                WriteTempateAttributes(xmlWriter, name, templateAttributes);
            }
        }

        private void WriteTempateAttributes(XmlWriter xmlWriter, string name, TemplateAttributes attributes)
        {
            xmlWriter.WriteStartElement(name);
            xmlWriter.WriteAttributeString("guid", attributes.TemplateGuid);

            if (attributes.Attributes == null)
            {
                xmlWriter.WriteEndElement();
                return;
            }

            foreach (var attribute in attributes.Attributes)
            {
                WriteAttribute(xmlWriter, attribute);
            }

            xmlWriter.WriteEndElement();
        }

        private void WriteAttribute(XmlWriter xmlWriter, TemplateAttribute attribute)
        {
            xmlWriter.WriteStartElement(AttributeElementName);

            //In that exact order
            xmlWriter.WriteAttributeString("ГуйДопРек", attribute.AttributeTypeGuid);
            xmlWriter.WriteAttributeString("ТипДопРек", attribute.ValueType);
            xmlWriter.WriteAttributeString("ГуйУслуги", attribute.ServiceGuid);
            xmlWriter.WriteAttributeString("ГуйШаблонаБП", attribute.BusinessProcessTemplateGuid);

            xmlWriter.WriteValue(attribute.Value);

            xmlWriter.WriteEndElement();
        }
    }
}
