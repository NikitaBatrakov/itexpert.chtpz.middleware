﻿namespace ITExpert.CHPTZ.Serializer.Request.Serializers
{
    using System.Collections.Generic;
    using System.Xml;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Request.Serializers.Common;
    using ITExpert.Xml.Monica.Serialization;

    internal class QueryEntitySerializer : MSerializer<IDictionary<string, QueryEntityPropertyValue>>
    {
        private IMResolver Resolver { get; }
        private XmlWriter XmlWriter { get; set; }
        private PropertySerializer PropertySerializer { get; set; }

        public QueryEntitySerializer(IMResolver resolver)
        {
            Resolver = resolver;
        }

        public override void Serialize(XmlWriter xmlWriter, string name, IEnumerable<IDictionary<string, QueryEntityPropertyValue>> entities)
        {
            XmlWriter = xmlWriter;
            PropertySerializer = new PropertySerializer(Resolver, xmlWriter);
            foreach (var entity in entities)
            {
                WriteEntity(name, entity);
            }
        }

        private void WriteEntity(string elementName, IDictionary<string, QueryEntityPropertyValue> entity)
        {
            XmlWriter.WriteStartElement(elementName);
            XmlWriter.WriteAttributeString("Type", "Query");
            foreach (var pair in entity)
            {
                PropertySerializer.WriteProperty(pair.Key, pair.Value);
            }
            XmlWriter.WriteEndElement();
        }
    }
}
