﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class EntitiesResponseDocument : ResultableResponseDocument
    {
        public MetadataElement Metadata { get; }
        public EntitiesDeserializableElement Entities { get; }
        public TotalResultElement TotalResult { get; }

        public EntitiesResponseDocument(string xml, Type type, IMResolver resolver, bool withMetadata = false)
        {
            Metadata = new MetadataElement(type, resolver);
            Entities = new EntitiesDeserializableElement(type, resolver);
            TotalResult = new TotalResultElement();
            AddElements(Entities, TotalResult);
            if (withMetadata)
            {
                AddElements(Metadata);
            }
            Deserialize(xml);
        }
    }
}
