﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class AttributesTypesDocument : ResultableResponseDocument
    {
        public AttributesDetailElement AttributesDetail { get; }

        public AttributesTypesDocument(string xml, IMResolver resolver)
        {
            AttributesDetail = new AttributesDetailElement(resolver);
            AddElements(AttributesDetail);
            Deserialize(xml);
        }
    }
}
