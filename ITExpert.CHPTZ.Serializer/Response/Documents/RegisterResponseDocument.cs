﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class RegisterResponseDocument : ResultableResponseDocument
    {
        public MetadataElement Metadata { get; }
        public RegistersElement Entities { get; }
        public TotalResultElement TotalResult { get; }

        public RegisterResponseDocument(string xml, Type type, IMResolver resolver, bool withMetadata = false)
        {
            Metadata = new MetadataElement(type, resolver);
            Entities = new RegistersElement(type, resolver);
            TotalResult = new TotalResultElement();
            AddElements(Entities, TotalResult);
            if (withMetadata)
            {
                AddElements(Metadata);
            }
            Deserialize(xml);
        }
    }
}
