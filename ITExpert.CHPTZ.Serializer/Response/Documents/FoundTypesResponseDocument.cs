﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class FoundTypesResponseDocument : ResultableResponseDocument
    {
        public FoundTypesElement Types { get; }

        public FoundTypesResponseDocument(string xml, IMResolver resolver)
        {
            Types = new FoundTypesElement(resolver);
            AddElements(Types);
            Deserialize(xml);
        }
    }
}
