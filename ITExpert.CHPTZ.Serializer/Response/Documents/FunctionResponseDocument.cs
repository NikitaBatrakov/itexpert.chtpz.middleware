﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class FunctionResponseDocument : ResultableResponseDocument
    {
        public ActionsElement Actions { get; }
        public MetadataElement Metadata { get; }
        public EntitiesDeserializableElement Entities { get; }

        public FunctionResponseDocument(string xml, Type entityType, IMResolver resolver)
        {
            Actions = new ActionsElement();
            Entities = new EntitiesDeserializableElement(entityType, resolver);
            Metadata = new MetadataElement(entityType, resolver);
            AddElements(Actions, Entities, Metadata);
            Deserialize(xml);
        }
    }
}
