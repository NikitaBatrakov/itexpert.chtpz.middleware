﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class QueryEntitiesDocument : ResultableResponseDocument
    {
        public QueryEntitiesElement Entities { get; }
        public TotalResultElement TotalResult { get; }

        public QueryEntitiesDocument(string xml, Type type, IMResolver resolver)
        {
            Entities = new QueryEntitiesElement(type, resolver);
            TotalResult = new TotalResultElement();
            AddElements(Entities, TotalResult);
            Deserialize(xml);
        }
    }
}
