﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class SearchEntitiesDocument : ResultableResponseDocument
    {
        public SearchEntityElement Entities { get; }
        public TotalResultElement TotalResult { get; }

        public SearchEntitiesDocument(string xml, Type type, IMResolver resolver)
        {
            Entities = new SearchEntityElement(type, resolver);
            TotalResult = new TotalResultElement();
            AddElements(Entities, TotalResult);
            Deserialize(xml);
        }
    }
}
