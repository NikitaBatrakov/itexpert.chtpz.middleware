﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class UniversalEntityDocument : ResultableResponseDocument
    {
        public UniversalEntitiesElement Entities { get; }
        public TotalResultElement TotalResult { get; }

        public UniversalEntityDocument(string xml, Type type, IMResolver resolver)
        {
            Entities = new UniversalEntitiesElement(type, resolver);
            TotalResult = new TotalResultElement();
            AddElements(Entities, TotalResult);
            Deserialize(xml);
        }
    }
}
