﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Entities;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.Xml.Monica.Deserialization;

    public class CartOrderListResponseDocument : ResultableResponseDocument
    {
        public MDeserializableElement<Order> Orders { get; }
        public MDeserializableElement<OrderComment> Comments { get; }
        public MDeserializableElement<ServiceTemplateAdditionalAttribute> TemplatesAttributes { get; }

        public CartOrderListResponseDocument(string xml, IMResolver resolver)
        {
            const string elementName = "Entity";
            const bool addEntityPrefixToProperties = true;

            var orderDeserializer = new BaseEntityDeserializer<Order>(typeof(Order), resolver,
                                                                      addEntityPrefixToProperties);
            Orders = new MDeserializableElement<Order>(elementName, orderDeserializer);
            AddElements(Orders);

            var commentDeserializer = new BaseEntityDeserializer<OrderComment>(typeof(OrderComment), resolver,
                                                                               addEntityPrefixToProperties);
            Comments = new MDeserializableElement<OrderComment>(elementName, commentDeserializer);
            AddElements(Comments);

            var templatesAttributesDeserializer =
                new BaseEntityDeserializer<ServiceTemplateAdditionalAttribute>(
                    typeof(ServiceTemplateAdditionalAttribute), resolver, addEntityPrefixToProperties);
            TemplatesAttributes = new MDeserializableElement<ServiceTemplateAdditionalAttribute>(elementName,
                                                                                                 templatesAttributesDeserializer);
            AddElements(TemplatesAttributes);
            Deserialize(xml);
        }
    }
}
