﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents.Common
{
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.CHPTZ.Serializer.Response.Elements;
    using ITExpert.Xml.Monica.Deserialization;

    public class ResultableResponseDocument : MResponseDocument
    {
        public ServiceResultElement ServiceResult { get; }

        protected ResultableResponseDocument()
        {
            ServiceResult = new ServiceResultElement(new ServiceResultDeserializer());
            Elements.Add(ServiceResult);
        }

        public ResultableResponseDocument(string xml)
        {
            ServiceResult = new ServiceResultElement(new ServiceResultDeserializer());
            Elements.Add(ServiceResult);
            Deserialize(xml);
        }
    }
}
