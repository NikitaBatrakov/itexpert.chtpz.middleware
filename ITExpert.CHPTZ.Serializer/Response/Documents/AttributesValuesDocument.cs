﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class AttributesValuesDocument : ResultableResponseDocument
    {
        public AttributesValuesElement AttributesValues { get; }

        public AttributesValuesDocument(string xml, IMResolver resolver)
        {
            AttributesValues = new AttributesValuesElement(resolver);
            AddElements(AttributesValues);
            Deserialize(xml);
        }
    }
}
