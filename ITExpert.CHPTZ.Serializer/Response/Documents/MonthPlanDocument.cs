﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class MonthPlanDocument : ResultableResponseDocument
    {
        public MonthPlanChangeRequestElement Entities { get; }
        public TotalResultElement TotalResult { get; }

        public MonthPlanDocument(string xml, Type type, IMResolver resolver)
        {
            Entities = new MonthPlanChangeRequestElement(type, resolver);
            TotalResult = new TotalResultElement();
            AddElements(Entities, TotalResult);
            Deserialize(xml);
        }
    }
}
