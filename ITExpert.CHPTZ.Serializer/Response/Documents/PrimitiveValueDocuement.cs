﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using System.Collections.Generic;

    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.Xml.Monica.Deserialization;

    public class PrimitiveValueDocuement<T> : ResultableResponseDocument
    {
        public MDeserializableElement<T> Values { get; }

        public PrimitiveValueDocuement(string xml)
        {
            Values = new MDeserializableElement<T>("Value", new PrimitiveValueDeserializer<T>());
            AddElements(Values);
            Deserialize(xml);
        }
    }
}
