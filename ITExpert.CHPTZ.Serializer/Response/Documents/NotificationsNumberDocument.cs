﻿namespace ITExpert.CHPTZ.Serializer.Response.Documents
{
    using ITExpert.CHPTZ.Serializer.Response.Documents.Common;
    using ITExpert.CHPTZ.Serializer.Response.Elements;

    public class NotificationsNumberDocument : ResultableResponseDocument
    {
        public NotificationsNumberElement Entities { get; }

        public NotificationsNumberDocument(string xml)
        {
            Entities = new NotificationsNumberElement();
            AddElements(Entities);
            Deserialize(xml);
        }
    }
}
