﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.ObjectModel.Entities.MonthPlanEntities;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;

    internal class MonthPlanDeserializer : BaseEntityDeserializer<IMonthPlanEntity>
    {
        public MonthPlanDeserializer(Type type, IMResolver resolver)
            : base(type, resolver) { }

        #region Overrides of BaseEntityDeserializer<IMonthPlanEntity>

        protected override IMonthPlanEntity SetEntityProperties(XElement xEntity, IMonthPlanEntity entity)
        {
            var teams = GetTeams(xEntity).ToArray();
            if (teams.Any())
            {
                entity.Teams = teams;
            }

            var actions = GetActions(xEntity).ToArray();
            if (actions.Any())
            {
                entity.Actions = actions;
            }

            return base.SetEntityProperties(xEntity, entity);
        }

        #endregion

        private IEnumerable<MonthPlanTeam> GetTeams(XElement xEntity)
        {
            var xTeams = xEntity.Element("teams");
            if (xTeams == null) yield break;
            foreach (var xTeam in xTeams.Elements())
            {
                yield return GetPlan(xTeam);
            }
        }

        private MonthPlanTeam GetPlan(XElement xTeam)
        {
            var team = new MonthPlanTeam();
            foreach (var xProp in xTeam.Elements())
            {
                switch (xProp.Name.LocalName.ToLowerInvariant())
                {
                    case "team":
                        team.Team = GetLink(xProp);
                        break;
                    case "resourceall":
                        team.TotalResource = ParseDouble(xProp.Value);
                        break;
                    case "resource":
                        team.Resource = ParseDouble(xProp.Value);
                        break;
                    case "main":
                        team.IsMain = ParseBool(xProp.Value);
                        break;
                    case "зниссылка":
                        team.ChangeRequest = GetLink(xProp);
                        break;
                    default:
                        break;
                }
            }
            return team;
        }

        

        private Link1C GetLink(XElement xProp)
        {
            var type = TypeDeserializer.Deserialize(xProp);
            var linkPerformance = xProp.Attribute("LinkPerformance")?.Value;
            return new Link1C(xProp.Value, linkPerformance, type?.Type);
        }

        private static IEnumerable<MonthPlanAction> GetActions(XElement xEntity)
        {
            var xActions = xEntity.Elements("Actions");
            foreach (var xAction in xActions.Elements())
            {
                var availability = ParseBool(xAction.Attribute("Availability").Value);
                var visibility = ParseBool(xAction.Attribute("Visibility").Value);
                yield return new MonthPlanAction(xAction.Name.LocalName, availability, visibility);
            }
        }

        private static bool? ParseBool(string value)
        {
            bool parsedValue;
            var isParsed = bool.TryParse(value, out parsedValue);
            if (isParsed)
            {
                return parsedValue;
            }
            return null;
        }

        private static double? ParseDouble(string value)
        {
            double parsedValue;
            var isParsed = double.TryParse(value, out parsedValue);
            if (isParsed)
            {
                return parsedValue;
            }
            return null;
        }
    }
}
