﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Common;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.Xml.Monica.Deserialization;

    internal class MetadataDeserializer : IMDeserializer<IDictionary<string, PropertyMetadata>>
    {
        private const string AvailableAttribute = "Availability";
        private const string VisibleAttribute = "Visibility";
        private const string RequiredAttribute = "Required";

        private IMResolver Resolver { get; }
        private IDictionary<string, PropertyCachedValue> Cache { get; }
        private TypeDeserializer TypeDeserializer { get; }

        public MetadataDeserializer(Type type, IMResolver resolver)
        {
            Cache = TypeCache.Get(type);
            Resolver = resolver;
            TypeDeserializer = new TypeDeserializer(resolver);
        }

        public IEnumerable<IDictionary<string, PropertyMetadata>> Deserialize(IEnumerable<XElement> xElements)
        {
            var xMeta = xElements.First();
            var metadata = new Dictionary<string, PropertyMetadata>();
            foreach (var prop in Cache)
            {
                var xPropMeta = xMeta.Element(prop.Value.NameAttribute.Name);
                if (xPropMeta == null) continue;
                var lowerKey = prop.Key.LowerFirstLetter();

                var type = IsPropertyMapped(prop.Value)
                    ? PropertyTypeFactory.Create(prop.Value.TypeAttributes, Resolver)
                    : TypeDeserializer.Deserialize(xPropMeta);
                var verbose = prop.Value.NameAttribute.Verbose;

                var meta = new PropertyMetadata(type, verbose);
                SetFlags(xPropMeta, meta);
                metadata.Add(lowerKey, meta);
            }
            return new[] {metadata};
        }

        private void SetFlags(XElement propMeta, PropertyMetadata propertyMetadata)
        {
            bool available, visible, required;
            var aSuccess = bool.TryParse(propMeta.Attribute(AvailableAttribute)?.Value, out available);
            var vSuccess = bool.TryParse(propMeta.Attribute(VisibleAttribute)?.Value, out visible);
            var rSuccess = bool.TryParse(propMeta.Attribute(RequiredAttribute)?.Value, out required);
            if (aSuccess) propertyMetadata.Available = available;
            if (vSuccess) propertyMetadata.Visible = visible;
            if (rSuccess) propertyMetadata.Required = required;
        }

        private bool IsPropertyMapped(PropertyCachedValue prop)
        {
            return prop.TypeAttributes != null && prop.TypeAttributes.Any();
        }
    }
}
