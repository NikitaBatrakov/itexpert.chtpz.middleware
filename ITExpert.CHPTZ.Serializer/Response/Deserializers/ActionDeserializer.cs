﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.Xml.Monica.Deserialization;

    internal class ActionDeserializer : IMDeserializer<FormAction>
    {
        private const string AvailableAttribute = "Availability";
        private const string VisibleAttribute = "Visibility";

        public IEnumerable<FormAction> Deserialize(IEnumerable<XElement> xElements)
        {
            var xElement = xElements.First();
            var actions = xElement.Elements().Select(GetAction).ToArray();
            return actions.Length > 0 ? actions : null;
        }

        private FormAction GetAction(XElement xElement)
        {
            var action = new FormAction(xElement.Name.ToString());
            var availableStr = xElement.Attribute(AvailableAttribute)?.Value;
            if (!string.IsNullOrEmpty(availableStr))
            {
                action.Available = bool.Parse(availableStr);
            }
            var visisbleStr = xElement.Attribute(VisibleAttribute)?.Value;
            if (!string.IsNullOrEmpty(visisbleStr))
            {
                action.Visible = bool.Parse(visisbleStr);
            }
            return action;
        }
    }
}
