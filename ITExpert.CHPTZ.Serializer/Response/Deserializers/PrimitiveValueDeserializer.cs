﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.Xml.Monica.Deserialization;

    class PrimitiveValueDeserializer<T> : IMDeserializer<T>
    {
        #region Implementation of IMDeserializer<out T>

        public IEnumerable<T> Deserialize(IEnumerable<XElement> xElements)
        {
            return xElements.Select(ParseValue);
        }

        #endregion

        private static T ParseValue(XElement xElement)
        {
            var type = xElement.Attribute("Type").Value;
            var value = xElement.Value;
            return (T)PropertyType.ParsePrimitiveValue(type, value);
        }
    }
}
