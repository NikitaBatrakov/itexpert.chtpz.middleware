﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.Xml.Monica.Deserialization;

    internal class MessagesDeserializer : IMDeserializer<FormMessage>
    {
        private const string FieldAttributeName = "Field";

        public IEnumerable<FormMessage> Deserialize(IEnumerable<XElement> xElements)
        {
            var xMessages = xElements.First();
            return xMessages.Elements()
                .Select(msg =>
                    new FormMessage(msg.Attribute(FieldAttributeName)?.Value, msg.Value))
                .ToArray();
        }
    }
}
