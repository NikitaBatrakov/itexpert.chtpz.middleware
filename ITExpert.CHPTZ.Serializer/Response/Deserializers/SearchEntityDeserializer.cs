﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.ObjectModel.Dtos.SearchDtos;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.Xml.Monica.Deserialization;

    internal class SearchEntityDeserializer : IMDeserializer<UserFilter>
    {
        private Type Type { get; }
        private IMResolver Resolver { get; }
        private TypeDeserializer TypeDeserializer { get; }
        private ValueDeserializer ValueDeserializer { get; }
        private IDictionary<string, PropertyCachedValue> Cache { get; }

        public SearchEntityDeserializer(Type type, IMResolver resolver)
        {
            Cache = TypeCacheWith1CNamesAsKeys.Get(type);
            Type = type;
            Resolver = resolver;
            TypeDeserializer = new TypeDeserializer(resolver);
            ValueDeserializer = new ValueDeserializer(resolver, TypeDeserializer);
        }

        public IEnumerable<UserFilter> Deserialize(IEnumerable<XElement> xElements)
        {
            return new[] { GetEntity(xElements.Elements()) };
        }

        private UserFilter GetEntity(IEnumerable<XElement> xElements)
        {
            if (xElements == null) return null;
            var dto = new UserFilter();
            foreach (var xElement in xElements)
            {
                switch (xElement.Name.ToString().ToLower())
                {
                    case "serchparametrs":
                        dto.Parameters = ParseParameters(xElement);
                        break;
                    case "serchtable":
                        dto.Filters = ParseFilterTable(xElement.Elements());
                        break;
                    case "fieldstable":
                        dto.Fields = ParseFieldsTable(xElement.Elements());
                        break;
                    default:
                        continue;
                }
            }
            return dto;
        }

        private UserFilterParameters ParseParameters(XElement xElement) => GetEntity<UserFilterParameters>(xElement);

        private IEnumerable<AvailableEntityFilter> ParseFilterTable(IEnumerable<XElement> xElements)
        {
            foreach (var xElement in xElements)
            {
                var entity = new AvailableEntityFilter {Operators = GetOperators(xElement)};
                foreach (var xProperty in xElement.Elements())
                {
                    switch (xProperty.Name.LocalName)
                    {
                        case "String.ИмяПоля":
                            var fullName = $"{Type.GetName1C()}.{xProperty.Value}";
                            var prop = Cache[fullName];
                            entity.Field = prop.PropertyInfo.Name;
                            var type = TypeDeserializer.Deserialize(xProperty);
                            entity.Type = type.Type;
                            entity.IsLink = type.IsLink;
                            break;
                        case "String.ДопРек":
                            entity.IsAdditionalAttribute = bool.Parse(xProperty.Value);
                            break;
                        case "String.Обязательный":
                            entity.IsRequired = bool.Parse(xProperty.Value);
                            break;
                        case "String.Синоним":
                            entity.Verbose = xProperty.Value;
                            break;
                        case "String.Ключ":
                            entity.IsKey = bool.Parse(xProperty.Value);
                            break;
                        default:
                            break;
                    }
                }
                yield return entity;
            }
        }

        private IEnumerable<AvailableEntityField> ParseFieldsTable(IEnumerable<XElement> xElements)
        {
            foreach (var xElement in xElements)
            {
                var entity = new AvailableEntityField();
                foreach (var xProperty in xElement.Elements())
                {
                    switch (xProperty.Name.LocalName)
                    {
                        case "String.ИмяПоля":
                            var fullName = $"{Type.GetName1C()}.{xProperty.Value}";
                            var prop = Cache[fullName];
                            entity.Name = prop.PropertyInfo.Name;
                            break;
                        case "String.ДопРек":
                            entity.IsAdditionalAttribute = bool.Parse(xProperty.Value);
                            break;
                        case "String.Синоним":
                            entity.Verbose = xProperty.Value;
                            break;
                        case "String.Ключ":
                            entity.IsKey = bool.Parse(xProperty.Value);
                            break;
                        default:
                            break;
                    }
                }
                yield return entity;
            }
        } 

        private T GetEntity<T>(XElement xElement) where T : new()
        {
            var entity = new T();
            foreach (var prop in typeof(T).GetProperties())
            {
                var name = prop.GetCustomAttribute<Property1CAttribute>()?.Name;
                if (name == null) continue;
                var xProp = xElement.Element(name);
                if (xProp == null) continue;

                var val = prop.PropertyType == typeof(PropertyType)
                              ? TypeDeserializer.Deserialize(xProp)
                              : ValueDeserializer.GetSimpleTypeValue(xProp);
                prop.SetValue(entity, val);
            }
            return entity;
        }

        private IEnumerable<ComparisonOperator1C> GetOperators(XElement xElement)
        {
            var xAttrts = xElement.Attributes();
            return xAttrts.Select(xAttr => ComparisonOperator1C.ParseVerbose(xAttr.Value));
        }
    }
}
