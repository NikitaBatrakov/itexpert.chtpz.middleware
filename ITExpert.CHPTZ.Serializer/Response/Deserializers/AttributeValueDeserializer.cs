﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.Xml.Monica.Deserialization;

    public class AttributeValueDeserializer : IMDeserializer<AdditionalAttributeValue>
    {
        private const string ObjectElementName = "РегистрСведений.ДополнительныеСведения.Объект";
        private const string DetailsElementName = "РегистрСведений.ДополнительныеСведения.Реквизит";
        private const string ValueElementName = "РегистрСведений.ДополнительныеСведения.Значение";

        private ValueDeserializer ValueDeserializer { get; }

        private IMResolver Resolver { get; }

        public AttributeValueDeserializer(IMResolver resolver)
        {
            Resolver = resolver;
            ValueDeserializer = new ValueDeserializer(resolver);
        }

        public IEnumerable<AdditionalAttributeValue> Deserialize(IEnumerable<XElement> xElements)
        {
            foreach (var xElement in xElements)
            {
                Link1C obj = null, attrType = null;
                object value = null;
                foreach (var xProperty in xElement.Elements())
                {
                    switch (xProperty.Name.ToString())
                    {
                        case ObjectElementName:
                            var type = Resolver.GetInternalType(xProperty.Attribute("Type").Value);
                            var linkPerformance = xProperty.Attribute("LinkPerformance")?.Value;
                            obj = new Link1C(xProperty.Value, linkPerformance, type.Name);
                            break;
                        case DetailsElementName:
                            linkPerformance = xProperty.Attribute("LinkPerformance")?.Value;
                            attrType = new Link1C(xProperty.Value, linkPerformance, null);
                            break;
                        case ValueElementName:
                            value = ValueDeserializer.GetSimpleTypeValue(xProperty);
                            break;
                        default:
                            break;
                    }
                }

                if (obj == null || attrType == null)
                    throw new Exception("AttributeValue Deserialization failed: not all necessary values were retreived");

                yield return new AdditionalAttributeValue(obj, attrType, value);
            }
        }
    }
}
