namespace ITExpert.CHPTZ.Serializer.Response.Deserializers.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;

    internal class ValueDeserializer
    {
        private IMResolver Resolver { get; }
        private TypeDeserializer TypeDeserializer { get; }

        public ValueDeserializer(IMResolver resolver)
        {
            Resolver = resolver;
            TypeDeserializer = new TypeDeserializer(resolver);
        }

        public ValueDeserializer(IMResolver resolver, TypeDeserializer typeDeserializer)
        {
            Resolver = resolver;
            TypeDeserializer = typeDeserializer;
        }

        public object Deserialize(XElement xProp, PropertyCachedValue prop)
        {
            var typeAttrs = prop.TypeAttributes.ToArray();

            object value = null;
            if (typeAttrs.Length == 1)
            {
                value = typeAttrs[0].IsComposite
                            ? GetCompositeTypeValue(xProp)
                            : GetSimpleTypeValue(xProp);
            }
            else if (typeAttrs.Length > 1)
            {
                value = GetTableTypeValue(xProp, typeAttrs);
            }
            
            return value;
        }

        public object GetSimpleTypeValue(XElement xProperty)
        {
            var type = TypeDeserializer.GetInternalTypeName(xProperty);
            var value = xProperty.Value;
            if (string.IsNullOrEmpty(type) || type == "Null" || string.IsNullOrEmpty(value))
            {
                return null;
            }
            return PropertyType.IsPrimitiveType(type)
                       ? PropertyType.ParsePrimitiveValue(type, value)
                       : new Link1C(value, xProperty.Attribute(TypeDeserializer.LinkPerformanceAttribute)?.Value, type);
        }

        public object GetCompositeTypeValue(XElement xProperty)
        {
            var linkPerformance = xProperty.Attribute(TypeDeserializer.LinkPerformanceAttribute)?.Value;
            var type1C = xProperty.Attribute(TypeDeserializer.TypeAttribute)?.Value;
            if (string.IsNullOrEmpty(type1C) || type1C == "Null" || string.IsNullOrEmpty(xProperty.Value))
            {
                //Value is white space, so it won't be cut when serialized into JSON
                return new Composite1C(" ", " ");
            }
            var value = PropertyType.IsPrimitiveType(type1C)
                                ? PropertyType.ParsePrimitiveValue(type1C, xProperty.Value)
                                : xProperty.Value;
            return new Composite1C(value, TypeDeserializer.GetInternalTypeName(xProperty), linkPerformance);
        }

        public Table1C GetTableTypeValue(XElement xProperty, IReadOnlyList<Type1CAttribute> typeAttrs)
        {
            var rows = new List<List<object>>();
            foreach (var cols in xProperty.Elements().Select(row => row.Elements().ToArray()))
            {
                if (cols.Length != typeAttrs.Count)
                {
                    throw new Exception($"1C DataTable and Dto Table dimensions do not match. Xml Property: {xProperty.Name}");
                }
                var col = cols.Select((t, i) => GetSimpleTypeValue(t)).ToList();
                rows.Add(col);
            }
            return new Table1C(rows);
        }
    }
}