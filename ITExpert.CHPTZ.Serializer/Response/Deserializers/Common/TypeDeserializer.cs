﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Serializer.Common;

    internal class TypeDeserializer
    {
        private IMResolver Resolver { get; }

        public const string TypeAttribute = "Type";
        public const string TypePerformanceAttribute = "TypePerformance";
        public const string LinkPerformanceAttribute = "LinkPerformance";
        public const string UndefinedValue = "Undefined";

        public TypeDeserializer(IMResolver resolver)
        {
            Resolver = resolver;
        }

        public PropertyType Deserialize(XElement propMeta)
        {
            if (propMeta.HasElements)
            {
                return PropertyTypeFactory.CreateTable(GetTableTypes(propMeta));
            }

            var types = GetTypes(propMeta).ToArray();
            TryResolveTypes(types);
            return types.Length > 1 ? PropertyTypeFactory.CreateComposite(types) : types.First();
        }

        public Type GetInternalType(XElement element)
        {
            if (element == null)
                return null;
            var typeXml = GetInternalType(element, 0)?.Name ?? element.Attribute(TypeAttribute)?.Value;
            return Resolver.GetInternalType(typeXml, throwOnError:false);
        }

        public Type GetInternalType(XElement element, int ind)
        {
            var typeXml = element?.Attribute(TypeAttribute + ind)?.Value;
            return typeXml == null ? null : Resolver.GetInternalType(typeXml, throwOnError:false);
        }

        public string GetInternalTypeName(XElement element)
        {
            return GetInternalType(element)?.Name
                   ?? element?.Attribute(TypeAttribute + 0)?.Value
                   ?? element?.Attribute(TypeAttribute)?.Value;
        }

        private void TryResolveTypes(IEnumerable<PropertyType> types)
        {
            //TODO: Don't check for nulls. Try\catch instead.
            foreach (var type1C in types)
            {
                var type = Resolver.GetInternalType(type1C.Type, throwOnError:false)?.Name;
                if (type != null)
                {
                    type1C.Type = type;
                }
                if (type1C.IsComposite || type1C.IsTable)
                {
                    TryResolveTypes(type1C.Types);
                }
            }
        }

        private IEnumerable<PropertyType> GetTypes(XElement xProperty)
        {
            var i = 0;
            var type = xProperty.Attribute(TypeAttribute);
            var typePerf = xProperty.Attribute(TypePerformanceAttribute);
            if (type != null)
            {
                yield return new PropertyType(type.Value, typePerf?.Value);
            }

            while (true)
            {
                type = xProperty.Attribute(TypeAttribute + i);
                if (type == null) yield break;
                typePerf = xProperty.Attribute(TypePerformanceAttribute + i);
                i++;
                yield return new PropertyType(type.Value, typePerf?.Value);
            }
        }

        private IEnumerable<PropertyType> GetTableTypes(XElement propMeta)
        {
            var rows = propMeta.Elements().ToList();
            if (!rows.Any())
            {
                return null;
            }
            var types = rows.Select(Deserialize).ToList();
            TryResolveTypes(types);
            return types.Any() ? types : null;
        }
    }
}
