﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.Xml.Monica.Deserialization;

    using Activator = ITExpert.CHPTZ.Common.Activator;

    internal class BaseEntityDeserializer<T> : IMDeserializer<T>
    {
        protected Type Type { get; }
        protected IMResolver Resolver { get; }
        private Activator Activator { get; }
        private IDictionary<string, PropertyCachedValue> Cache { get; }
        protected TypeDeserializer TypeDeserializer { get; }
        private ValueDeserializer ValueDeserializer { get; }

        private bool AddEntityPrefixToPropertiesNames { get; }
        private string EntityName { get; }

        public BaseEntityDeserializer(Type type, IMResolver resolver, bool addEntityPrefixToPropertiesNames = false)
        {
            Type = type;
            Resolver = resolver;
            AddEntityPrefixToPropertiesNames = addEntityPrefixToPropertiesNames;
            Cache = TypeCacheWith1CNamesAsKeys.Get(type);
            Activator = new Activator(type);
            TypeDeserializer = new TypeDeserializer(resolver);
            ValueDeserializer = new ValueDeserializer(resolver, TypeDeserializer);
            EntityName = type.GetName1C();
        }

        public IEnumerable<T> Deserialize(IEnumerable<XElement> xElements)
        {
            var type1C = Type.GetName1C();
            return xElements.Where(x => x.Attribute("Type").Value == type1C).Select(GetEntity);
        }

        private T GetEntity(XElement xEntity)
        {
            var entity = (T)Activator.CreateInstance();
            return SetEntityProperties(xEntity, entity);
        }

        protected virtual T SetEntityProperties(XElement xEntity, T entity)
        {
            foreach (var xProp in xEntity.Elements())
            {
                ProcessProperty(xProp, entity);
            }
            return entity;
        }

        private void ProcessProperty(XElement xProp, T entity)
        {
            SetPropertyValue(xProp, entity);
        }

        private void SetPropertyValue(XElement xProp, T entity)
        {
            PropertyCachedValue prop;
            var propName = AddEntityPrefixToPropertiesNames
                               ? $"{EntityName}.{xProp.Name.LocalName}"
                               : xProp.Name.LocalName;
            var isPropMissing = !Cache.TryGetValue(propName, out prop);
            if (isPropMissing)
            {
                return;
            }

            var value = ValueDeserializer.Deserialize(xProp, prop);
            if (value == null)
            {
                return;
            }
            prop.PropertyInfo.SetValue(entity, value);
        }
    }
}
