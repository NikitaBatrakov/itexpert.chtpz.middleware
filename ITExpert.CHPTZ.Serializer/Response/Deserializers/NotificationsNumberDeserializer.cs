﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.Xml.Monica.Deserialization;

    public class NotificationsNumberDeserializer : IMDeserializer<NotificationsNumber>
    {
        #region Implementation of IMDeserializer<out NotificationsNumber>

        public IEnumerable<NotificationsNumber> Deserialize(IEnumerable<XElement> xElements)
        {
            foreach (var xElement in xElements)
            {
                var guid = "";
                var number = 0;

                foreach (var xProperty in xElement.Elements())
                {
                    switch (xProperty.Name.LocalName.ToLowerInvariant())
                    {
                        case "объект":
                            number = int.Parse(xProperty.Value);
                            break;
                        case "сотрудник":
                            guid = xProperty.Value;
                            break;
                        default:
                            continue;
                    }
                }

                if (string.IsNullOrEmpty(guid))
                {
                    throw new FormatException("Number or EmployeeGuid were not set while deserializing notifications number");
                }
                yield return new NotificationsNumber(number, guid);

            }
        }

        #endregion
    }
}
