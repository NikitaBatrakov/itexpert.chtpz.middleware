﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.Xml.Monica.Deserialization;

    internal class TotalResultDeserializer : IMDeserializer<int>
    {
        public IEnumerable<int> Deserialize(IEnumerable<XElement> xElements)
        {
            var element = xElements.First();
            var str = element.Value.CleanNumberString1C();
            return new[] {int.Parse(str)};
        }
    }
}
