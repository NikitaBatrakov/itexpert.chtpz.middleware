﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.Xml.Monica.Deserialization;

    using Activator = ITExpert.CHPTZ.Common.Activator;

    internal class RegisterDeserializer : IMDeserializer<IRegister>
    {
        public Type Type { get; }
        private IMResolver Resolver { get; }
        private Activator Activator { get; }
        private IDictionary<string, PropertyCachedValue> Cache { get; }
        private TypeDeserializer TypeDeserializer { get; }
        private ValueDeserializer ValueDeserializer { get; }

        public RegisterDeserializer(Type type, IMResolver resolver)
        {
            Type = type;
            Resolver = resolver;
            Cache = TypeCacheWith1CNamesAsKeys.Get(type);
            Activator = new Activator(type);
            TypeDeserializer = new TypeDeserializer(resolver);
            ValueDeserializer = new ValueDeserializer(Resolver, TypeDeserializer);
        }

        public IEnumerable<IRegister> Deserialize(IEnumerable<XElement> xElements)
        {
            return xElements.Select(GetEntity);
        }

        private IRegister GetEntity(XElement xEntity)
        {
            var entity = (IRegister)Activator.CreateInstance();
            foreach (var xProp in xEntity.Elements())
            {
                SetPropertyValue(xProp, entity);
            }
            return entity;
        }

        private void SetPropertyValue(XElement xProp, IRegister entity)
        {
            PropertyCachedValue prop;
            var isPropMissing = !Cache.TryGetValue(xProp.Name.LocalName, out prop);
            if (isPropMissing)
            {
                return;
            }

            var value = ValueDeserializer.Deserialize(xProp, prop);
            if (value == null)
            {
                return;
            }
            prop.PropertyInfo.SetValue(entity, value);
        }
    }
}
