﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.Xml.Monica.Deserialization;

    internal class ServiceResultDeserializer : IMDeserializer<ServiceResult>
    {
        private const string ServiceResultCode = "Code";
        private const string ServiceResultText = "Text";

        public IEnumerable<ServiceResult> Deserialize(IEnumerable<XElement> xElements)
        {
            var sericeResult = xElements.First();

            var code = 0;
            var codeStr = sericeResult.Element(ServiceResultCode)?.Value;
            if (!string.IsNullOrEmpty(codeStr))
            {
                code = int.Parse(codeStr);
            }

            var text = sericeResult.Element(ServiceResultText)?.Value;
            if (text == "") text = null;

            return new[] {new ServiceResult(code, text)};
        }
    }
}
