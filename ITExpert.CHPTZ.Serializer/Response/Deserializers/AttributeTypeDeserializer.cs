﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.Xml.Monica.Deserialization;

    public class AttributeTypeDeserializer : IMDeserializer<AdditionalAttributeType>
    {
        private const string LinkAttribute = "ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения.Ссылка";
        private const string NameAttribute = "ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения.Наименование";
        private const string TypeAttribute = "ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения.ТипЗначения";
        private const string IsRequiredAttribute = "ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения.Обязательный";

        private IMResolver Resolver { get; }

        public AttributeTypeDeserializer(IMResolver resolver)
        {
            Resolver = resolver;
        }

        public IEnumerable<AdditionalAttributeType> Deserialize(IEnumerable<XElement> xElements)
        {
            var typeDeserializer = new TypeDeserializer(Resolver);
            foreach (var xElement in xElements)
            {
                string guid = "", linkPerformance = "", name = "";
                PropertyType type = null;
                var isRequired = false;
                foreach (var xProperty in xElement.Elements())
                {
                    switch (xProperty.Name.ToString())
                    {
                        case LinkAttribute:
                            guid = xProperty.Value;
                            linkPerformance = xProperty.Attribute("LinkPerformance")?.Value;
                            break;
                        case NameAttribute:
                            name = xProperty.Value;
                            break;
                        case TypeAttribute:
                            type = typeDeserializer.Deserialize(xProperty);
                            break;
                        case IsRequiredAttribute:
                            var isRequiredStr = xProperty.Value;
                            isRequired = bool.Parse(isRequiredStr);
                            break;
                        default:
                            break;
                    }
                }

                if (string.IsNullOrEmpty(guid) || string.IsNullOrEmpty(name) || type == null)
                    throw new Exception("AttributeType Deserialization failed: not all necessary values were retreived");

                yield return new AdditionalAttributeType(name, new Link1C(guid, linkPerformance, "Id"), type) {IsRequired = isRequired};
            }
        }
    }
}
