﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.Xml.Monica.Deserialization;

    using Activator = ITExpert.CHPTZ.Common.Activator;

    internal class QueryEntityDeserializer : IMDeserializer<IEntity>
    {
        private Type Type { get; }
        private IMResolver Resolver { get; }
        private ValueDeserializer ValueDeserializer { get; }
        private IDictionary<string, PropertyCachedValue> Cache { get; }

        public QueryEntityDeserializer(Type type, IMResolver resolver)
        {
            Type = type;
            Resolver = resolver;
            ValueDeserializer = new ValueDeserializer(resolver);
            Cache = TypeCache.Get(type);
        }

        public IEnumerable<IEntity> Deserialize(IEnumerable<XElement> xElements)
        {
            foreach (var xElement in xElements)
            {
                var entity = (IEntity)Activator.CreateInstance(Type);
                foreach (var xProp in xElement.Elements())
                {
                    SetPropertyValue(xProp, entity);
                }
                yield return entity;
            }
        }

        private void SetPropertyValue(XElement xProp, IEntity entity)
        {
            PropertyCachedValue prop;
            var isPropMissing = !Cache.TryGetValue(xProp.Name.LocalName.LowerFirstLetter(), out prop);
            if (isPropMissing)
            {
                return;
            }

            var value = ValueDeserializer.Deserialize(xProp, prop);
            if (value == null)
            {
                return;
            }
            prop.PropertyInfo.SetValue(entity, value);
        }
    }
}
