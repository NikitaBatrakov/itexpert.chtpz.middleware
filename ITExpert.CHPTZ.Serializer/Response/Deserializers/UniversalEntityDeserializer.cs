﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;
    using ITExpert.Xml.Monica.Deserialization;

    internal class UniversalEntityDeserializer : IMDeserializer<Object1C>
    {
        public Type Type { get; }
        private IMResolver Resolver { get; }
        private IDictionary<string, PropertyCachedValue> Cache { get; }
        private TypeDeserializer TypeDeserializer { get; }
        private ValueDeserializer ValueDeserializer { get; }

        public UniversalEntityDeserializer(Type type, IMResolver resolver)
        {
            Type = type;
            Resolver = resolver;
            Cache = TypeCacheWith1CNamesAsKeys.Get(type);
            TypeDeserializer = new TypeDeserializer(resolver);
            ValueDeserializer = new ValueDeserializer(Resolver, TypeDeserializer);
        }

        public IEnumerable<Object1C> Deserialize(IEnumerable<XElement> xElements)
        {
            foreach (var xEntity in xElements)
            {
                var entity = GetEntity(xEntity);
                yield return entity;
            }
        }

        private Object1C GetEntity(XElement xEntity)
        {
            var entity = new Object1C();
            foreach (var xProp in xEntity.Elements())
            {
                SetPropertyValue(xProp, entity);
            }
            return entity;
        }

        private void SetPropertyValue(XElement xProp, Object1C entity)
        {
            PropertyCachedValue prop;
            var isPropMissing = !Cache.TryGetValue(xProp.Name.LocalName, out prop);
            if (isPropMissing)
            {
                return;
            }

            var value = ValueDeserializer.Deserialize(xProp, prop);
            if (value == null)
            {
                return;
            }

            entity[prop.PropertyInfo.Name] = new ObjectProperty1C(value);
        }
    }
}
