﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers.Common;

    internal class EntityDeserializer : BaseEntityDeserializer<object>
    {
        public EntityDeserializer(Type type, IMResolver resolver) : base(type, resolver)
        {
        }

        #region Overrides of BaseEntityDeserializer

        protected override object SetEntityProperties(XElement xEntity, object entity)
        {
            var castedEntity = entity as IEntity;
            if (castedEntity == null)
            {
                return base.SetEntityProperties(xEntity, entity);
            }

            var addAttr = GetAdditionalAttributes(xEntity).ToArray();
            if (addAttr.Any())
            {
                castedEntity.Attributes = addAttr;
            }

            var attachmnets = GetAttachments(xEntity).ToArray();
            if (attachmnets.Any())
            {
                castedEntity.Attachments = attachmnets;
            }

            return base.SetEntityProperties(xEntity, entity);
        }

        #endregion

        private IEnumerable<AdditionalAttribute1C> GetAdditionalAttributes(XElement xEntity)
        {
            var ind = 0;
            var entityName = Type.GetName1C();
            while (true)
            {
                var xAddAttr = xEntity.Element($"{entityName}.ДополнительныйРеквизит_{++ind}");
                if (xAddAttr == null) yield break;
                var type1C = TypeDeserializer.Deserialize(xAddAttr);
                var guid = xAddAttr.Attribute("Link")?.Value;
                var verb = xAddAttr.Attribute("Performance")?.Value;
                var value = xAddAttr.Value;
                yield return new AdditionalAttribute1C(type1C, guid, verb, value);
            }
        }

        private static IEnumerable<Attachment1C> GetAttachments(XElement xEntity)
        {
            var ind = 0;
            while (true)
            {
                var xAttachment = xEntity.Element($"Attachment_{++ind}");
                if (xAttachment == null)
                {
                    yield break;
                }
                yield return GetAttachmentProperty(xAttachment);
            }
        }

        private static Attachment1C GetAttachmentProperty(XElement xProperty)
        {
            var attachment = new Attachment1C();
            foreach (var attrXml in xProperty.Attributes())
            {
                var value = attrXml.Value;
                var type = attrXml.Name.ToString().ToLower();

                switch (type.ToLowerInvariant())
                {
                    case "size":
                        attachment.Size = int.Parse(value.CleanNumberString1C());
                        break;
                    case "name":
                        attachment.Name = value;
                        break;
                    case "time":
                        attachment.Date = DateTime.Parse(value);
                        break;
                    case "service":
                        attachment.Service = int.Parse(value.CleanNumberString1C());
                        break;
                    case "attachmentguid":
                        attachment.Guid = value;
                        break;
                    case "employeename":
                        attachment.EmployeeName = value;
                        break;
                    case "employeeguid":
                        attachment.EmployeeGuid = value;
                        break;
                    case "ismarkedtodelete":
                        attachment.IsMarkedToDelete = bool.Parse(value);
                        break;
                    case "isnotrelevant":
                        attachment.IsNotRelevant = bool.Parse(value);
                        break;
                    case "attachmenttype":
                        attachment.Type = value;
                        break;
                    default:
                        continue;
                }
            }

            return attachment;
        }
    }
}
