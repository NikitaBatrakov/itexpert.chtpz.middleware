﻿namespace ITExpert.CHPTZ.Serializer.Response.Deserializers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.Xml.Monica.Deserialization;

    public class FoundTypesDeserializer : IMDeserializer<FoundType>
    {
        private IMResolver Resolver { get; }

        public FoundTypesDeserializer(IMResolver resolver)
        {
            Resolver = resolver;
        }

        #region Implementation of IMDeserializer<out FoundType>

        public IEnumerable<FoundType> Deserialize(IEnumerable<XElement> xElements)
        {
            return xElements.Select(ParseType);
        }

        #endregion

        private FoundType ParseType(XElement xElement)
        {
            var type1С = xElement.Attribute("Type").Value;
            var type = Resolver.GetInternalType(type1С);
            var count = int.Parse(xElement.Attribute("Count").Value);
            return new FoundType(type.Name, count);
        }
    }
}
