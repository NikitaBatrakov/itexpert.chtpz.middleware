﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Entities.MonthPlanEntities;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class MonthPlanChangeRequestElement : MDeserializableElement<IMonthPlanEntity>
    {
        public MonthPlanChangeRequestElement(IMDeserializer<IMonthPlanEntity> deserializer)
            : base("Entity", deserializer) { }

        public MonthPlanChangeRequestElement(Type type, IMResolver resolver) 
            : this(new MonthPlanDeserializer(type, resolver)) { }
    }
}
