﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class ActionsElement : MDeserializableElement<FormAction>
    {
        public ActionsElement()
            : this(new ActionDeserializer()) { }

        public ActionsElement(IMDeserializer<FormAction> deserializer)
            : base("Actions", deserializer) { }
    }
}
