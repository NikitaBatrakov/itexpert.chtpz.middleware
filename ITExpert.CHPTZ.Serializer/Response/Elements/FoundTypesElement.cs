﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class FoundTypesElement : MDeserializableElement<FoundType>
    {
        public FoundTypesElement(IMResolver resolver)
            : this(new FoundTypesDeserializer(resolver)) { }

        public FoundTypesElement(IMDeserializer<FoundType> deserializer)
            : base("Search", deserializer) { }
    }
}
