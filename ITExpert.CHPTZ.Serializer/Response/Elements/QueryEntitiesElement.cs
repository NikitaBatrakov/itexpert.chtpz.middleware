﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class QueryEntitiesElement : MDeserializableElement<IEntity>
    {
        public QueryEntitiesElement(Type type, IMResolver resolver) 
            : this(new QueryEntityDeserializer(type, resolver)) { }

        public QueryEntitiesElement(IMDeserializer<IEntity> deserializer)
            : base("Entity", deserializer) { }
    }
}
