﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos.SearchDtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class SearchEntityElement : MDeserializableElement<UserFilter>
    {
        public SearchEntityElement(Type type, IMResolver resolver) 
            : this(new SearchEntityDeserializer(type, resolver)) { }

        public SearchEntityElement(IMDeserializer<UserFilter> deserializer)
            : base("Entity", deserializer) { }
    }
}
