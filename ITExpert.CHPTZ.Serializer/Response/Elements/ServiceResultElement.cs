namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class ServiceResultElement : MDeserializableElement<ServiceResult>
    {
        public ServiceResultElement() 
            : this(new ServiceResultDeserializer()) { }

        public ServiceResultElement(IMDeserializer<ServiceResult> deserializer) 
            : base("ServiceResult", deserializer) { }
    }
}