﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class UniversalEntitiesElement : MDeserializableElement<Object1C>
    {
        public UniversalEntitiesElement(Type type, IMResolver resolver) 
            : this(new UniversalEntityDeserializer(type, resolver)) { }

        public UniversalEntitiesElement(IMDeserializer<Object1C> deserializer)
            : base("Entity", deserializer) { }
    }
}
