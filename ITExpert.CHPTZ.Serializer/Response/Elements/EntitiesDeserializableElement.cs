namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class EntitiesDeserializableElement : MDeserializableElement<object>
    {
        public EntitiesDeserializableElement(Type type, IMResolver resolver) 
            : this(new EntityDeserializer(type, resolver)) { }

        public EntitiesDeserializableElement(IMDeserializer<object> deserializer) 
            : base("Entity", deserializer) { }        
    }
}