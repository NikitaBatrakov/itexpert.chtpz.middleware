namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Extensions;
    using ITExpert.CHPTZ.Common.Extensions;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Common;
    using ITExpert.CHPTZ.Serializer.PropertiesCache;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class MetadataElement : MDeserializableElement<IDictionary<string, PropertyMetadata>>
    {
        public MetadataElement(Type type, IMResolver resolver) 
            : this(new MetadataDeserializer(type, resolver)) { }

        public MetadataElement(IMDeserializer<IDictionary<string, PropertyMetadata>> deserializer) 
            : base("Metadata", deserializer) { }

        public static IDictionary<string, PropertyMetadata> Create(Type type, IMResolver resolver)
        {
            var cache = TypeCache.Get(type);
            return cache.ToDictionary(x => x.Key,
                x => new PropertyMetadata(PropertyTypeFactory.Create(x.Value.TypeAttributes, resolver), x.Value.NameAttribute.Verbose));

        }

        public static IDictionary<string, PropertyMetadata> Create(Type type, IMResolver resolver,
                                                                   params string[] fields)
        {
            if (fields == null || !fields.Any())
            {
                return Create(type, resolver);
            }

            var cache = TypeCache.Get(type);
            var result = new Dictionary<string, PropertyMetadata>();
            foreach (var field in fields)
            {
                var key = type.GetPropertyIgnoreCase(field).Name.LowerFirstLetter();
                var value = cache[key];
                var propType = PropertyTypeFactory.Create(value.TypeAttributes, resolver);
                result.Add(field, new PropertyMetadata(propType, value.NameAttribute.Verbose));
            }
            return result;
        }
    }
}