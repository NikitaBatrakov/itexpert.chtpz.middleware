﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class AttributesDetailElement : MDeserializableElement<AdditionalAttributeType>
    {
        public AttributesDetailElement(IMResolver resolver)
            : this(new AttributeTypeDeserializer(resolver)) { }

        public AttributesDetailElement(IMDeserializer<AdditionalAttributeType> deserializer) 
            : base("Entity", deserializer) { }
    }
}
