﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using System;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class RegistersElement : MDeserializableElement<IRegister>
    {
        public RegistersElement(Type type, IMResolver resolver) 
            : this(new RegisterDeserializer(type, resolver)) { }

        public RegistersElement(IMDeserializer<IRegister> deserializer)
            : base("Entity", deserializer) { }
    }
}
