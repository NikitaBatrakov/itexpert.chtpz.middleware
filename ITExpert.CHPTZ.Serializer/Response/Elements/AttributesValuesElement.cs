﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class AttributesValuesElement : MDeserializableElement<AdditionalAttributeValue>
    {
        public AttributesValuesElement(IMResolver resolver) 
            : this(new AttributeValueDeserializer(resolver)) { }

        public AttributesValuesElement(IMDeserializer<AdditionalAttributeValue> deserializer) 
            : base("Entity", deserializer) { }
    }
}
