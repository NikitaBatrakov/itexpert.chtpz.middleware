namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class TotalResultElement : MDeserializableElement<int>
    {
        public TotalResultElement() 
            : this(new TotalResultDeserializer()) { }

        public TotalResultElement(IMDeserializer<int> deserializer) 
            : base("TotalResults", deserializer) { }
    }
}