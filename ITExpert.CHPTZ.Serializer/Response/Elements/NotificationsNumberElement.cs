﻿namespace ITExpert.CHPTZ.Serializer.Response.Elements
{
    using ITExpert.CHPTZ.ObjectModel.Dtos;
    using ITExpert.CHPTZ.Serializer.Response.Deserializers;
    using ITExpert.Xml.Monica.Deserialization;

    public class NotificationsNumberElement : MDeserializableElement<NotificationsNumber>
    {
        public NotificationsNumberElement() 
            : this("Entity", new NotificationsNumberDeserializer()) { }

        public NotificationsNumberElement(string name, IMDeserializer<NotificationsNumber> deserializer)
            : base(name, deserializer) { }
    }
}
