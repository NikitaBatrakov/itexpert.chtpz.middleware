﻿namespace ITExpert.CHPTZ.Serializer.Common.Extensions
{
    using System;
    using System.Reflection;

    using ITExpert.CHPTZ.BackendTypes.Fundamentals;
    using ITExpert.CHPTZ.BackendTypes.PropertyTypes;

    public static class PropertyInfoExtensions
    {
        public static Type1CInfo GetType1CInfo(this PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType == typeof(string))
            {
                return Type1CInfo.String;
            }
            if (propertyInfo.PropertyType == typeof(double?))
            {
                return Type1CInfo.Number;
            }
            if (propertyInfo.PropertyType == typeof(bool?))
            {
                return Type1CInfo.Boolean;
            }
            if (propertyInfo.PropertyType == typeof(DateTime?))
            {
                return Type1CInfo.DataTime;
            }
            if (propertyInfo.PropertyType == typeof(Link1C))
            {
                return Type1CInfo.Link;
            }
            if (propertyInfo.PropertyType == typeof(Composite1C))
            {
                return Type1CInfo.Composite;
            }
            if (propertyInfo.PropertyType == typeof(Table1C))
            {
                return Type1CInfo.Table;
            }
            
            throw new ArgumentException("PropertyType does not match any 1C types.");
        }
    }
}
