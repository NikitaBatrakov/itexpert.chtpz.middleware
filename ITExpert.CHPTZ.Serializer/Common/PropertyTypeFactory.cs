﻿namespace ITExpert.CHPTZ.Serializer.Common
{
    using System.Collections.Generic;
    using System.Linq;

    using ITExpert.CHPTZ.BackendTypes;
    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.BackendTypes.Fundamentals;

    //TODO: Rewrite
    internal class PropertyTypeFactory
    {
        internal static PropertyType Create(Type1CAttribute attribute, IMResolver resolver = null)
        {
            var typeXml = attribute.Type;
            var type = resolver == null ? typeXml : resolver.GetInternalType(typeXml, throwOnError:false)?.Name ?? typeXml;
            //var types = attribute.Types?.Select(compType => new PropertyType(compType));
            var types = attribute.Types != null ? GetComposite(attribute, resolver) : null;
            return new PropertyType(type, attribute.TypePerformance, types);
        }

        internal static PropertyType Create(IEnumerable<Type1CAttribute> attributes, IMResolver resolver = null)
        {
            var attrArr = attributes.ToArray();
            return attrArr.Length > 1
                ? new PropertyType(PropertyType.Table, attrArr.Select(x => Create(x, resolver)))
                : Create(attrArr[0], resolver);
        }

        internal static PropertyType CreateComposite(IEnumerable<PropertyType> types)
        {
            return new PropertyType(PropertyType.Composite, types);
        }

        internal static PropertyType CreateTable(IEnumerable<PropertyType> types)
        {
            return new PropertyType(PropertyType.Table, types);
        }

        private static IEnumerable<PropertyType> GetComposite(Type1CAttribute attribute, IMResolver resolver)
        {
            return attribute.Types.Select((t, i) => ParseType(t, attribute.TypesPerfomances[i], resolver));
        }

        private static PropertyType ParseType(string type, string typePerformance, IMResolver resolver)
        {
            if (!PropertyType.IsPrimitiveType(type))
            {
                type = resolver.GetInternalType(type, throwOnError:false)?.Name ?? type;
            }
            
            return new PropertyType(type, typePerformance);
        }
    }
}
