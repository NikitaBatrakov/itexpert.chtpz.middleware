namespace ITExpert.CHPTZ.Serializer.PropertiesCache
{
    using System.Reflection;

    using ITExpert.CHPTZ.BackendTypes.Attributes;

    public struct PropertyCachedValue
    {
        public Property1CAttribute NameAttribute { get; }
        public Type1CAttribute[] TypeAttributes { get; }
        public PropertyInfo PropertyInfo { get; }

        public PropertyCachedValue(Property1CAttribute nameAttribute, Type1CAttribute[] typeAttributes, PropertyInfo prop)
        {
            NameAttribute = nameAttribute;
            TypeAttributes = typeAttributes;
            PropertyInfo = prop;
        }        
    }
}