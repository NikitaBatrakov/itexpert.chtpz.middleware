namespace ITExpert.CHPTZ.Serializer.PropertiesCache
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using ITExpert.CHPTZ.BackendTypes.Attributes;
    using ITExpert.CHPTZ.Common.Extensions;

    public class TypeCache
    {
        private static readonly ConcurrentDictionary<Type, IDictionary<string, PropertyCachedValue>> CacheDictionary 
            = new ConcurrentDictionary<Type, IDictionary<string, PropertyCachedValue>>();

        public static IDictionary<string, PropertyCachedValue> Get(Type type)
        {
            return CacheDictionary.GetOrAdd(type, ValueFactory);
        }

        private static IDictionary<string, PropertyCachedValue> ValueFactory(Type type)
        {
            var dict = new Dictionary<string, PropertyCachedValue>();
            foreach (var prop in type.GetProperties())
            {
                var prop1C = prop.GetCustomAttribute<Property1CAttribute>();
                var types1C = prop.GetCustomAttributes<Type1CAttribute>();
                if (prop1C == null)
                {
                    continue;
                }
                dict.Add(prop.Name.LowerFirstLetter(), new PropertyCachedValue(prop1C, types1C.ToArray(), prop));
            }

            return dict;
        }

        
    }
}